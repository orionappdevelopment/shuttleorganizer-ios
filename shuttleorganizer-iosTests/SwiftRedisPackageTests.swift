import XCTest
@testable import shuttleorganizer_ios

final class SwiftRedisPackageTests: XCTestCase {
    //    func testExample() {
    //        // This is an example of a functional test case.
    //        // Use XCTAssert and related functions to verify your tests produce the correct
    //        // results.
    //        XCTAssertEqual(SwiftRedisPackage().text, "Hello, World!")
    //    }
    
    //    static var allTests = [
    //        ("testExample", testExample),
    //    ]
}

class RedisInterfaceTests: XCTestCase
{
    func testReadmeExample()
    {
        
        let redis = RedisInterface(host: SwiftRedisConnectionParamsTest.serverAddress, port: SwiftRedisConnectionParamsTest.serverPort, auth: SwiftRedisConnectionParamsTest.auth)
        //        let redis = RedisInterface(host: <host-address String>, port: <port Int>, auth: <auth String>)
        
        // Queue a request to initiate a connection.
        // Once a connection is established, an AUTH command will be issued with the auth parameters specified above.
        redis.connect()
        
        // Queue a request to set a value for a key in the Redis database.  This command will only
        // execute after the connection is established and authenticated.
        redis.setValueForKey("some:key", stringValue: "a value", completionHandler: { success, cmd in
            // this completion handler will be executed after the SET command returns
            if success {
                print("value stored successfully")
            } else {
                print("value was not stored")
            }
        })
        
        // Queue a request to get the value of a key in the Redis database.  This command will only
        // execute after the previous command is complete.
        redis.getValueForKey("some:key", completionHandler: { success, key, data, cmd in
            if success {
                print("the stored data for \(key) is \(data!.stringVal)")
            } else {
                print("could not get value for \(key)")
            }
        })
        
        // Queue a QUIT command (the connection will close when the QUIT command returns)
        var quitComplete: Bool = false
        let doneExpectation = expectation(description: "done")
        redis.quit({success, cmd in
            quitComplete = true
            doneExpectation.fulfill()
        })
        
        waitForExpectations(timeout: 10, handler: { error in
            XCTAssert(quitComplete)
        })
    }
    
    func testGet()
    {
        let r = RedisInterface(host: SwiftRedisConnectionParamsTest.serverAddress, port: SwiftRedisConnectionParamsTest.serverPort, auth: SwiftRedisConnectionParamsTest.auth)
        
        r.connect()
        
        let storedExpectation0 = expectation(description: "testkey0 retrived")
        let storedExpectation1 = expectation(description: "testkey0 exists")
        r.existsDataForKey("testkey0", completionHandler: { success, key, data, cmd in
            XCTAssertTrue(success, "expecting success existing data for testkey0")
            storedExpectation1.fulfill()
            
            if data?.intVal != nil && data!.intVal! > 0 {
                print("exists int val \(data!.intVal!)")
                r.getDataForKey("testkey0", completionHandler: { success, key, data, cmd in
                    XCTAssertTrue(success, "expecting success retrieveing data for testkey0")
                    print("success retrieveing data for testkey0")
                    storedExpectation0.fulfill()
                })
            }else{
                storedExpectation0.fulfill()
            }
            
        })
        
        waitForExpectations(timeout: 200, handler: { error in
            XCTAssertNil(error, "expecting operation to succeed")
        })
        
        
//        let storedExpectation2 = expectation(description: "testkey1 exists")
//        r.existsDataForKey("testkey1", completionHandler: { success, key, data, cmd in
//            XCTAssertTrue(success, "expecting success existing data for testkey0")
//            storedExpectation2.fulfill()
//        })
//
//        waitForExpectations(timeout: 2, handler: { error in
//            XCTAssertNil(error, "expecting operation to succeed")
//        })
//
//        let storedExpectation0 = expectation(description: "testkey0 retrived")
//        r.getDataForKey("testkey0", completionHandler: { success, key, data, cmd in
//            XCTAssertTrue(success, "expecting success storing data for testkey0")
//            storedExpectation0.fulfill()
//        })
//
//        waitForExpectations(timeout: 2, handler: { error in
//            XCTAssertNil(error, "expecting operation to succeed")
//        })
        
        
    }
    
    func testSetAndGet()
    {
        let r = RedisInterface(host: SwiftRedisConnectionParamsTest.serverAddress, port: SwiftRedisConnectionParamsTest.serverPort, auth: SwiftRedisConnectionParamsTest.auth)
        
        r.connect()
        
        let arr = [UInt8](repeating: 6, count: 230*1024)
        let data1 = Data(arr)
        let data3 = "hi".data(using: String.Encoding.utf8)!
        let data2 = "hello, world".data(using: String.Encoding.utf8)!
        
        
        // store data1
        let storedExpectation1 = expectation(description: "testkey1 stored")
        r.setDataForKey("testkey-getset", data: data1, completionHandler: { success, cmd in
            XCTAssertTrue(success, "expecting success storing data for testkey1")
            storedExpectation1.fulfill()
        })
        waitForExpectations(timeout: 20, handler: { error in
            XCTAssertNil(error, "expecting operation to succeed")
        })
        
        // retrieve data1
        let storedExpectation2 = expectation(description: "testkey1 retrieved")
        r.getDataForKey("testkey-getset", completionHandler: { success, key, data, cmd in
            storedExpectation2.fulfill()
            XCTAssertTrue(success)
            XCTAssert(key == "testkey-getset")
            XCTAssert(data! == RedisResponse(dataVal: data1))
        })
        waitForExpectations(timeout: 20, handler: { error in
            XCTAssertNil(error, "expecting operation to succeed")
        })
        
        
        // store data2
        let storedExpectation3 = expectation(description: "testkey2 stored")
        r.setDataForKey("testkey-getset", data: data2, completionHandler: { success, cmd in
            XCTAssertTrue(success, "could not store testkey2")
            storedExpectation3.fulfill()
        })
        waitForExpectations(timeout: 5, handler: { error in
            XCTAssertNil(error, "Error")
        })
        
        // retrieve data2
        let storedExpectation4 = expectation(description: "testkey2 stored")
        r.getDataForKey("testkey-getset", completionHandler: { success, key, data, cmd in
            storedExpectation4.fulfill()
            XCTAssertTrue(success)
            XCTAssert(key == "testkey-getset")
            XCTAssert(data! == RedisResponse(dataVal: data2))
        })
        waitForExpectations(timeout: 5, handler: { error in
            XCTAssertNil(error, "Error")
        })
        
        
        // store data3
        let storedExpectation_d3a = expectation(description: "testkey3 stored")
        r.setDataForKey("testkey-getset", data: data3, completionHandler: { success, cmd in
            XCTAssertTrue(success, "could not store testkey3")
            storedExpectation_d3a.fulfill()
        })
        waitForExpectations(timeout: 5, handler: { error in
            XCTAssertNil(error, "Error")
        })
        
        // retrieve data3
        let storedExpectation_d3b = expectation(description: "testkey3 stored")
        r.getDataForKey("testkey-getset", completionHandler: { success, key, data, cmd in
            storedExpectation_d3b.fulfill()
            XCTAssertTrue(success)
            XCTAssert(key == "testkey-getset")
            XCTAssert(data! == RedisResponse(dataVal: data3))
        })
        waitForExpectations(timeout: 5, handler: { error in
            XCTAssertNil(error, "Error")
        })
        
        
        // quit
        let storedExpectation5 = expectation(description: "quit complete")
        r.quit(
            {(success: Bool, cmd: RedisCommand) -> () in
                storedExpectation5.fulfill()
        })
        
        //        r.quit({ success in
        //                storedExpectation5.fulfill()
        //            })
        waitForExpectations(timeout: 5, handler: { error in
            XCTAssertNil(error, "Error")
        })
        
        
        
    }
    
    
    func testSkipPendingCommandsAndQuit()
    {
        let r = RedisInterface(host: SwiftRedisConnectionParamsTest.serverAddress, port: SwiftRedisConnectionParamsTest.serverPort, auth: SwiftRedisConnectionParamsTest.auth)
        
        r.connect()  // this stores an AUTH command in the queue.  since we are running in a single thread,
        // the command will not be sent before this routine reaches "waitForExpectation"
        
        let storedExpectation = expectation(description: "a handler was called")
        
        
        // queue a command that we do not expect will execute
        r.setValueForKey("testkey1", stringValue: "a value", completionHandler: { success, cmd in
            XCTAssertFalse(true, "not expecting this handler to be called, because the call to testSkipPendingCommandsAndQuit() should have removed the command from the queue")
            storedExpectation.fulfill()
        })
        
        var quitHandlerCalled = false
        
        r.skipPendingCommandsAndQuit({ success, cmd in
            XCTAssertTrue(success == true, "expecting quit handler to succeed")
            quitHandlerCalled = true
            storedExpectation.fulfill()
        })
        
        waitForExpectations(timeout: 2, handler: { error in
            XCTAssertNil(error, "expecting operation to succeed")
            XCTAssertTrue(quitHandlerCalled, "expecting quit handler to have been called")
        })
        
    }
    
    func testPubSub()
    {
        let riPublish = RedisInterface(host: SwiftRedisConnectionParamsTest.serverAddress, port: SwiftRedisConnectionParamsTest.serverPort, auth: SwiftRedisConnectionParamsTest.auth)
        riPublish.connect()
        
        let riSubscribe = RedisInterface(host: SwiftRedisConnectionParamsTest.serverAddress, port: SwiftRedisConnectionParamsTest.serverPort, auth: SwiftRedisConnectionParamsTest.auth)
        riSubscribe.connect()
        
        let expectingSubscribeToReturn1 = expectation(description: "subscribe operation returned once")
        var expectingSubscribeToReturn2: XCTestExpectation? = nil
        var expectingSubscribeToReturn3: XCTestExpectation? = nil
        var subscribeReturnCount = 0
        
        // subscribe to channel "testchannel"
        // important assumption:  no one else is subscribed to this channel!!!
        riSubscribe.subscribe("testchannel", completionHandler: { success, channel, data, cmd in
            
            // this completion handler should be called several times.
            // the first time: to acknowledge that the subscribe operation was registered
            // the next two times:  in response to publish operations
            
            switch subscribeReturnCount {
            case 0:
                XCTAssertTrue(success)
                XCTAssert(channel == "testchannel")
                XCTAssert(data! == RedisResponse(arrayVal: [
                    RedisResponse(dataVal: "subscribe".data(using: String.Encoding.utf8)),
                    RedisResponse(dataVal: "testchannel".data(using: String.Encoding.utf8)),
                    RedisResponse(intVal: 1)
                ]))
                XCTAssertNotNil(expectingSubscribeToReturn1)
                expectingSubscribeToReturn1.fulfill()
                
                subscribeReturnCount += 1
                
            case 1:
                XCTAssertTrue(success)
                XCTAssert(channel == "testchannel")
                XCTAssert(data! == RedisResponse(arrayVal: [
                    RedisResponse(dataVal: "message".data(using: String.Encoding.utf8)),
                    RedisResponse(dataVal: "testchannel".data(using: String.Encoding.utf8)),
                    RedisResponse(dataVal: "publish op 1".data(using: String.Encoding.utf8)),
                ]))
                
                XCTAssertNotNil(expectingSubscribeToReturn2)
                expectingSubscribeToReturn2!.fulfill()
                subscribeReturnCount += 1
                
            case 2:
                XCTAssertTrue(success)
                XCTAssert(channel == "testchannel")
                XCTAssert(data! == RedisResponse(arrayVal: [
                    RedisResponse(dataVal: "message".data(using: String.Encoding.utf8)),
                    RedisResponse(dataVal: "testchannel".data(using: String.Encoding.utf8)),
                    RedisResponse(dataVal: "publish op 2".data(using: String.Encoding.utf8)),
                ]))
                
                XCTAssertNotNil(expectingSubscribeToReturn3)
                expectingSubscribeToReturn3!.fulfill()
                subscribeReturnCount+=1
                
            default:
                XCTAssert(false)
            }
        })
        
        // wait for the subscribe operation to complete
        waitForExpectations(timeout: 1, handler: { error in
            XCTAssertNil(error, "expecting operation to succeed")
        })
        
        XCTAssertEqual(subscribeReturnCount, 1)
        // -----
        
        // publish something to the test channel
        expectingSubscribeToReturn2 = expectation(description: "subscribe operation returned twice")
        let expectingPublishToComplete1 = expectation(description: "publish operation 1 completed")
        riPublish.publish("testchannel", value: "publish op 1", completionHandler: { success, key, data, cmd in
            expectingPublishToComplete1.fulfill()
        })
        
        // wait for both the publish to complete, and the subscribe to return the 2nd time
        waitForExpectations(timeout: 1, handler: { error in
            XCTAssertNil(error, "expecting publish 1 to complete, and subscribe to return 2nd time")
        })
        
        XCTAssertEqual(subscribeReturnCount, 2)
        
        // -----
        
        // publish something else to the test channel
        expectingSubscribeToReturn3 = expectation(description: "subscribe operation returned third time")
        let expectingPublishToComplete2 = expectation(description: "publish operation 2 completed")
        riPublish.publish("testchannel", value: "publish op 2", completionHandler: { success, key, data, cmd in
            expectingPublishToComplete2.fulfill()
        })
        
        // wait for both the publish to complete, and the subscribe to return the 2nd time
        waitForExpectations(timeout: 2, handler: { error in
            XCTAssertNil(error, "expecting publish 2 to complete, and subscribe to return 3nd time")
        })
        
        XCTAssertEqual(subscribeReturnCount, 3)
        
        let expectingUnsubscribeToReturn = expectation(description: "unsubscribe operation returned")
        riSubscribe.unsubscribe("testchannel", completionHandler: { success, channel, data, cmd in
            XCTAssertTrue(success)
            XCTAssert(channel == "testchannel")
            XCTAssert(data! == RedisResponse(arrayVal: [
                RedisResponse(dataVal: "unsubscribe".data(using: String.Encoding.utf8)),
                RedisResponse(dataVal: "testchannel".data(using: String.Encoding.utf8)),
                RedisResponse(intVal: 0)
            ]))
            expectingUnsubscribeToReturn.fulfill()
        })
        
        waitForExpectations(timeout: 20, handler: { error in
            XCTAssertNil(error, "expecting unsubscribe to complete")
        })
    }
    
    func testPubSubPublishSubscribeSubscribe()
    {
        let riPublish = RedisInterface(host: SwiftRedisConnectionParamsTest.serverAddress, port: SwiftRedisConnectionParamsTest.serverPort, auth: SwiftRedisConnectionParamsTest.auth)
        riPublish.connect()
        
        let riSubscribe = RedisInterface(host: SwiftRedisConnectionParamsTest.serverAddress, port: SwiftRedisConnectionParamsTest.serverPort, auth: SwiftRedisConnectionParamsTest.auth)
        riSubscribe.connect()
        
        let expectingSubscribeToReturn1 = expectation(description: "subscribe operation returned once")
        var expectingSubscribeToReturn2: XCTestExpectation? = nil
        //        var expectingSubscribeToReturn3: XCTestExpectation? = nil
        var subscribeReturnCount1 = 0
        
        // subscribe to channel "channel1"
        // important assumption:  no one else is subscribed to this channel!!!
        riSubscribe.subscribe("channel1", completionHandler: { success, channel, data, cmd in
            
            // this completion handler should be called several times.
            // the first time: to acknowledge that the subscribe operation was registered
            // the next two times:  in response to publish operations
            
            switch subscribeReturnCount1 {
            case 0:
                XCTAssertTrue(success)
                XCTAssert(channel == "channel1")
                XCTAssert(data! == RedisResponse(arrayVal: [
                    RedisResponse(dataVal: "subscribe".data(using: String.Encoding.utf8)),
                    RedisResponse(dataVal: "channel1".data(using: String.Encoding.utf8)),
                    RedisResponse(intVal: 1)
                ]))
                XCTAssertNotNil(expectingSubscribeToReturn1)
                expectingSubscribeToReturn1.fulfill()
                
                subscribeReturnCount1 += 1
                
            case 1:
                XCTAssertTrue(success)
                XCTAssert(channel == "channel1")
                XCTAssert(data! == RedisResponse(arrayVal: [
                    RedisResponse(dataVal: "message".data(using: String.Encoding.utf8)),
                    RedisResponse(dataVal: "channel1".data(using: String.Encoding.utf8)),
                    RedisResponse(dataVal: "publish op 1".data(using: String.Encoding.utf8)),
                ]))
                
                XCTAssertNotNil(expectingSubscribeToReturn2)
                expectingSubscribeToReturn2!.fulfill()
                subscribeReturnCount1 += 1
                
            case 2:
                //                XCTAssertTrue(success)
                //                XCTAssert(channel == "channel1")
                //                XCTAssert(data! == RedisResponse(arrayVal: [
                //                    RedisResponse(dataVal: "message".data(using: String.Encoding.utf8)),
                //                    RedisResponse(dataVal: "channel1".data(using: String.Encoding.utf8)),
                //                    RedisResponse(dataVal: "publish op 2".data(using: String.Encoding.utf8)),
                //                ]))
                //
                //                XCTAssertNotNil(expectingSubscribeToReturn3)
                //                expectingSubscribeToReturn3!.fulfill()
                //                subscribeReturnCount1 += 1
                print("do nothing")
                
            default:
                XCTAssert(false)
            }
        })
        
        let expectingSubscribeToReturn4 = expectation(description: "subscribe operation returned once")
        var expectingSubscribeToReturn5: XCTestExpectation? = nil
        let expectingSubscribeToReturn6: XCTestExpectation? = nil
        var subscribeReturnCount2 = 0
        
        //second subscribe
        riSubscribe.subscribe("channel1", completionHandler: { success, channel, data, cmd in
            
            // this completion handler should be called several times.
            // the first time: to acknowledge that the subscribe operation was registered
            // the next two times:  in response to publish operations
            
            switch subscribeReturnCount2 {
            case 0:
                XCTAssertTrue(success)
                XCTAssert(channel == "channel1")
                XCTAssert(data! == RedisResponse(arrayVal: [
                    RedisResponse(dataVal: "subscribe".data(using: String.Encoding.utf8)),
                    RedisResponse(dataVal: "channel1".data(using: String.Encoding.utf8)),
                    RedisResponse(intVal: 1)
                ]))
                XCTAssertNotNil(expectingSubscribeToReturn1)
                expectingSubscribeToReturn4.fulfill()
                
                subscribeReturnCount2 += 1
                
            case 1:
                XCTAssertTrue(success)
                XCTAssert(channel == "channel1")
                XCTAssert(data! == RedisResponse(arrayVal: [
                    RedisResponse(dataVal: "message".data(using: String.Encoding.utf8)),
                    RedisResponse(dataVal: "channel1".data(using: String.Encoding.utf8)),
                    RedisResponse(dataVal: "publish op 1".data(using: String.Encoding.utf8)),
                ]))
                
                XCTAssertNotNil(expectingSubscribeToReturn5)
                expectingSubscribeToReturn5!.fulfill()
                subscribeReturnCount2 += 1
                
            case 2:
                XCTAssertTrue(success)
                XCTAssert(channel == "channel1")
                XCTAssert(data! == RedisResponse(arrayVal: [
                    RedisResponse(dataVal: "message".data(using: String.Encoding.utf8)),
                    RedisResponse(dataVal: "channel1".data(using: String.Encoding.utf8)),
                    RedisResponse(dataVal: "publish op 2".data(using: String.Encoding.utf8)),
                ]))
                
                XCTAssert(data?.arrayVal?[0].stringVal == "message")
                XCTAssert(data?.arrayVal?[1].stringVal == "channel1")
                XCTAssert(data?.arrayVal?[2].stringVal == "publish op 2")
                
                XCTAssertNotNil(expectingSubscribeToReturn6)
                expectingSubscribeToReturn6!.fulfill()
                subscribeReturnCount2 += 1
                
            default:
                XCTAssert(false)
            }
        })
        
        
        // wait for the subscribe operation to complete
        waitForExpectations(timeout: 1, handler: { error in
            XCTAssertNil(error, "expecting operation to succeed")
        })
        
        XCTAssertEqual(subscribeReturnCount1, 1)
        XCTAssertEqual(subscribeReturnCount2, 1)
        // -----
        
        // publish something to the test channel
        expectingSubscribeToReturn2 = expectation(description: "subscribe operation returned twice")
        expectingSubscribeToReturn5 = expectation(description: "subscribe operation returned twice")
        let expectingPublishToComplete1 = expectation(description: "publish operation 1 completed")
        riPublish.publish("channel1", value: "publish op 1", completionHandler: { success, key, data, cmd in
            expectingPublishToComplete1.fulfill()
        })
        
        // wait for both the publish to complete, and the subscribe to return the 2nd time
        waitForExpectations(timeout: 100, handler: { error in
            XCTAssertNil(error, "expecting publish 1 to complete, and subscribe to return 2nd time")
        })
        
        XCTAssertEqual(subscribeReturnCount1, 2)
        XCTAssertEqual(subscribeReturnCount2, 2)
        
        // -----
        
        //start unsubscribe probably subscriber1
        let expectingUnsubscribeToReturn = expectation(description: "unsubscribe operation returned")
        riSubscribe.unsubscribe("channel1", completionHandler: { success, channel, data, cmd in
            XCTAssertTrue(success)
            XCTAssert(channel == "channel1")
            XCTAssert(data! == RedisResponse(arrayVal: [
                RedisResponse(dataVal: "unsubscribe".data(using: String.Encoding.utf8)),
                RedisResponse(dataVal: "channel1".data(using: String.Encoding.utf8)),
                RedisResponse(intVal: 0)
            ]))
            expectingUnsubscribeToReturn.fulfill()
        })
        
        waitForExpectations(timeout: 6, handler: { error in
            XCTAssertNil(error, "expecting unsubscribe to complete")
        })
        //end unsubscribe probably subscriber1
        
        // publish something else to the test channel
        //        expectingSubscribeToReturn3 = expectation(description: "subscribe operation returned third time")
        //        expectingSubscribeToReturn6 = expectation(description: "subscribe operation returned third time")
        let expectingPublishToComplete2 = expectation(description: "publish operation 2 completed")
        riPublish.publish("channel1", value: "publish op 2", completionHandler: { success, key, data, cmd in
            expectingPublishToComplete2.fulfill()
        })
        
        // wait for both the publish to complete, and the subscribe to return the 2nd time
        waitForExpectations(timeout: 2, handler: { error in
            XCTAssertNil(error, "expecting publish 2 to complete, and subscribe to return 3nd time")
        })
        
        XCTAssertEqual(subscribeReturnCount1, 2)
        XCTAssertEqual(subscribeReturnCount2, 2)
        
        //        let expectingUnsubscribeToReturn = expectation(description: "unsubscribe operation returned")
        //        riSubscribe.unsubscribe("channel1", completionHandler: { success, channel, data, cmd in
        //            XCTAssertTrue(success)
        //            XCTAssert(channel == "channel1")
        //            XCTAssert(data! == RedisResponse(arrayVal: [
        //                RedisResponse(dataVal: "unsubscribe".data(using: String.Encoding.utf8)),
        //                RedisResponse(dataVal: "channel1".data(using: String.Encoding.utf8)),
        //                RedisResponse(intVal: 0)
        //            ]))
        //            expectingUnsubscribeToReturn.fulfill()
        //        })
        //
        //        waitForExpectations(timeout: 20, handler: { error in
        //            XCTAssertNil(error, "expecting unsubscribe to complete")
        //        })
    }
    
    func testPubSubSubscribeUnsubscribe(){
        let riSubscribe = RedisInterface(host: SwiftRedisConnectionParamsTest.serverAddress, port: SwiftRedisConnectionParamsTest.serverPort, auth: SwiftRedisConnectionParamsTest.auth)
        riSubscribe.connect()
        
        // subscribe to channel "testchannel"
        // important assumption:  no one else is subscribed to this channel!!!
        var subscribed = false
        let expectingSubscribe = expectation(description: "subscribe operation returned")
        riSubscribe.subscribe("testchannel", completionHandler: { success, channel, data, cmd in
            XCTAssertTrue(success)
            XCTAssert(channel == "testchannel")
            XCTAssert(data! == RedisResponse(arrayVal: [
                RedisResponse(dataVal: "subscribe".data(using: String.Encoding.utf8)),
                RedisResponse(dataVal: "testchannel".data(using: String.Encoding.utf8)),
                RedisResponse(intVal: 1)
            ]))
            subscribed = true
            expectingSubscribe.fulfill()
        })
        
        waitForExpectations(timeout: 1, handler: { error in
            XCTAssertNil(error, "expecting unsubscribe to complete")
            print(error)
        })
        
        XCTAssertEqual(subscribed, true)
        
        let expectingUnsubscribeToReturn = expectation(description: "unsubscribe operation returned")
        riSubscribe.unsubscribe("testchannel", completionHandler: { success, channel, data, cmd in
            XCTAssertTrue(success)
            XCTAssert(channel == "testchannel")
            XCTAssert(data! == RedisResponse(arrayVal: [
                RedisResponse(dataVal: "unsubscribe".data(using: String.Encoding.utf8)),
                RedisResponse(dataVal: "testchannel".data(using: String.Encoding.utf8)),
                RedisResponse(intVal: 0)
            ]))
            expectingUnsubscribeToReturn.fulfill()
            
        })
        
        waitForExpectations(timeout: 2, handler: { error in
            XCTAssertNil(error, "expecting unsubscribe to complete")
            print(error)
        })
    }
    
}




class RedisConnectionTests: XCTestCase {
    
    let authCmd = RedisCommand.Auth(SwiftRedisConnectionParamsTest.auth, handler: nil)
    
    func testAuthentication()
    {
        let r = RedisConnection(serverAddress: SwiftRedisConnectionParamsTest.serverAddress, serverPort: SwiftRedisConnectionParamsTest.serverPort)
        r.connect()
        
        // test that it works once
        let storedExpectation1 = expectation(description: "set command handler activated")
        let cmd = RedisCommand.Auth(SwiftRedisConnectionParamsTest.auth, handler: { success, cmd in
            XCTAssertTrue(success, "auth command expected to succeed")
            XCTAssert(cmd.response! == RedisResponse(stringVal: "OK"), "expecting response from Redis to be OK")
            storedExpectation1.fulfill()
        })
        
        r.setPendingCommand(cmd)
        
        waitForExpectations(timeout: 2, handler: { error in
            XCTAssertNil(error, "Error")
        })
        
    }
    
    func testDisconnect()
    {
        let r = RedisConnection(serverAddress: SwiftRedisConnectionParamsTest.serverAddress, serverPort: SwiftRedisConnectionParamsTest.serverPort)
        r.connect()
        
        // test that it works once
        let storedExpectation1 = expectation(description: "set command handler activated")
        let cmd = RedisCommand.Auth(SwiftRedisConnectionParamsTest.auth, handler: { success, cmd in
            XCTAssertTrue(success, "auth command expected to succeed")
            XCTAssert(cmd.response! == RedisResponse(stringVal: "OK"), "expecting response from Redis to be OK")
            storedExpectation1.fulfill()
        })
        
        r.setPendingCommand(cmd)
        
        waitForExpectations(timeout: 2, handler: { error in
            XCTAssertNil(error, "Error")
        })
        
        
        r.disconnect()
        r.connect()
        
        // test that it works again
        let storedExpectation2 = expectation(description: "set command handler activated second time")
        let cmd2 = RedisCommand.Auth(SwiftRedisConnectionParamsTest.auth, handler: { success, cmd in
            XCTAssertTrue(success, "auth command expected to succeed again")
            XCTAssert(cmd.response! == RedisResponse(stringVal: "OK"), "expecting second response from Redis to be OK")
            storedExpectation2.fulfill()
        })
        
        r.setPendingCommand(cmd2)
        
        waitForExpectations(timeout: 2, handler: { error in
            XCTAssertNil(error, "Error")
        })
        
    }
    
    /*
     func testSavingDataWithoutAuthentication()
     {
     let r = RedisConnection(serverAddress: SwiftRedisConnectionParamsTest.serverAddress, serverPort: SwiftRedisConnectionParamsTest.serverPort)
     r.connect()
     
     // test that it works once
     let storedExpectation1 = expectation(description: "set command handler activated")
     let cmd = RedisCommand.Set("A", valueToSet: "1", handler: { success, cmd in
     XCTAssertFalse(success, "set command expected to fail")
     XCTAssert(cmd.response! == RedisResponse(errorVal: "NOAUTH Authentication required"), "expecting response from Redis to be NOAUTH Authentication Required")
     storedExpectation1.fulfill()
     })
     
     r.setPendingCommand(cmd)
     
     waitForExpectations(timeout: 2, handler: { error in
     XCTAssertNil(error, "Error")
     })
     
     
     // now ensure that works the second time too
     let storedExpectation2 = expectation(description: "set command handler activated again")
     let cmd2 = RedisCommand.Set("A", valueToSet: "1", handler: { success, cmd in
     XCTAssertFalse(success, "set command expected to fail")
     XCTAssert(cmd.response! == RedisResponse(errorVal: "NOAUTH Authentication required"), "expecting response from Redis to be NOAUTH Authentication Required")
     storedExpectation2.fulfill()
     })
     
     r.setPendingCommand(cmd2)
     
     waitForExpectations(timeout: 2, handler: { error in
     XCTAssertNil(error, "Error")
     })
     
     }*/
}

class RedisParserTests: XCTestCase {
    func testRedisResponse()
    {
        var r = RedisResponse(stringVal: "abc")
        XCTAssert(r.responseType == .string)
        
        r = RedisResponse(intVal: 3)
        XCTAssert(r.responseType == .int)
        
        r = RedisResponse(errorVal: "abc")
        XCTAssert(r.responseType == .error)
        
        r = RedisResponse(dataVal: Data())
        XCTAssert(r.responseType == .data)
        
        r = RedisResponse(dataVal: NSMutableData() as Data)
        XCTAssert(r.responseType == .data)
        
        r = RedisResponse(arrayVal: [RedisResponse(intVal: 1), RedisResponse(stringVal: "hi")])
        XCTAssert(r.responseType == .array)
        
        
        XCTAssert(RedisResponse(intVal: 1) == RedisResponse(intVal: 1))
        XCTAssert(RedisResponse(intVal: 1) != RedisResponse(intVal: 2))
        XCTAssert(RedisResponse(stringVal: "a") != RedisResponse(intVal: 2))
        XCTAssert(RedisResponse(arrayVal: [RedisResponse(stringVal: "a"), RedisResponse(errorVal: "err")]) == RedisResponse(arrayVal: [RedisResponse(stringVal: "a"), RedisResponse(errorVal: "err")]))
        XCTAssert(RedisResponse(arrayVal: [RedisResponse(stringVal: "a"), RedisResponse(errorVal: "err")]) != RedisResponse(arrayVal: [RedisResponse(stringVal: "a"), RedisResponse(errorVal: "err1")]))
    }
    
    func testRedisParser()
    {
        let parser = RedisResponseParser()
        
        let resp1 = "+OK\r\n"
        parser.storeReceivedData(resp1.data(using: String.Encoding.utf8)!)
        
        XCTAssertEqual(parser.haveResponse, true)
        XCTAssert(parser.lastResponse! == RedisResponse(stringVal: "OK"))
        
        
        parser.storeReceivedString("+")
        XCTAssertEqual(parser.haveResponse, false)
        parser.storeReceivedString("OK")
        XCTAssertEqual(parser.haveResponse, false)
        parser.storeReceivedString("\r\n")
        XCTAssertEqual(parser.haveResponse, true)
        XCTAssert(parser.lastResponse! == RedisResponse(stringVal: "OK"))
        
        parser.storeReceivedString("+")
        XCTAssertEqual(parser.haveResponse, false)
        parser.storeReceivedString("OK")
        XCTAssertEqual(parser.haveResponse, false)
        parser.storeReceivedString("\r")
        XCTAssertEqual(parser.haveResponse, false)
        parser.storeReceivedString("\r\n")
        XCTAssertEqual(parser.haveResponse, true)
        XCTAssert(parser.lastResponse! == RedisResponse(stringVal: "OK\r"))
        
        parser.storeReceivedString(":476\r\n")
        XCTAssert(parser.lastResponse! == RedisResponse(intVal: 476))
        
        parser.storeReceivedString("$12\r\nabcde\r\nfghij\r\n")
        let respData = "abcde\r\nfghij".data(using: String.Encoding.utf8)
        XCTAssert(parser.lastResponse! == RedisResponse(dataVal: respData))
        
        let data2 = "hello\r\n, world".data(using: String.Encoding.utf8)
        parser.storeReceivedString("$\(data2!.count)\r\n")
        parser.storeReceivedData(data2!)
        XCTAssertEqual(parser.haveResponse, false)
        parser.storeReceivedString("\r\n")
        XCTAssertEqual(parser.haveResponse, true)
        XCTAssert(parser.lastResponse! == RedisResponse(dataVal: data2))
        
        parser.storeReceivedString(":123\r\n")
        XCTAssert(parser.lastResponse! == RedisResponse(intVal: 123))
        
        
        parser.storeReceivedString("$12\r\nabcde\r\nfghij")
        XCTAssertEqual(parser.haveResponse, false)
        parser.storeReceivedString("\r\n")
        XCTAssertEqual(parser.haveResponse, true)
        XCTAssert(parser.lastResponse! == RedisResponse(dataVal: respData))
        
    }
    
    func testArrayParsing()
    {
        let parser = RedisResponseParser()
        
        parser.storeReceivedString("*3\r\n+subscribe\r\n+ev1\r\n:1\r\n")
        XCTAssertEqual(parser.haveResponse, true)
        if parser.haveResponse {
            XCTAssert(parser.lastResponse! == RedisResponse(arrayVal: [
                RedisResponse(stringVal: "subscribe"),
                RedisResponse(stringVal: "ev1"),
                RedisResponse(intVal: 1)
            ]))
        }
        
    }
    
    
    func testAbortWhileParsing()
    {
        // this class allows us to test whether the parser correctly reports the abort to its delegate
        class ParserDelegateForTestingAbort : RedisResponseParserDelegate {
            var errorReported = false
            var responseReported = false
            var abortReported = false
            
            func errorParsingResponse(_ error: String?) {
                errorReported = true
            }
            func parseOperationAborted() {
                abortReported = true
            }
            func receivedResponse(_ response: RedisResponse) {
                responseReported = true
            }
            func reset() {
                errorReported = false
                abortReported = false
                responseReported = false
            }
        }
        let d = ParserDelegateForTestingAbort()
        
        let parser = RedisResponseParser()
        parser.setDelegate(d)
        
        
        // in the middle of processing an array element
        parser.storeReceivedString("*4\r\n+sub")
        XCTAssertEqual(parser.haveResponse, false)
        parser.abortParsing()
        
        XCTAssertFalse(d.errorReported, "Parser should not report an error to delegate")
        XCTAssertTrue(d.abortReported, "Parser should report an abort to delegate")
        XCTAssertFalse(d.responseReported, "Parser should not report a response to delegate")
        
        d.reset()
        
        /// ensure can now process a full array
        parser.storeReceivedString("*3\r\n+subscribe\r\n+ev1\r\n:1\r\n")
        XCTAssertEqual(parser.haveResponse, true)
        if parser.haveResponse {
            XCTAssert(parser.lastResponse! == RedisResponse(arrayVal: [
                RedisResponse(stringVal: "subscribe"),
                RedisResponse(stringVal: "ev1"),
                RedisResponse(intVal: 1)
            ]))
        }
        
        XCTAssertFalse(d.errorReported, "Parser should not report an error to delegate")
        XCTAssertFalse(d.abortReported, "Parser should not report an abort to delegate")
        XCTAssertTrue(d.responseReported, "Parser should report a response to delegate")
        
        
        // in the middle of processing an array
        d.reset()
        parser.storeReceivedString("*4\r\n+sub\r\n")
        XCTAssertEqual(parser.haveResponse, false)
        parser.abortParsing()
        
        XCTAssertFalse(d.errorReported, "Parser should not report an error to delegate")
        XCTAssertTrue(d.abortReported, "Parser should report an abort to delegate")
        XCTAssertFalse(d.responseReported, "Parser should not report a response to delegate")
        
        
        // in the middle of processing a string
        d.reset()
        parser.storeReceivedString("+sub")
        XCTAssertEqual(parser.haveResponse, false)
        parser.abortParsing()
        
        XCTAssertFalse(d.errorReported, "Parser should not report an error to delegate")
        XCTAssertTrue(d.abortReported, "Parser should report an abort to delegate")
        XCTAssertFalse(d.responseReported, "Parser should not report a response to delegate")
        
    }
    
    func testErrorHandling()
    {
        
        // TODO:  test error handling
        
    }
    
    
    func testRedisBuffer()
    {
        let buf = RedisBuffer()
        
        // simple test
        buf.storeReceivedString("123")
        XCTAssertEqual(buf.getNextStringOfSize(1), "1")
        XCTAssertEqual(buf.getNextStringOfSize(1), "2")
        XCTAssertEqual(buf.getNextStringOfSize(1), "3")
        XCTAssertEqual(buf.getNextStringOfSize(1), nil)
        
        // now get longer string back
        buf.storeReceivedString("123")
        XCTAssertEqual(buf.getNextStringOfSize(3), "123")
        XCTAssertEqual(buf.getNextStringOfSize(1), nil)
        
        // store in parts
        buf.storeReceivedString("1")
        buf.storeReceivedString("2")
        buf.storeReceivedString("34")
        XCTAssertEqual(buf.getNextStringOfSize(4), "1234")
        XCTAssertEqual(buf.getNextStringOfSize(1), nil)
        
        // ensure nil if not enough data
        buf.storeReceivedString("123")
        XCTAssertEqual(buf.getNextStringOfSize(4), nil)
        XCTAssertEqual(buf.getNextStringOfSize(3), "123")
        XCTAssertEqual(buf.getNextStringOfSize(1), nil)
        
        
        // basic "CRLF" behavior
        buf.storeReceivedString("abcde\r\n")
        XCTAssertEqual(buf.getNextStringUntilCRLF(), "abcde")
        XCTAssertEqual(buf.getNextStringUntilCRLF(), nil)
        XCTAssertEqual(buf.getNextStringOfSize(1), nil)
        
        // partial string behavior
        buf.storeReceivedString("abcde\r")
        XCTAssertEqual(buf.getNextStringUntilCRLF(), nil)
        XCTAssertEqual(buf.getNextStringOfSize(1), "a")
        buf.storeReceivedString("\n")
        XCTAssertEqual(buf.getNextStringUntilCRLF(), "bcde")
        XCTAssertEqual(buf.getNextStringOfSize(1), nil)
        XCTAssertEqual(buf.getNextStringUntilCRLF(), nil)
        
    }
    
    func testRedisCommands()
    {
        let getCmd = RedisCommand.Get("aKey", handler: nil)
        XCTAssertEqual(getCmd.getCommandString(), "*2\r\n$3\r\nGET\r\n$4\r\naKey\r\n".data(using: String.Encoding.utf8))
        
        let authCmd = RedisCommand.Auth("12345", handler: nil)
        XCTAssertEqual(authCmd.getCommandString(), "*2\r\n$4\r\nAUTH\r\n$5\r\n12345\r\n".data(using: String.Encoding.utf8))
        
        
        let setCmd = RedisCommand.Set("aKey", valueToSet: "abc".data(using: String.Encoding.utf8)!, handler: nil)
        XCTAssertEqual(setCmd.getCommandString(), "*3\r\n$3\r\nSET\r\n$4\r\naKey\r\n$3\r\nabc\r\n".data(using: String.Encoding.utf8))
        
        
        let genericCmd = RedisCommand.Generic("SET", "mykey", "1", "EX", "3", handler: nil)
        
        XCTAssertEqual(String(data: genericCmd.getCommandString()!, encoding: String.Encoding.utf8), "*5\r\n$3\r\nSET\r\n$5\r\nmykey\r\n$1\r\n1\r\n$2\r\nEX\r\n$1\r\n3\r\n")
        
    }
    
    
    func testListCommands()
    {
        let r = RedisInterface(host: SwiftRedisConnectionParamsTest.serverAddress, port: SwiftRedisConnectionParamsTest.serverPort, auth: SwiftRedisConnectionParamsTest.auth)
        r.connect()
        
        let lrange1Expectation = expectation(description: "range element 0 0")
        r.lrange("myList", start: 0, stop: -1, completionHandler: { success, key, data, cmd in
            lrange1Expectation.fulfill()
            XCTAssertTrue(success, "expecting success lrange for myList")
            XCTAssert(key == "myList")
            XCTAssert(data!.arrayVal!.count == 1)
            XCTAssert(data! ==  RedisResponse(arrayVal: [RedisResponse(dataVal: "hello".data(using: String.Encoding.utf8))]))
            for element in data!.arrayVal! {
                if let strVal = element.stringVal {
                    XCTAssert(strVal == "hello")
                }
            }
        })
        waitForExpectations(timeout: 20, handler: { error in
            XCTAssertNil(error, "expecting operation to succeed")
        })
        
        let rpush1Expectation = expectation(description: "push element to the right of the list")
        let pushData1 = "push1".data(using: String.Encoding.utf8)!
        r.rpush("myList", data: pushData1, completionHandler: { success, key, data, cmd in
            rpush1Expectation.fulfill()
            XCTAssertTrue(success, "expecting success storing pushData1 for myList")
            XCTAssert(key == "myList")
            XCTAssert(data! ==  RedisResponse(intVal: 2))
            XCTAssert(data!.intVal! == 2)
        })
        waitForExpectations(timeout: 20, handler: { error in
            XCTAssertNil(error, "expecting operation to succeed")
        })
        
        let lrange3Expectation = expectation(description: "range element 0 0")
        r.lrange("myList", start: 0, stop: -1, completionHandler: { success, key, data, cmd in
            lrange3Expectation.fulfill()
            XCTAssertTrue(success, "expecting success lrange for myList")
            XCTAssert(key == "myList")
            XCTAssert(data!.arrayVal!.count == 2)
            XCTAssert(data! ==  RedisResponse(arrayVal: [RedisResponse(dataVal: "hello".data(using: String.Encoding.utf8)), RedisResponse(dataVal: "push1".data(using: String.Encoding.utf8))]))
            let expectedArray : [String] = ["hello", "push1"]
            for (index, element) in data!.arrayVal!.enumerated() {
                if let strVal = element.stringVal {
                    XCTAssert(strVal == expectedArray[index])
                }
            }
        })
        waitForExpectations(timeout: 20, handler: { error in
            XCTAssertNil(error, "expecting operation to succeed")
        })
        
        let lrange4Expectation = expectation(description: "range element 0 0")
        r.lrange("myList", start: 1, stop: 1, completionHandler: { success, key, data, cmd in
            lrange4Expectation.fulfill()
            XCTAssertTrue(success, "expecting success lrange for myList")
            XCTAssert(key == "myList")
            XCTAssert(data!.arrayVal!.count == 1)
            XCTAssert(data! ==  RedisResponse(arrayVal: [RedisResponse(dataVal: "push1".data(using: String.Encoding.utf8))]))
            XCTAssert(data!.arrayVal![0].stringVal == "push1")
        })
        waitForExpectations(timeout: 20, handler: { error in
            XCTAssertNil(error, "expecting operation to succeed")
        })
        
        let rpop1Expectation = expectation(description: "pop element from right of the list")
        r.rpop("myList", completionHandler: { success, key, data, cmd in
            rpop1Expectation.fulfill()
            XCTAssertTrue(success)
            XCTAssert(key == "myList")
            XCTAssert(data! == RedisResponse(dataVal: pushData1))
            XCTAssert(data!.stringVal == "push1")
        })
        waitForExpectations(timeout: 20, handler: { error in
            XCTAssertNil(error, "expecting operation to succeed")
        })
        
        let lengthExpectation1 = expectation(description: "length of list retrieved")
        r.llen("myList", completionHandler: { success, key, data, cmd in
            lengthExpectation1.fulfill()
            XCTAssertTrue(success)
            XCTAssert(key == "myList")
            XCTAssert(data! ==  RedisResponse(intVal: 1))
            XCTAssert(data!.intVal! == 1)
        })
        waitForExpectations(timeout: 20, handler: { error in
            XCTAssertNil(error, "expecting operation to succeed")
        })
        
        // quit
        let storedExpectation5 = expectation(description: "quit complete")
        r.quit(
            {(success: Bool, cmd: RedisCommand) -> () in
                storedExpectation5.fulfill()
        })
        
        //        r.quit({ success in
        //                storedExpectation5.fulfill()
        //            })
        waitForExpectations(timeout: 5, handler: { error in
            XCTAssertNil(error, "Error")
        })
        
    }
    
    func testLinkedList()
    {
        var ll = LinkedList<Int>()
        ll.append(2)
        ll.append(3)
        ll.insertAt(value: 0, atIndex: 0)
        ll.insertAt(value: 1, atIndex: 1)
        print("after insert \(ll)")
        XCTAssertEqual(ll.first!, 0)
        ll.removeFirst()
        print("after remove \(ll)")
        XCTAssertEqual(ll.first!, 1)
        print(ll)
        //        let nn = ll.nodeAt(atIndex: 0)
        //
        //        ll.remove(optionalNode: nn)
        ll.removeAt(atIndex: 0)
        XCTAssertEqual(ll.count, 2)
        //        var a : Node = ll.node
        //        ll.removeFirst(0)
        //        ll.removeAll()
        //        ll.popFirst()
        //        ll.rem
        print(ll)
        
    }
    
    func testQueueLinkedList()
    {
        var q  = QueueLinkedList<Int>()
        XCTAssertEqual(q.isEmpty, true)
        q.enqueue(0)
        q.enqueue(1)
        XCTAssertEqual(q.isEmpty, false)
        
        XCTAssertEqual(q.isPeekNil(), false)
        XCTAssertEqual(q.dequeue()!, 0)
        XCTAssertEqual(q.dequeue()!, 1)
        XCTAssertEqual(q.isPeekNil(), true)
    }
    
}


class RedisSkipListTests: XCTestCase {

    func testSkipListAdd()
    {
        let l  = SkipList<Int>()
        
        l.insert(key: 4)

        l.insert(key: 1)
        
        l.insert(key: 9)
        
        
        l.insert(key: -5)
        l.insert(key: 20)
        l.insert(key: 9)
        l.insert(key: 9)
        
        l.insert(key: 3)
        
        
        XCTAssertEqual(l.getSortedElements(), [-5, 1, 3, 4, 9, 20])
        
        l.remove(key: 3)
        XCTAssertEqual(l.getSortedElements(), [-5, 1, 4, 9, 20])
        
        l.remove(key: -5)
        XCTAssertEqual(l.getSortedElements(), [1, 4, 9, 20])
        
        
        var tl : [Int] = []
        
        for i in (0...100).reversed() {
            l.insert(key: i)
            tl.append(100-i)
        }
        XCTAssertEqual(l.getSortedElements(), tl)
        
//        print(l.getSortedElements())
        
    }
}
