//
//  ConnectionParams.swift
//  SwiftRedisTests
//
//  Created by macos on 7/19/20.
//  Copyright © 2020 Ron Perry. All rights reserved.
//

import Foundation
class SwiftRedisConnectionParamsTest {
    static let serverAddress = "46.101.106.121"
    static let serverPort = UInt32(6379)
    static let auth = "Rovingy.6878!"  // warning:  including the authentication secret as an unecrypted string in an iOS app is extremely unsecure:  hackers can easily grab this string from the app.  Use this method only for unit testing, or if the app is installed only on trusted devices.
}
