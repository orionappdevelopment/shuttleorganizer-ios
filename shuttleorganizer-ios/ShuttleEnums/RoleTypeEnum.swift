//
//  RoleTypeEnum.swift
//  Landmarks
//
//  Created by macos on 5/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public enum RoleTypeEnum : Int , CaseIterable, Codable, Hashable{
    case DRIVER = 0
    case PASSENGER = 1
    case OBSERVER =  2
    case ADMIN = 10
    case NONE = 101


    public static func RoleTypeEnum(value : Int) -> RoleTypeEnum{
        // retrieve status from status code
        if (value == DRIVER.rawValue) {
            return DRIVER;
        } else if (value == PASSENGER.rawValue) {
            return PASSENGER;
        } else if (value == OBSERVER.rawValue) {
            return OBSERVER;
        } else if (value == ADMIN.rawValue) {
            return ADMIN;
        }else {
            return NONE;
        }
    }
}
