//
//  DayEnum.swift
//  Landmarks
//
//  Created by macos on 5/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public enum DayEnum : Int, CaseIterable, Codable, Hashable {
    
    case MONDAY = 0
    case TUESDAY = 1
    case WEDNESDAY = 2
    case THURSDAY = 3
    case FRIDAY = 4
    case SATURDAY = 5
    case SUNDAY = 6
    
    static private let primeValues = [2, 3, 5, 7 ,11, 13, 17]
    
    static public var weekDays: Int {
        return 2 * 3 * 5 * 7 * 11
    }
    
    static public var weekEnd: Int {
        return 13 * 17
    }
    
    public var allWeek: Int {
        return 2 * 3 * 5 * 7 * 11 * 13 * 17
    }
    
    public static func primeValue(dayEnum : DayEnum) -> Int{
        let primeValue : Int = primeValues[dayEnum.rawValue]
        
        return primeValue
    }
    
    public static func  daysFromMultiplication(multiplication : Int) -> [Bool]{
        
        var days : [Bool] = []
        
        var val : Bool
        for dayEnum in DayEnum.allCases {
            val = false
            if(multiplication %  primeValue(dayEnum : dayEnum) == 0) {
                val =  true;
            }
            days.append(val)
        }
        
        return days;
    }
    
    public static func dayListFromMultiplication(multiplication : Int) -> [Int]{
        var days : [Int] = []
        
        var index : Int = 0
        for dayEnum in DayEnum.allCases {
            if(multiplication %  primeValue(dayEnum : dayEnum) == 0) {
                days.append(index);
            }
            index += 1
        }
        return days;
    }
    
    public static func multiplicationFromDays(days : [Bool]) -> Int{
        var multiplication : Int = 1;
        
        for i in  0 ..< days.count {
            if(i < DayEnum.allCases.count && days[i] == true) {
                multiplication = multiplication * primeValues[i]
            }else {
//                LogUtil.getAppLogger().info("DayEnum day " + i + " is larger than " +  DayEnum.values().length );
            }
        }
        
        return multiplication;
    }
    
    public static func multiplicationFromDays(days : [Int]) -> Int{
        var multiplication : Int = 1
        
        for day in days {
            if day < primeValues.count {
                multiplication = multiplication * primeValues[day]
            }else {
//                LogUtil.getAppLogger().info("DayEnum day " + day + " is larger than " +  DayEnum.values().length );
            }
        }
        
        return multiplication;
    }

}
