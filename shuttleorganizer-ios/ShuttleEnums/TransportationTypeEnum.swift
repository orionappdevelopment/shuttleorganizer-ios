//
//  TransportationTypeEnum.swift
//  Landmarks
//
//  Created by macos on 5/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation


public enum TransportationTypeEnum : Int , CaseIterable, Codable, Hashable, CustomStringConvertible {
    case DEPARTURE = 0
    case RETURN = 1
    case NONE = 101

    public static func toTransportationTypeEnum(_ value : Int) -> TransportationTypeEnum {
        // retrieve status from status code
        if (value == DEPARTURE.rawValue) {
            return DEPARTURE;
        } else if (value == RETURN.rawValue) {
            return RETURN;
        } else {
            return NONE;
        }
    }
    
    public var description: String {
        if (self == TransportationTypeEnum.DEPARTURE) {
            return toStringFromLocalizedStringKey(localizedStringKey: "TransportationTypeEnum.Departure.text")
        } else if (self == TransportationTypeEnum.RETURN) {
            return toStringFromLocalizedStringKey(localizedStringKey: "TransportationTypeEnum.Return.text")
        } else {
            return toStringFromLocalizedStringKey(localizedStringKey: "TransportationTypeEnum.None.text")
        }
    }
}
