//
//  NotificationEnum.swift
//  Landmarks
//
//  Created by macos on 5/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public enum NotificationEnum : Int, CaseIterable, Codable, Hashable {
    case NONE = 0
    case MANAGEMENT = 1
    case TRANSPORTATION = 2
    case GROUP_CHAT = 3
    case HIDE_MY_PHONE_NUMBER = 4
    case HIDE_MY_EMAIL = 5


    public static func toNotificationEnum(value : Int) -> NotificationEnum{
        // retrieve status from status code
        if (value == MANAGEMENT.rawValue) {
            return MANAGEMENT;
        }else if (value == TRANSPORTATION.rawValue) {
            return TRANSPORTATION;
        }else if (value == GROUP_CHAT.rawValue) {
            return GROUP_CHAT;
        }else if (value == HIDE_MY_PHONE_NUMBER.rawValue) {
            return HIDE_MY_PHONE_NUMBER;
        }else if (value == HIDE_MY_EMAIL.rawValue) {
            return HIDE_MY_EMAIL;
        }else {
            return NONE;
        }
    }
}

