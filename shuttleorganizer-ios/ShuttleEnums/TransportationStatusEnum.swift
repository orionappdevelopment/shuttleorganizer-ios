//
//  TransportationStatusEnum.swift
//  Landmarks
//
//  Created by macos on 5/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public enum TransportationStatusEnum: Int , CaseIterable, Codable, Hashable {
    case IN_THE_GARAGE = 0
    case EN_ROUTE = 1
    case NONE = 101
    
    public static func toTransportationStatusEnum(value : Int) -> TransportationStatusEnum {
        // retrieve status from status code
        if value == IN_THE_GARAGE.rawValue {
            return IN_THE_GARAGE;
        } else if value == EN_ROUTE.rawValue {
            return EN_ROUTE;
        } else {
            return NONE;
        }
    }
    
}
