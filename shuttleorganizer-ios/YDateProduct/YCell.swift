//
//  YCell.swift
//  YCalendar
//

import SwiftUI

struct YCell: View {
    
    var yDate: YDate
    
    var cellWidth: CGFloat
    
    var body: some View {
        VStack{
            if yDate.isAbsent {
                Text(yDate.getText())
                    .fontWeight(yDate.getFontWeight())
                    .foregroundColor(yDate.getTextColor())
                    .frame(width: cellWidth, height: cellWidth)
//                    .font(.system(size: 20))
                    .background(yDate.getBackgroundColor())
                    .cornerRadius(cellWidth/2)
//                    .padding(.top, 10)
//                Text("AttendanceInfoView.YDate.Cell.Absent.text")
//                    .fontWeight(.black)
//                    .font(.system(size: 7))
//                    .offset(x: 0, y: -8)
            } else if yDate.detail != ""{
                Text(yDate.getText())
                    .fontWeight(yDate.getFontWeight())
                    .foregroundColor(yDate.getTextColor())
                    .frame(width: cellWidth, height: cellWidth)
//                    .font(.system(size: 20))
                    .background(yDate.getBackgroundColor())
                    .cornerRadius(cellWidth/2)
//                    .padding(.top, 10)
//                Text(yDate.detail)
//                    .fontWeight(.black)
//                    .font(.system(size: 7))
//                    .offset(x: 0, y: -8)
            } else {
                Text(yDate.getText())
                .fontWeight(yDate.getFontWeight())
                .foregroundColor(yDate.getTextColor())
                .frame(width: cellWidth, height: cellWidth)
//                .font(.system(size: 20))
                .background(yDate.getBackgroundColor())
                .cornerRadius(cellWidth/2)
            }
        }
    }
}

#if DEBUG
struct YCellPreviews : PreviewProvider {
    static var previews: some View {
        Group {
            YCell(yDate:  YDate(date: Date(), yManager: YManager(calendar: Calendar.current, minimumDate: Date(), maximumDate: Date().addingTimeInterval(60*60*24*365), mode: 0), isDisabled: false, isToday: false, isSelected: false, isBetweenStartAndEnd: false, isAbsent: false), cellWidth: CGFloat(32))
                .previewDisplayName("Control")
            YCell(yDate: YDate(date: Date(), yManager: YManager(calendar: Calendar.current, minimumDate: Date(), maximumDate: Date().addingTimeInterval(60*60*24*365), mode: 0), isDisabled: true, isToday: false, isSelected: false, isBetweenStartAndEnd: false, isAbsent: false), cellWidth: CGFloat(32))
                .previewDisplayName("Disabled Date")
            YCell(yDate: YDate(date: Date(), yManager: YManager(calendar: Calendar.current, minimumDate: Date(), maximumDate: Date().addingTimeInterval(60*60*24*365), mode: 0), isDisabled: false, isToday: true, isSelected: false, isBetweenStartAndEnd: false, isAbsent: false), cellWidth: CGFloat(32))
                .previewDisplayName("Today")
            YCell(yDate: YDate(date: Date(), yManager: YManager(calendar: Calendar.current, minimumDate: Date(), maximumDate: Date().addingTimeInterval(60*60*24*365), mode: 0), isDisabled: false, isToday: false, isSelected: true, isBetweenStartAndEnd: false, isAbsent: false), cellWidth: CGFloat(32))
                .previewDisplayName("Selected Date")
            YCell(yDate: YDate(date: Date(), yManager: YManager(calendar: Calendar.current, minimumDate: Date(), maximumDate: Date().addingTimeInterval(60*60*24*365), mode: 0), isDisabled: false, isToday: false, isSelected: true, isBetweenStartAndEnd: false, isAbsent: true), cellWidth: CGFloat(32))
            .previewDisplayName("Absent Date")
            HStack{
                YCell(yDate: YDate(date: Date(), yManager: YManager(calendar: Calendar.current, minimumDate: Date(), maximumDate: Date().addingTimeInterval(60*60*24*365), mode: 0), isDisabled: false, isToday: false, isSelected: false, isBetweenStartAndEnd: true, isAbsent: false), cellWidth: CGFloat(32))
                    .previewDisplayName("Between Two Dates")
                YCell(yDate: YDate(date: Date(), yManager: YManager(calendar: Calendar.current, minimumDate: Date(), maximumDate: Date().addingTimeInterval(60*60*24*365), mode: 0), isDisabled: false, isToday: false, isSelected: false, isBetweenStartAndEnd: false, isAbsent: true, detail: "Absent"), cellWidth: CGFloat(32))
                    .previewDisplayName("Between Two Dates")
            }
        }
        .previewLayout(.fixed(width: 300, height: 70))
            .environment(\.sizeCategory, .accessibilityExtraExtraExtraLarge)
    }
}
#endif



