//
//  YDate.swift
//  YCalendar
//

import SwiftUI

struct YDate {
    
    var date: Date
    let yManager: YManager
    var detail : String
    
    var isDisabled: Bool = false
    var isToday: Bool = false
    var isSelected: Bool = false
    var isBetweenStartAndEnd: Bool = false
    var isAbsent : Bool = false
    
    init(date: Date, yManager: YManager, isDisabled: Bool, isToday: Bool, isSelected: Bool, isBetweenStartAndEnd: Bool, isAbsent: Bool, detail : String = "") {
        self.date = date
        self.yManager = yManager
        self.isDisabled = isDisabled
        self.isToday = isToday
        self.isSelected = isSelected
        self.isBetweenStartAndEnd = isBetweenStartAndEnd
        self.isAbsent = isAbsent
        self.detail = detail
    }
    
    func getText() -> String {
        let day = formatDate(date: date, calendar: self.yManager.calendar)
        return day
    }
    
    func getTextColor() -> Color {
        var textColor = yManager.colors.textColor
        if isDisabled {
            textColor = yManager.colors.disabledColor
        } else if isSelected {
            textColor = yManager.colors.selectedColor
        } else if isToday {
            textColor = yManager.colors.todayColor
        } else if isBetweenStartAndEnd {
            textColor = yManager.colors.betweenStartAndEndColor
        } else if isAbsent {
            textColor = yManager.colors.absentColor
        }
        return textColor
    }
    
    func getBackgroundColor() -> Color {
        var backgroundColor = yManager.colors.textBackColor
        if isBetweenStartAndEnd {
            backgroundColor = yManager.colors.betweenStartAndEndBackColor
        }
        if isToday {
            backgroundColor = yManager.colors.todayBackColor
        }
        if isDisabled {
            backgroundColor = yManager.colors.disabledBackColor
        }
        if isSelected && yManager.mode == 5 {
            backgroundColor = yManager.colors.betweenStartAndEndBackColor
        }else if isSelected {
            backgroundColor = yManager.colors.selectedBackColor
        }
        if isAbsent {
            backgroundColor = yManager.colors.absentBackColor
        }
        return backgroundColor
    }
    
    func getFontWeight() -> Font.Weight {
        var fontWeight = Font.Weight.medium
        if isDisabled {
            fontWeight = Font.Weight.thin
        } else if isSelected {
            fontWeight = Font.Weight.heavy
        } else if isToday {
            fontWeight = Font.Weight.heavy
        } else if isBetweenStartAndEnd {
            fontWeight = Font.Weight.heavy
        } else if isAbsent {
            fontWeight = Font.Weight.heavy
        }
        return fontWeight
    }
    
    // MARK: - Date Formats
    
    func formatDate(date: Date, calendar: Calendar) -> String {
        let formatter = dateFormatter()
        return stringFrom(date: date, formatter: formatter, calendar: calendar)
    }
    
    func dateFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = .current
        formatter.dateFormat = "d"
        return formatter
    }
    
    func stringFrom(date: Date, formatter: DateFormatter, calendar: Calendar) -> String {
        if formatter.calendar != calendar {
            formatter.calendar = calendar
        }
        return formatter.string(from: date)
    }
}

