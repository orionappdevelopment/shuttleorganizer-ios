//
//  YViewController.swift
//  YCalendar
//

import SwiftUI

struct YViewController: View {
    
    @ObservedObject var isPresented: GenericObservableModel<Bool>
    
    @ObservedObject var yManager: YManager
    
    @ObservedObject var isButtonClicked :  GenericObservableModel<Bool> = GenericObservableModel(value: false)
    
    var onLoad : () -> Void? = {return}
    
//    init(isPresented : Binding<Bool>, yManager : YManager, isButtonClicked : GenericObservableModel<Bool> = GenericObservableModel(value: false)) {
//        self._isPresented = isPresented
//        self.yManager = yManager
//        self.isButtonClicked = isButtonClicked
//    }
    
    var body: some View {
        GeometryReader { geometry in
            VStack{
                HStack(alignment: .top){
                    Text("AttendanceInfoView.YDate.Title.text")
                        .padding(.leading,5)
                        .font(Font.headline)
                        .foregroundColor(Color.colorText)
                    Spacer()
                    
                    VStack{
                        Image(systemName:"xmark.circle.fill")
                            .renderingMode(.template)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 25, height: 25, alignment: .center)
                            .foregroundColor(Color.colorIosCloseButtonOutsideXColor)
                            .background(Color.colorIosCloseButtonInsideXColor)
                            .clipShape(Circle())
                            .onTapGesture {
                                self.isPresented.value = false
                            }
                    }.padding(.trailing, 10)
                }.padding(.top, 10)
                ZStack{
                    VStack {
                        if self.yManager.mode == 5 {
                            VStack{
                                HStack{
                                    VStack(alignment: .leading){
                                        Text("AttendanceInfoView.YDate.StartDate.text")
                                            .padding(.bottom, 2)
                                            .font(.headline)
                                            .foregroundColor(Color.colorText)
                                        Text(self.yManager.startDate != nil ? getYearMonthDateFormat().string(from: self.yManager.startDate) : "-")
                                            .foregroundColor(Color.colorText)
                                            
                                    }
                                        .padding(EdgeInsets(top: 15, leading: 5, bottom: 0, trailing: 0))
                                        .frame(width: 170, height: 50, alignment: Alignment.leading)
                                        
                                    Spacer()
                                    
                                    VStack(alignment: .leading){
                                        Text("AttendanceInfoView.YDate.EndDate.text")
                                            .padding(.bottom, 2)
                                            .font(.headline)
                                            .foregroundColor(Color.colorText)
                                        Text(self.yManager.endDate != nil ? getYearMonthDateFormat().string(from: self.yManager.endDate) : "-")
                                            .foregroundColor(Color.colorText)
                                    }
                                        .padding(EdgeInsets(top: 15, leading: 0, bottom: 0, trailing: 0))
                                        .frame(width: 170, height: 55, alignment: Alignment.leading)

                                    Spacer()
                                }
                                Divider().background(Color.black)
                            }
                            .background(Color(.systemGray5))
                        }
                        YWeekdayHeader(yManager: self.yManager)
                            .padding(.top, 0)
                        Divider()
                        List {
                            ForEach(0..<self.numberOfMonths()) { index in
                                YMonth(isPresented: self.isPresented, yManager: self.yManager, monthOffset: index)
                            }
                            Divider()
                        }
                    }
                    
                    if self.yManager.mode == 5 && self.yManager.startDate != nil && self.yManager.endDate != nil {
                        VStack(){
                            Spacer()
                            Button(action: {
    //                                print("save")
                                    self.yManager.callBack()
                                    self.isButtonClicked.value = true
    //                                self.yManager.buttonClicked = true
    //                                self.yManager.startDate = nil
    //                                self.yManager.endDate = nil
    //                                self.isPresented = false
                                    }, label: {
                                        Text("AttendanceInfoView.YDate.Button.Absent.text")
                                            .padding(.top, 10)
                                            .frame(width: geometry.size.width, height: 90, alignment: .top)
                                            .background(Color.colorSecondary)
                                            .foregroundColor(Color.white)
                                    }
                                )
                            }
                            .offset(x: 0, y: 50)
                    }
                }
            }
        }
        .onAppear(){
            self.onLoad()
        }
        .onDisappear(){
            self.yManager.startDate = nil
            self.yManager.endDate = nil
        }
    }

    func numberOfMonths() -> Int {
        return yManager.calendar.dateComponents([.month], from: yManager.minimumDate, to: YMaximumDateMonthLastDay()).month! + 1
    }
    
    func YMaximumDateMonthLastDay() -> Date {
        var components = yManager.calendar.dateComponents([.year, .month, .day], from: yManager.maximumDate)
        components.month! += 1
        components.day = 0
        
        return yManager.calendar.date(from: components)!
    }
}

#if DEBUG
struct YViewController_Previews : PreviewProvider {
    static var previews: some View {
        Group {
            YViewController(isPresented: GenericObservableModel(value: false), yManager: YManager(calendar: Calendar.current, minimumDate: Date(), maximumDate: Date().addingTimeInterval(60*60*24*365), mode: 5))
            YViewController(isPresented: GenericObservableModel(value: false), yManager: YManager(calendar: Calendar.current, minimumDate: Date(), maximumDate: Date().addingTimeInterval(60*60*24*32), mode: 0))
                .environment(\.colorScheme, .dark)
                .environment(\.layoutDirection, .rightToLeft)
        }
    }
}
#endif

