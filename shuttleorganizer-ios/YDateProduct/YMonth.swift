//
//  YMonth.swift
//  YCalendar
//

import SwiftUI

struct YMonth: View {

    @ObservedObject var isPresented: GenericObservableModel<Bool>
    
    @ObservedObject var yManager: YManager
    
    let monthOffset: Int
    
    let calendarUnitYMD = Set<Calendar.Component>([.year, .month, .day])
    let daysPerWeek = 7
    var monthsArray: [[Date]] {
        monthArray()
    }
    let cellWidth = CGFloat(32)
    
    @State var showTime = false
    
    
    var body: some View {
        VStack(alignment: HorizontalAlignment.center, spacing: 10){
            Text(getMonthHeader()).foregroundColor(self.yManager.colors.monthHeaderColor)
            VStack(alignment: .leading, spacing: 5) {
                ForEach(monthsArray, id:  \.self) { row in
                    HStack() {
                        ForEach(row, id:  \.self) { column in
                            HStack() {
                                Spacer()
                                if self.isThisMonth(date: column) {
                                    YCell(yDate: YDate(
                                        date: column,
                                        yManager: self.yManager,
                                        isDisabled: !self.isEnabled(date: column),
                                        isToday: self.isToday(date: column),
                                        isSelected: self.isSpecialDate(date: column),
                                        isBetweenStartAndEnd: self.isBetweenStartAndEnd(date: column),
                                        isAbsent: self.isOneOfAbsentDate(date: column),
                                        detail: self.oneOfTextDate(date: column)),
                                        cellWidth: self.cellWidth)
                                        .onTapGesture { self.dateTapped(date: column) }
                                } else {
                                    Text("").frame(width: self.cellWidth, height: self.cellWidth)
                                }
                                Spacer()
                            }
                        }
                    }
                }
            }.frame(minWidth: 0, maxWidth: .infinity)
        }.background(yManager.colors.monthBackColor)
    }

     func isThisMonth(date: Date) -> Bool {
         return self.yManager.calendar.isDate(date, equalTo: firstOfMonthForOffset(), toGranularity: .month)
     }
    
    func dateTapped(date: Date) {
        if self.isEnabled(date: date) {
//            print(yManager.mode)
            switch self.yManager.mode {
            case 0:
                if self.yManager.selectedDate != nil &&
                    self.yManager.calendar.isDate(self.yManager.selectedDate, inSameDayAs: date) {
                    self.yManager.selectedDate = nil
                } else {
                    self.yManager.selectedDate = date
                }
                self.isPresented.value = false
            case 1:
                self.yManager.startDate = date
                self.yManager.endDate = nil
                self.yManager.mode = 2
            case 2:
                self.yManager.endDate = date
                if self.isStartDateAfterEndDate() {
                    self.yManager.endDate = nil
                    self.yManager.startDate = nil
                }
                self.yManager.mode = 1
                self.isPresented.value = false
            case 3:
                if self.yManager.selectedDatesContains(date: date) {
                    if let ndx = self.yManager.selectedDatesFindIndex(date: date) {
                        yManager.selectedDates.remove(at: ndx)
                    }
                } else {
                    self.yManager.selectedDates.append(date)
                }
            case 5:
                if(self.isOneOfAbsentDate(date: date)){
                    return
                }
                
                if(self.yManager.startDate != nil && self.yManager.endDate != nil){
                    self.yManager.startDate = date
                    self.yManager.endDate = nil
                }else if self.yManager.startDate == nil {
                    self.yManager.startDate = date
                }else if isFirstDateSameOrBeforeSecondDate(first: yManager.startDate, second: date) {
                    self.yManager.endDate = date
                }else{
                    self.yManager.startDate = date
                    self.yManager.endDate = nil
                }
//                print(yManager.startDate)
//                print(yManager.endDate)
                if yManager.startDate != nil &&
                   yManager.endDate != nil &&
                   isFirstDateSameOrBeforeSecondDate(first: yManager.startDate, second: yManager.endDate){
//                    self.isPresented = false
                    print("both of them valid")
                }
            default:
                self.yManager.selectedDate = date
//                self.isPresented = false
            }
        }
    }
     
    func monthArray() -> [[Date]] {
        var rowArray = [[Date]]()
        for row in 0 ..< (numberOfDays(offset: monthOffset) / 7) {
            var columnArray = [Date]()
            for column in 0 ... 6 {
                let abc = self.getDateAtIndex(index: (row * 7) + column)
                columnArray.append(abc)
            }
            rowArray.append(columnArray)
        }
        return rowArray
    }
    
    func getMonthHeader() -> String {
        let headerDateFormatter = DateFormatter()
        headerDateFormatter.calendar = yManager.calendar
        headerDateFormatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "yyyy LLLL", options: 0, locale: yManager.calendar.locale)
        
        return headerDateFormatter.string(from: firstOfMonthForOffset()).uppercased()
    }
    
    func getDateAtIndex(index: Int) -> Date {
        let firstOfMonth = firstOfMonthForOffset()
        let weekday = yManager.calendar.component(.weekday, from: firstOfMonth)
        var startOffset = weekday - yManager.calendar.firstWeekday
        startOffset += startOffset >= 0 ? 0 : daysPerWeek
        var dateComponents = DateComponents()
        dateComponents.day = index - startOffset
        
        return yManager.calendar.date(byAdding: dateComponents, to: firstOfMonth)!
    }
    
    func numberOfDays(offset : Int) -> Int {
        let firstOfMonth = firstOfMonthForOffset()
        let rangeOfWeeks = yManager.calendar.range(of: .weekOfMonth, in: .month, for: firstOfMonth)
        
        return (rangeOfWeeks?.count)! * daysPerWeek
    }
    
    func firstOfMonthForOffset() -> Date {
        var offset = DateComponents()
        offset.month = monthOffset
        
        return yManager.calendar.date(byAdding: offset, to: YFirstDateMonth())!
    }
    
    func YFormatDate(date: Date) -> Date {
        let components = yManager.calendar.dateComponents(calendarUnitYMD, from: date)
        
        return yManager.calendar.date(from: components)!
    }
    
    func YFormatAndCompareDate(date: Date, referenceDate: Date) -> Bool {
        let refDate = YFormatDate(date: referenceDate)
        let clampedDate = YFormatDate(date: date)
        return refDate == clampedDate
    }
    
    func YFirstDateMonth() -> Date {
        var components = yManager.calendar.dateComponents(calendarUnitYMD, from: yManager.minimumDate)
        components.day = 1
        
        return yManager.calendar.date(from: components)!
    }
    
    // MARK: - Date Property Checkers
    
    func isToday(date: Date) -> Bool {
        return YFormatAndCompareDate(date: date, referenceDate: Date())
    }
     
    func isSpecialDate(date: Date) -> Bool {
        return isSelectedDate(date: date) ||
            isStartDate(date: date) ||
            isEndDate(date: date) ||
            isOneOfSelectedDates(date: date)
    }
    
    func isOneOfSelectedDates(date: Date) -> Bool {
        return self.yManager.selectedDatesContains(date: date)
    }

    func isSelectedDate(date: Date) -> Bool {
        if yManager.selectedDate == nil {
            return false
        }
        return YFormatAndCompareDate(date: date, referenceDate: yManager.selectedDate)
    }
    
    func isStartDate(date: Date) -> Bool {
        if yManager.startDate == nil {
            return false
        }
        return YFormatAndCompareDate(date: date, referenceDate: yManager.startDate)
    }
    
    func isEndDate(date: Date) -> Bool {
        if yManager.endDate == nil {
            return false
        }
        return YFormatAndCompareDate(date: date, referenceDate: yManager.endDate)
    }
    
    func isBetweenStartAndEnd(date: Date) -> Bool {
        if yManager.startDate == nil {
            return false
        } else if yManager.endDate == nil {
            return false
        } else if yManager.calendar.compare(date, to: yManager.startDate, toGranularity: .day) == .orderedAscending {
            return false
        } else if yManager.calendar.compare(date, to: yManager.endDate, toGranularity: .day) == .orderedDescending {
            return false
        }
        return true
    }
    
    func isOneOfDisabledDates(date: Date) -> Bool {
        return self.yManager.disabledDatesContains(date: date)
    }
    
    func isOneOfAbsentDate(date: Date) -> Bool {
       return self.yManager.absentDatesContains(date: date)
    }
    
    func isEnabled(date: Date) -> Bool {
        let clampedDate = YFormatDate(date: date)
        if yManager.calendar.compare(clampedDate, to: yManager.minimumDate, toGranularity: .day) == .orderedAscending || yManager.calendar.compare(clampedDate, to: yManager.maximumDate, toGranularity: .day) == .orderedDescending {
            return false
        }
        return !isOneOfDisabledDates(date: date)
    }
    
    func isStartDateAfterEndDate() -> Bool {
        if yManager.startDate == nil {
            return false
        } else if yManager.endDate == nil {
            return false
        } else if yManager.calendar.compare(yManager.endDate, to: yManager.startDate, toGranularity: .day) == .orderedDescending {
            return false
        }
        return true
    }
    
    
    func oneOfTextDate(date: Date) -> String {
        if let textValue = self.yManager.textDatesGetValue(date: date) {
            return textValue
        } else {
            return ""
        }
    }
}

#if DEBUG
struct YMonth_Previews : PreviewProvider {
    static var previews: some View {
        YMonth(isPresented: GenericObservableModel(value: false),yManager: YManager(calendar: Calendar.current, minimumDate: Date(), maximumDate: Date().addingTimeInterval(60*60*24*365), mode: 0), monthOffset: 0)
    }
}
#endif

