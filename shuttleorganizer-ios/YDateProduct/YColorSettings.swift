//
//  YColorSettings.swift
//  YCalendar
//

import Foundation
import Combine
import SwiftUI

class YColorSettings : ObservableObject {

    // foreground colors
    @Published var textColor: Color = Color.colorText
    @Published var todayColor: Color = Color.white
    @Published var selectedColor: Color = Color.white
    @Published var disabledColor: Color = Color.gray
    @Published var betweenStartAndEndColor: Color = Color.white
    @Published var absentColor: Color = Color.white
    // background colors
    @Published var textBackColor: Color = Color.clear
    @Published var todayBackColor: Color = Color.gray
    @Published var selectedBackColor: Color = Color.red
    @Published var disabledBackColor: Color = Color.clear
    @Published var betweenStartAndEndBackColor: Color = Color.colorSecondary
    @Published var absentBackColor: Color = Color.orange
    // headers foreground colors
    @Published var weekdayHeaderColor: Color = Color.black
    @Published var weekdayHeaderColorOld: Color = Color.colorPrimary
    @Published var monthHeaderColor: Color = Color.colorPrimary
    // headers background colors
    @Published var weekdayHeaderBackColor: Color = Color.white
    @Published var monthBackColor: Color = Color.clear

}
