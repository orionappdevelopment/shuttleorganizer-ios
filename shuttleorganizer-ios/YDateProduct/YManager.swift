//
//  YManager.swift
//  YCalendar
//

import SwiftUI

class YManager : ObservableObject {

    @Published var calendar = Calendar.current
    @Published var minimumDate: Date = Date()
    @Published var maximumDate: Date = Date()
    @Published var disabledDates: [Date] = [Date]()
    @Published var selectedDates: [Date] = [Date]()
    @Published var selectedDate: Date! = nil
    @Published var startDate: Date! = nil
    @Published var endDate: Date! = nil
    
    @Published var mode: Int = 0
    @Published var textDates : [LDate: String] = [LDate: String]()
    @Published var absentDates: [Date] = [Date]()
    
    @Published var callBack : () -> ()
    
    var colors = YColorSettings()
  
    init(calendar: Calendar, minimumDate: Date, maximumDate: Date, selectedDates: [Date] = [Date](), mode: Int, callBack : @escaping ()->() = {}) {
        self.calendar = calendar
        self.minimumDate = minimumDate
        self.maximumDate = maximumDate
        self.selectedDates = selectedDates
        self.mode = mode
        self.callBack = callBack
    }
    
    func selectedDatesContains(date: Date) -> Bool {
        if let _ = self.selectedDates.first(where: { calendar.isDate($0, inSameDayAs: date) }) {
            return true
        }
        return false
    }
    
    func selectedDatesFindIndex(date: Date) -> Int? {
        return self.selectedDates.firstIndex(where: { calendar.isDate($0, inSameDayAs: date) })
    }
    
    func disabledDatesContains(date: Date) -> Bool {
        if let _ = self.disabledDates.first(where: { calendar.isDate($0, inSameDayAs: date) }) {
            return true
        }
        return false
    }
    
    func disabledDatesFindIndex(date: Date) -> Int? {
        return self.disabledDates.firstIndex(where: { calendar.isDate($0, inSameDayAs: date) })
    }
    
    func absentDatesContains(date: Date) -> Bool {
        if let _ = self.absentDates.first(where: { calendar.isDate($0, inSameDayAs: date) }) {
            return true
        }
        return false
    }
    
    func absentDatesFindIndex(date: Date) -> Int? {
        return self.absentDates.firstIndex(where: { calendar.isDate($0, inSameDayAs: date) })
    }
    
    func testDatesContains(date: Date) -> Bool {
        let lDate : LDate = YManager.toMyLDate(date: date)
        if let _ = self.textDates[lDate] {
            return true
        }
        return false
    }
    
    func textDatesGetValue(date: Date) -> String? {
        let lDate : LDate = YManager.toMyLDate(date: date)
        let value = self.textDates[lDate]
        return value
    }
    
    public static func toMyLDate(date : Date) -> LDate{
//        let date = Date() // save date, so all components use the same date
        let calendar = Calendar.current // or e.g. Calendar(identifier: .persian)

        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        return LDate(year: year, month: month, dayOfMonth: day)
    }
}
