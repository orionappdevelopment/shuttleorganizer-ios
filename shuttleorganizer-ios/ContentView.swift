//
//  ContentView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/23/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct ContentView: View {
//    @State private var selection = 0
    @ObservedObject private var selection: GenericObservableModel<Int> = GenericObservableModel(value: 0)

    @State private var mainTransportationsTabview : MainTransportationsTabview = MainTransportationsTabview()
    @State private var activeView : ActiveView = ActiveView()
    @State private var profileView : ProfileView = ProfileView()
    
    func notificationCame(_ notification: Notification) {
        if let shuttleNotificationWrapper : ShuttleNotificationWrapper = notification.object as? ShuttleNotificationWrapper{
            print("shuttleNotificationWrapper: \(shuttleNotificationWrapper)")
            let shuttleNotificationTypeEnum : ShuttleNotificationTypeEnum = shuttleNotificationWrapper.shuttleNotificationModel.getShuttleNotificationTypeEnum()
            if shuttleNotificationTypeEnum == .CHAT_MESSAGE_NOTIFICATION && self.selection.value != 1 {
                self.selection.value = 1
            }else if shuttleNotificationTypeEnum == .TRANSPORTATION_NOTIFICATION && self.selection.value != 0 {
                self.selection.value = 0
            }else{
                print("ContentView notification event will disregard for notification type: \(shuttleNotificationTypeEnum)")
            }
        }else{
            print("Error : ContentView can not parse notification for notification.object:\(notification.object ?? "")")
        }
    }
    
    init() {
        //UITabBar.appearance().barTintColor = UIColor.colorBarBackground
        UITabBar.appearance().backgroundColor = UIColor.colorBarBackground
        
        UITabBar.appearance().unselectedItemTintColor = UIColor.colorTabUnSeledted
        UITabBar.appearance().tintColor = UIColor.colorTabSeledted
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.colorTabUnSeledted!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.colorTabSeledted!], for: .selected)
        
        let chatMessageNotificationName = ShuttleNotificationEventFactory.getInstance().getEventNotificationName(shuttleNotificationTypeEnum: .CHAT_MESSAGE_NOTIFICATION)
        NotificationCenter.default.addObserver(forName: chatMessageNotificationName, object: nil, queue: nil, using: self.notificationCame)
        
        let transportationNotificationName = ShuttleNotificationEventFactory.getInstance().getEventNotificationName(shuttleNotificationTypeEnum: .TRANSPORTATION_NOTIFICATION)
        NotificationCenter.default.addObserver(forName: transportationNotificationName, object: nil, queue: nil, using: self.notificationCame)
    }

    var body: some View {

        TabView(selection: $selection.value){
//        TabView(selection: $selection){
//            MainTransportationsTabview()
            mainTransportationsTabview
//                .font(.title)
                .tabItem {
                    VStack {
                        Image("shuttle_updated")
                            .renderingMode(.template)
                        Text("MainBottomTabView.MyShuttles.text")
                    }
                }
                .tag(0)
//            ActiveView()
            activeView
//                .font(.title)
                .tabItem {
                    VStack {
                        Image("active_updated")
                            .renderingMode(.template)
                        Text("MainBottomTabView.Active.text")
                    }
                }
                .tag(1)
//            ProfileView()
            profileView
//                .font(.title)
                .tabItem {
                    VStack {
                        Image(systemName:"person.fill")
                            .renderingMode(.template)
                        Text("MainBottomTabView.Profile.text")
                    }
                }
                .tag(2)
        }
        .accentColor(Color.colorTabSeledted)
        .onAppear(){
            if SingleNotificationManager.getInstance().isWindowsCreated == false {
                SingleNotificationManager.getInstance().isWindowsCreated = true
                SingleNotificationManager.getInstance().postNotification()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//        BottomTabView([
//            TabBarItem(view: MainTransportationsTabview(),
//                       image: Image("shuttle")
//                                .renderingMode(.template)
//                                .resizable()
//                                .aspectRatio(contentMode: .fit)
//                                .frame(width: 30, height: 30)
//                                .padding(5)
//                                .foregroundColor(Color.colorPrimary)
//                                .padding(.trailing, 7),
//                       title: "MyShuttles"),
//            TabBarItem(view: Text("Harita Olacak"),
//                       image: Image("active")
//                                .renderingMode(.template)
//                                .resizable()
//                                .aspectRatio(contentMode: .fit)
//                                .frame(width: 50, height: 40)
//                                .padding(EdgeInsets(top: 0, leading: 0, bottom: -5, trailing: 0))
//                                .foregroundColor(Color.colorPrimary)
//                                .padding(.trailing, 7),
//                       title: "Active"),
//            TabBarItem(view: Text("Profil Olacak"),
//                       image: Image("user")
//                                .renderingMode(.template)
//                                .resizable()
//                                .aspectRatio(contentMode: .fit)
//                                .frame(width: 30, height: 30)
//                                .padding(5)
//                                .foregroundColor(Color.colorPrimary)
//                                .padding(.trailing, 7),
//                       title: "Me")
//
//        ])
