//
//  KeyboardAdaptive.swift
//  KeyboardAvoidanceSwiftUI
//
//  Created by Vadim Bulavin on 3/27/20.
//  Copyright © 2020 Vadim Bulavin. All rights reserved.
//

import SwiftUI
import Combine

/// Note that the `KeyboardAdaptive` modifier wraps your view in a `GeometryReader`,
/// which attempts to fill all the available space, potentially increasing content view size.
struct KeyboardViewAdaptiveMove: ViewModifier {
    @State private var bottomPadding: CGFloat = 0

    func body(content: Content) -> some View {
        GeometryReader { geometry in
            content
                .padding(.bottom, self.bottomPadding)
                .onReceive(Publishers.keyboardHeight) { keyboardHeight in
                    let keyboardTop = geometry.frame(in: .global).height - keyboardHeight
                    let focusedTextInputBottom = UIResponder.currentFirstResponder?.globalFrame?.maxY ?? 0
                    self.bottomPadding = max(0, focusedTextInputBottom - keyboardTop - geometry.safeAreaInsets.bottom)
                    print("StreamChatView bottomPadding:\(self.bottomPadding)")
            }
            .animation(.easeOut(duration: 0.16))
        }
    }
}

extension View {
    func keyboardViewAdaptiveMove() -> some View {
        ModifiedContent(content: self, modifier: KeyboardViewAdaptiveMove())
    }
}

struct KeyboardViewAllMove: ViewModifier {
    @State private var bottomPadding: CGFloat = 0

    func body(content: Content) -> some View {
        GeometryReader { geometry in
            content
                .padding(.bottom, self.bottomPadding)
                .onReceive(Publishers.keyboardHeight) { keyboardHeight in
                    print("StreamChatView keyboardHeight:\(keyboardHeight)")
                    let keyboardTop = geometry.frame(in: .global).height - keyboardHeight
                    print("StreamChatView keyboardTop:\(keyboardTop)")
                    let focusedTextInputBottom = UIResponder.currentFirstResponder?.globalFrame?.maxY ?? 0
                    print("StreamChatView focusedTextInputBottom:\(focusedTextInputBottom)")
//                    if focusedTextInputBottom > keyboardHeight * 2{
//                        self.bottomPadding = 0
//                    }else{
                    let unsafeBottomPadding = UIApplication.shared.windows[0].safeAreaInsets.bottom
                    self.bottomPadding = max(0, keyboardHeight - unsafeBottomPadding)
//                    }
                    print("StreamChatView bottomPadding:\(self.bottomPadding)")
            }
            .animation(.easeOut(duration: 0.16))
        }
    }
}

extension View {
    func keyboardViewAllMove() -> some View {
        ModifiedContent(content: self, modifier: KeyboardViewAllMove())
    }
}

struct KeyboardViewNoMove: ViewModifier {
    @State private var bottomPadding: CGFloat = 0

    func body(content: Content) -> some View {
        GeometryReader { geometry in
            content
                .padding(.bottom, self.bottomPadding)
                .onReceive(Publishers.keyboardHeight) { keyboardHeight in
                    self.bottomPadding = 0
                    print("StreamChatView bottomPadding:\(self.bottomPadding)")
            }
//            .animation(.easeOut(duration: 0.16))
        }
    }
}

extension View {
    func keyboardViewNoMove() -> some View {
        ModifiedContent(content: self, modifier: KeyboardViewNoMove())
    }
}

struct KeyboardIgnoreSafeArea: ViewModifier {
    func body(content: Content) -> some View {
            GeometryReader { geometry in
            if #available(iOS 14.0, *) {
                content
                    .ignoresSafeArea(.keyboard)
            }else{
                content
            }
        }
    }
}

extension View {
    func keyboardIgnoreSafeArea() -> some View {
        ModifiedContent(content: self, modifier: KeyboardIgnoreSafeArea())
    }
}

