//
//  StringExtensions.swift
//  shuttleorganizer-ios
//
//  Created by macos on 2/7/21.
//  Copyright © 2021 Orion. All rights reserved.
//

import Foundation

extension Character {
    var asciiValue: UInt32? {
     return String(self).unicodeScalars.filter{$0.isASCII}.first?.value
    }
}

extension String {
    // 2) ascii array to map our string
    var asciiArray: [UInt32] {
     return unicodeScalars.filter{$0.isASCII}.map{$0.value}
    }

    // this is our hashCode function, which produces equal output to the Java or Android hash function
    func javaHashCode() -> Int32 {
     var h : Int32 = 0
     for i in self.asciiArray {
      h = 31 &* h &+ Int32(i) // Be aware of overflow operators,
     }
     return h
    }
}
