//
//  FBRemoteConfig.swift
//  shuttleorganizer-ios
//
//  Created by macos on 2/13/21.
//  Copyright © 2021 Orion. All rights reserved.
//

import Foundation
import Firebase

class FBRemoteConfig{
    
    public class RemoteConfigConstants{
        static var ShuttleUpdateRange : Double = 50
        static var ShuttlePersonNotifyRemainingRange: Double = 500
    }
    
    static private var instance : FBRemoteConfig? = nil
    var remoteConfig: RemoteConfig!
    let defaultConfigValues = ["welcome.text" : "isd"]
    
    
    static func getInstance() -> FBRemoteConfig {
        if instance == nil {
            instance = FBRemoteConfig()
            instance?.fetchConfig()
        }
        return instance!
    }
    
    private init(){
        // [START get_remote_config_instance]
        remoteConfig = RemoteConfig.remoteConfig()
        // [END get_remote_config_instance]
        // Create a Remote Config Setting to enable developer mode, which you can use to increase
        // the number of fetches available per hour during development. See Best Practices in the
        // README for more information.
        // [START enable_dev_mode]
//        let settings = RemoteConfigSettings()
//        settings.minimumFetchInterval = 0
//        remoteConfig.configSettings = settings
        // [END enable_dev_mode]
        // [END enable_dev_mode]
        // Set default Remote Config parameter values. An app uses the in-app default values, and
        // when you need to adjust those defaults, you set an updated value for only the values you
        // want to change in the Firebase console. See Best Practices in the README for more
        // information.
        // [START set_default_values]
            remoteConfig.setDefaults(fromPlist: "RemoteConfigDefaults")
        // [END set_default_values]
    }


    private func fetchConfig() {
        // [START fetch_config_with_callback]
        remoteConfig.fetch() {[unowned self] (status, error) -> Void in
          if status == .success {
            print("Config fetched!")
            self.remoteConfig.activate() { (changed, error) in
                print("remoteConfig changed:\(changed)")
                print("remoteConfig error:\(String(describing: error))")
              // ...
                
                let str : String? = self.getValueFromKey(key: "welcome_text")
                print("welcome_Text:\(String(describing: str))")
                
                let dbl1 : NSNumber? = self.getValueFromKey(key: "ShuttleUpdateRange")
                if let validDbl1 : Double = dbl1?.doubleValue{
                    RemoteConfigConstants.ShuttleUpdateRange = validDbl1
                    print("ShuttleUpdateRange:\(RemoteConfigConstants.ShuttleUpdateRange)")
                }
                
                let dbl2 : NSNumber? = self.getValueFromKey(key: "ShuttlePersonNotifyRemainingRange")
                if let validDbl2 : Double = dbl2?.doubleValue{
                    RemoteConfigConstants.ShuttlePersonNotifyRemainingRange = validDbl2
                    print("ShuttlePersonNotifyRemainingRange:\(RemoteConfigConstants.ShuttlePersonNotifyRemainingRange)")
                }
            }
          } else {
            print("Config not fetched")
            print("Error: \(error?.localizedDescription ?? "No error available.")")
          }
        }
        // [END fetch_config_with_callback]
    }
    
    private func getValueFromKey <T> (key : String) -> T?{
        if remoteConfig ==  nil{
            print("RemoteConfig is nil, will not return anything")
        }else{
            print("T:\(T.self)")
            var optionalValue : T? = nil
            if T.self is String.Type{
                optionalValue = self.remoteConfig?[key].stringValue as! T?
            }else if T.self is Bool.Type{
                optionalValue = self.remoteConfig?[key].boolValue as! T?
            }else if T.self is NSNumber.Type{
                optionalValue = self.remoteConfig?[key].numberValue as! T?
            }
            return optionalValue
        }
        return nil
        
    }

}

