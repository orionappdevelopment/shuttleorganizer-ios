//
//  Utils.swift
//  Landmarks
//
//  Created by macos on 5/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit
import GooglePlaces
import GoogleMaps

struct Utils{

    static func fromJsonToString(encodedJson : Data) -> String{
        guard let jsonString = String(data: encodedJson, encoding: .utf8)
            else { return "" }
        return jsonString
    }
    
    //use LTime instead of Time
//    public static Comparator<Time> TimeComparator = Comparator<Time>() {
//        let HOUR : Int = 3600;
//        let MINUTES : Int = 60;
//        private Int getSeconds(time : Time) {
//            return time.getHours() * HOUR + time.getMinutes() * MINUTES;
//        }
//        
//        @Override
//        public Int compare(time1 : Time, time2 : Time) {
//            if(time1 != null && time2 != null) {
//                var thisTimeSeconds : Int = getSeconds(time1);
//                var anotherTimeSeconds : Int = getSeconds(time2);
//                if(thisTimeSeconds == anotherTimeSeconds) {
//                    return 0;
//                }else if( thisTimeSeconds - anotherTimeSeconds > 21 * HOUR){
//                    return -1;
//                }else if(anotherTimeSeconds - thisTimeSeconds  > 21 * HOUR) {
//                    return 1;
//                }else if( thisTimeSeconds < anotherTimeSeconds) {
//                    return -1;
//                }else if( anotherTimeSeconds < thisTimeSeconds) {
//                    return 1;
//                }
//                
//            }
//            return 0;
//        }};
    //TODO: do after findNextValidFullDate
//    public static Comparator<Transportation> TransportationComparator = Comparator<Transportation>() {
//        private Map<Transportation, FDate> transportationFDateMap = HashMap<>();
//        @Override
//        public Int compare(transportation1 : Transportation, transportation2 : Transportation) {
//            if(transportation1 != null && transportation2 != null) {
//
//                var nextValidFullDate1 : FDate = transportationFDateMap.get(transportation1);
//                if(nextValidFullDate1 == null) {
//                    nextValidFullDate1 = findNextValidFullDate(transportation1);
//                    transportationFDateMap.put(transportation1, nextValidFullDate1);
//                }
//
//                var nextValidFullDate2 : FDate = transportationFDateMap.get(transportation2);
//                if(nextValidFullDate2 == null) {
//                    nextValidFullDate2 = findNextValidFullDate(transportation2);
//                    transportationFDateMap.put(transportation2, nextValidFullDate2);
//                }
//
//                return nextValidFullDate1.compareTo(nextValidFullDate2);
//            }
//            return 0;
//        }};
    
//    public static func TimeToString (time : Time)  -> String {
//        if(time == null) {
//            return "00:00";
//        }
//        var str : String = String.format("%02d", time.getHours()) + ":" + String.format("%02d", time.getMinutes());
//        return str;
//    }
    
    public static func toDate(lTime : LTime) -> Date {
        let date = Calendar.current.date(bySettingHour: lTime.hour, minute: lTime.minute, second: lTime.second, of: Date())!
        return date
    }
    
    public static func toLTime(time : Date) -> LTime {
//        let date = Date() // save date, so all components use the same date
        let calendar = Calendar.current // or e.g. Calendar(identifier: .persian)

        let hour = calendar.component(.hour, from: time)
        let minute = calendar.component(.minute, from: time)
        let second = calendar.component(.second, from: time)
        return LTime(hour: hour, minute: minute, second: second)
    }
    
    public static func toDate(lDate : LDate) -> Date {
        // Specify date components
        var dateComponents = DateComponents()
        dateComponents.year = lDate.year
        dateComponents.month = lDate.month
        dateComponents.day = lDate.day
//        dateComponents.timeZone = TimeZone(abbreviation: "JST") // Japan Standard Time
        dateComponents.hour = 0
        dateComponents.minute = 0
        dateComponents.second = 0

        // Create date from components
        let userCalendar = Calendar.current // user calendar
        let someDateTime = userCalendar.date(from: dateComponents)!
        
        return someDateTime
    }
    
    public static func toLDate(date : Date) -> LDate{
//        let date = Date() // save date, so all components use the same date
        let calendar = Calendar.current // or e.g. Calendar(identifier: .persian)

        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        return LDate(year: year, month: month, dayOfMonth: day)
    }
    
    public static func toDate(fDate : FDate) -> Date {
        // Specify date components
        var dateComponents = DateComponents()
        dateComponents.year = fDate.year
        dateComponents.month = fDate.month
        dateComponents.day = fDate.day
        //dateComponents.timeZone = TimeZone(abbreviation: "JST") // Japan Standard Time
        dateComponents.hour = fDate.hour
        dateComponents.minute = fDate.minute
        dateComponents.second = fDate.second

        // Create date from components
        let userCalendar = Calendar.current // user calendar
        let someDateTime = userCalendar.date(from: dateComponents)!
        
        return someDateTime
    }
    
    public static func toFDate(date : Date) -> FDate {
        let calendar = Calendar.current // or e.g. Calendar(identifier: .persian)

        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        
        return FDate(year: year, month: month, dayOfMonth: day, hour: hour, minute: minute, second: second)
    }
    
    
    /**
     * @param lTime1
     * @param lTime2
     *
     * returns always positive diff
     * return timeDiff for lTime2 - lTime1
     * when lTime2 >= lTime1 return lTime2 - lTime1
     * when lTime2 < lTime1 return lTime2 + DaySeconds - lTime1
     * @return
     */
    public static func lTimesDiffInSeconds(lTime1 : LTime, lTime2 : LTime) -> Int {
        let MINUTES_PER_HOUR : Int = 60;
        let SECONDS_PER_MINUTE : Int = 60;
        let SECONDS_PER_HOUR : Int = SECONDS_PER_MINUTE * MINUTES_PER_HOUR;
        let SECONDS_PER_DAY : Int = 24 * SECONDS_PER_HOUR;
        let timeInSeconds1 : Int = lTime1.hour * SECONDS_PER_HOUR + lTime1.minute * SECONDS_PER_MINUTE + lTime1.second
        let timeInSeconds2 : Int = lTime2.hour * SECONDS_PER_HOUR + lTime2.minute * SECONDS_PER_MINUTE + lTime2.second
        
        if(timeInSeconds2 >= timeInSeconds1) {
            return timeInSeconds2 - timeInSeconds1;
        }else{
            return timeInSeconds2 + SECONDS_PER_DAY - timeInSeconds1;
        }
    }
    
    public static func addSecondsToLTime(lTime : LTime, addSeconds : Int) -> LTime {
        let SECONDS_OF_DAY : Int = 86400;
        let SECONDS_OF_HOUR : Int = 3600;
        let SECONDS_OF_MINUTE : Int = 60;
        let lseconds : Int = lTime.toSecondOfDay();
        var nseconds : Int = lseconds + addSeconds;
        while(nseconds < 0) {
            nseconds += SECONDS_OF_DAY;
        }
        nseconds = nseconds % SECONDS_OF_DAY;
        
        let hour : Int = nseconds / SECONDS_OF_HOUR;
        nseconds = nseconds % SECONDS_OF_HOUR;
        let minute : Int = nseconds / SECONDS_OF_MINUTE;
        nseconds = nseconds % SECONDS_OF_MINUTE;
        let second : Int = nseconds;
        
        return LTime(hour: hour, minute: minute, second: second);
    }
    
    public static func addSecondsToFDate (fDate : FDate, addSeconds : Int) -> FDate {
        var dateComponents = DateComponents()
        dateComponents.year = fDate.year
        dateComponents.month = fDate.month
        dateComponents.day = fDate.day
        //dateComponents.timeZone = TimeZone(abbreviation: "JST") // Japan Standard Time
        dateComponents.hour = fDate.hour
        dateComponents.minute = fDate.minute
        dateComponents.second = fDate.second

        // Create date from components
        let userCalendar = Calendar.current // user calendar
        let someDateTime = userCalendar.date(from: dateComponents)!
        let addedDate = Calendar.current.date(byAdding: .second, value: addSeconds, to: someDateTime, wrappingComponents: false)!
        
        let calendar = Calendar.current // or e.g. Calendar(identifier: .persian)
        let year = calendar.component(.year, from: addedDate)
        let month = calendar.component(.month, from: addedDate)
        let day = calendar.component(.day, from: addedDate)
        let hour = calendar.component(.hour, from: addedDate)
        let minute = calendar.component(.minute, from: addedDate)
        let second = calendar.component(.second, from: addedDate)
        
        return FDate(year: year, month: month, dayOfMonth: day, hour: hour, minute: minute, second: second)
    }
    //no need
//    public static func toLTime (localTime : LocalTime) -> LTime {
//        return LTime(localTime.getHour(), localTime.getMinute(), localTime.getSecond());
//    }
//    public static func toLDate (localDate : LocalDate) -> LDate {
//        return LDate(localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth());
//    }
    
    //defined in Attendance
//    public static Comparator<Attendance> AttendanceComparator = Comparator<Attendance>() {
//        @Override
//        public Int compare(a1 : Attendance, a2 : Attendance) {
//            if(a1 != null && a2 != null) {
//                var startDateCompare : Int = a1.getStartDate().compareTo(a2.getStartDate());
//                var endDateCompare : Int = a1.getEndDate().compareTo(a2.getEndDate());
//                if(startDateCompare != 0) {
//                    return startDateCompare;
//                }else if( startDateCompare == 0){
//                    return endDateCompare;
//                }
//            }
//            return 0;
//        }};
        
    
    public static func getTimeZoneOfComputer () -> Int {
        let HOUR : Int = 60 * 60
        let dst : Int = Int(TimeZone.current.daylightSavingTimeOffset())
        let timeZoneOfmachine : Int = (dst + TimeZone.current.secondsFromGMT()) / HOUR
        return timeZoneOfmachine
    }
    
    public static func getDateForTimeZone (timeZone : Int) -> LDate {
        //timeZone is GMT - UTC
        
        let now : Date = Date()
        
        let myTimeZone : Int = getTimeZoneOfComputer();
        
        let updatedDate : Date = Calendar.current.date(byAdding: .hour, value: timeZone - myTimeZone, to: now, wrappingComponents: false)!
        
        let calendar = Calendar.current
        let year = calendar.component(.year, from: updatedDate)
        let month = calendar.component(.month, from: updatedDate)
        let day = calendar.component(.day, from: updatedDate)
        
        let currentDateForTimeZone : LDate = LDate(year: year, month: month, dayOfMonth: day)
        return currentDateForTimeZone;
    }
    
    /**
     * @param startDate
     * @param endDate
     *
     * returns list of dates between dates
     * startDate: 28.02.2020
     * endDate: 02.03.2020
     * @return
     * list of following dates
     * 28.02.2020, 29.02.2020, 01.03.2020, 02.03.2020
     */
    public static func getDateListBetweenDates (startDate : LDate, endDate : LDate) -> [Date] {
        var dates : [Date] = []
        
        var itrDate : Date = toDate(lDate: startDate)
        let lastDate : Date = toDate(lDate: endDate)
        
        let calendar = Calendar.current
        
        while(lastDate >= itrDate) {
            dates.append(itrDate)
            itrDate = calendar.date(byAdding: .day, value: 1, to: itrDate, wrappingComponents: false)!
        }
        
        return dates;
    }
    
    public static func getTimeZone (timeZoneHour : Int) -> TimeZone {
        let HOUR = 60 * 60
        
        if(timeZoneHour > 12 || timeZoneHour < -11) {
            preconditionFailure("timeZoneHour:\(timeZoneHour) is not beetween -11 and 12");
        }
        
        let timeZone : TimeZone = TimeZone(secondsFromGMT: (HOUR * timeZoneHour))!
        
        return timeZone
    }
    
    public static func findNextValidDate (transportation : Transportation) -> LDate? {
//        let DAY : Int = 24*60*60*1000;
        let HOUR : Int = 60*60
//        let MINUTE : Int = 60*1000;
//        let SECOND : Int = 1000;
        
        let LDAY : Int64 = 24*60*60*1000;
        let LHOUR : Int64 = 60*60*1000;
        let LMINUTE : Int64 = 60*1000;
        let LSECOND : Int64 = 1000;

        var now : Date = Date()

        //make Calendar to transportation timezone
        
        let timeZoneOfmachine : Int = (Int(TimeZone.current.daylightSavingTimeOffset()) + TimeZone.current.secondsFromGMT()) / HOUR
        let timeZoneOfTransportation : Int = transportation.timeZone
//        System.out.println(getTimeZone(timeZoneOfTransportation - timeZoneOfmachine));
        
        now = Calendar.current.date(byAdding: .hour, value: timeZoneOfTransportation - timeZoneOfmachine, to: now, wrappingComponents: false)!

        //get current date for transportation
        let calendar = Calendar.current // or e.g. Calendar(identifier: .persian)
        var year = calendar.component(.year, from: now)
        var month = calendar.component(.month, from: now)
        var day = calendar.component(.day, from: now)
        let hour = calendar.component(.hour, from: now)
        let minute = calendar.component(.minute, from: now)
        let second = calendar.component(.second, from: now)
        
//        var millis : Int = now.get(Calendar.MILLISECOND);
        let dayOfWeek : Int = (calendar.component(.weekday, from: now) + 5) % 7; //make Monday index 0
//        System.out.println(now.getTimeZone());
        
        let transportationStartHour : Int64 = Int64(transportation.startTime.hour)
        let transportationStartMin : Int64 = Int64(transportation.startTime.minute)
        let transporatationStartSec : Int64 = Int64(transportation.startTime.second)
        let transportationStartTimeInMillis : Int64 = (transportationStartHour * LHOUR) + (transportationStartMin * LMINUTE) + (transporatationStartSec * LSECOND)
        
        //get hours (0-24) for tranportation
        let currentTimeInMillis : Int64 = (Int64(hour) * LHOUR) + (Int64(minute) * LMINUTE) + (Int64(second) * LSECOND)
        let currentTimeInMillisForYesterday : Int64 = (24 * LHOUR) + currentTimeInMillis;
//        var startTimeInMillis : long = transportation.getStartTime().getTime();
        let startTimeInMillis : Int64 = transportationStartTimeInMillis;
        let endTimeInMillis : Int64 = startTimeInMillis + 2 * LHOUR;
        var result : LDate? = nil;
        
        let transportationDaysOfWeek : [Bool] = DayEnum.daysFromMultiplication(multiplication: transportation.days)
        
        if(transportationDaysOfWeek[dayOfWeek] && currentTimeInMillis <= endTimeInMillis &&  endTimeInMillis < LDAY + currentTimeInMillis){
            result = LDate(year: year, month: month, dayOfMonth: day);
            return result;
        }
        
        let yesterdayDayOfWeek :  Int = (dayOfWeek + 6) % 7;
        if(transportationDaysOfWeek[yesterdayDayOfWeek] && currentTimeInMillisForYesterday <= endTimeInMillis) {
            now = calendar.date(byAdding: .day, value: -1, to: now, wrappingComponents: false)!
            //yesterday still active
            year = calendar.component(.year, from: now)
            month = calendar.component(.month, from: now) // Note: zero based!
            day = calendar.component(.day, from: now)
            result = LDate(year: year, month: month, dayOfMonth: day)
            return result;
        }
        
        var itrDay : Int = (dayOfWeek + 1 ) % 7;

        var numberOfDay : Int = 1;
        //find nearest valid date for transportation, hours not matter after that
        for _ in 1...7 {
            if(transportationDaysOfWeek[itrDay]){
                now = calendar.date(byAdding: .day, value: numberOfDay, to: now, wrappingComponents: false)!
                year = calendar.component(.year, from: now)
                month = calendar.component(.month, from: now) // Note: zero based!
                day = calendar.component(.day, from: now)
                result = LDate(year: year, month: month, dayOfMonth: day)
                return result;
            }
            itrDay = (itrDay + 1) % 7;
            numberOfDay += 1
        }

        return result;
    }
    //do at the end
    /*
    public static func findNextValidFullDate (transportation : Transportation) -> FDate {
        var lDate : LDate = findNextValidDate(transportation);
        
        var fDate : FDate = FDate(lDate.getYear(), lDate.getMonth(), lDate.getDay(),
                transportation.getStartTime().getHour(), transportation.getStartTime().getMinute(), transportation.getStartTime().getSecond());
        
        return fDate;
    }*/
    
    /**
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     *
     * method converted from javascript code
     * you can find code here
     * https://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
     *
     * @return
     * distance in km
     */
    public static func distanceBetweenTwoPoint(lat1 : Float64, lon1 : Float64, lat2 : Float64, lon2 : Float64) -> Float64 {
        let R : Float64 = 6371; // Radius of the earth in km
        let dLat : Float64 = deg2rad(lat2 - lat1);
        let dLon : Float64 = deg2rad(lon2 - lon1);
        
        let a : Float64 = sin(dLat / 2) * sin(dLat / 2) +
                cos(deg2rad(lat1)) * cos(deg2rad(lat2)) *
                sin(dLon / 2) * sin(dLon / 2);
        let c : Float64 = 2 * atan2(sqrt(a), sqrt(1 - a));
        let d : Float64 = R * c; // Distance in km
        
        return d;
    }
    
    public static func deg2rad(_ number: Double) -> Double {
        return number * .pi / 180
    }
    
    public static func getWeekDayShortName(dayIndex : Int) -> String {
        var calendar = Calendar(identifier: .gregorian)
        calendar.locale = Locale.current
//        let calendarIndex = dayIndex
        let calendarIndex = (dayIndex + 1) % 7
        return calendar.veryShortWeekdaySymbols[calendarIndex]
    }
    
    public static func isValidTimeDiffBeetweenDepartureAndPersonGetOntime(transportationTime : LTime, personGetOnTime : LTime) -> Bool {
            
        let maxTimeDiffInSec = 60 * 60 * 2
        let timeDiff = Utils.lTimesDiffInSeconds(lTime1: transportationTime, lTime2: personGetOnTime)
        if timeDiff > maxTimeDiffInSec {
            return false
        }
        return true
    }
    
    public static func isTransportationActiveNow (transportation : Transportation) -> Bool {
//        let DAY : Int = 24*60*60*1000;
        let HOUR : Int = 60*60
//        let MINUTE : Int = 60*1000;
//        let SECOND : Int = 1000;
        
        let LDAY : Int64 = 24*60*60*1000;
        let LHOUR : Int64 = 60*60*1000;
        let LMINUTE : Int64 = 60*1000;
        let LSECOND : Int64 = 1000;

        var now : Date = Date()

        //make Calendar to transportation timezone
        
        let timeZoneOfmachine : Int = (Int(TimeZone.current.daylightSavingTimeOffset()) + TimeZone.current.secondsFromGMT()) / HOUR
        let timeZoneOfTransportation : Int = transportation.timeZone
//        System.out.println(getTimeZone(timeZoneOfTransportation - timeZoneOfmachine));
        
        now = Calendar.current.date(byAdding: .hour, value: timeZoneOfTransportation - timeZoneOfmachine, to: now, wrappingComponents: false)!

        //get current date for transportation
        let calendar = Calendar.current // or e.g. Calendar(identifier: .persian)
//        var year = calendar.component(.year, from: now)
//        var month = calendar.component(.month, from: now)
//        var day = calendar.component(.day, from: now)
        let hour = calendar.component(.hour, from: now)
        let minute = calendar.component(.minute, from: now)
        let second = calendar.component(.second, from: now)
        
//        var millis : Int = now.get(Calendar.MILLISECOND);
        let dayOfWeek : Int = (calendar.component(.weekday, from: now) + 5) % 7; //make Monday index 0
//        System.out.println(now.getTimeZone());
        
        let transportationStartHour : Int64 = Int64(transportation.startTime.hour)
        let transportationStartMin : Int64 = Int64(transportation.startTime.minute)
        let transporatationStartSec : Int64 = Int64(transportation.startTime.second)
        let transportationStartTimeInMillis : Int64 = (transportationStartHour * LHOUR) + (transportationStartMin * LMINUTE) + (transporatationStartSec * LSECOND)
        
        //get hours (0-24) for tranportation
        let currentTimeInMillis : Int64 = (Int64(hour) * LHOUR) + (Int64(minute) * LMINUTE) + (Int64(second) * LSECOND)
        let currentTimeInMillisForYesterday : Int64 = (24 * LHOUR) + currentTimeInMillis;
//        var startTimeInMillis : long = transportation.getStartTime().getTime();
        let startTimeInMillis : Int64 = transportationStartTimeInMillis;
        let endTimeInMillis : Int64 = startTimeInMillis + 2 * LHOUR;
//        let endTimeInMillis : Int64 = startTimeInMillis + 24 * LHOUR;
        
        let transportationDaysOfWeek : [Bool] = DayEnum.daysFromMultiplication(multiplication: transportation.days)
        
        if(transportationDaysOfWeek[dayOfWeek]
            && currentTimeInMillis <= endTimeInMillis && currentTimeInMillis >= startTimeInMillis
            && endTimeInMillis < LDAY + currentTimeInMillis){
            return true;
        }
        
        let yesterdayDayOfWeek :  Int = (dayOfWeek + 6) % 7;
        if(transportationDaysOfWeek[yesterdayDayOfWeek]
            && currentTimeInMillisForYesterday <= endTimeInMillis && currentTimeInMillisForYesterday >= startTimeInMillis) {
            return true;
        }
        
        return false;
    }

    /**
     get Current TimeZone Hour Minute from milliseconds
    */
    public static func getHourMinuteFromMilliseconds(timeIntervalSince1970 : Int64) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "HH:mm"
        let doubleDate = Double(timeIntervalSince1970/1000)
        let date1 = Date(timeIntervalSince1970: doubleDate)
        print(date1)
        let localHourMinute = dateFormatter.string(from: date1)
        return localHourMinute
    }
    
    /**
     get Current TimeZone Hour Minute Seconds from milliseconds
    */
    public static func getHourMinuteSecondsFromMilliseconds(timeIntervalSince1970 : Int64) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "HH:mm:ss"
        let doubleDate = Double(timeIntervalSince1970/1000)
        let date1 = Date(timeIntervalSince1970: doubleDate)
        print(date1)
        let localHourMinute = dateFormatter.string(from: date1)
        return localHourMinute
    }
    
    public static func getImageDirectoryFromFileName(_ fileName : String) -> String {
        let hash : Int32 = fileName.javaHashCode()
        let mask : Int32 = 255;
        let fileSeparator : String = String(UnicodeScalar(UInt8(47)))
        let firstDir : Int32 = hash & mask;
        let secondDir : Int32 = (hash >> 8)  & mask;
        let path = fileSeparator +
            String(format:"%03d", firstDir) +
            fileSeparator +
            String(format:"%03d", secondDir) +
            fileSeparator
            return path;
        }
    
    public static func getCountryCode() -> String?{
        let countryCode = NSLocale.current.regionCode
        print("Country Code: \(String(describing: countryCode))")
        return countryCode
    }
    
    public static func getNorthEastBorder(coordinate : CLLocationCoordinate2D) -> CLLocationCoordinate2D{
//        if let validCoordinate = coordinate{
            let northLatitude = min(coordinate.latitude + 2, 90)
            let eastLongitude = min(coordinate.longitude + 2, 180)
            let northEastBorder = CLLocationCoordinate2D(latitude: northLatitude, longitude: eastLongitude)
            return northEastBorder
//        }
//        return coordinate
    }
    
    public static func getSouthWestBorder(coordinate : CLLocationCoordinate2D) -> CLLocationCoordinate2D{
//        if let validCoordinate = coordinate{
            let southLatitude = max(coordinate.latitude - 2, -90)
            let westLongitude = max(coordinate.longitude - 2, -180)
            let southWestBorder = CLLocationCoordinate2D(latitude: southLatitude, longitude: westLongitude)
            return southWestBorder
//        }
//        return coordinate
    }
    
    public static func toJsonArray(from object:[Any]) -> String {
        let emptyResult = "[]"

        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return emptyResult
        }

        if let jsonArray = String(data: data, encoding: String.Encoding.utf8){
            return jsonArray
        }

        return emptyResult
    }
    
//    public static func toJsonArray(from list:[String]) -> String {
//
//        let emptyResult = "[]"
//        if list.isEmpty{
//            return emptyResult
//        }
//
//        var buf : String = "["
//        for (index, str) in list.enumerated(){
//            buf.append("\"");
//            buf.append(str)
//            buf.append("\"")
//            if index < (list.count - 1){
//                buf.append(",")
//            }
//
//        }
//        buf.append("]")
//
//        return buf
//    }
    
    public static func jsonToObject <T:Codable> (data : Data?) -> T?{
        var newData : T? = nil
        do {
            if let validData = data{
                let dataString = String(data: validData, encoding: .utf8) ?? ""
                print("data will be parser to json dataAsString:\(dataString)")
                newData = try JSONDecoder().decode(T.self, from: validData)
            }
        } catch{
            print("can not parse data:\(String(describing: data)) and error:\(error)")
        }
        return newData
    }
    
    public static func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
}
