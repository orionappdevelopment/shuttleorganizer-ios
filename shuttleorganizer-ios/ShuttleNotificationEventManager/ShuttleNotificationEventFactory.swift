//
//  ShuttleNotificationEventFactory.swift
//  shuttleorganizer-ios
//
//  Created by macos on 10.07.2021.
//  Copyright © 2021 Orion. All rights reserved.
//

import Foundation
import SwiftUI
import UserNotifications
import UIKit

class ShuttleNotificationEventFactory : NSObject {
    
    private static var instance : ShuttleNotificationEventFactory? = nil
    
    public static func getInstance() -> ShuttleNotificationEventFactory{
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        if ShuttleNotificationEventFactory.instance == nil{
            ShuttleNotificationEventFactory.instance = ShuttleNotificationEventFactory()
        }
            
        return ShuttleNotificationEventFactory.instance!
    }
    
    private override init() {
        super.init()
    }
    
    public func getEventNotificationName(shuttleNotificationTypeEnum : ShuttleNotificationTypeEnum) -> Notification.Name{
        if shuttleNotificationTypeEnum == .CHAT_MESSAGE_NOTIFICATION{
            return Notification.Name(shuttleNotificationTypeEnum.description)
        }else if shuttleNotificationTypeEnum == .TRANSPORTATION_NOTIFICATION{
            return Notification.Name(shuttleNotificationTypeEnum.description)
        }else if shuttleNotificationTypeEnum == .MANAGEMENT_NOTIFICATION{
            return Notification.Name(shuttleNotificationTypeEnum.description)
        }else if shuttleNotificationTypeEnum == .NONE{
            return Notification.Name(shuttleNotificationTypeEnum.description)
        }else{
            print("Error \(shuttleNotificationTypeEnum.description) not defined!")
            return Notification.Name(shuttleNotificationTypeEnum.description)
        }
    }
}
