//
//  ShuttleNotificationWrapper.swift
//  shuttleorganizer-ios
//
//  Created by macos on 10.07.2021.
//  Copyright © 2021 Orion. All rights reserved.
//

import Foundation
struct ShuttleNotificationWrapper : Equatable, Hashable, Codable {

    var shuttleNotificationModel : ShuttleNotificationModel
    var notificationPressedTime : Int64
    
    public init(shuttleNotificationModel : ShuttleNotificationModel, notificationPressedTime : Int64 = Utils.getCurrentMillis()) {
        self.shuttleNotificationModel = shuttleNotificationModel
        self.notificationPressedTime = notificationPressedTime
    }
    
    static func == (lhs: ShuttleNotificationWrapper, rhs: ShuttleNotificationWrapper) -> Bool {
        return lhs.shuttleNotificationModel == rhs.shuttleNotificationModel && lhs.notificationPressedTime == rhs.notificationPressedTime
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(self.shuttleNotificationModel)
        hasher.combine(self.notificationPressedTime)
    }
}
