//
//  SharingLocationTransportationManager.swift
//  shuttleorganizer-ios
//
//  Created by macos on 1/16/21.
//  Copyright © 2021 Orion. All rights reserved.
//

import Foundation


class SharingLocationTransportationManager{
    
    private var activeTransportations : Dictionary<String, Transportation>
    private static let DataKey = "activeTransportations"
    
    enum CacheOperationType { case add, remove, update}
    
    private static var instance : SharingLocationTransportationManager? = SharingLocationTransportationManager()
    
    public static func getInstance() -> SharingLocationTransportationManager{
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        if SharingLocationTransportationManager.instance == nil{
            SharingLocationTransportationManager.instance = SharingLocationTransportationManager()
        }
            
        return SharingLocationTransportationManager.instance!
    }
    
    init(){
        let cachedDictionary = SharingLocationTransportationManager.retrieveData()
        self.activeTransportations = [:]
        if let dict = cachedDictionary {
            for (key, val) in dict {
//                if val is Transportation{
                    let transportation = val
                    if Utils.isTransportationActiveNow(transportation: transportation){
                        self.activeTransportations.updateValue(val, forKey:key)
                    }
//                }
            }
        }
    }
    
    func getActivePositionSharingTranportation() -> [Transportation]{
        var transportations : [Transportation] = []
        self.removeInactiveTransporations()
        for (_, val) in self.activeTransportations{
            transportations.append(val)
        }
        return transportations
    }
    
    func addTransportation(transportation : Transportation) -> Bool{
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        var isAdded = false
        if !Utils.isTransportationActiveNow(transportation: transportation){
            print("transportation is not active so can not add transportation:\(transportation)")
            return false
        }
        
        if transportation.id == nil {
            print("transportation id is nil so can not add transportation: \(transportation)")
            return false
        }
        
        let transportationOld = self.activeTransportations[transportation.id!]
        if transportationOld == nil {
            isAdded = true
        }
        if transportationOld != nil && isTransportationTimeUpdated(transportation1: transportationOld!, transportation2: transportation){
            isAdded = true
        }
        activeTransportations.updateValue(transportation, forKey: transportation.id!)
        storeData()
        return isAdded
    }
    
    func removeTransportation(transportation : Transportation) -> Bool{
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        var isRemoved = false
        
        if transportation.id == nil {
            print("transportation id is nil so can not add transportation: \(transportation)")
            return false
        }
        
        let transportationOld = self.activeTransportations[transportation.id!]
        if transportationOld != nil {
            isRemoved = true
        }
        activeTransportations.removeValue(forKey: transportation.id!)
        SingleLocationManagerBasic.getInstance().deleteTransportationLocation(transportationId: transportation.id!)
        storeData()
        return isRemoved
    }
    
//    func removeAllTransportation(){
//        self.activeTransportations.removeAll()
//    }
    
    //https://stackoverflow.com/questions/41355427/attempt-to-insert-non-property-list-object-when-trying-to-save-a-custom-object-i
    private func storeData(){
        print("storeData activetransportations :\(self.activeTransportations)")
        if let encoded = try? JSONEncoder().encode(self.activeTransportations) {
            UserDefaults.standard.set(encoded, forKey: SharingLocationTransportationManager.DataKey)
        }
    }
    
    static func retrieveData() -> Dictionary<String, Transportation>?{
        var dictionary : Dictionary<String, Transportation>? = nil
        if let data = UserDefaults.standard.object(forKey: SharingLocationTransportationManager.DataKey) as? Data,
            let value = try? JSONDecoder().decode(Dictionary<String, Transportation>.self, from: data) {
            dictionary = value
        }
        return dictionary
    }
    
    func removeAllTransportationAndDb(){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        for (_, transportation) in activeTransportations {
            if let transportationId = transportation.id{
                SingleLocationManagerBasic.getInstance().deleteTransportationLocation(transportationId: transportationId)
            }
        }
        
        activeTransportations.removeAll()
        UserDefaults.standard.removeObject(forKey: SharingLocationTransportationManager.DataKey)
    }
    
    func isTransportationLocationSharing(transportation : Transportation) -> Bool{
        var isSharing = false
        if !Utils.isTransportationActiveNow(transportation: transportation){
            print("transportation is not active so will remove:\(transportation)")
            _ = removeTransportation(transportation: transportation)
            return false
        }
        
        if transportation.id == nil {
            print("transportation id is nil so can not share: \(transportation)")
            return false
        }
        
        let currentTransportation = self.activeTransportations[transportation.id!]
        if currentTransportation != nil {
            isSharing = true
        }
        
        return isSharing
    }
    
    func toggleTransportationLocationSharing(transportation : Transportation) -> Bool{
        var isSharing = false
        if !Utils.isTransportationActiveNow(transportation: transportation){
            print("transportation is not active so will remove:\(transportation)")
            _ = removeTransportation(transportation: transportation)
            return false
        }
        
        if transportation.id == nil {
            print("transportation id is nil so can not share: \(transportation)")
            return false
        }
        
        let currentTransportation = self.activeTransportations[transportation.id!]
        if currentTransportation != nil {
            _ = removeTransportation(transportation: transportation)
            isSharing = false
        }else{
            _ = addTransportation(transportation: transportation)
            isSharing = true
        }
        
        //before startLocationManager and stopLocationManager, SharingTransportationManager should be called
        if self.activeTransportations.count < 1{
            SingleLocationManagerBasic.getInstance().stopLocationManager()
        }else{
            SingleLocationManagerBasic.getInstance().startLocationManager()
        }
        
        return isSharing
    }
    
    func removeInactiveTransporations(){
        for (_, value) in activeTransportations{
            if Utils.isTransportationActiveNow(transportation: value) == false{
                _ = self.removeTransportation(transportation: value)
            }
        }
    }
    
    func isTransportationTimeUpdated(transportation1 : Transportation, transportation2 : Transportation) -> Bool{
        if transportation1.days != transportation2.days {
            return true
        }
        if transportation1.startTime != transportation2.startTime {
            return true
        }
        if transportation1.timeZone != transportation2.timeZone {
            return true
        }
        return false
    }
    
    
    
}
