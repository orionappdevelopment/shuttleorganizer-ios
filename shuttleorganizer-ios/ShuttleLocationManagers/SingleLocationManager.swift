//
//  Location1.swift
//  LocationBackground
//
//  Created by macos on 1/6/21.
//  Copyright © 2021 Orion. All rights reserved.
//
//https://stackoverflow.com/questions/44369117/receiving-location-even-when-app-is-not-running-in-swift

import Foundation

import MapKit
import CoreLocation
import SwiftUI
import UserNotifications
/*
class SingleLocationManager : NSObject, CLLocationManagerDelegate{
    
    private var locationManager: CLLocationManager? = nil
    private var myLocation: CLLocation?
    let RegionInMeters: Double = 1
    let AccuracyInMeters : Double = 50
    private var isActivated = false
    
    private var redisGpsPublisher : RedisGpsPublisher? = nil
    
//    private(set) var locationAuthorizationStatus: CLAuthorizationStatus? = nil
    
    private static var instance : SingleLocationManager? = SingleLocationManager()
    
    public static func getInstance(didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> SingleLocationManager{
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        if SingleLocationManager.instance == nil{
            SingleLocationManager.instance = SingleLocationManager(didFinishLaunchingWithOptions: launchOptions)
        }
            
        return SingleLocationManager.instance!
    }
    
    private init(didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil)
    {
        super.init()
        if launchOptions?[UIApplication.LaunchOptionsKey.location] != nil {
            print("Terminated location manager found")
            locationManager = nil
            locationManager = CLLocationManager()
            locationManager?.delegate = self
//            locationManager?.distanceFilter = 10
            locationManager?.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager?.allowsBackgroundLocationUpdates = true
//            locationManager?.startUpdatingLocation()
//            startUpdateLocation()
            startLocationManager()
        } else {
            if locationManager == nil {
                locationManager = CLLocationManager()
                locationManager?.delegate = self
//                locationManager?.distanceFilter = 10
                locationManager?.desiredAccuracy = kCLLocationAccuracyBestForNavigation
                locationManager?.allowsBackgroundLocationUpdates = true
                
                if CLLocationManager.authorizationStatus() == .notDetermined {
//                    locationManager?.requestAlwaysAuthorization()
                }
                else if CLLocationManager.authorizationStatus() == .denied {
                    print("User denied location services")
                }
                else if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
//                    locationManager?.requestAlwaysAuthorization()
                }
                else if CLLocationManager.authorizationStatus() == .authorizedAlways {
//                    startUpdateLocation()
                    startLocationManager()
                }
                
//                locationAuthorizationStatus = CLLocationManager.authorizationStatus();
            }
        }
        
        
//        checkLocationServices()
    }
    
    func requestAlwaysLocationPermission(){
        self.locationManager?.requestAlwaysAuthorization()
    }
    
//    func setupLocationManager(){
//        locationManager.delegate = self
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.allowsBackgroundLocationUpdates = true
//        locationManager.activityType = .otherNavigation
//        locationManager.pausesLocationUpdatesAutomatically = false
//        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//        locationManager.startMonitoringSignificantLocationChanges()
//        monitorRegionAtLocation(identifier: "Home")
//    }
    
    
//    func checkLocationServices() {
//        if CLLocationManager.locationServicesEnabled() {
//            setupLocationManager()
//            checkLocationAuthorization()
//        } else {
//            // Show alert letting the user know they have to turn this on.
//        }
//    }
    
    
//    func checkLocationAuthorization(_ alert: UIAlertController? = nil) {
//        switch CLLocationManager.authorizationStatus() {
//        case .authorizedAlways:
////            locationManager?.startUpdatingLocation()
//            break
//        case .authorizedWhenInUse:
////            centerViewOnUserLocation()
////            locationManager?.startUpdatingLocation()
//            break
//        case .denied:
//            // Show alert instructing them how to turn on permissions
//            if let alert = alert{
//                UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true)
//            }
//            break
//        case .notDetermined:
//            locationManager?.requestAlwaysAuthorization()
//        case .restricted:
//            // Show an alert letting them know what's up
//            break
//        @unknown default:
//            locationManager?.requestAlwaysAuthorization()
//        }
//    }
        
    
    //MARK:- LocationManager Delegates
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
//        locationAuthorizationStatus = CLLocationManager.authorizationStatus();
//        print(locationAuthorizationStatus == CLAuthorizationStatus.authorizedAlways)
        
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            startLocationManager()
        }else{
            stopLocationManager()
        }
    }
    public func stopLocationManager(){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        self.isActivated = false
        self.locationManager?.stopUpdatingLocation()
        if let validLocationManager = self.locationManager{
            for region in validLocationManager.monitoredRegions{
                validLocationManager.stopMonitoring(for: region)
            }
        }
    }

    public func startLocationManager(){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        if (CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse)
            && isTransporationLocationUpdateNeeded(){
            self.isActivated = true
            self.locationManager?.startUpdatingLocation()
        }
    }
    
    private func stopUpdateLocation(){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        if self.isActivated {
            self.locationManager?.stopUpdatingLocation()
        }
    }
    
    private func startUpdateLocation(){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        if self.isActivated {
            self.locationManager?.startUpdatingLocation()
        }
    }
    
    private func restartUpdateLocation(){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        if self.isActivated {
            self.locationManager?.stopUpdatingLocation()
            self.locationManager?.startUpdatingLocation()
        }
    }
    
    private func stopRegionMonitorStartUpdateLocation(didExitRegion region: CLRegion){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        if self.isActivated {
            self.locationManager?.stopMonitoring(for: region)
            self.locationManager?.startUpdatingLocation()
        }
    }
    
    private func stopUpdateLocationStartRegionMonitor(didEnterRegion region: CLRegion){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        if self.isActivated {
            self.locationManager?.stopUpdatingLocation()
            self.locationManager?.startMonitoring(for: region)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            print("location is null can not update location")
            return }
        print("didUpdateLocations \(location.coordinate) and \(location.horizontalAccuracy)")
        
        if location.horizontalAccuracy <= AccuracyInMeters || location.verticalAccuracy <= AccuracyInMeters{
            myLocation = location
//            if !(UIApplication.shared.applicationState == .active) {
                self.createRegion()
//                self.publishTransportationLocation(cLLocation: location)
//            }
        } else {
            restartUpdateLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("didEnterRegion")
        SingleNotificationManager.getInstance().scheduleLocalNotification(alert: "didEnterRegion")
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("didExitRegion")
        if let currentLocation = self.myLocation{
            self.publishTransportationLocation(cLLocation: currentLocation)
        }
        
        SingleNotificationManager.getInstance().scheduleLocalNotification(alert: "didExitRegion")
        stopRegionMonitorStartUpdateLocation(didExitRegion: region)
        
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("\(error.localizedDescription)")
        SingleNotificationManager.getInstance().scheduleLocalNotification(alert: error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("\(error.localizedDescription)")
        SingleNotificationManager.getInstance().scheduleLocalNotification(alert: error.localizedDescription)
    }
    
    //MARK:- AppDelegate operations
    func createRegion() {
        guard let location = self.myLocation else {
            print("Problem with location in creating region")
            return
        }
        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            
            let coordinate = CLLocationCoordinate2DMake((location.coordinate.latitude), (location.coordinate.longitude))
            let regionRadius = RegionInMeters
            
            let region = CLCircularRegion(center: CLLocationCoordinate2D(
                latitude: coordinate.latitude,
                longitude: coordinate.longitude),
                                          radius: regionRadius,
                                          identifier: "aabb")
            
//            region.notifyOnEntry = true
            region.notifyOnExit = true
            
            SingleNotificationManager.getInstance().scheduleLocalNotification(alert: "Region Created \(location.coordinate) with \(location.horizontalAccuracy)")
            print("Region Created \(location.coordinate) with \(location.horizontalAccuracy)")
            print("stopUpdatingLocation")
            stopUpdateLocationStartRegionMonitor(didEnterRegion: region)
            print("startMonitoring")
        }
        else {
            print("System can't track regions")
        }
    }
    
    func isTransporationLocationUpdateNeeded() -> Bool{
        let transportations : [Transportation] = SharingLocationTransportationManager.getInstance().getActivePositionSharingTranportation()
        return transportations.count > 0
    }
    
    func publishTransportationLocation(cLLocation : CLLocation){
        let transportations : [Transportation] = SharingLocationTransportationManager.getInstance().getActivePositionSharingTranportation()
        print(transportations.count > 0)
        if(transportations.count > 0){
            if self.redisGpsPublisher == nil{
                self.redisGpsPublisher = RedisGpsPublisher()
                self.redisGpsPublisher?.initialize()
            }
            for transportation in transportations{
                if transportation.id != nil {
                    let time = Date().toMilliseconds()
                    let transportationId : String = transportation.id!
                    let senderId = MyUser.getInstance().getUserId()
                    let senderName = MyUser.getInstance().name
                    let latitude : Float64 = cLLocation.coordinate.latitude
                    let longitude : Float64 = cLLocation.coordinate.longitude
                    let course : Float64 = cLLocation.course
                    let speed : Float64 = cLLocation.speed
                    let gpsMessage : GpsMessage = GpsMessage(time: time, transportationId: transportationId, senderId: senderId, senderName: senderName, latitude: latitude, longitude: longitude, course: course, speed: speed)
                    
                    do {
                        let json = try JSONEncoder().encode(gpsMessage)
                        if let content = String(data: json, encoding: .utf8) {
                            // here `content` is the JSON data decoded as a String
                            print(content)
                            self.redisGpsPublisher?.publish(channel: transportationId+"_gps", message: content)
                        }
                    } catch  {
                        print("can not encode the gpsMessage")
                    }
                }
            }
        }else{
            self.stopLocationManager()
        }
    }
}
*/
