//
//  Location1.swift
//  LocationBackground
//
//  Created by macos on 1/6/21.
//  Copyright © 2021 Orion. All rights reserved.
//
//https://stackoverflow.com/questions/44369117/receiving-location-even-when-app-is-not-running-in-swift

import Foundation

import MapKit
import CoreLocation
import SwiftUI
import UserNotifications

class SingleLocationManagerBasic : NSObject, CLLocationManagerDelegate{
    
    private var locationManager: CLLocationManager? = nil
    private var myLocation: CLLocation?
    let RegionInMeters: Double = 50
    let AccuracyInMeters : Double = 50
    private var isActivated = false
    
    private var redisGpsPublisher : RedisGpsPublisher? = nil
    private var redisGpsCommand : RedisGpsCommand? = nil
    
//    private(set) var locationAuthorizationStatus: CLAuthorizationStatus? = nil
    
    private static var instance : SingleLocationManagerBasic? = SingleLocationManagerBasic()
    
    public static func getInstance(didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> SingleLocationManagerBasic{
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        if SingleLocationManagerBasic.instance == nil{
            SingleLocationManagerBasic.instance = SingleLocationManagerBasic(didFinishLaunchingWithOptions: launchOptions)
        }
            
        return SingleLocationManagerBasic.instance!
    }
    
    private init(didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil)
    {
        super.init()
        if launchOptions?[UIApplication.LaunchOptionsKey.location] != nil {
            print("Terminated location manager found")
            locationManager = nil
            locationManager = CLLocationManager()
            locationManager?.delegate = self
//            locationManager?.distanceFilter = 10
            locationManager?.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager?.allowsBackgroundLocationUpdates = true
//            locationManager?.startUpdatingLocation()
//            startUpdateLocation()
            startLocationManager()
        } else {
            if locationManager == nil {
                locationManager = CLLocationManager()
                locationManager?.delegate = self
//                locationManager?.distanceFilter = 10
                locationManager?.desiredAccuracy = kCLLocationAccuracyBestForNavigation
                locationManager?.allowsBackgroundLocationUpdates = true

                let cLLocationManager = CLLocationManager()
                
                if cLLocationManager.authorizationStatus == .notDetermined {
//                    locationManager?.requestAlwaysAuthorization()
                }
                else if cLLocationManager.authorizationStatus == .denied {
                    print("User denied location services")
                }
                else if cLLocationManager.authorizationStatus == .authorizedWhenInUse {
//                    locationManager?.requestAlwaysAuthorization()
                }
                else if cLLocationManager.authorizationStatus == .authorizedAlways {
//                    startUpdateLocation()
                    startLocationManager()
                }
                
//                locationAuthorizationStatus = CLLocationManager.authorizationStatus();
            }
        }
        
        
//        checkLocationServices()
    }
    
    func requestAlwaysLocationPermission(){
        self.locationManager?.requestAlwaysAuthorization()
    }
    
//    func setupLocationManager(){
//        locationManager.delegate = self
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.allowsBackgroundLocationUpdates = true
//        locationManager.activityType = .otherNavigation
//        locationManager.pausesLocationUpdatesAutomatically = false
//        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//        locationManager.startMonitoringSignificantLocationChanges()
//        monitorRegionAtLocation(identifier: "Home")
//    }
    
    
//    func checkLocationServices() {
//        if CLLocationManager.locationServicesEnabled() {
//            setupLocationManager()
//            checkLocationAuthorization()
//        } else {
//            // Show alert letting the user know they have to turn this on.
//        }
//    }
    
    
//    func checkLocationAuthorization(_ alert: UIAlertController? = nil) {
//        switch CLLocationManager.authorizationStatus() {
//        case .authorizedAlways:
////            locationManager?.startUpdatingLocation()
//            break
//        case .authorizedWhenInUse:
////            centerViewOnUserLocation()
////            locationManager?.startUpdatingLocation()
//            break
//        case .denied:
//            // Show alert instructing them how to turn on permissions
//            if let alert = alert{
//                UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true)
//            }
//            break
//        case .notDetermined:
//            locationManager?.requestAlwaysAuthorization()
//        case .restricted:
//            // Show an alert letting them know what's up
//            break
//        @unknown default:
//            locationManager?.requestAlwaysAuthorization()
//        }
//    }
        
    
    //MARK:- LocationManager Delegates
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
//        locationAuthorizationStatus = CLLocationManager.authorizationStatus();
//        print(locationAuthorizationStatus == CLAuthorizationStatus.authorizedAlways)
        
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            startLocationManager()
        }else{
            stopLocationManager()
        }
    }
    public func stopLocationManager(){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        self.isActivated = false
        self.locationManager?.stopUpdatingLocation()
        if let validLocationManager = self.locationManager{
            for region in validLocationManager.monitoredRegions{
                validLocationManager.stopMonitoring(for: region)
            }
        }
        SingleNotificationManager.getInstance().scheduleLocalNotification(alert: "stopLocationManager")
    }

    public func startLocationManager(){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        let cLLocationManager = CLLocationManager()
        if (cLLocationManager.authorizationStatus == .authorizedAlways || cLLocationManager.authorizationStatus == .authorizedWhenInUse)
            && isTransporationLocationUpdateNeeded(){
            self.isActivated = true
            self.locationManager?.startUpdatingLocation()
            SingleNotificationManager.getInstance().scheduleLocalNotification(alert: "startLocationManager")
        }
        
    }
    
    private func restartUpdateLocation(){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        if self.isActivated {
            self.locationManager?.stopUpdatingLocation()
            self.locationManager?.startUpdatingLocation()
            SingleNotificationManager.getInstance().scheduleLocalNotification(alert: "restartUpdateLocation")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        guard let location = locations.last else {
            print("location is null can not update location")
            return }
        print("didUpdateLocations \(location.coordinate) and \(location.horizontalAccuracy)")
        
        SingleNotificationManager.getInstance().scheduleLocalNotification(alert: "didUpdateLocations with location:\(location)")
        
        if location.horizontalAccuracy <= AccuracyInMeters || location.verticalAccuracy <= AccuracyInMeters{
            var distance : Float64 = 1_000_000
            
            if let oldLocation = myLocation {
                //in km
                distance = Utils.distanceBetweenTwoPoint(lat1 : oldLocation.coordinate.latitude, lon1 : oldLocation.coordinate.longitude, lat2 : location.coordinate.latitude, lon2 : location.coordinate.longitude)
                distance = distance * 1000 //in meter
            }
            
            print("distance : \(distance)")
            if distance > RegionInMeters{
                myLocation = location
                self.publishTransportationLocation(cLLocation: location)
                print("update location")
            }else{
                print("no need to update location")
            }
        } else {
            restartUpdateLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("\(error.localizedDescription)")
        SingleNotificationManager.getInstance().scheduleLocalNotification(alert: error.localizedDescription)
    }
    
    //MARK:- AppDelegate operations
    
    func isTransporationLocationUpdateNeeded() -> Bool{
        let transportations : [Transportation] = SharingLocationTransportationManager.getInstance().getActivePositionSharingTranportation()
        return transportations.count > 0
    }
    
    func deleteTransportationLocation(transportationId : String){
        if self.redisGpsCommand == nil{
            self.redisGpsCommand = RedisGpsCommand()
            self.redisGpsCommand?.initialize()
        }
        self.redisGpsCommand?.deleteValue(key: RedisConstants.getRedisLocationChannelId(transportationId: transportationId))
    }
    
    func publishTransportationLocation(cLLocation : CLLocation){
        let transportations : [Transportation] = SharingLocationTransportationManager.getInstance().getActivePositionSharingTranportation()
        print(transportations.count > 0)
        if(transportations.count > 0){
            if self.redisGpsPublisher == nil{
                self.redisGpsPublisher = RedisGpsPublisher()
                self.redisGpsPublisher?.initialize()
            }
            if self.redisGpsCommand == nil{
                self.redisGpsCommand = RedisGpsCommand()
                self.redisGpsCommand?.initialize()
            }
            for transportation in transportations{
                if transportation.id != nil {
                    let time = Date().toMilliseconds()
                    let transportationId : String = transportation.id!
                    let senderId = MyUser.getInstance().getUserId()
                    let senderName = MyUser.getInstance().name
                    let latitude : Float64 = cLLocation.coordinate.latitude
                    let longitude : Float64 = cLLocation.coordinate.longitude
                    let course : Float64 = cLLocation.course
                    let speed : Float64 = cLLocation.speed
                    let gpsMessage : GpsMessage = GpsMessage(time: time, transportationId: transportationId, senderId: senderId, senderName: senderName, latitude: latitude, longitude: longitude, course: course, speed: speed)
                    
                    do {
                        let json = try JSONEncoder().encode(gpsMessage)
                        if let content = String(data: json, encoding: .utf8) {
                            // here `content` is the JSON data decoded as a String
                            print(content)
                            self.redisGpsPublisher?.publish(channel: RedisConstants.getRedisLocationChannelId(transportationId: transportationId), message: content)
                            self.redisGpsCommand?.writeValue(key: RedisConstants.getRedisLocationChannelId(transportationId: transportationId), message: content)
                        }
                    } catch  {
                        print("can not encode the gpsMessage")
                    }
                }
            }
        }else{
            self.stopLocationManager()
        }
    }
}
