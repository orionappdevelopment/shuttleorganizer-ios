//
//  CreateButton.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/25/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct CreateButton: View {
    var body: some View {
        Button(action: {
                print("action")
            }, label: {
                HStack{
                    Text("save.text")
                        .padding(.horizontal)
                }
            })
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 50)
            .background(RoundedRectangle(cornerRadius: 15).fill(Color.colorSecondaryLight))
//                        .overlay(RoundedRectangle(cornerRadius: 5))
//            .disabled(!isValid())
//                        .padding(.bottom, 15)
    }
}

struct CreateButton_Previews: PreviewProvider {
    static var previews: some View {
        CreateButton()
    }
}
