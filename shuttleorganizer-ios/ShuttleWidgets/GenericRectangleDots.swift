//
//  RectangleDots.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/24/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct GenericRectangleDots<T : View> : View {
    var insideView : T
    var width : CGFloat = 100
    var height : CGFloat = 100
    
    var body: some View {
        ZStack(){
            Rectangle()
                .stroke(Color.colorPrimary, style: StrokeStyle(lineWidth: 5, miterLimit: 1, dash: [5], dashPhase: 30))
                .frame(width: self.width, height: self.height)
                .cornerRadius(5)
            self.insideView
        }
    }
}

struct GenericRectangleDots_Previews: PreviewProvider {
    
    static var previews: some View {
        GenericRectangleDots(insideView : Text("123456789a"))
    }
}
