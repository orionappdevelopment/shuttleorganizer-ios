//
//  MapLocationViewWithTopLabel.swift
//  shuttleorganizer-ios
//
//  Created by macos on 23.05.2021.
//  Copyright © 2021 Orion. All rights reserved.
//

import SwiftUI
import Foundation
import UIKit
import GooglePlaces
import GoogleMaps

struct MapLocationViewWithTopLabel: View {
    @State var width : CGFloat = 200
    @State var height : CGFloat = 50
    @State var titleEdgeInsets : EdgeInsets = EdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 5)
    @State var titleForegroundColor : Color = Color.colorPrimary
    @State var titleBackgroundColor : Color = Color.white
    @ObservedObject var titleValue  : GenericObservableModel<String> = GenericObservableModel(value: "")
    @ObservedObject var observedCoordinate : GenericObservableModel<CLLocationCoordinate2D>
    
    var body: some View {
        MapLocationView(observedCoordinate: observedCoordinate, cornerRadius: 10)
            .frame(width: self.width, height: self.height)
            .overlay(
                ZStack{
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.colorPrimary, lineWidth: 1)
                    HStack{
                        Text(toStringFromLocalizedStringKey(localizedStringKey: titleValue.value))
                            .padding(titleEdgeInsets)
                            .foregroundColor(titleForegroundColor)
                            .background(titleBackgroundColor)
                            .offset(x: self.width * 0.04, y: -self.height * 0.5)
                            
                        Spacer()
                    }
                }
            )
    }
}

struct MapLocationViewWithTopLabel_Previews: PreviewProvider {
    static var previews: some View {
        MapLocationViewWithTopLabel(observedCoordinate: GenericObservableModel(value: CLLocationCoordinate2D(latitude: 41.0370, longitude: 28.9851)))
    }
}
