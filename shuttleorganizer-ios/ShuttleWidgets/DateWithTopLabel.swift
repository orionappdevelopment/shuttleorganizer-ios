//
//  TextFieldWithTopLabel.swift
//  TextFieldApp
//
//  Created by macos on 20.05.2021.
//

import SwiftUI

struct DateWithTopLabel: View {
    @State var width : CGFloat = 200
    @State var height : CGFloat = 50
    @State var roundedRectangleStrokeColor = Color.colorPrimary
    @State var titleEdgeInsets : EdgeInsets = EdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 5)
    @State var titleForegroundColor : Color = Color.colorPrimary
    @State var titleBackgroundColor : Color = Color.white
    @ObservedObject var titleValue : GenericObservableModel<String> = GenericObservableModel(value: "")
    @State var dateEdgeInsets : EdgeInsets = EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10)
    @State var dateForegroundColor : Color = Color.colorText
    @ObservedObject var observedDate : GenericObservableModel<Date>
    
    var body: some View {
        Text(getHourMinuteFormat().string(from: self.observedDate.value))
            .font(.system(size: 20))
            .multilineTextAlignment(.center)
            .foregroundColor(dateForegroundColor)
            .padding(self.dateEdgeInsets)
            .frame(width: self.width, height: self.height)
            .overlay(
                ZStack{
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(roundedRectangleStrokeColor, lineWidth: 1)
                    HStack{
                        Text(toStringFromLocalizedStringKey(localizedStringKey: titleValue.value))
                            .padding(titleEdgeInsets)
                            .foregroundColor(titleForegroundColor)
                            .background(titleBackgroundColor)
                            .offset(x: self.width * 0.04, y: -self.height * 0.5)
                            
                        Spacer()
                    }
                }
            )
    }
}

struct DateWithTopLabel_Previews: PreviewProvider {
    static var previews: some View {
        DateWithTopLabel(observedDate: GenericObservableModel(value: Date()))
    }
}
