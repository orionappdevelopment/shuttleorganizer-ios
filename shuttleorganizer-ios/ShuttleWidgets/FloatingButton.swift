//
//  FloatingButton.swift
//  Landmarks
//
//  Created by macos on 5/17/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import SwiftUI

struct FloatingButton: View {
    @Binding var isLinkVisible : Bool
    var doNavigationDisappear : () -> Void?
    
    var body: some View {
        VStack {
                Spacer()
                HStack {
                    Spacer()
                    VStack(alignment: .trailing){
                        if isLinkVisible {
                            NavigationLink(
                                destination: CreateTransportationView()
                                    .onDisappear(){
                                        self.doNavigationDisappear()
                                    }
                                , label: {
                                    Text("FloatingButton.CreateShuttle.text")
                                        .foregroundColor(Color.colorListRowTitle)
                                        .padding(EdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5))
                                        .background(Color.colorSecondary)
                                    
                                    ZStack{
                                        Circle().frame(width: 40, height: 40)
                                            .foregroundColor(Color.colorSecondary)
                                        Image("create_shuttle")
                                            .renderingMode(.template)
                                            .resizable()
                                            .aspectRatio(contentMode: .fit)
                                            .frame(width: 25, height: 25)
                                            .foregroundColor(Color.white)
                                    }
                                    .padding(EdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 0))
                                    
                                })
                                .padding(EdgeInsets(top: 0, leading: 0, bottom: 5, trailing: 10))
                                
                            NavigationLink(
                                destination: JoinTransportationView()
                                    .onDisappear(){
                                        self.doNavigationDisappear()
                                    }
                                , label: {
                                    Text("FloatingButton.JoinShuttle.text")
                                        .foregroundColor(Color.colorListRowTitle)
                                        .padding(EdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5))
                                        .background(Color.colorSecondary)
                                    
                                    ZStack{
                                        Circle().frame(width: 40, height: 40)
                                            .foregroundColor(Color.colorSecondary)
                                        Image("join_shuttle")
                                            .renderingMode(.template)
                                            .resizable()
                                            .aspectRatio(contentMode: .fit)
                                            .frame(width: 25, height: 25)
                                            .foregroundColor(Color.white)
                                    }
                                    .padding(EdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 0))

                                })
                                .padding(EdgeInsets(top: 0, leading: 0, bottom: 5, trailing: 10))
                        }
                    }
                    
                }
                
                HStack {
                    Spacer()
                    VStack{
                        Button(action: {
        //                        self.items.append(Item(value: "Item"))
                                self.isLinkVisible.toggle()
                            }, label: {
                                Text("+")
                                .font(.system(.largeTitle))
                                .frame(width: 66, height: 60)
                                .foregroundColor(Color.white)
                                .padding(.bottom, 7)
                                .rotationEffect(.degrees(isLinkVisible ? -45 : 0))
                                .animation(.linear(duration: 0.1))
                            })
                            .background(Color.colorSecondary)
                            .cornerRadius(50)
                            .padding(EdgeInsets(top: 0, leading: 0, bottom: 25, trailing: 15))
                            .shadow(color: Color.black.opacity(0.8),
                                    radius: 3)
//                            .shadow(color: Color.black.opacity(0.3),
//                                    radius: 3,
//                                    x: 3,
//                                    y: 3)
                    }
                    
                }
            }
        .padding(.bottom, 0)
    }
}

struct FloatingButton_Previews: PreviewProvider {
    static var previews: some View {
        FloatingButton(isLinkVisible: .constant(false), doNavigationDisappear: {print("NavigationLink Disappeared")})
    }
}
