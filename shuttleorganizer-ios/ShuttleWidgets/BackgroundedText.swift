//
//  1_View.swift
//  Landmarks
//
//  Created by macos on 5/19/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import SwiftUI

public struct BackgroundedText: View {

    var first_color = Color.green
    var second_color = Color.white
    var text_color = Color.green

    var size = CGSize(width: 200, height: 100)
//    var xOffset: CGFloat = 50
//    var yOffset: CGFloat = 50

    var text = "Hello world!"

    init(_ txt: String, _ txt_color: Color, _ fColor: Color, _ sColor: Color, _ size: CGSize, _ xOff: CGFloat, _ yOff: CGFloat) {
        self.text = txt
        self.text_color = txt_color
        self.first_color = fColor
        self.second_color = sColor
        self.size = size
//        self.xOffset = xOff
//        self.yOffset = yOff
    }


    public var body: some View {
        VStack{
            ZStack(alignment: .leading){
                Rectangle()
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .frame(height: self.size.height)
        //                .frame(width: self.size.width,
        //                       height: self.size.height)
                    .foregroundColor(self.first_color)

        //            Rectangle()
        //            .frame(width: self.size.width - xOffset,
        //                   height: self.size.height - yOffset)
        //            .foregroundColor(self.second_color)

                Text(self.text)
                    .foregroundColor(self.text_color)
                    .font(.title)
                    .padding(5)

            }
        }
       
    }
}

struct BackgroundedText_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundedText("Hello", .black, .green, .green, CGSize(width: 200, height: 100), 50, 50)
    }
}

