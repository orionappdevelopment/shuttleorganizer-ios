//
//  BottomTabView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 6/13/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI
//not WORKING FOR NOW
struct BottomTabView: View {
    var views: [TabBarItem]
    @State var selectedIndex: Int = 0

    init(_ views: [TabBarItem]) {
        self.views = views
    }

    var body: some View {
        ZStack {
            ForEach(views.indices) { i in
                self.views[i].view
                    .opacity(self.selectedIndex == i ? 1 : 0)
            }
            GeometryReader { geometry in
                VStack {
                    Spacer()
                    ZStack(alignment: .top) {
                        LinearGradient(gradient: Gradient(colors: [Color.black.opacity(0.3), Color.black.opacity(0.4)]), startPoint: .top, endPoint: .bottom)
                            .frame(height: 70 + geometry.safeAreaInsets.bottom)

                        HStack {
                            ForEach(self.views.indices) { i in
                                Button(action: {
                                    self.selectedIndex = i
                                }) {
                                    VStack {
                                        if self.selectedIndex == i {
                                            self.views[i].image
                                                .foregroundColor(.white)
                                                 .padding(.top,10)
                                                .font(.title)
                                        } else {
                                            self.views[i].image
                                                .foregroundColor(Color.white.opacity(0.4))
                                                .padding(.top,10)
                                               .font(.title)
                                        }
                                        Text(self.views[i].title)
                                            .foregroundColor(.white)
                                            .font(Font.system(size: 16, weight: .bold))
                                            .padding(.top,10)
                                            .opacity(0.5)
                                    }
                                    .frame(maxWidth: .infinity)
                                }
                            }
                        }
                    }
                }
                .edgesIgnoringSafeArea(.bottom)
                .animation(.easeInOut)
            }
        }
    }
}

struct TabBarItem {
    var view: AnyView
    var image: AnyView
    var title: String

    init<V1: View, V2: View>(view: V1, image: V2, title: String) {
        self.view = AnyView(view)
        self.image = AnyView(image)
        self.title = title
    }
}

struct BottomTabView_Previews: PreviewProvider {
    static var previews: some View {
        BottomTabView([
          TabBarItem(view: Text("This is home screen"),
                     image: Image(systemName:"house.fill").resizable().aspectRatio(contentMode: .fit).frame(width: 10, height: 10),
                     title: "home"),
          TabBarItem(view: Text("2"),
                     image: Image(systemName:"heart.fill"),
                     title: "favorite"),

        ])
    }
}

/// Main View
//struct ContentView: View {
//  var body: some View {
//    BottomTabView([
//      TabBarItem(view: Text("This is home screen"),
//                 image: Image(systemName:"house.fill"),
//                 title: "home"),
//      TabBarItem(view: Text("2"),
//                 image: Image(systemName:"heart.fill"),
//                 title: "favorite"),
//
//    ])
//  }
//}
