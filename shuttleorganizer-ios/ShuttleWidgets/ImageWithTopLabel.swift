//
//  ImageWithTopLabel.swift
//  shuttleorganizer-ios
//
//  Created by macos on 30.05.2021.
//  Copyright © 2021 Orion. All rights reserved.
//

import SwiftUI

struct ImageWithTopLabel: View {
    @State var width : CGFloat = 200
    @State var height : CGFloat = 50
    @State var titleEdgeInsets : EdgeInsets = EdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 5)
    @State var titleForegroundColor : Color = Color.colorPrimary
    @State var titleBackgroundColor : Color = Color.white
    @ObservedObject var titleValue : GenericObservableModel<String> = GenericObservableModel(value: "")
    @State var imageEdgeInsets : EdgeInsets = EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10)
    @State var image : Image
    @State var imageWidth : CGFloat?  = nil
    @State var imageHeight : CGFloat? = nil
    
    var body: some View {
        image
            .renderingMode(.template)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: self.imageWidth == nil ? self.width * 0.3 : imageWidth!,
                   height: self.imageHeight == nil ? self.height*0.6 : self.imageHeight!,
                   alignment: .center)
            .foregroundColor(Color.colorText)
            .padding(self.imageEdgeInsets)
            .frame(width: self.width, height: self.height)
            .overlay(
                ZStack{
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.colorPrimary, lineWidth: 1)
                    HStack{
                        Text(toStringFromLocalizedStringKey(localizedStringKey: titleValue.value))
                            .padding(titleEdgeInsets)
                            .foregroundColor(titleForegroundColor)
                            .background(titleBackgroundColor)
                            .offset(x: self.width * 0.04, y: -self.height * 0.5)
                            
                        Spacer()
                    }
                }
            )
    }
}

struct ImageWithTopLabel_Previews: PreviewProvider {
    static var previews: some View {
        ImageWithTopLabel(image: Image("person"))
    }
}
