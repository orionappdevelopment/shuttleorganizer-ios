//
//  RectangleDots.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/24/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct RectangleDots<T : View> : View {
    @State var width : CGFloat = 130
    @State var height : CGFloat = 30
    @State var insideView : T
    
    
    var body: some View {
        ZStack(){
            Rectangle()
                .stroke(Color.colorPrimary, style: StrokeStyle(lineWidth: 5, miterLimit: 1, dash: [5], dashPhase: 30))
                .frame(width: width, height: height)
                .cornerRadius(5)
            insideView
        }
//        .padding(.top, 13)
    }
}

struct RectangleDots_Previews: PreviewProvider {
    
    static var previews: some View {
        RectangleDots(insideView : Text("123456789a"))
    }
}
