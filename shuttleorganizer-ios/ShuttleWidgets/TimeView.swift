//
//  TimeView.swift
//  TimeWidgetCustomize
//
//  Created by macos on 12/30/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct TimeView: View {
    @Binding var date: Date
    @Binding var isPresented : Bool
    var doSubmit = {}
    var body: some View {
        VStack(){
            ZStack{
                VStack{
                    HStack{
                        Spacer()
                        Text("TimeView.SelectTimeTitle.text")
                            .foregroundColor(Color.colorText)
                            .padding(.top, 10)
                            .offset(x: 12.5)
                        Spacer()
                        Image(systemName:"xmark.circle.fill")
                            .renderingMode(.template)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 25, height: 25, alignment: .center)
                            .foregroundColor(Color.colorIosCloseButtonOutsideXColor)
                            .background(Color.colorIosCloseButtonInsideXColor)
                            .clipShape(Circle())
                            .onTapGesture {
//                                    self.isPresented.value = false
                                self.isPresented = false
                            }
                            .padding(EdgeInsets(top: 10, leading: 10, bottom: 0, trailing: 10))
                    }
                    
                Spacer()
                Button(action: {
                    print("date:\(date)")
                    self.doSubmit()
                    self.isPresented = false
                    }, label: {
                        HStack{
                            Spacer()
                            Text("TimeView.OkButton.text")
                                .foregroundColor(Color.colorPrimary)
                            Spacer()
                        }
                        .background(Color.white)
                    })
    //                .offset(y: -10)
                    .padding(.bottom, 10)
                }
                VStack{
                    Spacer()
                    HStack{
                        DatePicker("", selection: self.$date, displayedComponents: .hourAndMinute)
                            .datePickerStyle(WheelDatePickerStyle())
//                            .transformEffect(.init(scaleX: 0.8, y: 0.8))
                    }
                    Spacer()
                }
                .offset(x: -3)
            }
        }
        .background(Color.white)
        .frame(width: 250, height: 290)
    }
}

struct TimeView_Previews: PreviewProvider {
    static var previews: some View {
        TimeView(date: .constant(Date()), isPresented: .constant(true))
    }
}
