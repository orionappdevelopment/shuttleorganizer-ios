//
//  ToastView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/25/20.
//  Copyright © 2020 Orion. All rights reserved.
//
/*
https://stackoverflow.com/questions/56550135/swiftui-global-overlay-that-can-be-triggered-from-any-view
 
To add it to your content view:

struct ContentView : View {
    @State private var liked: Bool = false

    var body: some View {
        VStack {
            LikeButton(liked: $liked)
        }
        // make it bigger by using "frame" or wrapping it in "NavigationView"
        .toast(isShowing: $liked, text: Text("Hello toast!"))
    }
}
How to hide the toast afte 2 seconds (as requested):

Append this code after .transition(.slide) in the toast VStack.

.onAppear {
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
      withAnimation {
        self.isShowing = false
      }
    }
}
*/
import SwiftUI

struct ToastView<Presenting>: View where Presenting: View {

    /// The binding that decides the appropriate drawing in the body.
    @Binding var isShowing: Bool
    /// The view that will be "presenting" this toast
    let presenting: () -> Presenting
    /// The text to show
    let text: Text

    var body: some View {

        GeometryReader { geometry in

            ZStack(alignment: .center) {

                self.presenting()
//                    .blur(radius: self.isShowing ? 1 : 0)
                VStack{
                    VStack {
                        self.text
                    }
    //                .frame(width: geometry.size.width / 2,
    //                       height: geometry.size.height / 5)
                    .frame(width: 250, height: 50)
    //                .background(Color.secondary.colorInvert())
    //                .foregroundColor(Color.primary)
                    .background(Color.black)
                    .foregroundColor(Color.white)
                    .cornerRadius(15)
                    .transition(.slide)
                    .onAppear {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                          withAnimation {
                            self.isShowing = false
                          }
                        }
                    }
                    .opacity(self.isShowing ? 1 : 0)
                }
                .offset(y : geometry.size.height * 0.33)
            }
            
        }
    }
}

extension View {
    func toast(isShowing: Binding<Bool>, text: Text) -> some View {
        ToastView(isShowing: isShowing,
              presenting: { self },
              text: text)
    }
}

struct ToastView_Previews: PreviewProvider {
    static var previews: some View {
        ToastView(isShowing: .constant(true), presenting: {Text("")}, text: Text("wqdqwd"))
//        ToastView(presenting: .constant(true), {Text("asd")}, text: Text("zasdasd"))
    }
}
