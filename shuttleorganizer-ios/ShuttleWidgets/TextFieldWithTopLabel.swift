//
//  TextFieldWithTopLabel.swift
//  TextFieldApp
//
//  Created by macos on 20.05.2021.
//

import SwiftUI

struct TextFieldWithTopLabel: View {
    @State var width : CGFloat = 200
    @State var height : CGFloat = 50
    @State var titleEdgeInsets : EdgeInsets = EdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 5)
    @State var titleForegroundColor : Color = Color.colorPrimary
    @State var titleBackgroundColor : Color = Color.white
    @State var titleValue : GenericObservableModel<String> = GenericObservableModel(value: "")
    @State var textFieldEdgeInsets : EdgeInsets = EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10)
    @ObservedObject var textFieldValue : GenericObservableModel<String>
    
    var body: some View {
        TextField("", text: $textFieldValue.value)
            .font(.system(size: 20))
            .multilineTextAlignment(.center)
            .foregroundColor(Color.colorText)
            .padding(self.textFieldEdgeInsets)
            .frame(width: self.width, height: self.height)
            .overlay(
                ZStack{
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.colorPrimary, lineWidth: 1)
                    HStack{
                        Text(toStringFromLocalizedStringKey(localizedStringKey: titleValue.value))
                            .padding(titleEdgeInsets)
                            .foregroundColor(titleForegroundColor)
                            .background(titleBackgroundColor)
                            .offset(x: self.width * 0.04, y: -self.height * 0.5)
                            
                        Spacer()
                    }
                }
            )
    }
}

struct TextFieldWithTopLabel_Previews: PreviewProvider {
    static var previews: some View {
        TextFieldWithTopLabel(textFieldValue: GenericObservableModel(value: ""))
    }
}
