//
//  UrlConstants.swift
//  Landmarks
//
//  Created by macos on 5/9/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct UrlConstants{
    
   static let UrlBase = "https://www.uppersoftwaresolutions.com:8443/shuttle/";

    //personService
   static let UrlSavePerson = UrlBase + "personservice/save";
   static let UrlGetPerson = UrlBase + "personservice/get";
    
        //transportationService
    static let UrlCreateTransportation = UrlBase + "transportationservice/save";
    static let UrlGetTransportation = UrlBase + "transportationservice/get";
    static let UrlGetCreatedTransportationList = UrlBase + "transportationservice/getcreatedtransportations";
    static let UrlDeleteTransportation = UrlBase + "transportationservice/delete";

        //personTransportationService
    static let UrlSavePersonTransportation = UrlBase + "persontransportationservice/save";
    static let UrlGetJoinedTransportationList = UrlBase + "persontransportationservice/getjoinedtransportations";
    static let UrlGetPersonTransportationList = UrlBase + "persontransportationservice/gettransportationpersons";
    static let UrlGetPersonTransportation = UrlBase + "persontransportationservice/get";
    static let UrlDeletePersonTransportation = UrlBase + "persontransportationservice/delete";

        //personTransportationAttendanceService
    static let UrlGetPersonTransportationAttendanceList = UrlBase + "persontransportationattendanceservice/getpersontransportationattendancesbytransportation";

        //attendanceService
    static let UrlGetAttendance = UrlBase + "attendanceservice/get";
    static let UrlGetAttendanceList = UrlBase + "attendanceservice/getattendances";
    static let UrlSaveAttendance = UrlBase + "attendanceservice/save";
    static let UrlDeleteAttendance = UrlBase + "attendanceservice/delete";

        //userService
    static let UrlCreateUser = UrlBase + "personservice/save";

        //notificationService
    static let UrlGetNotification = UrlBase + "personnotificationservice/get";
    static let UrlSaveNotification = UrlBase + "personnotificationservice/save";
    static let UrlDeleteNotification = UrlBase + "personnotificationservice/delete";
    static let UrlGetPersonNotificationList = UrlBase + "personnotificationservice/getpersonnotifications";
    
    //image save get image
    static let UrlGetImage = "http://46.101.106.121/test/img/";
//    static let UrlSaveImage = "http://46.101.106.121/test/upload_image.php?";
    static let UrlSaveImage = "http://46.101.106.121/test/upload_image.php?";
//    static let UrlSendNotification = "http://46.101.106.121/test/send_notification.php?";
    static let UrlSendNotification = "http://46.101.106.121/test/send_notification_yaefe.php?";
}
