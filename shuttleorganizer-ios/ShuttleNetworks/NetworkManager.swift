//
//  NetworkManager.swift
//  Landmarks
//
//  Created by macos on 5/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct NetworkManager{
    static let isNetworkAvaliable = true
    
    static func fetch <T: Codable, P: Codable> (url : String, parameters : P, handle : @escaping (_ newData: T) -> (), errorHandle : @escaping (_ newError: Error) -> ()){
        if isNetworkAvaliable == false {
            return
        }
        print(url)
        
        guard let validUrl = URL(string: url)
            else {
                print("Invalid Url: \(url)")
                return }

        var request = URLRequest(url: validUrl)
        request.httpMethod = "POST"

        do {
            request.httpBody = try JSONEncoder().encode(parameters)
//            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print("Parameters JSONSerialization Error: \(error.localizedDescription)")
            errorHandle(error)
            return
        }

        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        request.timeoutInterval = 20

        URLSession.shared.dataTask(with: request as URLRequest){(data, response, error) in
            do {
                guard let errorResponseData = data else {
                    print("There in no response from Server")
                    throw NetworkErrors.NoResponse
                }
                
                if let validJson = data
                {
                    let decodedData = try JSONDecoder().decode(T.self, from: validJson)
                    DispatchQueue.main.async {
                        handle(decodedData)
                        }
                    return
                } else {
                    if let jsonData = data {
                        print("Invalid Json Data: \(jsonData)")
                        throw NetworkErrors.InvalidResponse
                    }
                    if let validError = error {
                        print("Error Data: \(validError)")
                        throw NetworkErrors.ErrorResponse
                    }
                }
                
                if let errorResponseJson = try JSONSerialization.jsonObject(with: errorResponseData, options: .mutableContainers) as? [String: Any] {
                    print("Server return response with error: \(errorResponseJson)")
                    throw NetworkErrors.DetailedErrorResponse
                }

            } catch {
                print("Catched Error: \(error)")
                errorHandle(error)
            }
        }.resume()

    }
    
    static func fetch <T: Codable> (url : String, parameters : [String : String], handle : @escaping (_ newData: T) -> (), errorHandle : @escaping (_ newError: Error) -> ()){
        if isNetworkAvaliable == false {
            return
        }
        print(url)
        
        guard let validUrl = URL(string: url)
            else {
                print("Invalid Url: \(url)")
                return }

        var request = URLRequest(url: validUrl)
        request.httpMethod = "POST"

        do {
            request.httpBody = encodeParameters(parameters: parameters)
        } catch let error {
            print("Parameters JSONSerialization Error: \(error.localizedDescription)")
            errorHandle(error)
            return
        }

//        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
//        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Accept")
        

        request.timeoutInterval = 20

        URLSession.shared.dataTask(with: request as URLRequest){(data, response, error) in
            do {
                guard let errorResponseData = data else {
                    print("There in no response from Server")
                    throw NetworkErrors.NoResponse
                }
                
                if let responseObject : T = Utils.jsonToObject(data: data){
                    DispatchQueue.main.async {
                        handle(responseObject)
                    }
                    return
                }else {
//                    if let jsonData = data {
//                        print("Invalid Json Data: \(jsonData)")
//                        throw NetworkErrors.InvalidResponse
//                    }
                    if let validData = data, let dataString = String(data: validData, encoding: .utf8){
                        print("Error can not parse data to json and dataAsString:\(dataString)")
                    }
                    
                    if let validError = error {
                        print("Error Data: \(validError)")
                        throw NetworkErrors.ErrorResponse
                    }
                }
                
                if let errorResponseJson = try JSONSerialization.jsonObject(with: errorResponseData, options: .mutableContainers) as? [String: Any] {
                    print("Server return response with error: \(errorResponseJson)")
                    throw NetworkErrors.DetailedErrorResponse
                }

            } catch {
                print("Catched Error: \(error)")
                errorHandle(error)
            }
        }.resume()

    }
    
    static private func percentEscapeString(_ string: String) -> String {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: "-._* ")

        return string
         .addingPercentEncoding(withAllowedCharacters: characterSet)!
         .replacingOccurrences(of: " ", with: "+")
         .replacingOccurrences(of: " ", with: "+", options: [], range: nil)
    }
     
    static private func  encodeParameters(parameters: [String : String]) -> Data? {
       
        let parameterArray = parameters.map { (arg) -> String in
         let (key, value) = arg
            return "\(key)=\(self.percentEscapeString(value))"
        }
       
        let httpBody = parameterArray.joined(separator: "&").data(using: String.Encoding.utf8)
        let httpBodyString = String(data: httpBody!, encoding: .utf8) ?? ""
        print("httpBodyString: \(httpBodyString)")
        return httpBody
     }
    
    
//    static func workingfetch <T: Codable> (url : String, parameters : [String: Any], handle : @escaping (_ newData: T) -> ()?, errorHandle : @escaping (_ newError: Error) -> ()?){
//        print(url)
//
//        guard let validUrl = URL(string: url)
//            else {
//                print("Invalid Url: \(url)")
//                return }
//
//        var request = URLRequest(url: validUrl)
//        request.httpMethod = "POST"
//
//        do {
//            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
//        } catch let error {
//            print("Parameters JSONSerialization Error: \(error.localizedDescription)")
//            errorHandle(error)
//            return
//        }
//
//        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
//
//        request.timeoutInterval = 20
//
//        URLSession.shared.dataTask(with: request as URLRequest){(data, response, error) in
//            do {
//                guard let errorResponseData = data else {
//                    print("There in no response from Server")
//                    throw NetworkErrors.NoResponse
//                }
//                if let errorResponseJson = try JSONSerialization.jsonObject(with: errorResponseData, options: .mutableContainers) as? [String: Any] {
//                    print("Server return response with error: \(errorResponseJson)")
//                    throw NetworkErrors.DetailedErrorResponse
//                }
//
//                if let validJson = data
//                {
//                    let decodedData = try JSONDecoder().decode(T.self, from: validJson)
//                    DispatchQueue.main.async {
//                        handle(decodedData)
//                        }
//                } else {
//                    if let jsonData = data {
//                        print("Invalid Json Data: \(jsonData)")
//                        throw NetworkErrors.InvalidResponse
//                    }
//                    if let validError = error {
//                        print("Error Data: \(validError)")
//                        throw NetworkErrors.ErrorResponse
//                    }
//                }
//
//            } catch {
//                print("Catched Error: \(error)")
//                errorHandle(error)
//            }
//        }.resume()
//
//    }
    
//    func fetch <T, E> (data : T, error: E, handle : () -> (),  errorHandle : () -> ()){
//    }
    
    
//    static func fetch <T: Codable> (url : String, parameters : [String: Any], observableData : inout T, observableError: inout Error){
//        print(url)
//
//
////        func handle(newData : T) {
////            observableData = newData
////        }
////        func handleError(newError : Error) {
////            observableError = newError
////        }
////
////        var defaultHandle : (T) -> () = handle
////        var defaultHandleError : (Error) -> () = handleError
//
//        guard let validUrl = URL(string: url)
//            else {
//                print("Invalid Url: \(url)")
//                return }
//
//        var request = URLRequest(url: validUrl)
//        request.httpMethod = "POST"
//
//        do {
//            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
//        } catch let error {
//            print("Parameters JSONSerialization Error: \(error.localizedDescription)")
//            observableError = error
//            return
//        }
//
//        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
//
//        request.timeoutInterval = 20
//
//        URLSession.shared.dataTask(with: request as URLRequest){(data, response, error) in
//            do {
//                guard let errorResponseData = data else {
//                    print("There in no response from Server")
//                    throw NetworkErrors.NoResponse
//                }
//                if let errorResponseJson = try JSONSerialization.jsonObject(with: errorResponseData, options: .mutableContainers) as? [String: Any] {
//                    print("Server return response with error: \(errorResponseJson)")
//                    throw NetworkErrors.DetailedErrorResponse
//                }
//
//                if let validJson = data
//                {
//                    let decodedData = try JSONDecoder().decode(T.self, from: validJson)
//                    DispatchQueue.main.async {
//                        observableData = decodedData
//                        }
//                } else {
//                    if let jsonData = data {
//                        print("Invalid Json Data: \(jsonData)")
//                        throw NetworkErrors.InvalidResponse
//                    }
//                    if let validError = error {
//                        print("Error Data: \(validError)")
//                        throw NetworkErrors.ErrorResponse
//                    }
//                }
//
//            } catch {
//                print("Catched Error: \(error)")
//                observableError = error
//            }
//        }.resume()
//
//    }
    
//    static func fetch <T: Codable> (url : String, parameters : [String: Any], observableData : inout T, observableError: inout Error, handle : @escaping (T) -> ()?, errorHandle : @escaping (Error) -> ()?){
//        print(url)
//        func defaultHandle(newData : T) {
//            observableData = newData
//        }
//        func defaultHandleError (newError : Error) {
//            observableError = newError
//        }
//
//        guard let validHandle = handle
//            else{
//                validHandle = defaultHandle
//            }
//
//        guard let validUrl = URL(string: url)
//            else {
//                print("Invalid Url: \(url)")
//                return }
//
//        var request = URLRequest(url: validUrl)
//        request.httpMethod = "POST"
//
//        do {
//            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
//        } catch let error {
//            print("Parameters JSONSerialization Error: \(error.localizedDescription)")
//            errorHandle(error)
//            return
//        }
//
//        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
//
//        request.timeoutInterval = 20
//
//        URLSession.shared.dataTask(with: request as URLRequest){(data, response, error) in
//            do {
//                guard let errorResponseData = data else {
//                    print("There in no response from Server")
//                    throw NetworkErrors.NoResponse
//                }
//                if let errorResponseJson = try JSONSerialization.jsonObject(with: errorResponseData, options: .mutableContainers) as? [String: Any] {
//                    print("Server return response with error: \(errorResponseJson)")
//                    throw NetworkErrors.DetailedErrorResponse
//                }
//
//                if let validJson = data
//                {
//                    let decodedData = try JSONDecoder().decode(T.self, from: validJson)
//                    DispatchQueue.main.async {
//                        handle(decodedData)
//                        }
//                } else {
//                    if let jsonData = data {
//                        print("Invalid Json Data: \(jsonData)")
//                        throw NetworkErrors.InvalidResponse
//                    }
//                    if let validError = error {
//                        print("Error Data: \(validError)")
//                        throw NetworkErrors.ErrorResponse
//                    }
//                }
//
//            } catch {
//                print("Catched Error: \(error)")
//                errorHandle(error)
//            }
//        }.resume()
//
//    }
    
//    static func defaultHandleError <T: Codable>(observaleError : inout T, newError : T) {
//        observaleError = newError
//    }
    
    
}

enum NetworkErrors: Error{
    case NoResponse
    case ErrorResponse
    case DetailedErrorResponse
    case InvalidResponse
    case OtherNetworkError
}
