//
//  CustomColors.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/24/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

extension Color {
    init(hex: String) {
        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int: UInt64 = 0
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }

        self.init(
            .sRGB,
            red: Double(r) / 255,
            green: Double(g) / 255,
            blue:  Double(b) / 255,
            opacity: Double(a) / 255
        )
    }
    public static let primary = Color(hex: "#455e94")
    public static let secondary = Color(hex: "#095608")
    public static let colorPrimary = Color(hex: "#455e94")
    public static let colorPrimaryDark = Color(hex: "#1564BF")
    public static let colorPrimaryLight = Color(hex: "#5AB1FF")
    public static let colorSecondary = Color(hex: "#095608")
    public static let colorSecondaryLight = Color(hex: "#FCD89F")
    public static let colorAccent = Color.secondary
    public static let colorSelectedItem = Color(hex: "#142A34")
    public static let colorBackground = Color(hex: "#fffcfb")
    public static let colorBarBackground = Color(hex: "#fef3f1")
    public static let colorListBold = Color(hex: "#DBDBDB")
    public static let colorListLight = Color(hex: "#FCFCFC")
    public static let colorListRowTitle = Color(hex: "#FCFCFC")
    public static let colorText = Color(hex: "#000")
    public static let colorBorder = Color(hex: "#BBB")
    public static let colorDarkGray = Color(hex: "#444")
    public static let colorBottomMenu = Color(hex: "#f9f9f9")
    public static let colorListViewBackground = Color.colorBackground
    public static let colorTabHeader = Color(hex: "#FFF")
    public static let colorTabSeledted = Color.colorPrimary
    public static let colorTabUnSeledted = Color(hex: "#828282")
    public static let colorTabSeledtedText = Color(hex: "#009688")
    public static let colorTabIndicator = Color(hex: "#009688")
    public static let colorTabText = Color(hex: "#000")
    public static let colorGreen = Color(hex: "#3ACE3A")
    public static let colorChatBackground = Color(hex: "#e1ffc7")
    public static let colorCalendarActiveMonthBackground = Color(hex: "#ffffff")
    public static let colorCalendarBackground = Color(hex: "#ffffff")
    public static let colorCalendarDivider = Color(hex: "#f5f5f5")
    public static let colorCalendarInactiveMonthBackground = Color(hex: "#ffffff")
    public static let colorCalendarSelectedDayBackground = Color.colorPrimary
    public static let colorCalendarHighlightedDayBackground = Color.colorPrimary
    public static let colorCalendarSelectedRangeBackground = Color.colorPrimary
    public static let colorCalendarUnavailableBackground = Color(hex: "#ef5350")
    public static let colorCalendarRangeMiddleUnavailableBackground = Color(hex: "#ef5350")
    public static let colorCalendarRangeMiddleDeactivatedBackground = Color(hex: "#80000000")
    public static let colorCalendarInactiveText = Color(hex: "#ffffff")
    public static let colorCalendarActiveText = Color(hex: "#212121")
    public static let colorCalendarSelectedText = Color(hex: "#ffffff")
    public static let colorCalendarUnselectedText = Color(hex: "#7ffefeff")
    public static let colorCalendarHighlightedText = Color(hex: "#f5f5f5")
    public static let colorDateTimeRangePickerStateDefault : Color = Color.black
    public static let colorDateTimeRangePickerStateNonSelectable = Color.white
    public static let colorDateTimeRangePickerStateToday = Color(hex: "#ffcf40")
    public static let colorDateTimeRangePickerHeaderText = Color.black
    public static let colorDateTimeRangePickerTitleText = Color.black
    public static let colorDateTimeRangePickerRangeColorActiveText = Color.white
    public static let colorDateTimeRangePickerRangeColorInactiveText = Color(hex: "#B9B9B9")
    public static let colorIosCloseButtonInsideXColor = Color(hex: "#7e7e83")
    public static let colorIosCloseButtonOutsideXColor = Color(hex: "#e2e2e3")
    public static let colorVeryLightGray = Color(hex: "#F8F8F8")
    public static let colorLightGray = Color(hex: "#EEEEEE")
}

extension UIColor{
    convenience init?(hex: String){
        let r, g, b, a: CGFloat

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            var hexColor = String(hex[start...])
            
            if hexColor.count == 6 {
                hexColor = hexColor + "FF"
            }else if hexColor.count == 3 {
                hexColor.insert("0", at: hexColor.startIndex)
                hexColor.insert("0", at: hexColor.index(hexColor.startIndex, offsetBy: 2))
                hexColor.insert("0", at: hexColor.index(hexColor.startIndex, offsetBy: 4))
                hexColor.insert(contentsOf: "FF", at: hexColor.endIndex)
            }

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }

        return nil
    }
    
    public static let primary = UIColor(hex: "#455e94")
    public static let secondary = UIColor(hex: "#095608")
    public static let colorPrimary = UIColor(hex: "#455e94")
    public static let colorPrimaryDark = UIColor(hex: "#1564BF")
    public static let colorPrimaryLight = UIColor(hex: "#5AB1FF")
    public static let colorSecondary = UIColor(hex: "#095608")
    public static let colorSecondaryLight = UIColor(hex: "#FCD89F")
    public static let colorAccent = UIColor.secondary
    public static let colorSelectedItem = UIColor(hex: "#142A34")
    public static let colorBackground = UIColor(hex: "#fffcfb")
    public static let colorBarBackground = UIColor(hex: "#fef3f1")
    public static let colorListBold = UIColor(hex: "#DBDBDB")
    public static let colorListLight = UIColor(hex: "#FCFCFC")
    public static let colorListRowTitle = UIColor(hex: "#FCFCFC")
    public static let colorText = UIColor(hex: "#000")
    public static let colorBorder = UIColor(hex: "#BBB")
    public static let colorDarkGray = UIColor(hex: "#444")
    public static let colorBottomMenu = UIColor(hex: "#f9f9f9")
    public static let colorListViewBackground = UIColor.colorBackground
    public static let colorTabHeader = UIColor(hex: "#FFF")
    public static let colorTabSeledted = UIColor.colorPrimary
    public static let colorTabUnSeledted = UIColor(hex: "#828282")
    public static let colorTabSeledtedText = UIColor(hex: "#009688")
    public static let colorTabIndicator = UIColor(hex: "#009688")
    public static let colorTabText = UIColor(hex: "#000")
    public static let colorGreen = UIColor(hex: "#3ACE3A")
    public static let colorWhite = UIColor(hex: "#ffffff")
    public static let colorChatBackground = UIColor(hex: "#e1ffc7")
    public static let colorCalendarActiveMonthBackground = UIColor(hex: "#ffffff")
    public static let colorCalendarBackground = UIColor(hex: "#ffffff")
    public static let colorCalendarDivider = UIColor(hex: "#f5f5f5")
    public static let colorCalendarInactiveMonthBackground = UIColor(hex: "#ffffff")
    public static let colorCalendarSelectedDayBackground = UIColor.colorPrimary
    public static let colorCalendarHighlightedDayBackground = UIColor.colorPrimary
    public static let colorCalendarSelectedRangeBackground = UIColor.colorPrimary
    public static let colorCalendarUnavailableBackground = UIColor(hex: "#ef5350")
    public static let colorCalendarRangeMiddleUnavailableBackground = UIColor(hex: "#ef5350")
    public static let colorCalendarRangeMiddleDeactivatedBackground = UIColor(hex: "#80000000")
    public static let colorCalendarInactiveText = UIColor(hex: "#ffffff")
    public static let colorCalendarActiveText = UIColor(hex: "#212121")
    public static let colorCalendarSelectedText = UIColor(hex: "#ffffff")
    public static let colorCalendarUnselectedText = UIColor(hex: "#7ffefeff")
    public static let colorCalendarHighlightedText = UIColor(hex: "#f5f5f5")
    public static let colorDateTimeRangePickerStateDefault : UIColor = UIColor.black
    public static let colorDateTimeRangePickerStateNonSelectable = UIColor.white
    public static let colorDateTimeRangePickerStateToday = UIColor(hex: "#ffcf40")
    public static let colorDateTimeRangePickerHeaderText = UIColor.black
    public static let colorDateTimeRangePickerTitleText = UIColor.black
    public static let colorDateTimeRangePickerRangeColorActiveText = UIColor.white
    public static let colorDateTimeRangePickerRangeColorInactiveText = UIColor(hex: "#B9B9B9")
    public static let colorIosCloseButtonInsideXColor = UIColor(hex: "#7e7e83")
    public static let colorIosCloseButtonOutsideXColor = UIColor(hex: "#e2e2e3")
}

struct ColorCustom: View {
    var body: some View {
//        Text("colorPrimary")
//            .background(Color.colorPrimary)
        ScrollView{
            Group{
            Text("colorPrimary").background(Color.colorPrimary);
            Text("colorPrimaryDark").background(Color.colorPrimaryDark);
            Text("colorPrimaryLight").background(Color.colorPrimaryLight);
            Text("colorSecondary").background(Color.colorSecondary);
            Text("colorSecondaryLight").background(Color.colorSecondaryLight);
            Text("colorAccent").background(Color.colorAccent);
            Text("colorSelectedItem").background(Color.colorSelectedItem);
            Text("colorBackground").background(Color.colorBackground);
            Text("colorListBold").background(Color.colorListBold);
            Text("colorListLight").background(Color.colorListLight);
            }
            Group{
            Text("colorText").background(Color.colorText);
            Text("colorBorder").background(Color.colorBorder);
            Text("colorDarkGray").background(Color.colorDarkGray);
            Text("colorBottomMenu").background(Color.colorBottomMenu);
            Text("colorListViewBackground").background(Color.colorListViewBackground);
            Text("colorTabHeader").background(Color.colorTabHeader);
            Text("colorTabSeledtedText").background(Color.colorTabSeledtedText);
            Text("colorTabIndicator").background(Color.colorTabIndicator);
            Text("colorTabText").background(Color.colorTabText);
            Text("colorGreen").background(Color.colorGreen);
            }
            Group{
            Text("colorChatBackground").background(Color.colorChatBackground);
            Text("colorCalendarActiveMonthBackground").background(Color.colorCalendarActiveMonthBackground);
            Text("colorCalendarBackground").background(Color.colorCalendarBackground);
            Text("colorCalendarDivider").background(Color.colorCalendarDivider);
            Text("colorCalendarInactiveMonthBackground").background(Color.colorCalendarInactiveMonthBackground);
            Text("colorCalendarSelectedDayBackground").background(Color.colorCalendarSelectedDayBackground);
            Text("colorCalendarHighlightedDayBackground").background(Color.colorCalendarHighlightedDayBackground);
            Text("colorCalendarSelectedRangeBackground").background(Color.colorCalendarSelectedRangeBackground);
            Text("colorCalendarUnavailableBackground").background(Color.colorCalendarUnavailableBackground);
            Text("colorCalendarRangeMiddleUnavailableBackground").background(Color.colorCalendarRangeMiddleUnavailableBackground);
            }
            Group{
            Text("colorCalendarRangeMiddleDeactivatedBackground").background(Color.colorCalendarRangeMiddleDeactivatedBackground);
            Text("colorCalendarInactiveText").background(Color.colorCalendarInactiveText);
            Text("colorCalendarActiveText").background(Color.colorCalendarActiveText);
            Text("colorCalendarSelectedText").background(Color.colorCalendarSelectedText);
            Text("colorCalendarUnselectedText").background(Color.colorCalendarUnselectedText);
            Text("colorCalendarHighlightedText").background(Color.colorCalendarHighlightedText);
            Text("colorDateTimeRangePickerStateDefault").background(Color.colorDateTimeRangePickerStateDefault);
            Text("colorDateTimeRangePickerStateNonSelectable").background(Color.colorDateTimeRangePickerStateNonSelectable);
            Text("colorDateTimeRangePickerStateToday").background(Color.colorDateTimeRangePickerStateToday);
            Text("colorDateTimeRangePickerHeaderText").background(Color.colorDateTimeRangePickerHeaderText);
            }
            Group{
            Text("colorDateTimeRangePickerTitleText").background(Color.colorDateTimeRangePickerTitleText);
            Text("colorDateTimeRangePickerRangeColorActiveText").background(Color.colorDateTimeRangePickerRangeColorActiveText);
            Text("colorDateTimeRangePickerRangeColorInactiveText").background(Color.colorDateTimeRangePickerRangeColorInactiveText);
            }
        }
    }
}

struct ColorCustom_Previews: PreviewProvider {
    static var previews: some View {
        ColorCustom()
    }
}
