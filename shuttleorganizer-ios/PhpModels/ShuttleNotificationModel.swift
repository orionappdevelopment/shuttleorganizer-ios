//
//  ShuttleNotificationModel.swift
//  shuttleorganizer-ios
//
//  Created by macos on 4.07.2021.
//  Copyright © 2021 Orion. All rights reserved.
//

import Foundation
struct ShuttleNotificationModel : Equatable, Hashable, Codable {

    var notificationId : String
    var shuttleNotificationType : Int
    var transportationId : String
    var transportationName :String
    var senderPersonId : String
    var senderPersonName : String
    
    public init(shuttleNotificationType : Int = ShuttleNotificationTypeEnum.NONE.rawValue, transportationId : String = "", transportationName : String = "", senderPersonId : String = "", senderPersonName : String = "") {
        self.shuttleNotificationType = shuttleNotificationType
        self.transportationId = transportationId
        self.transportationName = transportationName
        self.senderPersonId = senderPersonId
        self.senderPersonName = senderPersonName
        self.notificationId = ShuttleNotificationModel.getRandomUniqueId()
    }
    
    public func getShuttleNotificationTypeEnum() -> ShuttleNotificationTypeEnum{
        return ShuttleNotificationTypeEnum(rawValue: self.shuttleNotificationType) ?? ShuttleNotificationTypeEnum.NONE
    }
    
    static func == (lhs: ShuttleNotificationModel, rhs: ShuttleNotificationModel) -> Bool {
        return lhs.notificationId == rhs.notificationId
    }
    
    static func getRandomUniqueId() -> String {
        let randomUniqId : UUID = UUID()
        //f46709a2-f27c-11e8-a2f7-a088699d485b like self one //it is always 36 characters
        
        return randomUniqId.uuidString;
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(self.notificationId)
    }
}
