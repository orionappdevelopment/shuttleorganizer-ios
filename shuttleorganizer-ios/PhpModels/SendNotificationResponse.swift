//
//  SendNotificationResponse.swift
//  shuttleorganizer-ios
//
//  Created by macos on 18.05.2021.
//  Copyright © 2021 Orion. All rights reserved.
//

import Foundation

struct SendNotificationResponse : Equatable, Hashable, Codable {
//    enum PhpResult : String, Codable{
//        case ok = "ok"
//        case error = "error"
//    }
    struct NotificationResult : Equatable, Hashable, Codable {
        var error : String? = ""
        var message_id : String? = ""
    }
    
    var multicast_id : Int64 = 0
    var success : Int = 0
    var failure : Int = 0
    var results : [NotificationResult] = []
    var canonical_ids : Int = 0
    
    public init(multicast_id : Int64 = 0, success : Int = 0, failure : Int = 0, canonical_ids : Int = 0, results : [NotificationResult] = []) {
        self.multicast_id = multicast_id
        self.success = success
        self.failure = failure
        self.canonical_ids = canonical_ids
        self.results = results
    }
    
    static func == (lhs: SendNotificationResponse, rhs: SendNotificationResponse) -> Bool {
        return lhs.multicast_id == rhs.multicast_id
        
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(self.multicast_id)
    }
}
