//
//  UploadImageResponse.swift
//  shuttleorganizer-ios
//
//  Created by macos on 2/12/21.
//  Copyright © 2021 Orion. All rights reserved.
//

import Foundation

struct UploadImageResponse : Equatable, Hashable, Codable {
    enum PhpResult : String, Codable{
        case ok = "ok"
        case error = "error"
    }
    var result : PhpResult
    var url : String
    
    public init(result : PhpResult = PhpResult.ok, url : String = "") {
        self.result = result;
        self.url = url;
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.result == rhs.result && lhs.url == rhs.url
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(self.result)
        hasher.combine(self.url)
    }
}
