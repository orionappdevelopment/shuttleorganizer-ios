//
//  PersonNotification.swift
//  Landmarks
//
//  Created by macos on 5/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct PersonNotification : Equatable, Hashable, Codable {

    var personId : String

    var token : String
    
    var loggedIn : Bool

    var notification1 : Bool
    
    var notification2 : Bool
    
    var notification3 : Bool
    
    var notification4 : Bool
    
    var notification5 : Bool
    
    var notification6 : Bool
    
    var notification7 : Bool
    
    var notification8 : Bool
    
    var notification9 : Bool
    
    var notification10 : Bool


//    public PersonNotification(String token, boolean notification1, boolean notification2, boolean notification3, boolean notification4
//            , boolean notification5, boolean notification6, boolean notification7, boolean notification8, boolean notification9, boolean notification10) {
//        this(null, token, notification1, notification2, notification3, notification4, notification5,
//                notification6, notification7, notification8, notification9, notification10);
//    }
        
    public init(personId : String, token : String, loggedIn: Bool, notification1 : Bool, notification2 : Bool, notification3 : Bool, notification4 : Bool, notification5 : Bool, notification6 : Bool, notification7 : Bool, notification8 : Bool, notification9 : Bool, notification10  : Bool) {
        self.personId = personId;
        self.token = token;
        self.loggedIn = loggedIn
        self.notification1 = notification1;
        self.notification2 = notification2;
        self.notification3 = notification3;
        self.notification4 = notification4;
        self.notification5 = notification5;
        self.notification6 = notification6;
        self.notification7 = notification7;
        self.notification8 = notification8;
        self.notification9 = notification9;
        self.notification10 = notification10;
    }
    
    public init(personId : String, token : String) {
        self.personId = personId;
        self.token = token;
        self.loggedIn = false
        self.notification1 = false
        self.notification2 = false
        self.notification3 = false
        self.notification4 = false
        self.notification5 = false
        self.notification6 = false
        self.notification7 = false
        self.notification8 = false
        self.notification9 = false
        self.notification10 = false
    }
    
    public init(personId : String, token : String, loggedIn: Bool, notifications: [Bool])  {
        if notifications.count != 10 {
           preconditionFailure("notifications count:\(notifications.count) is not 10");
        }
        self.personId = personId;
        self.token = token;
        self.loggedIn = loggedIn;

        self.notification1 = notifications[0]
        self.notification2 = notifications[1]
        self.notification3 = notifications[2]
        self.notification4 = notifications[3]
        self.notification5 = notifications[4]
        self.notification6 = notifications[5]
        self.notification7 = notifications[6]
        self.notification8 = notifications[7]
        self.notification9 = notifications[8]
        self.notification10 = notifications[9]
    }
    
    public func getNotifications() -> [Bool] {
        return [self.notification1, self.notification2, self.notification3, self.notification4, self.notification5,
                self.notification6, self.notification7, self.notification8, self.notification9, self.notification10]
        
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.personId == rhs.personId &&
                lhs.token == rhs.token &&
                lhs.notification1 == rhs.notification1 &&
                lhs.notification2 == rhs.notification2 &&
                lhs.notification3 == rhs.notification3 &&
                lhs.notification4 == rhs.notification4 &&
                lhs.notification5 == rhs.notification5 &&
                lhs.notification6 == rhs.notification6 &&
                lhs.notification7 == rhs.notification7 &&
                lhs.notification8 == rhs.notification8 &&
                lhs.notification9 == rhs.notification9 &&
                lhs.notification10 == rhs.notification10;
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(self.personId)
        hasher.combine(self.token)
        hasher.combine(self.notification1)
        hasher.combine(self.notification2)
        hasher.combine(self.notification3)
        hasher.combine(self.notification1)
        hasher.combine(self.notification4)
        hasher.combine(self.notification5)
        hasher.combine(self.notification6)
        hasher.combine(self.notification7)
        hasher.combine(self.notification8)
        hasher.combine(self.notification9)
        hasher.combine(self.notification10)
    }
    
    
//    @Override
//    public boolean equals(Object that) {
//        boolean isEqual = EqualsBuilder.reflectionEquals(this, that);
//        return isEqual;
//    }
//
//    @Override
//    public int hashCode() {
//        int hashCode = HashCodeBuilder.reflectionHashCode(this, "personId", "token", "notification1", "notification2", "notification3", "notification4", "notification5"
//                , "notification6", "notification7", "notification8", "notification9", "notification10");
//        return hashCode;
//    }
//
//
//    @Override
//    public String toString() {
//        String str = ReflectionToStringBuilder.toString(this);
//        return str;
//    }

}
