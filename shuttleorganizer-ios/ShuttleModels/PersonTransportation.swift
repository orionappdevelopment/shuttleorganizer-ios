//
//  PersonTransportation.swift
//  Landmarks
//
//  Created by macos on 5/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation


struct PersonTransportation : Equatable, Hashable, Codable {
    var personId : String
    var transportationId : String
    var personRoleType :Int
    var latitude : Float64?
    var longitude : Float64?
    var dailyTimeDiffInSecs : Int
 
     
    init(personId : String, transportationId :String, personRoleType : Int = RoleTypeEnum.PASSENGER.rawValue) {
        self.personId = personId;
        self.transportationId = transportationId;
        self.personRoleType = personRoleType;
        self.latitude = nil;
        self.longitude = nil;
        self.dailyTimeDiffInSecs = 0;
    }

    init(personId : String, transportationId :String, personRoleType : Int, latitude : Float64?, longitude : Float64?, dailyTimeDiffInSecs : Int ) {
        self.personId = personId;
        self.transportationId = transportationId;
        self.personRoleType = personRoleType;
        self.latitude = latitude;
        self.longitude = longitude;
        self.dailyTimeDiffInSecs = dailyTimeDiffInSecs;
    }
    
//    init(personId : String, transportationId :String, personRoleType : Int, latitude : Float64, longitude : Float64, dailyTimeDiffInSecs : Int ) {
//        self.personId = personId;
//        self.transportationId = transportationId;
//        self.personRoleType = personRoleType;
//        self.latitude = latitude;
//        self.longitude = longitude;
//        self.dailyTimeDiffInSecs = dailyTimeDiffInSecs;
//    }
    
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.personId == rhs.personId &&
                lhs.transportationId == rhs.transportationId &&
                lhs.personRoleType == rhs.personRoleType
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(self.personId)
        hasher.combine(self.transportationId)
        hasher.combine(self.personRoleType)
    }
    
//    @Override
//    public boolean equals(Object that) {
//        boolean isEqual = EqualsBuilder.reflectionEquals(this, that,  "latitude", "longitude", "dailyTimeDiffInSecs");
//        return isEqual;
//    }
//
//    @Override
//    public int hashCode() {
//        int hashCode = HashCodeBuilder.reflectionHashCode(this, "personId", "transportationId", "personRoleType");
//        return hashCode;
//    }
//
//    @Override
//    public String toString() {
//        String str = ReflectionToStringBuilder.toString(this);
//        return str;
//    }
}


