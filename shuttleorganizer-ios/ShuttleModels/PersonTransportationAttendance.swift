//
//  PersonTransportationAttendance.swift
//  Landmarks
//
//  Created by macos on 5/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct PersonTransportationAttendance : Equatable, Hashable, Codable {

    //coming from personTransportationview
    var personId : String
    var transportationId : String
    var personName : String
    var personRoleType :Int
    var personLatitude : Float64?
    var personLongitude : Float64?
    var personDailyTimeDiffInSecs : Int
    
    //coming from transportation
    var transportationStartTime : LTime
    
    //coming from attendance
    var personAttended : Bool
    var personStartDate : LDate?
    var personEndDate : LDate?
    var shuttleNextValidDate : LDate?
    var personNextValidFullDate :FDate?
    var profilePictureURL : String?
    var phone : String?
    
    public init(personId : String, transportationId : String, transportationStartTime : LTime, personName : String, personRoleType : Int, personLatitude : Float64, personLongitude : Float64, personDailyTimeDiffInSecs : Int, personAttended : Bool, personStartDate : LDate, personEndDate : LDate, profilePictureURL : String?, phone : String?) {
        self.personId = personId;
        self.transportationId = transportationId;
        self.transportationStartTime =  transportationStartTime;
        self.personName = personName;
        self.personRoleType = personRoleType;
        
        self.personLatitude = personLatitude;
        self.personLongitude = personLongitude;
        self.personDailyTimeDiffInSecs = personDailyTimeDiffInSecs;
        self.personAttended = personAttended;
        self.personStartDate = personStartDate;
        self.personEndDate = personEndDate;
        self.shuttleNextValidDate = nil
        self.personNextValidFullDate = nil
        self.profilePictureURL = profilePictureURL
        self.phone = phone
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.personId == rhs.personId &&
                lhs.transportationId == rhs.transportationId &&
                lhs.transportationStartTime == rhs.transportationStartTime &&
                lhs.personName == rhs.personName &&
                lhs.personRoleType == rhs.personRoleType &&
                lhs.personLatitude == rhs.personLatitude &&
                lhs.personLongitude == rhs.personLongitude &&
                lhs.personDailyTimeDiffInSecs == rhs.personDailyTimeDiffInSecs &&
                lhs.personAttended == rhs.personAttended &&
                lhs.personStartDate == rhs.personStartDate &&
                lhs.personEndDate == rhs.personEndDate &&
                lhs.shuttleNextValidDate == rhs.shuttleNextValidDate &&
                lhs.personNextValidFullDate == rhs.personNextValidFullDate &&
                lhs.profilePictureURL == rhs.profilePictureURL &&
                lhs.phone == rhs.phone
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(self.personId)
        hasher.combine(self.transportationId)
        hasher.combine(self.transportationStartTime)
        hasher.combine(self.personName)
        hasher.combine(self.personRoleType)
        hasher.combine(self.personLatitude)
        hasher.combine(self.personLongitude)
        hasher.combine(self.personDailyTimeDiffInSecs)
        hasher.combine(self.personAttended)
        hasher.combine(self.personStartDate)
        hasher.combine(self.personEndDate)
        hasher.combine(self.shuttleNextValidDate)
        hasher.combine(self.personNextValidFullDate)
        hasher.combine(self.profilePictureURL)
        hasher.combine(self.phone)
    }

    /*
    @Override
    public boolean equals(Object that) {
        boolean isEqual = EqualsBuilder.reflectionEquals(this, that);
        return isEqual;
    }
    
    @Override
    public int hashCode() {
        int hashCode;
        
        if(this.personStartDate == null  || this.personEndDate == null ) {
            hashCode = HashCodeBuilder.reflectionHashCode(this, "personId", "transportationId", "transportationStartTime", "personName", "personRoleType", "personLatitude", "personLongitude", "personDailyTimeDiffInSecs"
                    ,"personAttended");
        }else {
            hashCode = HashCodeBuilder.reflectionHashCode(this, "personId", "transportationId", "transportationStartTime","personName", "personRoleType", "personLatitude", "personLongitude", "personDailyTimeDiffInSecs"
                    ,"personAttended", "personStartDate", "personEndDate");
        }
        return hashCode;
    }

    @Override
    public String toString() {
        String str = ReflectionToStringBuilder.toString(this);
        return str;
    }
*/
}
