//
//  PersonTransportationView.swift
//  Landmarks
//
//  Created by macos on 5/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation


struct PersonTransportationView : Equatable, Hashable, Codable{
    var personId : String
    var transportationId : String
    var personRoleType : Int
    var latitude : Float64
    var longitude : Float64
    var transportationStartTime : LTime
    var dailyTimeDiffInSecs : Int
    var personName : String
    var profilePictureURL : String?
 
     
    public init() {
        self.personId = ""
        self.transportationId = ""
        self.personRoleType = 0
        self.latitude = 0
        self.longitude = 0
        self.transportationStartTime = LTime(hour: 0, minute: 0, second: 0)
        self.dailyTimeDiffInSecs = 0
        self.personName = ""
        self.profilePictureURL = ""
    }
    
//    public PersonTransportationView(String personId, String transportationId, Integer personRoleType) {
//        this(personId, transportationId, personRoleType);
//    }
    
    public init(personId : String, transportationId : String, personRoleType : Int, latitude : Float64, longitude : Float64, transportationStartTime : LTime, dailyTimeDiffInSecs : Int, personName : String, profilePictureURL : String?) {
        self.personId = personId;
        self.transportationId = transportationId;
        self.personRoleType = personRoleType;
        self.latitude = latitude;
        self.longitude = longitude;
        self.transportationStartTime = transportationStartTime
        self.dailyTimeDiffInSecs = dailyTimeDiffInSecs;
        self.personName = personName;
        self.profilePictureURL = profilePictureURL
    }
    
    public init(personTransportationView : PersonTransportationView) {
        self.personId = personTransportationView.personId;
        self.transportationId = personTransportationView.transportationId;
        self.personRoleType = personTransportationView.personRoleType;
        self.latitude = personTransportationView.latitude;
        self.longitude = personTransportationView.longitude;
        self.transportationStartTime = personTransportationView.transportationStartTime
        self.dailyTimeDiffInSecs = personTransportationView.dailyTimeDiffInSecs;
        self.personName = personTransportationView.personName;
        self.profilePictureURL = personTransportationView.profilePictureURL
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.personId == rhs.personId &&
                lhs.transportationId == rhs.transportationId &&
                lhs.personRoleType == rhs.personRoleType &&
                lhs.latitude == rhs.latitude &&
                lhs.longitude == rhs.longitude &&
                lhs.transportationStartTime == rhs.transportationStartTime &&
                lhs.dailyTimeDiffInSecs == rhs.dailyTimeDiffInSecs &&
                lhs.personName == rhs.personName &&
                lhs.profilePictureURL == rhs.profilePictureURL
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(self.personId)
        hasher.combine(self.transportationId)
        hasher.combine(self.personRoleType)
        hasher.combine(self.latitude)
        hasher.combine(self.longitude)
        hasher.combine(self.transportationStartTime)
        hasher.combine(self.dailyTimeDiffInSecs)
        hasher.combine(self.personName)
        hasher.combine(self.profilePictureURL)
    }
    
    /*
    @Override
    public boolean equals(Object that) {
        boolean isEqual = EqualsBuilder.reflectionEquals(this, that);
        return isEqual;
    }

    @Override
    public int hashCode() {
        int hashCode = HashCodeBuilder.reflectionHashCode(this, "personId", "transportationId", "personRoleType", "latitude", "longitude", "personName");
        return hashCode;
    }

    @Override
    public String toString() {
        String str = ReflectionToStringBuilder.toString(this);
        return str;
    }
    */
}


