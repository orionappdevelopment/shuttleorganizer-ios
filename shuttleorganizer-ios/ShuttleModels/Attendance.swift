//
//  Attendance.swift
//  Landmarks
//
//  Created by macos on 5/6/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct Attendance : Equatable, Hashable, Codable, Identifiable {
    
    var id : Int64?
    
    var personId: String
    
    var transportationId: String
    
    var attended: Bool
    
    var startDate: LDate
    
    var endDate: LDate
    
    
    public init() {
        self.id = nil
        self.personId = "-1"
        self.transportationId = "-1"
        self.attended = false;
        self.startDate = LDate()
        self.endDate = LDate()
    }
    
    init(personId: String, transportationId: String, attended:Bool, startDate: LDate, endDate: LDate){
        self.id = nil
        self.personId = personId
        self.transportationId = transportationId
        self.attended = attended
        self.startDate = startDate
        self.endDate = endDate
    }
    
    init(id: Int64, personId: String, transportationId: String, attended:Bool, startDate: LDate, endDate: LDate){
        self.id = id
        self.personId = personId
        self.transportationId = transportationId
        self.attended = attended
        self.startDate = startDate
        self.endDate = endDate
        
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id && lhs.personId == rhs.personId && lhs.transportationId == rhs.transportationId
            && lhs.attended == rhs.attended && lhs.startDate == rhs.startDate && lhs.endDate == rhs.endDate;
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(self.id)
    }
    
  /*
    @Override
    public boolean equals(Object that) {
        boolean isEqual = EqualsBuilder.reflectionEquals(this, that);
        return isEqual;
    }

    @Override
    public int hashCode() {
        int hashCode = HashCodeBuilder.reflectionHashCode(this, "personId", "transportationId", "attended", "startDate", "endDate");
        return hashCode;
    }

    @Override
    public String toString() {
        String str = ReflectionToStringBuilder.toString(this);
        return str;
    }
 */
    
    struct DeletedAttance: Equatable, Hashable, Codable, Identifiable {
        var id : Int64
        var personId: String?
        var transportationId: String?
        var attended: Bool?
        var startDate: LDate?
        var endDate: LDate?
    }
    
}

extension Attendance : Comparable{
    static func < (lhs: Attendance, rhs: Attendance) -> Bool {
        
        if lhs.startDate < rhs.startDate {
            return true
        }else if lhs.startDate > rhs.startDate {
            return false
        }else if lhs.endDate < rhs.endDate{
            return true
        }else if lhs.endDate > rhs.endDate{
            return false
        }
       
        return false;
    }
    
}

