//
//  FDate.swift
//  Landmarks
//
//  Created by macos on 5/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct FDate : Comparable, Equatable, Hashable, Codable{

    /**
     *
     */
    private static let serialVersionUID : Int64 = 0000017;
    /**
     * The year.
     */
    let year : Int;
    /**
     * The month-of-year.
     */
    let month : Int;
    /**
     * The day-of-month.
     */
    let day : Int;
    
    /**
     * The day-of-hour.
     */
    let hour : Int;
    
    /**
     * The day-of-minute.
     */
    let minute : Int;
    
    /**
     * The day-of-second.
     */
    let second : Int;
    
    
    
    public static func isLeapYear(prolepticYear: Int) -> Bool{
        return ((prolepticYear & 3) == 0) && ((prolepticYear % 100) != 0 || (prolepticYear % 400) == 0)
    }

//    public FDate(){
//        this.year = 2001;
//        this.month = 11;
//        this.day = 11;
//        this.hour = 1;
//        this.minute = 22;
//        this.second = 30;
//    }
    
    public init(year: Int = 2001, month: Int = 11, dayOfMonth: Int = 11, hour : Int = 1, minute: Int = 1, second: Int = 1) {
        if (dayOfMonth > 28) {
            var dom :Int = 31;
            switch month {
                case 2:
                    dom = (FDate.isLeapYear(prolepticYear: year) ? 29 : 28)
                case 4, 6, 9, 11:
                    dom = 30
                default:
                    break
            }
            if (dayOfMonth > dom) {
                if (dayOfMonth == 29) {
                    preconditionFailure("Invalid date 'February 29' as '\(year)' is not a leap year")
                } else {
                    preconditionFailure("Invalid date for'\(month)'. month '\(dayOfMonth)'")
                }
            }
        }
        self.year = year;
        self.month = month;
        self.day = dayOfMonth;
        self.hour = hour;
        self.minute = minute;
        self.second = second;
    //        return new FDate(year, month, dayOfMonth);
    }

    public func compareTo(other : FDate) -> Int{
        var cmp = (year - other.year);
        if (cmp == 0) {
            cmp = (month - other.month);
            if (cmp == 0) {
                cmp = (day - other.day);
                if (cmp == 0) {
                    cmp = (hour - other.hour);
                    if (cmp == 0) {
                        cmp = (minute - other.minute);
                        if (cmp == 0) {
                            cmp = (second - other.second);
                        }
                    }
                }
            }
        }
        return cmp;
    }
    
    static func < (lhs: Self, rhs: Self) -> Bool {
        let cmp = lhs.compareTo(other: rhs)
        return cmp > 0
    }
    
    static func == (lhs: FDate, rhs: FDate) -> Bool {
        let cmp = lhs.compareTo(other: rhs)
        return cmp == 0
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(year)
        hasher.combine(month)
        hasher.combine(day)
        hasher.combine(hour)
        hasher.combine(minute)
        hasher.combine(second)
    }
    
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj instanceof FDate) {
//            return compareTo((FDate) obj) == 0;
//        }
//        return false;
//    }
//    
//    @Override
//    public int hashCode() {
//        int result = year;
//        result = 31* result + month;
//        result = 31* result + day;
//        result = 31* result + hour;
//        result = 31* result + minute;
//        result = 31* result + second;
//        return result;
//    }
//    
//    @Override
//    public String toString() {
//        int yearValue = year;
//        int monthValue = month;
//        int dayValue = day;
//        int hourValue = hour;
//        int minuteValue = minute;
//        int secondValue = second;
//        int absYear = Math.abs(yearValue);
//        StringBuilder buf = new StringBuilder(10);
//        if (absYear < 1000) {
//            if (yearValue < 0) {
//                buf.append(yearValue - 10000).deleteCharAt(1);
//            } else {
//                buf.append(yearValue + 10000).deleteCharAt(0);
//            }
//        } else {
//            if (yearValue > 9999) {
//                buf.append('+');
//            }
//            buf.append(yearValue);
//        }
//        return buf.append(monthValue < 10 ? "-0" : "-")
//            .append(monthValue)
//            .append(dayValue < 10 ? "-0" : "-")
//            .append(dayValue)
//            .append(hourValue < 10 ? "-0" : "-")
//            .append(hourValue)
//            .append(minuteValue < 10 ? "-0" : "-")
//            .append(minuteValue)
//            .append(secondValue < 10 ? "-0" : "-")
//            .append(secondValue).toString();
//            
//    }
//    
//    public final String toDateString() {
//        int yearValue = year;
//        int monthValue = month;
//        int dayValue = day;
//        int hourValue = hour;
//        int minuteValue = minute;
//        int secondValue = second;
//        int absYear = Math.abs(yearValue);
//        StringBuilder buf = new StringBuilder(10);
//        if (absYear < 1000) {
//            if (yearValue < 0) {
//                buf.append(yearValue - 10000).deleteCharAt(1);
//            } else {
//                buf.append(yearValue + 10000).deleteCharAt(0);
//            }
//        } else {
//            if (yearValue > 9999) {
//                buf.append('+');
//            }
//            buf.append(yearValue);
//        }
//        return buf.append(monthValue < 10 ? "-0" : "-")
//            .append(monthValue)
//            .append(dayValue < 10 ? "-0" : "-")
//            .append(dayValue)
//            .append(hourValue < 10 ? "-0" : "-")
//            .append(hourValue)
//            .append(minuteValue < 10 ? "-0" : "-")
//            .append(minuteValue)
//            .append(secondValue < 10 ? "-0" : "-")
//            .append(secondValue).toString();
//    }
}

