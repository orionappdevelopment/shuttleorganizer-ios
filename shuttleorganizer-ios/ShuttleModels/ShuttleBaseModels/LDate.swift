//
//  LDate.swift
//  Landmarks
//
//  Created by macos on 5/6/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct LDate : Comparable, Equatable, Hashable, Codable{

    private static let serialVersionUID: Int64 = 0000013;
    /**
     * The year.
     */
    let year: Int
    /**
     * The month-of-year.
     */
    let month: Int
    /**
     * The day-of-month.
     */
    let day: Int
    
    
    public static func isLeapYear(prolepticYear: Int) -> Bool{
        return ((prolepticYear & 3) == 0) && ((prolepticYear % 100) != 0 || (prolepticYear % 400) == 0)
    }

//     public static LDate of(int year, int month, int dayOfMonth) {
//        return create(year, month, dayOfMonth);
//    }
      
//    public init(){
//        self.year = 2001;
//        self.month = 11;
//        self.day = 11;
//    }
    
    public init(year: Int = 2001, month: Int = 11, dayOfMonth: Int = 11){
        if (dayOfMonth > 28) {
            var dom :Int = 31;
            switch month {
                case 2:
                    dom = (LDate.isLeapYear(prolepticYear: year) ? 29 : 28)
                case 4, 6, 9, 11:
                    dom = 30
                default:
                    break
            }
            if (dayOfMonth > dom) {
                if (dayOfMonth == 29) {
                    preconditionFailure("Invalid date 'February 29' as '\(year)' is not a leap year")
                } else {
                    preconditionFailure("Invalid date for'\(month)'. month '\(dayOfMonth)'")
                }
            }
        }
        self.year = year;
        self.month = month;
        self.day = dayOfMonth;
    //        return new LDate(year, month, dayOfMonth);
    }
    
    public func compareTo(other : LDate) -> Int {
        var cmp = (year - other.year);
        if (cmp == 0) {
            cmp = (month - other.month);
            if (cmp == 0) {
                cmp = (day - other.day);
            }
        }
        return cmp;
    }
    
    static func < (lhs: LDate, rhs: LDate) -> Bool {
        let cmp = lhs.compareTo(other: rhs)
        return cmp > 0
    }
    
    static func == (lhs: LDate, rhs: LDate) -> Bool {
        let cmp = lhs.compareTo(other: rhs)
        return cmp == 0
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(year)
        hasher.combine(month)
        hasher.combine(day)
    }
    
    func toDate() -> Date {
        // Specify date components
        var dateComponents = DateComponents()
        dateComponents.year = self.year
        dateComponents.month = self.month
        dateComponents.day = self.day
//        dateComponents.timeZone = TimeZone(abbreviation: "JST") // Japan Standard Time
        dateComponents.hour = 0
        dateComponents.minute = 0
        dateComponents.second = 0

        // Create date from components
        let userCalendar = Calendar.current // user calendar
        let someDateTime = userCalendar.date(from: dateComponents)!
        
        return someDateTime
    }
    
/*
    @Override
    public int compareTo(LDate otherDate) {
        int cmp = (year - otherDate.year);
        if (cmp == 0) {
            cmp = (month - otherDate.month);
            if (cmp == 0) {
                cmp = (day - otherDate.day);
            }
        }
        return cmp;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof LDate) {
            return compareTo((LDate) obj) == 0;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        int yearValue = year;
        int monthValue = month;
        int dayValue = day;
        return (yearValue & 0xFFFFF800) ^ ((yearValue << 11) + (monthValue << 6) + (dayValue));
    }
    
    @Override
    public String toString() {
        int yearValue = year;
        int monthValue = month;
        int dayValue = day;
        int absYear = Math.abs(yearValue);
        StringBuilder buf = new StringBuilder(10);
        if (absYear < 1000) {
            if (yearValue < 0) {
                buf.append(yearValue - 10000).deleteCharAt(1);
            } else {
                buf.append(yearValue + 10000).deleteCharAt(0);
            }
        } else {
            if (yearValue > 9999) {
                buf.append('+');
            }
            buf.append(yearValue);
        }
        return buf.append(monthValue < 10 ? "-0" : "-")
            .append(monthValue)
            .append(dayValue < 10 ? "-0" : "-")
            .append(dayValue)
            .toString();
    }
    
    public final String toDateString() {
        int yearValue = year;
        int monthValue = month;
        int dayValue = day;
        int absYear = Math.abs(yearValue);
        StringBuilder buf = new StringBuilder(10);
        if (absYear < 1000) {
            if (yearValue < 0) {
                buf.append(yearValue - 10000).deleteCharAt(1);
            } else {
                buf.append(yearValue + 10000).deleteCharAt(0);
            }
        } else {
            if (yearValue > 9999) {
                buf.append('+');
            }
            buf.append(yearValue);
        }
        return buf.append(monthValue < 10 ? "-0" : "-")
            .append(monthValue)
            .append(dayValue < 10 ? "-0" : "-")
            .append(dayValue)
            .toString();
    }
 */
}
