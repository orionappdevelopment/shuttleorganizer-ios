//
//  LTime.swift
//  Landmarks
//
//  Created by macos on 5/6/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct LTime: Comparable, Equatable, Hashable, Codable{
    
    /**
     * Serial id for Serializable
     */
    private static let serialVersionUID : Int64 = 17
    
    /**
     * Minutes per hour.
     */
    static let MINUTES_PER_HOUR : Int = 60
    /**
     * Seconds per minute.
     */
    static let SECONDS_PER_MINUTE : Int = 60
    /**
     * Seconds per hour.
     */
    static let SECONDS_PER_HOUR : Int = SECONDS_PER_MINUTE * MINUTES_PER_HOUR
    
     
    /**
     * The hour. the hour-of-day, from 0 to 23
     */
    let hour : Int
    /**
     * The minute. the minute-of-hour, from 0 to 59
     */
    let minute : Int
    /**
     * The second. the second-of-minute, from 0 to 59
     */
    let second :Int
    
//    public init(){
//        self.hour = 1;
//        self.minute = 1;
//        self.second = 1;
//    }
    
    public init(hour : Int = 1, minute: Int = 1, second: Int = 1) {
        if(hour > 24 || hour < 0){
            preconditionFailure("Hour should be in 0-23 self value:\(hour) is not ok")
        }
        if(minute > 59 && minute < 0){
            preconditionFailure("Minute should be in 0-59 self value:\(minute) is not ok")
        }
        if(second > 59 && second < 0){
            preconditionFailure("Second should be in 0-59 self value:\(second) is not ok")
        }
        // TODO Auto-generated constructor stub
        self.hour = hour;
        self.minute = minute;
        self.second = second;
    }
    
    public func compareTo(other: LTime) -> Int{
        var cmp = hour - other.hour
        
        if (cmp == 0) {
            cmp = minute - other.minute
            if (cmp == 0) {
                cmp = second - other.second
            }
        }
        return cmp;
    }
    
    public func isAfter(other: LTime) -> Bool{
        return compareTo(other: other) > 0;
    }

    public func isBefore(other: LTime) -> Bool{
        return compareTo(other: other) < 0;
    }
    
    public func toSecondOfDay() -> Int {
        var total = hour * LTime.SECONDS_PER_HOUR;
        total += minute * LTime.SECONDS_PER_MINUTE;
        total += second;
        return total;
    }
    
    static func < (lhs: LTime, rhs: LTime) -> Bool {
        let cmp = lhs.compareTo(other: rhs)
        return cmp > 0
    }
    
    static func == (lhs: LTime, rhs: LTime) -> Bool {
        let cmp = lhs.compareTo(other: rhs)
        return cmp == 0
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(hour)
        hasher.combine(minute)
        hasher.combine(second)
    }
    
    func toDate() -> Date {
        let date = Calendar.current.date(bySettingHour: self.hour, minute: self.minute, second: self.second, of: Date())!
        return date
    }
    
//    @Override
//    public Bool equals(Object obj) {
//        if (self == obj) {
//            return true;
//        }
//        if (obj instanceof LTime) {
//            LTime other = (LTime) obj;
//            return hour == other.hour && minute == other.minute &&
//                    second == other.second;
//        }
//        return false;
//    }
//
//    @Override
//    public int hashCode() {
//        int h = self.hour;
//        h = 31 * h + self.minute;
//        h = 31 * h + self.second;
//        return h;
//    }
    
//    @Override
//   public String toString() {
//       StringBuilder buf = new StringBuilder(18);
//       int hourValue = hour;
//       int minuteValue = minute;
//       int secondValue = second;
//       buf.append(hourValue < 10 ? "0" : "").append(hourValue)
//           .append(minuteValue < 10 ? ":0" : ":").append(minuteValue)
//           .append(secondValue < 10 ? ":0" : ":").append(secondValue);
//
//       return buf.toString();
//   }
//    public final String toTimeString() {
//       StringBuilder buf = new StringBuilder(18);
//       int hourValue = hour;
//       int minuteValue = minute;
//       int secondValue = second;
//       buf.append(hourValue < 10 ? "0" : "").append(hourValue)
//           .append(minuteValue < 10 ? ":0" : ":").append(minuteValue)
//           .append(secondValue < 10 ? ":0" : ":").append(secondValue);
//
//       return buf.toString();
//   }
    
}
