//
//  Transportation.swift
//  Landmarks
//
//  Created by macos on 5/9/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation


struct Transportation : Equatable, Hashable, Codable, Identifiable {
    
    var id : String?
    var name : String
    var latitude : Float64
    var longitude : Float64
    var transportationType : Int
    var days : Int
    var status : Int
    var creatorPersonId : String
    var startTime : LTime
    var timeZone : Int
    var plate : String
    
    public init(id : String? = nil, name : String = "", latitude : Float64 = 0, longitude : Float64 = 0,
                transportationType : Int = TransportationTypeEnum.DEPARTURE.rawValue, days : Int = DayEnum.weekDays,
                status : Int = TransportationStatusEnum.IN_THE_GARAGE.rawValue, creatorPersonId : String = "",
                startTime : LTime = LTime(), timeZone : Int = 3, plate : String = "") {
        self.id = id
        self.name = name
        self.latitude = latitude;
        self.longitude = longitude;
        self.transportationType = transportationType;
        self.days = days;
        self.status = status;
        self.creatorPersonId = creatorPersonId;
        self.startTime = startTime;
        self.timeZone = timeZone;
        self.plate = plate
    }
 
//    public init() {
//        self.id = nil
//        self.name = ""
//        self.latitude = 0;
//        self.longitude = 0;
//        self.transportationType = TransportationTypeEnum.DEPARTURE.rawValue;
//        self.days = DayEnum.weekDays;
//        self.status = TransportationStatusEnum.IN_THE_GARAGE.rawValue;
//        self.creatorPersonId = "";
//        self.startTime = LTime();
//        self.timeZone = 3;
//    }
    
    public init(name : String, latitude : Float64, longitude : Float64, transportationType : Int,
                          days : Int, status : Int, creatorPersonId : String, startTime : LTime, timeZone : Int, plate : String = "") {
        self.id = nil
        self.name = name
        self.latitude = latitude;
        self.longitude = longitude;
        self.transportationType = transportationType;
        self.days = days;
        self.status = status;
        self.creatorPersonId = creatorPersonId;
        self.startTime = startTime;
        self.timeZone = timeZone;
        self.plate = plate
    }

    public init(id : String, name : String, latitude : Float64, longitude : Float64, transportationType : Int,
                          days : Int, status : Int, creatorPersonId : String, startTime : LTime, timeZone : Int, plate : String = "") {
        self.id = id;
        self.name = name;
        self.latitude = latitude;
        self.longitude = longitude;
        self.transportationType = transportationType;
        self.days = days;
        self.status = status;
        self.creatorPersonId = creatorPersonId;
        self.startTime = startTime;
        self.timeZone = timeZone;
        self.plate = plate;
    }
    
    public init(optionalTransportation: OptionalTransportation) {
        self.id = optionalTransportation.id;
        self.name = optionalTransportation.name ?? "";
        self.latitude = optionalTransportation.latitude ?? 0;
        self.longitude = optionalTransportation.longitude ?? 0;
        self.transportationType = optionalTransportation.transportationType ?? TransportationTypeEnum.DEPARTURE.rawValue;
        self.days = optionalTransportation.days ?? 0;
        self.status = optionalTransportation.status ?? 0;
        self.creatorPersonId = optionalTransportation.creatorPersonId ?? "";
        self.startTime = optionalTransportation.startTime ?? LTime(hour: 0, minute: 0, second: 0)
        self.timeZone = optionalTransportation.timeZone ?? 0;
        self.plate = optionalTransportation.plate ?? "";
    }
    
    public init(transportation : Transportation) {
        self.id = transportation.id;
        self.name = transportation.name;
        self.latitude = transportation.latitude;
        self.longitude = transportation.longitude;
        self.transportationType = transportation.transportationType;
        self.days = transportation.days;
        self.status = transportation.status;
        self.creatorPersonId = transportation.creatorPersonId;
        self.startTime = transportation.startTime;
        self.timeZone = transportation.timeZone;
        self.plate = transportation.plate;
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(self.id)
        hasher.combine(self.name)
        hasher.combine(self.latitude)
        hasher.combine(self.longitude)
        hasher.combine(self.transportationType)
        hasher.combine(self.days)
        hasher.combine(self.status)
        hasher.combine(self.creatorPersonId)
        hasher.combine(self.startTime)
        hasher.combine(self.timeZone)
    }
    
    struct OptionalTransportation : Equatable, Hashable, Codable, Identifiable {
        var id : String?
        var name : String?
        var latitude : Float64?
        var longitude : Float64?
        var transportationType : Int?
        var days : Int?
        var status : Int?
        var creatorPersonId : String?
        var startTime : LTime?
        var timeZone : Int?
        var plate : String?
    }
    /*
    @Override
    public boolean equals(Object that) {
        boolean isEqual = EqualsBuilder.reflectionEquals(this, that, "name", "latitude", "longitude", "transportationType", "attendances",
                "days", "status", "creatorPersonId", "startTime", "timeZone");
        return isEqual;
    }

    @Override
    public int hashCode() {
        int hashCode = HashCodeBuilder.reflectionHashCode(this, "name", "latitude", "longitude", "transportationType", "attendances",
                "days", "status", "creatorPersonId", "startTime", "timeZone");
        return hashCode;
    }

    @Override
    public String toString() {
        String str = ReflectionToStringBuilder.toString(this);
        return str;
    }
    
    public String toStringForClient() {
        return this.name;
    }
    */
}

//TODO
//extension Transportation: Comparable {
//
//    static func < (lhs: Transportation, rhs: Transportation) -> Bool {
//          return lhs.id < rhs.id
//    }
//}
//
//public static Comparator<Transportation> TransportationComparator = Comparator<Transportation>() {
//private Map<Transportation, FDate> transportationFDateMap = HashMap<>();
//@Override
//public Int compare(transportation1 : Transportation, transportation2 : Transportation) {
//    if(transportation1 != null && transportation2 != null) {
//
//        var nextValidFullDate1 : FDate = transportationFDateMap.get(transportation1);
//        if(nextValidFullDate1 == null) {
//            nextValidFullDate1 = findNextValidFullDate(transportation1);
//            transportationFDateMap.put(transportation1, nextValidFullDate1);
//        }
//
//        var nextValidFullDate2 : FDate = transportationFDateMap.get(transportation2);
//        if(nextValidFullDate2 == null) {
//            nextValidFullDate2 = findNextValidFullDate(transportation2);
//            transportationFDateMap.put(transportation2, nextValidFullDate2);
//        }
//
//        return nextValidFullDate1.compareTo(nextValidFullDate2);
//    }
//    return 0;
//}};
