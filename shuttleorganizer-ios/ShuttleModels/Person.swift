//
//  Person.swift
//  Landmarks
//
//  Created by macos on 5/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct Person : Equatable, Hashable, Codable {

    var id : String

    var name :String

    var email : String
    
    var profilePictureURL : String
    
    var phone : String


    public init(id : String = "", name : String = "", email : String = "", profilePictureURL : String = "", phone : String = "") {
        self.id = id;
        self.name = name;
        self.email = email;
        self.profilePictureURL = profilePictureURL
        self.phone = phone
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(self.id)
    }
    
//    @Override
//    public boolean equals(Object that) {
//        boolean isEqual = EqualsBuilder.reflectionEquals(this, that, "name", "email");
//        return isEqual;
//    }
//
//    @Override
//    public int hashCode() {
//        int hashCode = HashCodeBuilder.reflectionHashCode(this, "name", "email");
//        return hashCode;
//    }
//
//    @Override
//    public String toString() {
//        String str = ReflectionToStringBuilder.toString(this);
//        return str;
//    }

}
