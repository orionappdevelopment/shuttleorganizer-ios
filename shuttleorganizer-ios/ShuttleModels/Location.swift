//
//  Location.swift
//  Landmarks
//
//  Created by macos on 5/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct Location : Equatable, Hashable, Codable {
    private var id : Int64
    private var personId : String
    private var transportationId : String
    private var latitude : Float64
    private var longitude : Float64
    
    public init(id : Int64 = -1, personId :String, transportationId : String, latitude : Float64, longitude : Float64) {
        self.id = id;
        self.personId = personId;
        self.transportationId = transportationId;
        self.latitude = latitude
        self.longitude = longitude
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher){
        hasher.combine(self.id)
    }
    
//    @Override
//    public boolean equals(Object that) {
//        boolean isEqual = EqualsBuilder.reflectionEquals(this, that, "personId", "transportationId", "latitude", "longitude");
//        return isEqual;
//    }
//
//    @Override
//    public int hashCode() {
//        int hashCode = HashCodeBuilder.reflectionHashCode(this, "personId", "transportationId", "latitude", "longitude");
//        return hashCode;
//    }
//
//    @Override
//    public String toString() {
//        String str = ReflectionToStringBuilder.toString(this);
//        return str;
//    }

}
