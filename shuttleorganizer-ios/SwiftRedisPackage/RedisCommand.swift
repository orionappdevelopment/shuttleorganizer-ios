//
//  RedisCommand.swift
//  ActivityKeeper
//
//  Created by Ron Perry on 11/7/15.
//  Copyright © 2015 ronp001. All rights reserved.
//

import Foundation

public protocol RedisCommandDelegate {
    func commandExecuted(_ cmd: RedisCommand)
    func commandFailed(_ cmd: RedisCommand)
}

public class RedisCommand : CustomStringConvertible
{
    var delegate: RedisCommandDelegate? = nil
    
    public var description: String {
        return "RedisCommand(\(commandType))"
    }
    
    enum OperationType { case get, del, exists, set, auth, publish, subscribe, unsubscribe, quit, generic, llen, lrange, rpush, rpop}
    typealias ValueCompletionHandler = (_ success: Bool, _ key: String, _ result: RedisResponse?, _ cmd: RedisCommand) -> Void
    typealias VoidCompletionHandler = (_ success: Bool, _ cmd: RedisCommand) -> Void
    
    var sent = false // connection object sets this to true when the command is sent
    
    let commandType: OperationType
    var param1: String?
    var param2: Data?
    var additionalParams: [String?]?
    
    let valueCompletionHandler: ValueCompletionHandler?
    let voidCompletionHandler: VoidCompletionHandler?
    let finishWhenResponseReceived: Bool
    
    var key: String? { get { return param1 }  set(value) { param1 = value } }
    var valueToSet: Data? { get { return param2 } set(value) {param2 = value} }
    
    init(type: OperationType, param1: String? = nil, param2: Data? = nil, valueCompletionHandler: ValueCompletionHandler? = nil, voidCompletionHandler: VoidCompletionHandler? = nil, additionalParams: [String?]? = nil )
    {
        self.commandType = type
        self.param1 = param1
        self.param2 = param2
        self.valueCompletionHandler = valueCompletionHandler
        self.voidCompletionHandler = voidCompletionHandler
        self.additionalParams = additionalParams
        
        finishWhenResponseReceived = (type != .subscribe) //yaefe comment out, I did not understand why we need it
//        finishWhenResponseReceived = true
    }
    
    // call the completion handler with a failure status
    func completionFailed()
    {
        switch self.commandType {
        case .get, .del, .exists, .subscribe, .unsubscribe, .publish, .generic, .llen, .lrange, .rpush, .rpop:
            valueCompletionHandler?(false, key!, nil, self)
        case .set, .auth, .quit:
            voidCompletionHandler?(false, self)
        }

        delegate?.commandFailed(self)
    }
    
    var response: RedisResponse? = nil
    
    func responseReceived(_ response: RedisResponse)
    {
        self.response = response

        print("received response: \(String(describing:self.response))")
        
        var success = true
        if response.responseType == .error {
            success = false
        }
        
        switch self.commandType {
        case .get, .del, .exists, .publish, .subscribe, .unsubscribe, .generic, .llen, .lrange, .rpush, .rpop:
            valueCompletionHandler?(success, key!, response, self)
        case .set, .auth, .quit:
            voidCompletionHandler?(success, self)
        }

        delegate?.commandExecuted(self)
    }
    
    static func Quit(_ handler: VoidCompletionHandler?) -> RedisCommand
    {
        return RedisCommand(type: .quit, voidCompletionHandler: handler)
    }
    
    static func Auth(_ password: String, handler: VoidCompletionHandler?) -> RedisCommand
    {
        return RedisCommand(type: .auth, param1: password, voidCompletionHandler: handler)
    }
    
    static func Set(_ key: String, valueToSet: Data, handler: VoidCompletionHandler?) -> RedisCommand
    {
        return RedisCommand(type: .set, param1: key, param2: valueToSet, voidCompletionHandler: handler)
    }
    
    static func Set(_ key: String, valueToSet: String, handler: VoidCompletionHandler?) -> RedisCommand
    {
        return Set(key, valueToSet: valueToSet.data(using: String.Encoding.utf8)!, handler: handler)
    }
    
    static func Get(_ key: String, handler: ValueCompletionHandler?) -> RedisCommand
    {
        return RedisCommand(type: .get, param1: key, valueCompletionHandler: handler)
    }
    
    static func Del(_ key: String, handler: VoidCompletionHandler?) -> RedisCommand
    {
        return RedisCommand(type: .del, param1: key, voidCompletionHandler: handler)
    }
    
    static func Exists(_ key: String, handler: ValueCompletionHandler?) -> RedisCommand
    {
        return RedisCommand(type: .exists, param1: key, valueCompletionHandler: handler)
    }
    
    static func Publish(_ channel: String, value: String, handler: ValueCompletionHandler?) -> RedisCommand
    {
        return RedisCommand(type: .publish, param1: channel, param2: value.data(using: String.Encoding.utf8)!, valueCompletionHandler: handler)
    }
    
    static func Subscribe(_ channel: String, handler: ValueCompletionHandler?) -> RedisCommand
    {
        return RedisCommand(type: .subscribe, param1: channel, valueCompletionHandler: handler)
    }
    
    static func Unsubscribe(_ channel: String, handler: ValueCompletionHandler?) -> RedisCommand
    {
        return RedisCommand(type: .unsubscribe, param1: channel, valueCompletionHandler: handler)
    }
    
    static func Generic(_ cmd: String, _ arg1: String? = nil, _ arg2: String? = nil, _ arg3: String? = nil, _ arg4: String? = nil, handler: ValueCompletionHandler? ) -> RedisCommand
    {
        return RedisCommand(type: .generic, param1: cmd, param2: nil, valueCompletionHandler: handler, voidCompletionHandler: nil, additionalParams: [arg1, arg2, arg3, arg4])
    }
    
    static func LLen(_ key: String, handler: ValueCompletionHandler?) -> RedisCommand
    {
        return RedisCommand(type: .llen, param1: key, valueCompletionHandler: handler)
    }
    
    static func LRange(_ key: String, start: Int, stop: Int, handler: ValueCompletionHandler?) -> RedisCommand
    {
        return RedisCommand(type: .lrange, param1: key, valueCompletionHandler: handler, additionalParams: [String(start), String(stop)])
    }
    
    static func RPush(_ key: String, valueToPush: Data, handler: ValueCompletionHandler?) -> RedisCommand
    {
        return RedisCommand(type: .rpush, param1: key, param2: valueToPush, valueCompletionHandler: handler)
    }
    
    static func RPop(_ key: String, handler: ValueCompletionHandler?) -> RedisCommand
    {
        return RedisCommand(type: .rpop, param1: key, valueCompletionHandler: handler)
    }
    
    func buildCommandString(_ words: [NSObject]) -> Data
    {
        var result = NSData(data: "*\(words.count)\r\n".data(using: String.Encoding.utf8)!) as Data
        
        for word in words {
            if let wordStr = word as? NSString {
                let lenStr = NSData(data: "$\(wordStr.length)\r\n".data(using: String.Encoding.utf8)!) as Data
                result.append(lenStr)
                
                let strStr = NSData(data: "\(wordStr)\r\n".data(using: String.Encoding.utf8)!) as Data
                result.append(strStr)
            } else if let wordData = word as? Data {
                let lenStr = NSData(data: "$\(wordData.count)\r\n".data(using: String.Encoding.utf8)!) as Data
                result.append(lenStr)
                
                result.append(wordData)
                result.append("\r\n".data(using: String.Encoding.utf8)!)
            } else {
                assert(false)
            }
        }
        
        return result
    }
    
    func getCommandString() -> Data? {
        
        switch commandType {
        case .get:
            return buildCommandString(["GET" as NSObject, self.param1! as NSObject])
        case .del:
            return buildCommandString(["DEL" as NSObject, self.param1! as NSObject])
        case .exists:
            return buildCommandString(["EXISTS" as NSObject, self.param1! as NSObject])
        case .set:
            return buildCommandString(["SET" as NSObject, self.param1! as NSObject, self.param2! as NSObject])
        case .publish:
            return buildCommandString(["PUBLISH" as NSObject, self.param1! as NSObject, self.param2! as NSObject])
        case .subscribe:
            return buildCommandString(["SUBSCRIBE" as NSObject, self.param1! as NSObject])
        case .unsubscribe:
            return buildCommandString(["UNSUBSCRIBE" as NSObject, self.param1! as NSObject])
        case .auth:
            return buildCommandString(["AUTH" as NSObject, self.param1! as NSObject])
        case .quit:
            return buildCommandString(["QUIT" as NSObject])
        case .llen:
            return buildCommandString(["LLEN" as NSObject, self.param1! as NSObject])
        case .lrange:
            return buildCommandString(["LRANGE" as NSObject, self.param1! as NSObject, self.additionalParams![0]! as NSObject, self.additionalParams![1]! as NSObject])
        case .rpush:
            return buildCommandString(["RPUSH" as NSObject, self.param1! as NSObject, self.param2! as NSObject])
        case .rpop:
            return buildCommandString(["RPOP" as NSObject, self.param1! as NSObject])
        case .generic:
            var cmdArray: [String] = []
            cmdArray += [self.param1!]
            if let additionalParams = self.additionalParams {
                for param in additionalParams {
                    if let param = param {
                        cmdArray += [param]
                    }
                }
            }
            return buildCommandString(cmdArray as [NSObject])
        }
    }
}
