
import Foundation

//yaefe: changed just for values is key not data

// Stack from : https://github.com/raywenderlich/swift-algorithm-club/tree/master/Stack
public struct Stack<T> {
  fileprivate var array: [T] = []

  public var isEmpty: Bool {
    return array.isEmpty
  }

  public var count: Int {
    return array.count
  }

  public mutating func push(_ element: T) {
    array.append(element)
  }

  public mutating func pop() -> T? {
    return array.popLast()
  }

  public func peek() -> T? {
    return array.last
  }
}

extension Stack: Sequence {
  public func makeIterator() -> AnyIterator<T> {
    var curr = self
    return AnyIterator { curr.pop() }
  }
}

private func coinFlip() -> Bool {
  return arc4random_uniform(2) == 1
}

public class DataNode<Key: Comparable> {
  public typealias Node = DataNode<Key>

  fileprivate var key: Key?
  var next: Node?
  var down: Node?

  public init(key: Key) {
    self.key  = key
  }

  public init(asHead head: Bool) {}

}

open class SkipList<Key: Comparable> {
  public typealias Node = DataNode<Key>

  fileprivate(set) var head: Node?

  public init() {}

}

// MARK: - Search lanes for a node with a given key
extension SkipList {

  func findNode(key: Key) -> Node? {
    var currentNode: Node? = head
    var isFound: Bool = false

    while !isFound {
      if let node = currentNode {

        switch node.next {
        case .none:

          currentNode = node.down
        case .some(let value) where value.key != nil:

          if value.key == key {
            isFound = true
            break
          } else {
            if key < value.key! {
              currentNode = node.down
            } else {
              currentNode = node.next
            }
          }

        default:
          continue
        }

      } else {
        break
      }
    }

    if isFound {
      return currentNode
    } else {
      return nil
    }

  }

  func search(key: Key) -> Key? {
    guard let node = findNode(key: key) else {
      return nil
    }

    return node.next!.key
  }

}

// MARK: - Insert a node into lanes depending on skip list status ( bootstrap base-layer if head is empty / start insertion from current head ).
extension SkipList {
  private func bootstrapBaseLayer(key: Key) {
    head       = Node(asHead: true)
    var node   = Node(key: key)

    head!.next = node

    var currentTopNode = node

    while coinFlip() {
      let newHead    = Node(asHead: true)
      node           = Node(key: key)
      node.down      = currentTopNode
      newHead.next   = node
      newHead.down   = head
      head           = newHead
      currentTopNode = node
    }

  }

  private func insertItem(key: Key) {
    var stack              = Stack<Node>()
    var currentNode: Node? = head

    while currentNode != nil {

      if let nextNode = currentNode!.next {
        if nextNode.key! > key {
          stack.push(currentNode!)
          currentNode = currentNode!.down
        } else {
          currentNode = nextNode
        }

      } else {
        stack.push(currentNode!)
        currentNode = currentNode!.down
      }

    }

    let itemAtLayer    = stack.pop()
    var node           = Node(key: key)
    node.next          = itemAtLayer!.next
    itemAtLayer!.next  = node
    var currentTopNode = node

    while coinFlip() {
      if stack.isEmpty {
        let newHead    = Node(asHead: true)

        node           = Node(key: key)
        node.down      = currentTopNode
        newHead.next   = node
        newHead.down   = head
        head           = newHead
        currentTopNode = node

      } else {
        let nextNode  = stack.pop()

        node           = Node(key: key)
        node.down      = currentTopNode
        node.next      = nextNode!.next
        nextNode!.next = node
        currentTopNode = node
      }
    }
  }

  @discardableResult
  public func insert(key: Key) -> Bool{
    var inserted = false
    if head != nil {
      if let node = findNode(key: key) {
        // replace, in case of key already exists.
        var currentNode = node.next
        while currentNode != nil && currentNode!.key == key {
//          currentNode!.data = data
          currentNode       = currentNode!.down
        }
      } else {
        insertItem(key: key)
        inserted = true
      }

    } else {
      bootstrapBaseLayer(key: key)
      inserted = true
    }
    
    return inserted
  }

}

// MARK: - Remove a node with a given key. First, find its position in layers at the top, then remove it from each lane by traversing down to the base layer.
extension SkipList {
  public func remove(key: Key) {
    guard let item = findNode(key: key) else {
      return
    }

    var currentNode = Optional(item)

    while currentNode != nil {
      let node   = currentNode!.next

      if node!.key != key {
        currentNode = node
        continue
      }

      let nextNode      = node!.next

      currentNode!.next = nextNode
      currentNode       = currentNode!.down

    }

  }
}

// MARK: - Get associated payload from a node with a given key.
extension SkipList {

  public func get(key: Key) -> Key? {
    return search(key: key)
  }
}

// MARK: - Get arraylist
extension SkipList {

  public func getSortedElements() -> [Key] {
    var currentNode: Node? = head
    var list : [Key] = []
    
    var added = false
    while(currentNode != nil)
    {
        if added == false && currentNode?.key != nil {
            list.append(currentNode!.key!)
            added = true
        }
        
        if currentNode?.down != nil {
            currentNode = currentNode?.down
        }else{
            currentNode = currentNode?.next
            added = false
        }
    }
    
    return list
  }
}
