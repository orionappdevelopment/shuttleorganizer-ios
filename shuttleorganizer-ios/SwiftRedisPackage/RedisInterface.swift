//
//  RedisInterface.swift
//  ActivityKeeper
//
//  Created by Ron Perry on 11/7/15.
//  Copyright © 2015 ronp001. All rights reserved.
//

import Foundation
import Network

public class RedisInterface : RedisCommandDelegate, RedisConnectionDelegate
{
    // MARK: properties
    let c: RedisConnection
    let auth: String
    
    init(host: String, port: UInt32, auth: String)
    {
        self.c = RedisConnection(serverAddress: host, serverPort: port)
        self.auth = auth
        self.c.redisInterface = self
//        self.auth = auth
    }
    
    deinit
    {
        disconnect()
    }
    
//    var commandQueue = [RedisCommand]()
    var commandQueue = LinkedList<RedisCommand>()
    var currentCommand: RedisCommand? = nil
    var isConnected = false
    
    var shouldBeConnected = false
    
    var isInternetConnectionHandlerStarted = false
    var isInternetConnected = false

    func connect()
    {
        c.delegate = self
        let connected = c.connect()
        if connected == false {
            startTimer()
        }
        
        shouldBeConnected = true
    }
    
    func disconnect()
    {
        stopTimer()
        commandQueue.removeAll()
        c.disconnect()
        shouldBeConnected = false
    }
    
    func addCommandToQueue(_ command: RedisCommand)
    {
        commandQueue.append(command)
        sendNextCommandIfPossible()
    }
    
    func skipPendingCommandsAndQuit(_ completionHandler: RedisCommand.VoidCompletionHandler? )
    {
        if commandQueue.count > 0 {
            print("RedisInterface -- skipPendingCommandsAndQuit: removing \(commandQueue.count) pending commands from redis queue")
            commandQueue.removeAll()
        } else {
            print("RedisInterface -- skipPendingCommandsAndQuit: no pending commands to remove")
        }
        self.quit(completionHandler)
    }
    
    func sendNextCommandIfPossible()
    {
        if !isConnected {
            print("sendNextCommandIfPossible will not execute because isConnected \(isConnected)")
            return
        }
        if commandQueue.count == 0 {
            print("sendNextCommandIfPossible will not execute because commandQueue.count \(commandQueue.count)")
            return
        }
        print("sendNextCommandIfPossible current commandQueue: \(commandQueue)")
        
        if currentCommand != nil { return }
        
        currentCommand = commandQueue.removeFirst()
        currentCommand?.delegate = self
        
        print("sendNextCommandIfPossible sending next command: \(String(describing:currentCommand))")
        c.setPendingCommand(currentCommand!)
    }

    func authenticationFailed()
    {
        print("Redis authentication failed")
        abort()
    }
    
    // MARK: RedisConnectionDelegate functions
    
    public func connectionError(_ error: String) {
        print("RedisInterface: connection error \(error)")
        isConnected = false
    }

    public func connected()
    {
        if isConnected {
            return
        }
        
        isConnected = true
        commandQueue.insertAt(value: RedisCommand.Auth(self.auth, handler: {success, cmd in
            if !success {
                self.shouldBeConnected = false
                self.authenticationFailed()
            }else {
                self.shouldBeConnected = true
                self.internetConnectionHandlerStart()
                self.startTimer()
            }
        }), atIndex: 0)
        
        if self.c.subscribedCommands.count > 0 {
            print("add subscribedCommand when connected")
            for subcribedCommand in self.c.subscribedCommands {
                subcribedCommand.sent = false
                commandQueue.append(subcribedCommand)
            }
            print("RedisInterface connected method commandQueue: \(commandQueue)")
            self.c.subscribedCommands.removeAll()
            print("after adding subscribedCmmand commandQueue:\(commandQueue)")
        }
        
        print("sendNextCommandIfPossble called at RedisInterface connected method")
        sendNextCommandIfPossible()
    }
    
    // MARK: RedisCommandDelegate functions

    public func commandExecuted(_ cmd: RedisCommand) {
        self.currentCommand = nil
        sendNextCommandIfPossible()
    }
    
    public func commandFailed(_ cmd: RedisCommand) {
        self.currentCommand = nil
        sendNextCommandIfPossible()
    }

    // MARK: operational interface
    
    func setDataForKey(_ key: String, data: Data, completionHandler: RedisCommand.VoidCompletionHandler? )
    {
        addCommandToQueue(RedisCommand.Set(key, valueToSet: data, handler: completionHandler))
    }
    
    func setValueForKey(_ key: String, stringValue: String, completionHandler: RedisCommand.VoidCompletionHandler? )
    {
        addCommandToQueue(RedisCommand.Set(key, valueToSet: stringValue, handler: completionHandler))
    }
    
    func getDataForKey(_ key: String, completionHandler: RedisCommand.ValueCompletionHandler? )
    {
        addCommandToQueue(RedisCommand.Get(key, handler: completionHandler))
    }
    
    func getValueForKey(_ key: String, completionHandler: RedisCommand.ValueCompletionHandler? )
    {
        getDataForKey(key, completionHandler: completionHandler)
    }
    
    func delDataForKey(_ key: String, completionHandler: RedisCommand.VoidCompletionHandler? )
    {
        addCommandToQueue(RedisCommand.Del(key, handler: completionHandler))
    }
    
    func delValueForKey(_ key: String, completionHandler: RedisCommand.VoidCompletionHandler? )
    {
        addCommandToQueue(RedisCommand.Del(key, handler: completionHandler))
    }
    
    func existsDataForKey(_ key: String, completionHandler: RedisCommand.ValueCompletionHandler? )
    {
        addCommandToQueue(RedisCommand.Exists(key, handler: completionHandler))
    }
    
    func existsValueForKey(_ key: String, completionHandler: RedisCommand.ValueCompletionHandler? )
    {
        existsDataForKey(key, completionHandler: completionHandler)
    }
    
    
    func subscribe(_ channel: String, completionHandler: RedisCommand.ValueCompletionHandler? )
    {
        addCommandToQueue(RedisCommand.Subscribe(channel, handler: completionHandler))
    }
    
    func unsubscribe(_ channel: String, completionHandler: RedisCommand.ValueCompletionHandler? )
    {
        addCommandToQueue(RedisCommand.Unsubscribe(channel, handler: completionHandler))
    }

    func publish(_ channel: String, value: String, completionHandler: RedisCommand.ValueCompletionHandler? )
    {
        addCommandToQueue(RedisCommand.Publish(channel, value: value, handler: completionHandler))
    }
    
    func llen(_ key: String, completionHandler: RedisCommand.ValueCompletionHandler? )
    {
        addCommandToQueue(RedisCommand.LLen(key, handler: completionHandler))
    }
    
    func lrange(_ key: String, start : Int, stop : Int, completionHandler: RedisCommand.ValueCompletionHandler? )
    {
        addCommandToQueue(RedisCommand.LRange(key, start: start, stop: stop, handler: completionHandler))
    }
    
    func rpush(_ key: String, data: Data, completionHandler: RedisCommand.ValueCompletionHandler? )
    {
        addCommandToQueue(RedisCommand.RPush(key, valueToPush: data, handler: completionHandler))
    }
    
    func rpop(_ key: String, completionHandler: RedisCommand.ValueCompletionHandler? )
    {
        addCommandToQueue(RedisCommand.RPop(key, handler: completionHandler))
    }
    
    func generic(_ cmd: String, _ arg1: String? = nil, _ arg2: String? = nil, _ arg3: String? = nil, _ arg4: String? = nil, completionHandler: RedisCommand.ValueCompletionHandler?)
    {
        addCommandToQueue(RedisCommand.Generic(cmd, arg1, arg2, arg3, arg4, handler: completionHandler))
    }
    
    func quit(_ completionHandler: RedisCommand.VoidCompletionHandler? )
    {
        addCommandToQueue(RedisCommand.Quit(completionHandler))
    }
    
    weak var timer: Timer?

    func startTimer() {
        timer?.invalidate()   // just in case you had existing `Timer`, `invalidate` it before we lose our reference to it
        timer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: true) { [weak self] _ in
            if InternetConnectivity.getInstance().isInternetConnected() == false {
                print("Device is not connected to internet watch dog will try later")
                return
            }
            
//            print("shouldBeConnected \(String(describing:self?.shouldBeConnected))")
//            print("isConnected \(String(describing:self?.isConnected))")
            if self?.shouldBeConnected == true && self?.isConnected == false {
                print("watchdog pending command is \(String(describing:self?.c.pendingCommand))")
                print("watchdog current command is \(String(describing:self?.currentCommand))")
                
                
                if(self != nil && self?.currentCommand != nil){
                    
                    for (tmpIndex, tmpCommand) in self!.commandQueue.enumerated() {
                        if tmpCommand.commandType == .auth{
                            _ = self!.commandQueue.removeAt(atIndex: tmpIndex)
                            print("StartTimer commandQueue, command \(tmpCommand) removed")
                        }
                    }
                    
                    self!.commandQueue.insertAt(value: self!.currentCommand!, atIndex: 0)
                    print("Output pending command successfully add to commandQueue")
                }
                
                self?.currentCommand?.sent = false
                self?.currentCommand = nil
                
                self?.c.pendingCommand = nil
                self?.c.sentCommandQueue.removeAll()
                
                while self != nil && self!.c.sentCommandQueue.isEmpty == false{
                    let failedCommand = self!.c.sentCommandQueue.dequeue()
                    failedCommand?.completionFailed()
                }
                
                self?.connect()
                print("WatchDog see that there is a disconnection so try to reconnect")
            }else if self?.isConnected == true {
//                self?.setValueForKey("key", stringValue: "1", completionHandler: { success, cmd in
//                    if success == false {
//                        print("WatchDog set has problem probably there is a problem with connection")
//                    }else {
//                        print("WatchDog successfully set key connection period will extend ")
//                    }
//                })
            }
        }
    }

    func stopTimer() {
        timer?.invalidate()
    }
    
    public func internetConnectionHandlerStart(){
//        if isInternetConnectionHandlerStarted == true{
//            print("No need to start isStarted : \(isInternetConnectionHandlerStarted)")
//            return
//        }
//
//        let monitor = NWPathMonitor()
//
//        monitor.pathUpdateHandler = { path in
//            if path.status == .satisfied {
//
//                print("We're iconnected!")
//                if self.shouldBeConnected == true && self.isInternetConnected == false {
//                    self.connect()
//                    print("WatchDog see that there is a disconnection so try to reconnect")
//                }
//                self.isInternetConnected = true
//            } else {
//                self.isInternetConnected = false
//                print("No iconnected!")
//            }
//        }
//        let queue = DispatchQueue(label: "Monitor")
//        monitor.start(queue: queue)
//
//        isInternetConnectionHandlerStarted = true
    }

    // if appropriate, make sure to stop your timer in `deinit`

    
}

