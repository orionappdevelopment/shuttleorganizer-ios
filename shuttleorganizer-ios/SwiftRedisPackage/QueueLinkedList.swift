import Foundation
public struct QueueLinkedList<T> {

    fileprivate var list = LinkedList<T>()

    public var isEmpty: Bool {
        return list.isEmpty
    }
  
    public mutating func addToStart(_ element: T) {
        list.insertAt(value: element, atIndex: 0)
    }
    
    public mutating func removeAll(){
        list.removeAll()
    }
    
    public mutating func enqueue(_ element: T) {
        list.append(element)
    }
    
    public mutating func dequeue() -> T? {
        guard !list.isEmpty, let element = list.first else { return nil }
        list.removeFirst()
        return element
    }
    
    public func peek() -> T? {
        return list.first
    }
    
    public func isPeekNil() -> Bool {
        return list.first == nil
    }
}
