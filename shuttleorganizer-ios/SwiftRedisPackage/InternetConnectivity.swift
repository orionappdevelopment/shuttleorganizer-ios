//
//  InternetConnectivity.swift
//  SwiftRedisChat
//
//  Created by macos on 8/31/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation
import Network

class InternetConnectivity{
    
    private static let instance = InternetConnectivity()
    private var isStarted = false
    private var isConnected: Bool = false
    
    private init(){
    }
    
    static public func getInstance() -> InternetConnectivity {
        return instance
    }
    
    public func isInternetConnected() -> Bool {
        return self.isConnected
    }
    
    public func start(){
        if isStarted == true{
            print("No need to start isStarted : \(isStarted)")
            return
        }
        
        let monitor = NWPathMonitor()
        
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                self.isConnected = true
                print("We're iconnected!")
            } else {
                self.isConnected = false
                print("No iconnected!")
            }
        }
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
        
        isStarted = true
    }
}
