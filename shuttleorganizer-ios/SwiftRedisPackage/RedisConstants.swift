//
//  RedisConstants.swift
//  shuttleorganizer-ios
//
//  Created by macos on 4/4/21.
//  Copyright © 2021 Orion. All rights reserved.
//

import Foundation

class RedisConstants{
    public static let REDIS_LOCATION_PREFIX : String = "location_"
    public static let REDIS_CHAT_PREFIX : String = "chat_"
    public static let REDIS_ATTENDANCE_PREFIX : String = "attendance_"
    
    public static func getRedisLocationChannelId(transportationId : String) -> String{
        return REDIS_LOCATION_PREFIX + transportationId
    }
    
    public static func getRedisChatChannelId(transportationId : String) -> String{
        return REDIS_CHAT_PREFIX + transportationId
    }
    
    public static func getRedisAttendanceChannelId(transportationId : String) -> String{
        return REDIS_ATTENDANCE_PREFIX + transportationId
    }
}
