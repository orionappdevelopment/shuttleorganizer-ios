//
//  GeneralUtils.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/24/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

//    var currentLocale = Locale.init(identifier: "tr")
//    var currentLocale = Locale.current

class LanguageBundle {
    
    private static var instance : LanguageBundle?
    
    var bundle : Bundle?
    
    private init() {
        bundle = nil
    }
    
    static func getInstance() -> LanguageBundle {
        if instance == nil {
            instance = LanguageBundle()
        }
        return instance!
    }
    
    func setLanguageBundle(language: String){
        let path = Bundle.main.path(forResource: language, ofType: "lproj")!
        bundle = Bundle(path: path)
    }
}


    public func toStringFromLocalizedStringKey(localizedStringKey : String, _ args : String...) -> String {
//        LanguageBundle.getInstance().setLanguageBundle(language: "tr")
        
        var nsLocalizedString = NSLocalizedString(localizedStringKey, comment: "")
        if let bundle = LanguageBundle.getInstance().bundle {
            nsLocalizedString = NSLocalizedString(localizedStringKey, bundle: bundle, comment: "")
        }
            
        var result = ""
        if args.count == 0 {
           result = String(format: nsLocalizedString)
        }else if args.count == 1 {
           result = String(format: nsLocalizedString, args[0])
        }else if args.count == 2 {
           result = String(format: nsLocalizedString, args[0], args[1])
        }else if args.count == 3 {
           result = String(format: nsLocalizedString, args[0], args[1], args[2])
        }else if args.count == 4 {
           result = String(format: nsLocalizedString, args[0], args[1], args[2], args[3])
        }else if args.count == 5 {
           result = String(format: nsLocalizedString, args[0], args[1], args[2], args[3], args[4])
        }else if args.count == 6 {
           result = String(format: nsLocalizedString, args[0], args[1], args[2], args[3], args[4], args[5])
        }else if args.count == 7 {
           result = String(format: nsLocalizedString, args[0], args[1], args[2], args[3], args[4], args[5], args[6])
        }
        
        return result
   }

//    public func toStringFromLocalizedStringKey(localizedStringKey : String, _ args : String...) -> String {
//
//        let nsLocalizedString = NSLocalizedString(localizedStringKey, comment: "")
//
//        var result = ""
//        if args.count == 0 {
//            result = String(format: nsLocalizedString, locale: currentLocale)
//        }else if args.count == 1 {
//            result = String(format: nsLocalizedString, locale: currentLocale, args[0])
//        }else if args.count == 2 {
//            result = String(format: nsLocalizedString, locale: Locale.init(identifier: "tr"), args[0], args[1])
//        }else if args.count == 3 {
//            result = String(format: nsLocalizedString, locale: currentLocale, args[0], args[1], args[2])
//        }else if args.count == 4 {
//            result = String(format: nsLocalizedString, locale: currentLocale, args[0], args[1], args[2], args[3])
//        }else if args.count == 5 {
//            result = String(format: nsLocalizedString, locale: currentLocale, args[0], args[1], args[2], args[3], args[4])
//        }else if args.count == 6 {
//            result = String(format: nsLocalizedString, locale: currentLocale, args[0], args[1], args[2], args[3], args[4], args[5])
//        }else if args.count == 7 {
//            result = String(format: nsLocalizedString, locale: currentLocale, args[0], args[1], args[2], args[3], args[4], args[5], args[6])
//        }
//
//        return result
//    }

    public func getHourMinuteFormat() -> DateFormatter{
        let dateFormat = DateFormatter.dateFormat (fromTemplate: "j",options:0, locale: Locale.current)
        
        let formatter = DateFormatter()
        if dateFormat == "HH" {
            formatter.dateFormat = "HH:mm"
        } else {
            formatter.dateFormat = "hh:mm a"
        }
        
        formatter.locale = Locale.current
        return formatter
    }

    public func getYearMonthDateFormat() -> DateFormatter{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy, E"
        
        formatter.locale = Locale.current
        return formatter
    }

    public func getMonthDateDayFormat() -> DateFormatter{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, E"
        formatter.locale = Locale.current
        return formatter
    }

    public func isFirstDateSameOrBeforeSecondDate(first : Date, second: Date) -> Bool {
        if Calendar.current.compare(first, to: second, toGranularity: .day) == .orderedDescending{
            return false
        }
        return true
    }

    private func getUniqueDeviceId() -> String {
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            return uuid
        }
        return ""
    }

    public func makeFullScreenForUIView(of childView: UIView, to parentView: UIView) {
        childView.translatesAutoresizingMaskIntoConstraints = false
        childView.leftAnchor.constraint(equalTo: parentView.leftAnchor).isActive = true
        childView.rightAnchor.constraint(equalTo: parentView.rightAnchor).isActive = true
        childView.topAnchor.constraint(equalTo: parentView.topAnchor).isActive = true
        childView.bottomAnchor.constraint(equalTo: parentView.bottomAnchor).isActive = true
    }

    public func getResizedUIImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        let newSize = CGSize(width: newWidth, height: newHeight)

        let renderer = UIGraphicsImageRenderer(size: newSize)

        let newImage = renderer.image { (context) in
            image.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
        }
        return newImage
    }

    public func mergedImageWith(topImage:UIImage, bottomImage: UIImage) -> UIImage{

        let width = topImage.size.width > bottomImage.size.width ? topImage.size.width : bottomImage.size.width
        let size = CGSize(width: width, height: topImage.size.height + bottomImage.size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)

        topImage.draw(in: CGRect(x: 0, y: 0, width: size.width, height: topImage.size.height))
        bottomImage.draw(in: CGRect(x: size.width/2 - bottomImage.size.width/2, y: topImage.size.height, width: bottomImage.size.width, height: bottomImage.size.height))

        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        //set finalImage to IBOulet UIImageView
        return newImage
    }

    public func createTriangle(side: CGFloat, color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: side, height: side), false, 0)
        let ctx = UIGraphicsGetCurrentContext()!
        ctx.saveGState()

        ctx.beginPath()
        ctx.move(to: CGPoint(x: side / 2, y: side))
        //### Add lines
        ctx.addLine(to: CGPoint(x: side, y: 0))
        ctx.addLine(to: CGPoint(x: 0, y: 0))
        //ctx.addLine(to: CGPoint(x: side / 2, y: 0)) //### path is automatically closed
        ctx.closePath()

        ctx.setFillColor(color.cgColor)

        ctx.drawPath(using: .fill) //### draw the path

        ctx.restoreGState()
        let img = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        return img
    }

    public func createMarkerImage(uiImage: UIImage, uiColor : UIColor) -> UIImage{
        let circleBorderedMarkerImage = uiImage
            .circularImage(cgSize: CGSize(width: 64, height: 64))!
            .addBorderForCircleImage(uiColor: uiColor, borderWidth: 10)
        let triangleImage = createTriangle(side: 5, color: uiColor)
        let mergedImage = mergedImageWith(topImage: circleBorderedMarkerImage, bottomImage: triangleImage)
        return mergedImage
    }

    public func imageWithColor(uiImage: UIImage, tintColor: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(uiImage.size, false, uiImage.scale)

        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: 0, y: uiImage.size.height)
        context.scaleBy(x: 1.0, y: -1.0);
        context.setBlendMode(.normal)

        let rect = CGRect(x: 0, y: 0, width: uiImage.size.width, height: uiImage.size.height) as CGRect
        context.clip(to: rect, mask: uiImage.cgImage!)
        tintColor.setFill()
        context.fill(rect)

        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        return newImage
    }

    public func imageWithBackground(uiImage: UIImage, color: UIColor, opaque: Bool = true) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(uiImage.size, opaque, uiImage.scale)
          
        guard let ctx = UIGraphicsGetCurrentContext(), let image = uiImage.cgImage else { return uiImage }
        defer { UIGraphicsEndImageContext() }
          
        let rect = CGRect(origin: .zero, size: uiImage.size)
        ctx.setFillColor(color.cgColor)
        ctx.fill(rect)
        ctx.concatenate(CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: uiImage.size.height))
        ctx.draw(image, in: rect)
          
        return UIGraphicsGetImageFromCurrentImageContext() ?? uiImage
    }


//    func drawImageWithProfilePic(pp: UIImage, image: UIImage) -> UIImage {
//
//        let imgView = UIImageView(image: image)
//        let picImgView = UIImageView(image: pp)
//        picImgView.frame = CGRect(x: 0, y: 0, width: 64, height: 64)
//        imgView.frame = CGRect(x: 0, y: 0, width: 64, height: 64)
//        
//        imgView.addSubview(picImgView)
//        picImgView.center.x = imgView.center.x
//        picImgView.center.y = imgView.center.y - 7
//        picImgView.layer.cornerRadius = picImgView.frame.width/2
//        picImgView.clipsToBounds = true
//        imgView.setNeedsLayout()
//        picImgView.setNeedsLayout()
//
////        let newImage = imageWithView(view: imgView)
//        var image: UIImage?
//        UIGraphicsBeginImageContextWithOptions(imgView.bounds.size, false, 0.0)
//        if let context = UIGraphicsGetCurrentContext() {
//            imgView.layer.render(in: context)
//            image = UIGraphicsGetImageFromCurrentImageContext()
//            UIGraphicsEndImageContext()
//        }
//        return image ?? UIImage()
//    }
//
//
//    func roundImageAndAddBorder(image: UIImage) -> UIImage {
//        let imageWidth = image.size.width
//        let imageHeight = image.size.height
//
//        let diameter = min(imageWidth, imageHeight)
//        let isLandscape = imageWidth > imageHeight
//
//        let xOffset = isLandscape ? (imageWidth - diameter) / 2 : 0
//        let yOffset = isLandscape ? 0 : (imageHeight - diameter) / 2
//
//        let imageSize = CGSize(width: diameter, height: diameter)
//
//        return UIGraphicsImageRenderer(size: imageSize).image { _ in
//
//            let ovalPath = UIBezierPath(ovalIn: CGRect(origin: .zero, size: imageSize))
//            ovalPath.addClip()
//            image.draw(at: CGPoint(x: -xOffset, y: -yOffset))
//            UIColor.black.setStroke()
//            ovalPath.lineWidth = diameter / 10
//            ovalPath.stroke()
//        }
//    }

public final class AtomicInteger {
    
    private let lock = DispatchSemaphore(value: 1)
    private var _value: Int
    
    public init(value initialValue: Int = 0) {
        _value = initialValue
    }
    
    public var value: Int {
        get {
            lock.wait()
            defer { lock.signal() }
            return _value
        }
        set {
            lock.wait()
            defer { lock.signal() }
            _value = newValue
        }
    }
    
    public func decrementAndGet() -> Int {
        lock.wait()
        defer { lock.signal() }
        _value -= 1
        return _value
    }
    
    public func incrementAndGet() -> Int {
        lock.wait()
        defer { lock.signal() }
        _value += 1
        return _value
    }
}
