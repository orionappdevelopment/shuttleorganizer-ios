//
//  LoginView.swift
//  SwiftLogin
//
//  Created by macos on 12/15/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI
import Firebase
import GoogleSignIn
import FBSDKLoginKit

struct LoginView : View {
    
    @State var mail = ""
    @State var pass = ""
//    @State var msg = ""
//    @State var alert = MyUser.getInstance().firebaseAlert
//    @State var alertMsg = MyUser.getInstance().firebaseAlertMsg
    @ObservedObject var myUser : MyUser = MyUser.getInstance()
    @State var showSignUp = false
    @State var isLoading = false
    
//    private func signInWithEmail(email: String,password : String,completion: @escaping (Bool,String)->Void){
//
//        Auth.auth().signIn(withEmail: email, password: password) { (res, err) in
//
//            if err != nil{
//
//                completion(false,(err?.localizedDescription)!)
//                return
//            }
//
//            completion(true,(res?.user.email)!)
//        }
//    }
    
    func doSingIn(){
        self.isLoading = true
        self.hideKeyboard()
        if self.mail == "" || self.pass == "" {
            let alert = UIAlertController(title: NSLocalizedString("", comment: ""), message: NSLocalizedString("LoginView.FillMailAndPassword.Alert.text", comment: ""), preferredStyle: .alert)
            let okButton = UIAlertAction(title: NSLocalizedString("LoginView.OkButton.Alert.text", comment: ""), style: .destructive){ (_) in
                self.isLoading = false
            }
            
            alert.addAction(okButton)
            UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true)
            return
        }
        
        Auth.auth().signIn(withEmail: self.mail, password: self.pass) { (res, err) in
            defer {
                self.isLoading = false
            }
            if let signInError = err{
                MyUser.getInstance().singInError(error: signInError)
                return
            }
            
            MyUser.getInstance().signIn(authDataResult: res)
        }
//                self.signInWithEmail(email: self.user, password: self.pass) { (verified, status) in
//
//                    if !verified{
//
//                        self.alertMsg = status
//                        self.alert.toggle()
//                    }
//                    else{
//
//                        UserDefaults.standard.set(true, forKey: "status")
//                        NotificationCenter.default.post(name: NSNotification.Name("statusChange"), object: nil)
//                    }
//                }
    }
    
    func doResetPassword(){
        self.isLoading = true
        self.hideKeyboard()
        let alert = UIAlertController(title: NSLocalizedString("ResetView.Tittle.text", comment: ""), message: NSLocalizedString("ResetView.EnterMailToResetPassword.text", comment: ""), preferredStyle: .alert)
        
        alert.addTextField { (password) in
            password.placeholder = NSLocalizedString("ResetView.Mail.text", comment: "")
        }
        
        let proceed = UIAlertAction(title: NSLocalizedString("ResetView.ResetButton.text", comment: ""), style: .default) { (_) in
            
            // Sending Password Link...
            
            if alert.textFields![0].text! != ""{
                
//                withAnimation{
//
//                    self.isLoading.toggle()
//                }
                
                Auth.auth().sendPasswordReset(withEmail: alert.textFields![0].text!) { (err) in
                    defer {
                        self.isLoading = false
                    }
//                    withAnimation{
//
//                        self.isLoading.toggle()
//                    }
                    
                    if err != nil{
                        self.myUser.singInError(error: err!)
//                        self.alertMsg = err!.localizedDescription
//                        self.alert.toggle()
                        return
                    }
                    
                    // ALerting User...
                    self.myUser.setFirebaseAlert(alertTitle : "", alertMsg: NSLocalizedString("ResetView.ResetLinkSentNotification.text", comment: ""))
//                    self.alertMsg = "Password Reset Link Has Been Sent !!!"
//                    self.alert.toggle()
                }
            }else{
                self.isLoading = false
                return
            }
        }
        
        let cancel = UIAlertAction(title: NSLocalizedString("ResetView.CancelButton.text", comment: ""), style: .destructive) { (_) in
            self.isLoading = false
        }
        
        alert.addAction(cancel)
        alert.addAction(proceed)
        
        // Presenting...
        
        UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true)
    }

    
    var body : some View{
        GeometryReader { geometry in
        VStack{
            VStack{
                Image("shuttleSignIn")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 100, height: 100)
                    .padding(.bottom, 20)
                
    //            Text("LoginView.SignInTitle.text" ).fontWeight(.heavy).font(.largeTitle)
    //                .padding(.bottom, 20)
                
                VStack(alignment: .leading){
                    
                    VStack(alignment: .leading){
                        
                        Text("LoginView.Mail.text").font(.headline).fontWeight(.light).foregroundColor(Color.init(.label).opacity(0.75))
                        
                        HStack{
                            
                            TextField(toStringFromLocalizedStringKey(localizedStringKey: "LoginView.HintMail.text"), text: self.$mail)
                                .autocapitalization(.none)
    //                        if user != ""{
    //                            Image("check").foregroundColor(Color.init(.label))
    //                        }
                            
                        }
                        
                        Divider()
                        
                    }
                        .padding(.bottom, 10)
                    
                    VStack(alignment: .leading){
                        
                        Text("LoginView.Password.text").font(.headline).fontWeight(.light).foregroundColor(Color.init(.label).opacity(0.75))
                            
                        SecureField("LoginView.HintPassword.text", text: self.$pass)
                        
                        Divider()
                    }

                }
                    .padding(.horizontal, 6)
                    .padding(.bottom, 10)
                
                HStack{
                    Button(action: self.doResetPassword) {
                        
                        Text("LoginView.ForgetPasswordButton.text")
                            .underline()
                            .font(.headline).fontWeight(.light).foregroundColor(Color.init(.label).opacity(0.75))
                    }
                    Spacer()
                }
                    .padding(.horizontal, 6)
                    .padding(.bottom, 10)
                
                Button(action: {
                    self.doSingIn()
                }) {
                    
    //                Text("Sign In").foregroundColor(.white).frame(width: UIScreen.main.bounds.width - 120).padding()
                    Text("LoginView.SignInButton.text")
                        .foregroundColor(.white)
                        .frame(width: geometry.size.width - 8, height: 38)
                }
                    .background(Color(.blue).opacity(0.7))
                    .buttonStyle(PlainButtonStyle())
                    .cornerRadius(2)
                    .onTapGesture {
                        self.doSingIn()
                    }
    //            .clipShape(Capsule())
    //            .padding(.top, 25)
    //            .frame(width:UIScreen.main.bounds.width-50, height:34)
    //            .padding(EdgeInsets(top: 0, leading: 4, bottom: 10, trailing: 4))
                
                Text("LoginView.or.text").foregroundColor(Color.gray.opacity(0.5)).padding(.top,15)
            }
            .onTapGesture {
                self.hideKeyboard()
            }
            
            GoogleSignInView()
                .frame(width: geometry.size.width - 2, height: 34)
//                .cornerRadius(0)
            
            FacebookSignInView()
                .frame(height: 38).padding(EdgeInsets(top: 15, leading: 4, bottom: 10, trailing: 4))
//                .cornerRadius(-5)
            
            HStack(spacing: 8){
                
                Text("LoginView.SingUpCreateAccountText.text").foregroundColor(Color.gray.opacity(0.5))
                
                Button(action: {
                    
                    self.showSignUp.toggle()
                    
                }) {
                    
                   Text("LoginView.SingUpButton.text")
                    
                }.foregroundColor(.blue)
                
            }.padding(.top, 5)
            
        }
        }
//            .background(Image("loginBackground1"))
            .padding()
            .allowsHitTesting(!(self.isLoading))
            .alert(isPresented: self.$myUser.firebaseAlert) {
                    
    //            Alert(title: Text("Error"), message: Text(self.myUser.firebaseAlertMsg), dismissButton: .default(Text("Ok")))
                Alert(title: Text(self.myUser.firebaseAlertTitle), message: Text(self.myUser.firebaseAlertMsg), dismissButton: .default(Text("Ok")))
            }.sheet(isPresented: $showSignUp) {
                SignUpView(show: self.$showSignUp)
            }
       
    //        .onAppear {
    //
    //            NotificationCenter.default.addObserver(forName: NSNotification.Name("loginAlert"), object: nil, queue: .main) { (_) in
    //                self.alertMsg = UserDefaults.standard.value(forKey: "loginAlertMsg") as? String ?? ""
    //                self.alert = UserDefaults.standard.value(forKey: "loginAlert") as? Bool ?? false
    //            }
    //        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
