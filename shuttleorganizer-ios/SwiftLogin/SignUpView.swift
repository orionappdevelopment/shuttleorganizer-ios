//
//  SignUp.swift
//  SwiftLogin
//
//  Created by macos on 12/15/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI
import Firebase
import GoogleSignIn

struct SignUpView : View {
    
    @State var mail = ""
    @State var pass = ""
    @State var name = ""
    @State var alert = false
    @State var alertMsg = ""
    @State var isLoading = false
    @State var isSignUpSuccessful = false
    @Binding var show : Bool
    
//    private func signInAfterSignUpWithEmail(email: String,password : String,completion: @escaping (Bool,String)->Void){
//
//        Auth.auth().createUser(withEmail: email, password: password) { (res, err) in
//
//            if err != nil{
//
//                completion(false,(err?.localizedDescription)!)
//                return
//            }
//
//            completion(true,(res?.user.email)!)
//        }
//    }
    
    var body : some View{
        GeometryReader { geometry in
//        ZStack{
//        if self.isLoading {
//            LoadingView()
//        }
        
        VStack{
            
                Image("shuttleSignIn")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 100, height: 100)
                    .padding(.bottom, 20)
//                Text("SignUpView.SignUpTitle.text").fontWeight(.heavy).font(.largeTitle)
//                    .padding(.bottom, 20)
////                    .padding(.top, 10)
                
                VStack(alignment: .leading){
                    
                    VStack(alignment: .leading){
                                            
                        Text("SignUpView.Name.text").font(.headline).fontWeight(.light).foregroundColor(Color.init(.label).opacity(0.75))
                        HStack{
                            TextField("SignUpView.HintName.text", text: self.$name)
                                .autocapitalization(.none)
                        }
                        Divider()
                    }
                        .padding(.bottom, 15)
                    
                    
                    VStack(alignment: .leading){
                        
                        Text("SignUpView.Mail.text").font(.headline).fontWeight(.light).foregroundColor(Color.init(.label).opacity(0.75))
                        
                        HStack{
                            
                            TextField("SignUpView.HintMail.text", text: self.$mail)
                                .autocapitalization(.none)
                            
//                            if user != ""{
//
//                                Image("check").foregroundColor(Color.init(.label))
//                            }
                            
                        }
                        
                        Divider()
                        
                    }
                        .padding(.bottom, 15)
                    
                    VStack(alignment: .leading){
                        
                        Text("SignUpView.Password.text").font(.headline).fontWeight(.light).foregroundColor(Color.init(.label).opacity(0.75))
                            
                        SecureField("SignUpView.HintPassword.text", text: self.$pass)
                        
                        Divider()
                    }
                    
                    
                }
                    .padding(.horizontal, 8)
                    .padding(.bottom, 15)
                
                Button(action: {
                    self.doSignUp()
//                    self.isSignUpClicked.toggle()
//                    self.signInAfterSignUpWithEmail(email: self.mail, password: self.pass) { (verified, status) in
//
//                        if !verified{
//
//                            self.msg = status
//                            self.alert.toggle()
//
//                        }
//                        else{
//
//                            UserDefaults.standard.set(true, forKey: "status")
//
//                            self.show.toggle()
//
//                            NotificationCenter.default.post(name: NSNotification.Name("statusChange"), object: nil)
//                        }
//                    }
                    
                }) {
                    Text("SignUpView.SingUpButton.text").foregroundColor(.white)
                        .frame(width: geometry.size.width - 8, height: 38)
                    
                }
                    .background(Color(.blue).opacity(0.7))
                    .buttonStyle(PlainButtonStyle())
                    .cornerRadius(2)
                    .onTapGesture {
                        self.doSignUp()
                    }
            
            
        }}.padding()
            .allowsHitTesting(!(self.isLoading))
        .alert(isPresented: $alert) {
                
            Alert(title: Text("SignUpView.AlertInfoTitle.text"), message: Text(self.alertMsg), dismissButton: .default(Text("SignUpView.AlertOkButton.text")){
                    if self.isSignUpSuccessful{
                        self.show = false
                    }
                    self.isLoading.toggle()
                })
        }
        .onTapGesture {
            self.hideKeyboard()
        }
//        .background(self.isLoading ? Color.black.opacity(0.45) : Color.black.opacity(0))
    }
    
    func doSignUp(){
        
        // checking....
        self.isLoading.toggle()
        
        if self.mail == "" || self.pass == "" || self.name == "" {
            
            self.alertMsg = NSLocalizedString("SignUpView.FillContentsAlert.text", comment: "")
            self.alert.toggle()
            return
        }
        
        Auth.auth().createUser(withEmail: self.mail, password: self.pass) { (result, err) in
            
            if err != nil{
                self.alertMsg = err!.localizedDescription
                self.alert.toggle()
                return
            }
            
            let request = result?.user.createProfileChangeRequest()
            request?.displayName = self.name
            request?.commitChanges {error in
                if error != nil {
                    print("can not update display name")
                }else{
                    print("display name updated")
                }
            }
            //added to go back to sign in page with alert after Verification Link removed --start
            self.isSignUpSuccessful = true
            MyUser.getInstance().signOut()
            self.alertMsg = NSLocalizedString("SignUpView.AccountSuccessfullyCreatedAlert.text", comment: "")
            self.alert.toggle()
            //added to go back to sign in page with alert after Verification Link removed --end
            // sending Verifcation Link....
            
//            result?.user.sendEmailVerification(completion: { (err) in
//
//                if err != nil{
//                    self.alertMsg = err!.localizedDescription
//                    self.alert.toggle()
//                    return
//                }
//                self.isSignUpSuccessful = true
//                //signout added to prevent auto sing in when application restart again
//                MyUser.getInstance().signOut()
//                // Alerting User To Verify Email...
//                self.alertMsg = NSLocalizedString("SignUpView.MailVerificationSentAlert.text", comment: "")
//                self.alert.toggle()
//            })
        }
    }
    
}


struct SignUpView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpView(show: .constant(false))
    }
}
