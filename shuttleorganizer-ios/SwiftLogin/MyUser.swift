//
//  MyUser.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/24/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import GoogleSignIn
import FBSDKCoreKit

class MyUser  : ObservableObject {
    
    private static var instance : MyUser? = nil
    
//    @Published var id : String = "bQm1w3Uqithl8EJ3DEFP7CvBvI73"
    @Published var id : String = ""
    var name : String = ""
    var firebaseId : String = ""
    var firebaseEmail = ""
    var firebasePassword = ""
    var firebaseName = ""
    var firebaseisSignUp = false
    var firebaseEmail_SignUp = ""
    var firebasePassword_SignUp = ""
    var firebaseReEnterPassword = ""
    var firebaseIsLinkSend = false
    @Published var firebaseAlert = false
    @Published var firebaseAlertTitle = ""
    @Published var firebaseAlertMsg = ""
    @Published var firebaseStatus = false
    var firebasePhotoUrl : String = ""
    var error : Error? = nil
    
    private init() {
//        firebaseStatus = UserDefaults.standard.value(forKey: "status") as? Bool ?? false
        if let user = Auth.auth().currentUser{
            self.firebaseId = user.uid
            self.id = user.uid
            self.firebaseEmail = user.email ?? ""
            self.firebaseStatus = true
            self.firebaseName = user.displayName ?? ""
            self.firebasePhotoUrl = user.photoURL?.absoluteString ?? ""
        }else {
            self.firebaseStatus = false
        }
//        firebaseId = "bQm1w3Uqithl8EJ3DEFP7CvBvI73"
//        name = "willBe"
//        id = "bQm1w3Uqithl8EJ3DEFP7CvBvI73"
        print("MyUser init() is called")
    }
    
    static func getInstance() -> MyUser {
        if instance == nil {
            instance = MyUser()
            if instance?.firebaseStatus != nil && instance?.firebaseStatus == true {
                instance?.fetchPerson()
            }
        }
        return instance!
    }
    
    public func getUserId() -> String{
        print("getUserId called firebaseId:\(firebaseId)")
        return self.firebaseId
    }
    
    public func setFirebaseAlert(alertTitle : String, alertMsg : String){
        self.firebaseAlert = true
        self.firebaseAlertTitle = alertTitle
        self.firebaseAlertMsg = alertMsg
    }
    
    func removeFirebaseAlert(){
        self.firebaseAlert = false
        self.firebaseAlertMsg = ""
    }
    
    func signIn(authDataResult : AuthDataResult?){
        if let authDataResult = authDataResult{
            if authDataResult.user.email == nil {
//                UserDefaults.standard.set(false, forKey: "status")
                self.setFirebaseAlert(alertTitle : "Error", alertMsg: "User does not have email")
                return
            }
            
            //TODO: yaefe to login complete
//            if !authDataResult.user.isEmailVerified{
//                UserDefaults.standard.set(false, forKey: "status")
//                self.setFirebaseAlert(alertMsg: "Please Verify Email Address!!!")
//                return
//            }
            print("signIn self.firebaseId:\(self.firebaseId)")
            self.firebaseId = authDataResult.user.uid
            self.id = authDataResult.user.uid
            self.firebaseName = authDataResult.user.displayName ?? ""
            self.firebasePhotoUrl = authDataResult.user.photoURL?.absoluteString ?? ""
            self.firebaseEmail = authDataResult.user.email ?? ""
            self.firebaseAlert = false
            self.firebaseAlertMsg = ""
//            UserDefaults.standard.set(true, forKey: "status")
            self.firebaseStatus = true
            self.fetchPerson()

            //TODO: add backend loggedIn change update
//            SingleNotificationManager.getInstance().fetchAndSavePersonNotification(loggedInOptional: true)
            SingleNotificationManager.getInstance().registerNotifications()
        }
    }
    
    func signOut(){

        try! Auth.auth().signOut()
        GIDSignIn.sharedInstance()?.signOut()
//        UserDefaults.standard.set(false, forKey: "status")
        //TODO: add backend loggedIn change update
        SingleNotificationManager.getInstance().fetchAndSavePersonNotification(loggedInOptional: false)
        SingleNotificationManager.getInstance().unregisterNotifications()
        self.firebaseId = ""
        self.id  = ""
        self.firebasePhotoUrl = ""
//        self.firebaseAlert = false
//        self.firebaseAlertMsg = ""
        self.firebaseStatus = false
        print("signOut self.firebaseId:\(self.firebaseId)")
        SharingLocationTransportationManager.getInstance().removeAllTransportationAndDb()
        
    }
    
    func singInError(error : Error){
        self.setFirebaseAlert(alertTitle: "Error", alertMsg: error.localizedDescription)
    }
    
    //TODO: add personExist interface for that
    private func fetchPerson(){
        let person : Person = Person(id: MyUser.getInstance().firebaseId)
        print("Person:\(person) will be fetched")
        NetworkManager.fetch(url: UrlConstants.UrlGetPerson, parameters: person
            , handle: {
                (newData : Person) -> ()  in
//                    self.savedPersonName = newData.name
                    print("Person:\(newData) successfully fetched")
                    if(newData.email != self.firebaseEmail){
                        print("person email:\(newData.email) is different than firebase email:\(self.firebaseEmail)")
                    }else{
                        self.name = newData.name
                        //update firebase if display name different
                        if newData.name != self.firebaseName{
                            self.changeDisplayNameInFirebase(displayName: newData.name)
                        }
                        
                        //update firebase if profile url different
                        print("newData.id:\(newData.id)")
                        if newData.profilePictureURL != "" && newData.profilePictureURL != self.firebasePhotoUrl {
                            self.changeProfilePhotoUrlInFirebase(photoUrl: newData.profilePictureURL)
                        }else if(newData.profilePictureURL == "" && self.firebasePhotoUrl != ""){
                            let person : Person = Person(id: newData.id, name: newData.name, email: newData.email, profilePictureURL: self.firebasePhotoUrl)
                            self.savePerson(person: person)
                        }
                    }
                }
            , errorHandle: {
                newError in
                self.error = newError
                print(newError)
                print("There is an error for fetching person, so save it with Firebase info")
                let person : Person = Person(id: self.id, name: self.firebaseName, email: self.firebaseEmail, profilePictureURL: self.firebasePhotoUrl)
                self.savePerson(person: person)
                
            })
    }
    
    func changeDisplayNameInFirebase(displayName : String){
        let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
        changeRequest?.displayName = displayName
        changeRequest?.commitChanges { (error) in
            if error != nil{
                print("Can not change Display Name in firebase because of error:\(String(describing: error))")
            }
        }
    }
    
    func changeProfilePhotoUrlInFirebase(photoUrl : String){
        let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
        print("URL(fileURLWithPath: photoUrl):\(String(describing: URL(string: photoUrl)))")
        changeRequest?.photoURL = URL(string: photoUrl)
        changeRequest?.commitChanges { (error) in
            if error != nil {
                print("Can not change PhotoUrl in firebase because of error:\(String(describing: error))")
            }
        }
    }
    
    public func savePerson(person : Person){
        
        print("Will save Person: \(person)")
        NetworkManager.fetch(url: UrlConstants.UrlSavePerson, parameters: person
            , handle: {
                (newData : Person) -> ()  in
                    print("Person Saved: \(newData)")
                    self.name = newData.name
                }
            , errorHandle: {
                newError in
                self.error = newError
                print("Can not save Person \(newError)")
            })
    }
    
}
