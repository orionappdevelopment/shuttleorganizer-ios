//
//  ContentView.swift
//  Custom Login
//
//  Created by Kavsoft on 11/12/19.
//  Copyright © 2019 Kavosft. All rights reserved.
//

// import google info.plist file to avoid error
// https://kavsoft.dev/Swift/LoginFirebase/

//facebook signin
//https://swiftsenpai.com/development/facebook-login-integration-in-swift/

import SwiftUI
import Firebase
import GoogleSignIn
import FBSDKLoginKit

struct LoginContentView: View {
    
//    @State var status = UserDefaults.standard.value(forKey: "status") as? Bool ?? false
    @ObservedObject var myUser : MyUser = MyUser.getInstance()
    
    var body: some View {
        
        VStack{
            
            if self.myUser.firebaseStatus{
                
                ContentView()
//                TemporaryHomeView()
            }
            else{
                
                LoginView()
            }
            
        }.animation(.spring())
        .onAppear {
//            self.status = MyUser.getInstance().firebaseStatus
//            NotificationCenter.default.addObserver(forName: NSNotification.Name("statusChange"), object: nil, queue: .main) { (_) in
//
//                let status = UserDefaults.standard.value(forKey: "status") as? Bool ?? false
//                self.status = status
//            }
        }
    
    }
}

struct LoginContentView_Previews: PreviewProvider {
    static var previews: some View {
        LoginContentView()
    }
}

