//
//  TemporaryHome.swift
//  SwiftLogin
//
//  Created by macos on 12/15/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI
import Firebase
import GoogleSignIn
import FBSDKLoginKit

struct TemporaryHomeView : View {
    
    var body : some View{
        
        VStack{
            
            Text("Home")
            Text("firebaseEmail:" + MyUser.getInstance().firebaseEmail)
            Text("firebaseId:" + MyUser.getInstance().firebaseId)
            Button(action: {
                
//                try! Auth.auth().signOut()
//                GIDSignIn.sharedInstance()?.signOut()
//                UserDefaults.standard.set(false, forKey: "status")
//                NotificationCenter.default.post(name: NSNotification.Name("statusChange"), object: nil)
                MyUser.getInstance().signOut()
                
            }) {
                
                Text("Logout")
            }
        }
    }
}

struct TemporaryHomeView_Previews: PreviewProvider {
    static var previews: some View {
        TemporaryHomeView()
    }
}
