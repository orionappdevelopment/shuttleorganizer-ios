//
//  FacebookSignInView.swift
//  SwiftLogin
//
//  Created by macos on 12/15/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI
import Firebase
import GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit

struct FacebookSignInView : UIViewRepresentable {
    func makeCoordinator() -> FacebookSignInView.Coordinator {
        return FacebookSignInView.Coordinator()
    }
    
    func makeUIView(context: UIViewRepresentableContext<FacebookSignInView>) -> FBLoginButton {
        //add to remove
        //NSLayoutConstraint:0x1c42828f0 FBSDKLoginButton:0x104432fd0'Log in'.height == 28 (active)>
        let facebookLoginButton = FBLoginButton()
        facebookLoginButton.removeConstraints(facebookLoginButton.constraints)
        facebookLoginButton.permissions = ["email"]
        facebookLoginButton.delegate = context.coordinator
        return facebookLoginButton
        
        
//        let button = FBLoginButton()
//        button.permissions = ["email"]
//        button.delegate = context.coordinator
//        return button
        
    }
    
    func updateUIView(_ uiView: FBLoginButton, context: UIViewRepresentableContext<FacebookSignInView>) {
        print("Facebook login updateUIView")
    }
    
    class Coordinator : NSObject, LoginButtonDelegate{
        func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
            if let signInError = error {
                print(signInError.localizedDescription)
                MyUser.getInstance().singInError(error: signInError)
                return
            }
            if AccessToken.current != nil {
                let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
                Auth.auth().signIn(with: credential) { (res, err) in
                    
//                    if let user = res?.user {
//                        let uid = user.uid
//                        let email = user.email
//                        let photoURL = user.photoURL
//                        print("uid:\(uid) email:\(email) photoURL:\(photoURL)")
//                    }
                    
                    if let signInError = err {
                        MyUser.getInstance().singInError(error: signInError)
                        return
                    }
                    
                    
                    print("Successfully Login With Facebook")
//                    UserDefaults.standard.set(true, forKey: "status")
//                    NotificationCenter.default.post(name: NSNotification.Name("statusChange"), object: nil)
                    MyUser.getInstance().signIn(authDataResult: res)
                }
            }
        }
        
        func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
//            try! Auth.auth().signOut()
//            print("Facebook sign out" )
            MyUser.getInstance().signOut()
        }
        
        
    }
    
}

struct FacebookSignInView_Previews: PreviewProvider {
    static var previews: some View {
        FacebookSignInView()
    }
}
