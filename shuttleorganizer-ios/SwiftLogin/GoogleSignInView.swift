//
//  GoogleSignInView.swift
//  SwiftLogin
//
//  Created by macos on 12/15/20.
//  Copyright © 2020 Orion. All rights reserved.
//
import SwiftUI
import Firebase
import GoogleSignIn
import FBSDKLoginKit

struct GoogleSignInView : UIViewRepresentable {
    func makeUIView(context: UIViewRepresentableContext<GoogleSignInView>) -> GIDSignInButton {
        
        let button = GIDSignInButton()
        button.colorScheme = .light
        button.style = .wide
        GIDSignIn.sharedInstance()?.presentingViewController = UIApplication.shared.windows.first?.rootViewController
        return button
        
    }
    
    func updateUIView(_ uiView: GIDSignInButton, context: UIViewRepresentableContext<GoogleSignInView>) {
        print("updateUIView")
    }
}

struct GoogleSignInView_Previews: PreviewProvider {
    static var previews: some View {
        GoogleSignInView()
    }
}
