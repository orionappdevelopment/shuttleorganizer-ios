//
//  TransportationTypeEnum.swift
//  Landmarks
//
//  Created by macos on 5/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation


public enum ShuttleNotificationTypeEnum : Int , CaseIterable, Codable, Hashable, CustomStringConvertible {
    case MANAGEMENT_NOTIFICATION = 0
    case TRANSPORTATION_NOTIFICATION = 1
    case CHAT_MESSAGE_NOTIFICATION = 2
    case NONE = 9

    public static func toShuttleNotificationTypeEnum(_ value : Int) -> ShuttleNotificationTypeEnum {
        // retrieve status from status code
        if (value == MANAGEMENT_NOTIFICATION.rawValue) {
            return MANAGEMENT_NOTIFICATION;
        } else if (value == TRANSPORTATION_NOTIFICATION.rawValue) {
            return TRANSPORTATION_NOTIFICATION;
        } else if (value == CHAT_MESSAGE_NOTIFICATION.rawValue) {
            return CHAT_MESSAGE_NOTIFICATION;
        } else {
            return NONE;
        }
    }
    
    public var description: String {
        if (self == ShuttleNotificationTypeEnum.MANAGEMENT_NOTIFICATION) {
            return "MANAGEMENT_NOTIFICATION";
        } else if (self == ShuttleNotificationTypeEnum.TRANSPORTATION_NOTIFICATION) {
            return "TRANSPORTATION_NOTIFICATION";
        } else if (self == ShuttleNotificationTypeEnum.CHAT_MESSAGE_NOTIFICATION) {
            return "CHAT_MESSAGE_NOTIFICATION";
        } else {
            return "NONE"
        }
    }
}
