//
//  SingleNotificationManager.swift
//  LocationBackground
//
//  Created by macos on 1/10/21.
//  Copyright © 2021 Orion. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftUI
import UserNotifications
import UIKit
import Firebase
import FirebaseMessaging
import GoogleMaps

class SingleNotificationManager : NSObject {
    let gcmMessageIDKey = "gcm.message_id"
    var personNotification : PersonNotification = PersonNotification(personId: MyUser.getInstance().getUserId(), token: "")
    
    private static var instance : SingleNotificationManager? = nil
    private(set) var isNotificationGranted : Bool? = nil

    private var notificationOfInitApp : ShuttleNotificationModel? = nil
    var isWindowsCreated : Bool = false
    
    public static func getInstance() -> SingleNotificationManager{
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        if SingleNotificationManager.instance == nil{
            SingleNotificationManager.instance = SingleNotificationManager()
        }
            
        return SingleNotificationManager.instance!
    }
    
    private override init() {
        super.init()
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
    }
    
    func unregisterNotifications() {
        DispatchQueue.main.async {
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        }
//        DispatchQueue.main.async {
//            UIApplication.shared.unregisterForRemoteNotifications()
//        }
    }
    
    func registerNotifications() {
        
//        //Start setting up Cloud Messaging
//        Messaging.messaging().delegate = self
//        //End setting up Cloud Messaging
//
//        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (granted:Bool, error:Error?) in
            if error != nil {
                print(error?.localizedDescription ?? "register notification error is nil")
                return
            }
            if granted {
                print("permission granted")
                self.isNotificationGranted = true
            }
            
            
            //start authorize for remote notifications, it can be anywhere
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
            //end authorize for remote notifications, it can be anywhere
        }
        
        self.savePersonNotification(loggedInOptional: true)
    }
    
    func scheduleLocalNotification(alert:String) {
        //comment out because no need for 03.04.2021
//        let content = UNMutableNotificationContent()
//        let requestIdentifier = UUID.init().uuidString
//
//        content.badge = 0
//        content.title = "Location Update"
//        content.body = alert
//        content.sound = UNNotificationSound.default
//
//        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1.0, repeats: false)
//
//        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
//
////        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
//        UNUserNotificationCenter.current().add(request) { (error:Error?) in
//
//            if error != nil {
//                print("request local notification error")
//                print(error?.localizedDescription ?? "")
//            }
//            print("Notification Request Success")
//        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        debugPrint(error.localizedDescription)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("Device Register For Remote Notification for deviceToken : \(deviceToken)")
    }
    
}

//Cloud Messaging
extension SingleNotificationManager : MessagingDelegate{
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(String(describing: fcmToken))")
        
        let dataDict:[String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        print("self.personNotification.token :" + self.personNotification.token)
        if let token : String = fcmToken, token != self.personNotification.token {
            //TODO: add backend loggedIn change updateIn
            self.personNotification.token = token
            //this part is calling before user sign in so below part will give error, so below part comment outs
            self.fetchAndSavePersonNotification(loggedInOptional: true)
        }
        
    }
}

//User Notifications ...
@available(iOS 10, *)
extension SingleNotificationManager : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        print("willPresent:\(notification.request)")
        print("notification content : \(notification.request.content)")
        
        //Do something with Msg Data
        if let messageID = userInfo[gcmMessageIDKey]{
            print("Message ID: \(messageID)")
        }
       
        //    UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        
        // Print full message.
        print("Notification came to method 1, when application is open")
        print("userinfo \(userInfo)")
        
        // Change this to your preferred presentation option
//        completionHandler([[.banner, .badge, .sound]])
        completionHandler([[.banner, .badge, .sound, .list]])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        // ...
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        //Do something with Msg Data
        if let messageID = userInfo[gcmMessageIDKey]{
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print("Notification came to method 2, when notification open")
        print(userInfo)
        if let shuttleNotificationModel = SingleNotificationManager.parseShuttleNotificationModel(userInfo: userInfo){
            handleNotification(shuttleNotificationModel: shuttleNotificationModel)
        }
        
        completionHandler()
    }

    func handleNotification(shuttleNotificationModel : ShuttleNotificationModel){
        DispatchQueue.main.async {
            UIApplication.shared.applicationIconBadgeNumber = 0
//            let nc = NotificationCenter.default
//            nc.post(name: Notification.Name("PushNotification_ChatMessage"), object: shuttleNotificationModel)
        }
        UNUserNotificationCenter.current().getPendingNotificationRequests { (notificationRequests) in
            let identifiers: [String] = SingleNotificationManager.getRedundantNotificationsWithCurrentNotification(shuttleNotificationModel: shuttleNotificationModel, notificationRequests: notificationRequests)
            
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: identifiers)
        }
        
        UNUserNotificationCenter.current().getDeliveredNotifications { (notifications) in
            let notificationRequests = notifications.map { $0.request }
            
            let identifiers: [String] = SingleNotificationManager.getRedundantNotificationsWithCurrentNotification(shuttleNotificationModel: shuttleNotificationModel, notificationRequests: notificationRequests)
            
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: identifiers)
        }
        
        if self.isWindowsCreated{
            self.postNotification(shuttleNotificationModelOptional: shuttleNotificationModel)
        }else{
            self.notificationOfInitApp = shuttleNotificationModel
        }
//        UNUserNotificationCenter.current().getPendingNotificationRequests { (notifications) in
//            print("Count1: \(notifications.count)")
//            UIApplication.shared.applicationIconBadgeNumber = notifications.count
//        }
//
//        UNUserNotificationCenter.current().getDeliveredNotifications { (notifications) in
//            print("Count2: \(notifications.count)")
//            UIApplication.shared.applicationIconBadgeNumber = notifications.count
//        }
    }
    
    func postNotification(shuttleNotificationModelOptional : ShuttleNotificationModel? = nil){
        if let shuttleNotificationModel = self.notificationOfInitApp{
            self.notificationOfInitApp = nil
            let notificationName = ShuttleNotificationEventFactory.getInstance().getEventNotificationName(shuttleNotificationTypeEnum: shuttleNotificationModel.getShuttleNotificationTypeEnum())
            NotificationCenter.default.post(name: notificationName, object: ShuttleNotificationWrapper(shuttleNotificationModel: shuttleNotificationModel))
        }else if let shuttleNotificationModel = shuttleNotificationModelOptional{
            let notificationName = ShuttleNotificationEventFactory.getInstance().getEventNotificationName(shuttleNotificationTypeEnum: shuttleNotificationModel.getShuttleNotificationTypeEnum())
            NotificationCenter.default.post(name: notificationName, object: ShuttleNotificationWrapper(shuttleNotificationModel: shuttleNotificationModel))
        }
    }
    
//    func fetchPersonNotification(){
//        if MyUser.getInstance().token == ""{
//            print("error can not fetch person Notification because token:\(MyUser.getInstance().token)")
//            return
//        }
//
//        let personNotification : PersonNotification = PersonNotification(personId: MyUser.getInstance().id, token: MyUser.getInstance().token)
//
//        NetworkManager.fetch(url: UrlConstants.UrlGetNotification, parameters: personNotification
//            , handle: {
//                (newData : PersonNotification) -> ()  in
//                    self.personNotification = newData
//                }
//            , errorHandle: {
//                newError in
//                    let errorString : String = "\(newError)"
//                    if errorString .contains("keyNotFound(CodingKeys(stringValue: \"personId\", intValue: nil)"){
//                        print("Can not found PersonNotification so directly create PersonNotification")
//                        self.savePersonNotification()
//                    }else{
//                        print(newError)
//                    }
//
//                })
//    }
//
    
    
    static func parseShuttleNotificationModel(userInfo : [AnyHashable : Any]) -> ShuttleNotificationModel? {
        do {
            if let jsonString : String = userInfo["shuttleNotificationModel"] as? String, let data : Data = jsonString.data(using: .utf8){
                let shuttleNotificationModel : ShuttleNotificationModel = try JSONDecoder().decode(ShuttleNotificationModel.self, from: data)
                print("shuttleNotificationModel.shuttleNotificationType: \(shuttleNotificationModel.shuttleNotificationType)")
                print("shuttleNotificationModel: \(shuttleNotificationModel)")
                return shuttleNotificationModel
            }
        } catch let error {
            print("shuttleNotificationModel can not parse when notification came: \(error.localizedDescription)")
        }
        
        return nil
    }
    
    static func getRedundantNotificationsWithCurrentNotification(shuttleNotificationModel : ShuttleNotificationModel ,notificationRequests : [UNNotificationRequest]) -> [String]{
        var identifiers: [String] = []
        for notification:UNNotificationRequest in notificationRequests {
            if let waitingShuttleNotificationModel = SingleNotificationManager.parseShuttleNotificationModel(userInfo: notification.content.userInfo){
                if shuttleNotificationModel.getShuttleNotificationTypeEnum() == .CHAT_MESSAGE_NOTIFICATION &&
                    waitingShuttleNotificationModel.getShuttleNotificationTypeEnum() == .CHAT_MESSAGE_NOTIFICATION{
                    if waitingShuttleNotificationModel.transportationId == shuttleNotificationModel.transportationId{
                        identifiers.append(notification.identifier)
                    }
                }else if shuttleNotificationModel.getShuttleNotificationTypeEnum() == .TRANSPORTATION_NOTIFICATION &&
                            waitingShuttleNotificationModel.getShuttleNotificationTypeEnum() == .TRANSPORTATION_NOTIFICATION{
                    if waitingShuttleNotificationModel.transportationId == shuttleNotificationModel.transportationId &&
                        waitingShuttleNotificationModel.senderPersonId == shuttleNotificationModel.senderPersonId{
                        identifiers.append(notification.identifier)
                    }
                }
            }
        }
        return identifiers
    }
    
    func savePersonNotification(loggedInOptional : Bool? = nil){

        if self.personNotification.token == ""{
            print("error SingleNotificationManager savePersonNotification can not fetch personNotification because token:\(self.personNotification.token)")
            return
        }
        
        if MyUser.getInstance().getUserId() == ""{
            print("error SingleNotificationManager savePersonNotification can not fetch personNotification because id:\(MyUser.getInstance().getUserId())")
            return
        }
        
        if let loggedIn = loggedInOptional{
            self.personNotification.loggedIn = loggedIn
        }

        NetworkManager.fetch(url: UrlConstants.UrlSaveNotification, parameters: self.personNotification
            , handle: {
                (newData : PersonNotification) -> ()  in
                    print("PersonNotification Saved: \(newData)")
                    self.personNotification = newData
                }
            , errorHandle: { newError in
                print("SavePersonNotification with loggedIn false Error:\(newError)")
            })
    }
    
    func fetchAndSavePersonNotification(loggedInOptional : Bool? = nil){
        if self.personNotification.token == ""{
            print("error fetchAndSavePersonNotification can not fetch personNotification because token:\(self.personNotification.token) self.personNotification:\(self.personNotification)")
            return
        }
        
        if MyUser.getInstance().getUserId() == ""{
            print("error fetchAndSavePersonNotification can not fetch personNotification because id:\(MyUser.getInstance().getUserId())")
            return
        }
        
        let personNotification : PersonNotification = PersonNotification(personId: MyUser.getInstance().getUserId(), token: self.personNotification.token)
        
        NetworkManager.fetch(url: UrlConstants.UrlGetNotification, parameters: personNotification
            , handle: {
                (newData : PersonNotification) -> ()  in
                    self.personNotification = newData
                    if let loggedIn = loggedInOptional{
                        self.personNotification.loggedIn = loggedIn
                    }
                
                    NetworkManager.fetch(url: UrlConstants.UrlSaveNotification, parameters: self.personNotification
                        , handle: {
                            (newData : PersonNotification) -> ()  in
                                print("PersonNotification Saved: \(newData)")
//                                if let loggedIn = loggedInOptional, loggedIn == false{
//                                    self.personNotification = PersonNotification(personId: "", token: "")
//                                }else{
                                    self.personNotification = newData
//                                }
                        }
                        , errorHandle: { newError in
                            print("SavePersonNotification with loggedIn false Error:\(newError)")
                        })
                }
            , errorHandle: {
                newError in
                    let errorString : String = "\(newError)"
                    if errorString .contains("keyNotFound(CodingKeys(stringValue: \"personId\", intValue: nil)"){
                        print("Can not found PersonNotification so directly create PersonNotification")
                        self.savePersonNotification(loggedInOptional: loggedInOptional)
                    }else{
                        print(newError)
                    }
                    
                })
    }
    
}
