//
//  SingleNotificationManager.swift
//  LocationBackground
//
//  Created by macos on 1/10/21.
//  Copyright © 2021 Orion. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftUI
import UserNotifications
import UIKit

class SendNotificationManager : NSObject {
    
    private static var instance : SendNotificationManager? = nil
    
    public static func getInstance() -> SendNotificationManager{
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        if SendNotificationManager.instance == nil{
            SendNotificationManager.instance = SendNotificationManager()
        }
            
        return SendNotificationManager.instance!
    }
    
    public func sendNotificationForTransportation(transportation : Transportation, body : String, notificationType : ShuttleNotificationTypeEnum){
        
        //fetch persons of transportation
        NetworkManager.fetch(url: UrlConstants.UrlGetPersonNotificationList, parameters: transportation
            , handle: {
                (newData : [PersonNotification]) in
                let personNotificationList = newData
                if personNotificationList.isEmpty == false {
                    var mapParams : [String : String] = [:]
                    var personTokens : [String] = []
//                    mapParams["shuttleNotificationType"] = notificationType.rawValue.description
//                    mapParams["thread-id"] = notificationType.rawValue.description
                    mapParams["body"] = body
                    mapParams["title"] = transportation.name
                    for personNotification in personNotificationList {
                        if personNotification.loggedIn && personNotification.personId != MyUser.getInstance().getUserId()
                            && personNotification.getNotifications()[notificationType.rawValue]{
                            personTokens.append(personNotification.token)
                        }
                    }
                    if personTokens.count == 0{
                        print("There is 0 personTokens to send Notifications personTokens:\(personTokens)")
                        return
                    }
                    mapParams["token"] = Utils.toJsonArray(from: personTokens)
                    
                    if transportation.id == nil{
                        print("Error: Transportation Id is nil, transportation value will be invalid at notification")
                    }
                    
                    let shuttleNotificationModel = ShuttleNotificationModel(shuttleNotificationType: notificationType.rawValue, transportationId: transportation.id ?? "", transportationName: transportation.name, senderPersonId: MyUser.getInstance().getUserId(), senderPersonName: MyUser.getInstance().firebaseName)
                    
                    do {
                        let jsonData = try JSONEncoder().encode(shuttleNotificationModel)
                        let json = String(data: jsonData, encoding: String.Encoding.utf8)
                        mapParams["shuttleNotificationModel"] = json
                    } catch let error {
                        print("Parameters JSONSerialization Error during SendingNotificationManager: \(error.localizedDescription)")
                    }
                    
                    
                    NetworkManager.fetch(url: UrlConstants.UrlSendNotification, parameters: mapParams
                        , handle: {
                            (newData : SendNotificationResponse) -> ()  in
                                print("Start Result of SendTransportationNotification")
                                for result : SendNotificationResponse.NotificationResult in newData.results{
                                    print("result : \(result)")
                                }
                                print("End Result of SendTransportationNotification")
                            }
                        , errorHandle: {
                            newError in
                                print("Error: newError can not send notification error:\(newError)")
                            })
                }
                
            }
            , errorHandle: {
                newError in
                    print("Error in sendNotificationForTransportationInfoChange error:\(newError)")
                
            })
    }
    
}
