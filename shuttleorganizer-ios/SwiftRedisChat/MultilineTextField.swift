//
//  MultilineTextField.swift
//  SwiftRedisChat
//
//  Created by macos on 9/20/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI
import UIKit

fileprivate class PassThroughTextView: UITextView {
    @State var isTappedToEdit : Bool = false
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        if let next = next {
//            next.touchesBegan(touches, with: event)
//        } else {
//            super.touchesBegan(touches, with: event)
//        }
        self.isTappedToEdit = true
        
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        if let next = next {
//            next.touchesEnded(touches, with: event)
//        } else {
//            super.touchesEnded(touches, with: event)
//        }
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
//        if let next = next {
//            next.touchesCancelled(touches, with: event)
//        } else {
//            super.touchesCancelled(touches, with: event)
//        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//        if let next = next {
//            next.touchesMoved(touches, with: event)
//        } else {
//            super.touchesMoved(touches, with: event)
//        }
    }
}

fileprivate struct UITextViewWrapper: UIViewRepresentable {
    typealias UIViewType = UITextView

    @Binding var text: String
    @Binding var calculatedHeight: CGFloat
    var onDone: (() -> Void)?

//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//            if let next = next {
//                next.touchesBegan(touches, with: event)
//            } else {
//                super.touchesBegan(touches, with: event)
//            }
//        }
    
    func makeUIView(context: UIViewRepresentableContext<UITextViewWrapper>) -> UITextView {
        let textField = PassThroughTextView()
        textField.delegate = context.coordinator

        textField.isEditable = true
        textField.font = UIFont.preferredFont(forTextStyle: .body)
        textField.isSelectable = true
        textField.isUserInteractionEnabled = true
        textField.isScrollEnabled = false
        textField.backgroundColor = UIColor.clear
        if nil != onDone {
            textField.returnKeyType = .done
        }

        textField.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        
        return textField
    }

    func updateUIView(_ uiView: UITextView, context: UIViewRepresentableContext<UITextViewWrapper>) {
        if uiView.text != self.text {
            uiView.text = self.text
        }
        if  uiView.window != nil, !uiView.isFirstResponder, uiView is PassThroughTextView{
            if let passThroughTextView = uiView as? PassThroughTextView, passThroughTextView.isTappedToEdit{
                uiView.becomeFirstResponder()
            }
        }
        
//        if uiView.window != nil, !uiView.isFirstResponder {
//            uiView.becomeFirstResponder()
//        }
//        if uiView.window != nil, uiView.isFirstResponder {
//            uiView.resignFirstResponder()
//        }
        UITextViewWrapper.recalculateHeight(view: uiView, result: $calculatedHeight)
    }

    fileprivate static func recalculateHeight(view: UIView, result: Binding<CGFloat>) {
        let newSize = view.sizeThatFits(CGSize(width: view.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        if result.wrappedValue != newSize.height {
            DispatchQueue.main.async {
                result.wrappedValue = newSize.height // !! must be called asynchronously
            }
        }
    }

    func makeCoordinator() -> Coordinator {
        return Coordinator(text: $text, height: $calculatedHeight, onDone: onDone)
    }

    final class Coordinator: NSObject, UITextViewDelegate {
        var text: Binding<String>
        var calculatedHeight: Binding<CGFloat>
        var onDone: (() -> Void)?

        init(text: Binding<String>, height: Binding<CGFloat>, onDone: (() -> Void)? = nil) {
            self.text = text
            self.calculatedHeight = height
            self.onDone = onDone
        }

        func textViewDidChange(_ uiView: UITextView) {
            text.wrappedValue = uiView.text
            UITextViewWrapper.recalculateHeight(view: uiView, result: calculatedHeight)
        }

        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if let onDone = self.onDone, text == "\n" {
                textView.resignFirstResponder()
                if textView is PassThroughTextView{
                    let passThroughTextView = textView as! PassThroughTextView
                    passThroughTextView.isTappedToEdit = false
                }
                onDone()
                return false
            }
            return true
        }
        
    }
}

struct MultilineTextField: View {

    private var placeholder: String
    private var onCommit: (() -> Void)?

    @Binding private var text: String
//    private var internalText: Binding<String> {
//        Binding<String>(get: { self.text } ) {
//            self.text = $0
//            self.showingPlaceholder = $0.isEmpty
//        }
//    }

    @State private var dynamicHeight: CGFloat = 5
//    @Binding private var showingPlaceholder : Bool

    init (_ placeholder: String = "", text: Binding<String>, onCommit: (() -> Void)? = nil) {
        self.placeholder = placeholder
        self.onCommit = onCommit
        self._text = text
//        self._showingPlaceholder = State<Bool>(initialValue: self.text.isEmpty)
    }

    var body: some View {
        UITextViewWrapper(text: self.$text, calculatedHeight: $dynamicHeight, onDone: onCommit)
            .frame(minHeight: dynamicHeight, maxHeight: dynamicHeight)
            .background(placeholderView, alignment: .topLeading)
    }

    var placeholderView: some View {
        Group {
            if self.text.isEmpty {
                Text(placeholder).foregroundColor(.gray)
                    .padding(.leading, 4)
                    .padding(.top, 8)
            }
        }
    }
}

#if DEBUG
struct MultilineTextField_Previews: PreviewProvider {
    static var test:String = ""//some very very very long description string to be initially wider than screen"
    static var testBinding = Binding<String>(get: { test }, set: {
//        print("New value: \($0)")
        test = $0 } )

    static var previews: some View {
        VStack(alignment: .leading) {
            Text("Description:")
            MultilineTextField("Enter some text here", text: testBinding, onCommit: {
                print("Final text: \(test)")
            })
                .overlay(RoundedRectangle(cornerRadius: 4).stroke(Color.black))
            Text("Something static here...")
            Spacer()
        }
        .padding()
    }
}
#endif
