//
//  ChatView.swift
//  SwiftRedisChat
//
//  Created by macos on 8/29/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct GpsChatView: View, RedisCallback {
    @ObservedObject var chats : GenericObservableModel<String> = GenericObservableModel<String>(value: "")
    @State var num = 0
//    var g : RedisGpsOneProcess
//    var g: RedisGpsProcess
    var g: RedisGpsProcessWithOneInterface
    
    init(){
//        g = RedisGpsOneProcess(channel: "asdasd", redisCallback: nil)
//        g = RedisGpsProcess(channel: "asdasd", redisCallback: nil)
        g = RedisGpsProcessWithOneInterface(channel: "asdasd", redisCallback: nil)
        g.redisCallback = self
        g.initialize()
    }
    
    var body: some View {
        VStack {
            Text(self.chats.value)
            Button(action: self.sendMessage) {
                Text("Send")
            }
        }
    }
    
    func sendMessage() {
        num += 1
        g.publish(message: String(num) )
//        g.getFirstGps()
    }
    
    func commandExecuted(message: String) {
        self.chats.value.append(message)
        print("command Executed with message  \(message)")
    }
    
    func commandFailed() {
        self.chats.value.append("ERROR")
    }

    
}


struct GpsChatView_Previews: PreviewProvider {
    static var previews: some View {
        GpsChatView()
    }
}
