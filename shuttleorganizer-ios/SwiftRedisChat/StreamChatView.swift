//
//  StreamChatView.swift
//  SwiftRedisChat
//
//  Created by macos on 9/12/20.
//  Copyright © 2020 Orion. All rights reserved.
//
import SwiftUI

struct StreamChatView: View, MyFunction {
    @ObservedObject var chats : GenericObservableModel<[ChatMessage]> = GenericObservableModel<[ChatMessage]>(value: [])
    @State var num = 0
    @State private var isLoadingShowing = false
    @ObservedObject var isPresented: GenericObservableModel<Bool>
    var transportation : Transportation
    var senderId : String
    var senderName : String
    
    var channel : String// = "chat2_" + "faTxShA8br"
    //channel = "1"
    var chatHistory : ChatHistory
//    var g : RedisGpsOneProcess
//    var g: RedisGpsProcess
    var redisGroupChatProcess: RedisGroupChatProcess
    @State var text: String = ""
    
    init(transportation: Transportation, senderId : String, senderName : String, isPresented : GenericObservableModel<Bool>){
//        g = RedisGpsOneProcess(channel: "asdasd", redisCallback: nil)
//        g = RedisGpsProcess(channel: "asdasd", redisCallback: nil)
        
        if #available(iOS 14.0, *) {
            // iOS 14 doesn't have extra separators below the list by default.
        } else {
            // To remove only extra separators below the list:
            UITableView.appearance().tableFooterView = UIView()
        }
        // To remove all separators including the actual ones:
        UITableView.appearance().separatorStyle = .none
        
        self.isPresented = isPresented
        self.transportation = transportation
        self.senderId = senderId
        self.senderName = senderName
        self.channel = RedisConstants.getRedisChatChannelId(transportationId: self.transportation.id!)
        self.chatHistory = ChatHistory(channel: self.channel)
        self.redisGroupChatProcess = RedisGroupChatProcess()
        self.redisGroupChatProcess.subscribe(channel : channel, callback : self)
        self.redisGroupChatProcess.initialize()
//        self.redisGroupChatProcess.getOldChatMessages(channel: self.channel, callback: self)
        
    }
    
    var body: some View {
        VStack{
        VStack {
            HStack{
                Spacer()
                Text(self.transportation.name)
                    .padding(EdgeInsets(top: 10, leading: 10, bottom: 0, trailing: 10))
                Spacer()
                
                VStack{
                    Image(systemName:"xmark.circle.fill")
                        .renderingMode(.template)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 25, height: 25, alignment: .center)
                        .foregroundColor(Color.colorIosCloseButtonOutsideXColor)
                        .background(Color.colorIosCloseButtonInsideXColor)
                        .clipShape(Circle())
                        .onTapGesture {
                            self.isPresented.value = false
                        }
                }.padding(EdgeInsets(top: 10, leading: 10, bottom: 0, trailing: 10))
            }
            Divider()
//            CustomScrollView(scrollToEnd: true){
            List{
                VStack{
//                    Spacer()
                    ForEach(self.chats.value) { chat in
                        if chat.messageType == MessageTypeEnum.DATE{
                            SingleDateView(message: chat)
                                .rotationEffect(.radians(.pi))
                                .scaleEffect(x: -1, y: 1, anchor: .center)
                        }else{
                            SingleChatMessageView(message: chat)
                                .rotationEffect(.radians(.pi))
                                .scaleEffect(x: -1, y: 1, anchor: .center)
                        }
        //                Text("\($0.senderName): \($0.content)")
        //                    .scaleEffect(x: 1, y: -1, anchor: .center)
                    }
                .padding(.bottom, 5)
                }
                .background(
                    //https://stackoverflow.com/questions/62588015/swiftui-get-current-scroll-position-from-scrollview
                    GeometryReader {
                        Color.clear.preference(key: ViewOffsetKey.self,
                            value: -$0.frame(in: .named("scroll")).origin.y)
                    })
                    .onPreferenceChange(ViewOffsetKey.self) {
//                        print("offset >> \($0)")
                        let offset = $0
                        if offset < 0 && self.isLoadingShowing == false{
                            self.isLoadingShowing = true
                            self.redisGroupChatProcess.getOldChatMessages(channel: self.channel, callback: self)
//                            self.isLoadingShowing = false
                        }
                    }
                
                .padding(.top, -5)
            }
//                .gesture(
//                    DragGesture().onChanged { value in
//                        print("DragGesture value: \(value)")
//                        print("DragGesture value.translation.height: \(value.translation.height)")
//                        if value.translation.height > 0 {
//                            print("Scroll down")
//                        } else {
//                            print("Scroll up")
//                         }
//                      }
//                   }
//                )
                .scaleEffect(x: -1, y: 1, anchor: .center)
                .rotationEffect(.radians(.pi))
            
            Divider()
            HStack {
//                TextField("Type a message", text: $text).textFieldStyle(RoundedBorderTextFieldStyle())
                VStack{
                    MultilineTextField(toStringFromLocalizedStringKey(localizedStringKey: "StreamChatView.SendMessageHint.text"), text: $text)
                }
                    .border(Color.gray, width: 1)
                Button(action: self.sendMessage) {
//                    Text("Send")
                    ZStack{
                        Circle().frame(width: 34, height: 34, alignment: .center)
                            .foregroundColor(.green)
                        Image("send")
                            .renderingMode(.template)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 22.0, height: 22.0)
                            .foregroundColor(.white)
                    }
                }
            }.padding(.horizontal, 5)
            Divider()
        }
        .keyboardViewAllMove()
        .keyboardIgnoreSafeArea()
        .onTapGesture {
            self.hideKeyboard()
        }
        }

//            .pullToRefresh(isShowing: self.$isLoadingShowing) {
//                   self.g.getOldChatMessages(channel: self.channel, callback: self)
//                   self.isLoadingShowing = false
//               }
        
       
//        .navigationBarTitle("General")
//        .onAppear(perform: onAppear)
    }
    
    
    func sendMessage() {
        let encoder = JSONEncoder()
        
        let msg = text
        if (msg.count == 0) {
            return
        } else {
            let cm = ChatMessageFactory.getInstance().getChatMessage(
                content: msg,
                senderId: self.senderId,
                senderName: self.senderName,
                messageType: MessageTypeEnum.TEXT)
            do {
                let json = try encoder.encode(cm)
                if let content = String(data: json, encoding: .utf8) {
                    // here `content` is the JSON data decoded as a String
                    print(content)
                    redisGroupChatProcess.publish(channel: channel, message: content)
                    
                    let notificationBody = toStringFromLocalizedStringKey(localizedStringKey: "Notification.ChatMessageSend.text", self.senderName, text)
                    SendNotificationManager.getInstance().sendNotificationForTransportation(transportation: transportation, body: notificationBody, notificationType: .CHAT_MESSAGE_NOTIFICATION)
                    
                    self.text = ""
                    
                    
                }
            } catch  {
                print("can not encode the messsage")
            }
        }
    }
        
    func apply(_ chatList: [String]) -> Bool {
        let result = chatHistory.add(chats: chatList)
        let messageList = chatHistory.getChatsArrayList()
//        var str = ""
//        for chat in messageList{
//            str += chat.content
//            str += "\n"
//        }
//        print("Chat String = \(str))")
//        self.chats.value.insert(messageList[messageList.count - 1], at: 0)
//        self.chats.value.append(messageList[messageList.count - 1])
        let reversedMessageList = Array(messageList.reversed())
        self.chats.value = reversedMessageList
//        self.chats.value.append(str)
        self.isLoadingShowing = false
        return result
    }
    
    func onAppear() {
//        channel.watch(options: [.all]) {
//            switch $0 {
//            case .success(let response):
//                self.messages += response.messages
//            case .failure(let error):
//                break
//            }
//        }
//
//        channel.subscribe(forEvents: [.messageNew]) {
//            switch $0 {
//            case .messageNew(let message, _, _, _):
//                self.messages += [message]
//            default:
//                break
//            }
//        }
    }
}

struct ViewOffsetKey: PreferenceKey {
    typealias Value = CGFloat
    static var defaultValue = CGFloat.zero
    static func reduce(value: inout Value, nextValue: () -> Value) {
        value += nextValue()
    }
}

struct StreamChatView_Previews: PreviewProvider {
    static var previews: some View {
        StreamChatView(transportation: Transportation(id: "id", name: "name", latitude: 1, longitude: 1, transportationType: 1, days: 2, status: 2, creatorPersonId: "creatorPersonId", startTime: LTime(hour: 6, minute: 0, second: 0), timeZone: 1),
            senderId: "1", senderName: "1", isPresented: GenericObservableModel(value: true))
    }
}
