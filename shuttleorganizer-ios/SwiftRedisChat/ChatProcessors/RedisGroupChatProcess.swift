//
//  RedisGroupChatProcess.swift
//  SwiftRedisChat
//
//  Created by macos on 9/8/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation

public class RedisGroupChatProcess{

    private var asyncPubSub : RedisInterface
//    private var redisAddress : String
//    private var client : RedisClient  = nil;
    private var maxChatListSize : Int  = 16;
    private var asyncConnection : RedisInterface
//    private var syncConnection : RedisConnection<String, String>  = nil;
//    private var executor : ListeningExecutorService  = MoreExecutors.sameThreadExecutor();
    
//    private var listenerMap : HashMap<String, List<RedisPubSubListener>>  = HashMap<>();
    
//    private RedisPubSubListener<String, String> createlistener(channel : String, MyFunction<Collection<?>, Bool > callback){
//        RedisPubSubListener<String, String> listener = new RedisPubSubListener<String, String>() {
//            @Override
//            public void message(String channel, String message) {
//                print("message1");
//                fillChatMessages(channel, callback);
//            }
//            @Override
//            public void message(String pattern, String channel, String message) {
//                print("message2");
//            }
//            @Override
//            public void subscribed(String channel, Int64 count) {
//                print("subscribed1");
//            }
//            @Override
//            public void psubscribed(String pattern, Int64 count) {
//                print("psubscribed1");
//            }
//            @Override
//            public void unsubscribed(String channel, Int64 count) {
//                print("unsubscribed1");
//            }
//            @Override
//            public void punsubscribed(String pattern, Int64 count) {
//                print("punsubscribed");
//            }
//        };
//        if(listenerMap.containsKey(channel)) {
//            listenerMap.get(channel).add(listener);
//        }else {
//            listenerMap.put(channel, ArrayList<>(Arrays.asList(listener)));
//        }
//
//        return listener;
//    }
    
    public init() {
        do {
            asyncPubSub = RedisInterface(host: RedisConnectionParams.serverAddress, port: RedisConnectionParams.serverPort, auth: RedisConnectionParams.auth)
            asyncConnection = RedisInterface(host: RedisConnectionParams.serverAddress, port: RedisConnectionParams.serverPort, auth: RedisConnectionParams.auth)
        } catch{
            print("RedisGroupChat initialize has error: \(error)")
        }
    }
    
    public func initialize(){
        asyncPubSub.connect()
        asyncConnection.connect()
    }

    public func publish(channel : String, message : String) {
        do {
            
            self.asyncConnection.rpush(channel + "_list", data: message.data(using: String.Encoding.utf8)!, completionHandler: { success, key, data, cmd in
                if !success {
                    print ("RedisGroupChatProcess rpush operation for channel \(channel) for message \(message) is not successful")
                }else{
                    self.asyncConnection.publish(channel, value: message, completionHandler: { success, key, data, cmd in
                        if !success {
                            print ("RedisGroupChatProcess publish operation for channel \(channel) for message \(message) is not successful")
                        }
                    })
                }
            })
            
        } catch{
            print("RedisGroupChat publish method has error: \(error)")
        }
    }

    public func subscribe(channel : String, callback : MyFunction) {
        
        self.asyncPubSub.subscribe(channel, completionHandler: { success, channel, data, cmd in
            
            if let arrayVal = data?.arrayVal{
                if arrayVal.count == 3 && arrayVal[0].stringVal == "message" && arrayVal[1].stringVal  == channel{
                    print("successfully get message for chat channel \(channel) and message \(arrayVal[2].stringVal ?? "")")
//                    self.redisCallback?.commandExecuted(message: arrayVal[2].stringVal ?? "")
                    self.fillChatMessages(channel: channel, callback: callback);
                }else if arrayVal.count == 3 && arrayVal[0].stringVal == "subscribe" && arrayVal[1].stringVal == channel{
                    print("successfully subscribed for chat channel \(channel)")
                }else if arrayVal.count == 3 && arrayVal[0].stringVal == "unsubscribe" && arrayVal[1].stringVal == channel{
                    print("successfully unsubscribed for chat channel \(channel)")
                }else{
                    print("there is a problem chat subscription for chat channel \(channel)")
                }
            }
        })

        fillChatMessages(channel: channel, callback: callback);
    }

    private func fillChatMessages(channel : String, callback : MyFunction) {
        
        self.asyncConnection.llen(channel + "_list", completionHandler: { success, key, data, cmd in
            if success && data != nil && data?.intVal != nil {
                do {
//                    Float((data?.intVal)!)
                    let length : Int = data!.intVal!
                    if(length == 0) {
                        return;
                    }

                    var start : Int = 0;
                    var end : Int = 0;
                    print("I am at accept of llen");
                    end = length - 1
                    start = length - self.maxChatListSize > -1 ? length - self.maxChatListSize : 0
                    MyListCoordinator.getInstance().setStartEnd(channel: channel, start: start, end: end);

                    self.asyncConnection.lrange(channel + "_list", start: start, stop: end, completionHandler: { success, key, data, cmd in
                        if success && data != nil && data?.arrayVal != nil {
                            
                            var chatList : [String] = []
                            for response in data!.arrayVal! {
                                if let str = response.stringVal{
                                    chatList.append(str)
                                }
                                
                            }
                            _ = callback.apply(chatList);
                        }
                    })

                }catch{
                    print("RedisGroupChat llen has error \(error)")
                }
                
            }
        })
        
    }

    public func getOldChatMessages(channel : String, callback : MyFunction ) {
         self.asyncConnection.llen(channel + "_list", completionHandler: { success, key, data, cmd in
            if success && data != nil && data?.intVal != nil {
                do {
                    let myCurrentStart = MyListCoordinator.getInstance().getStart(channel: channel);
                    var currentMaxChatListSize = MyListCoordinator.getInstance().getCurrentMaxChatListSize(channel: channel);

                    if(myCurrentStart == 0) {
                        return;
                    }else {
                        currentMaxChatListSize = currentMaxChatListSize >= self.maxChatListSize ? currentMaxChatListSize*2: self.maxChatListSize;
                        var start = 0;
                        var end = 0;

                        print("I am at accept of llen");
                        end = myCurrentStart - 1
                        start = myCurrentStart - currentMaxChatListSize > -1 ? myCurrentStart - currentMaxChatListSize : 0;

                        MyListCoordinator.getInstance().setStart(channel: channel, start: start);
                        self.asyncConnection.lrange(channel + "_list", start: start, stop: end, completionHandler: { success, key, data, cmd in
                            if success && data != nil && data?.arrayVal != nil {
                                var chatList : [String] = []
                                for response in data!.arrayVal! {
                                    if let str = response.stringVal{
                                        chatList.append(str)
                                    }
                                }
                                let isContinue : Bool = callback.apply(chatList)
                                if(isContinue) {
                                    MyListCoordinator.getInstance().setCurrentMaxChatListSize(channel: channel, currentMaxChatListSize: currentMaxChatListSize);
                                }
                            }
                        })
                    }
               }catch{
                    print("RedisGroupChat llen has error \(error)")
               }
            }
        })
    }
    
    
    public func unsubscribe(channel : String) {
        self.asyncPubSub.unsubscribe(channel, completionHandler: { success, channel, data, cmd in
            if !success {
                print("can not unsubscribe from \(channel)")
            }
            });
    }
    
    public func disconnect() {
        self.asyncConnection.disconnect()
        self.asyncPubSub.disconnect()
    }
    
//    public func unsubscribeAll() {
//        for (Iterator<Entry<String, List<RedisPubSubListener>>> iterator = listenerMap.entrySet().iterator(); iterator.hasNext();) {
//            Entry<String, List<RedisPubSubListener>> e =  iterator.next();
//            var list : List<RedisPubSubListener> = e.getValue();
//            for (Iterator<RedisPubSubListener> iterator2 = list.iterator(); iterator2.hasNext();) {
//                var listener : RedisPubSubListener = iterator2.next();
//                self.asyncPubSub.removeListener(listener);
//            }
//            self.asyncPubSub.unsubscribe(e.getKey());
//        }
//        listenerMap.clear();
//    }
    
}

class MyListCoordinator{
    private var map : [String : MyList] = [:]
    private init() {};
    private static var instance : MyListCoordinator = MyListCoordinator();

    public static func getInstance() -> MyListCoordinator {
        return self.instance;
    }

    func getStart (channel : String) -> Int {
        if map[channel] != nil {
            return map[channel]!.start;
        }
        return 0;
    }

    func getEnd (channel : String) -> Int {
        if map[channel] != nil {
            return map[channel]!.end;
        }
        return 0;
    }

    func getCurrentMaxChatListSize (channel : String) -> Int {
        if map[channel] != nil {
            return map[channel]!.currentMaxChatListSize;
        }
        return 0;
    }

    func setStart (channel : String, start : Int) {
        if let myList = map[channel] {
            myList.start = start;
        }else {
            map[channel] = MyList(start: start, end: start)
        }

    }

    func setEnd (channel : String, end : Int) {
        if let myList = map[channel] {
            myList.end = end;
        }else {
            map[channel] = MyList(start: end, end: end)
        }

    }

    func setCurrentMaxChatListSize (channel : String, currentMaxChatListSize : Int) {
        if let myList = map[channel] {
            myList.currentMaxChatListSize = currentMaxChatListSize;
        }else {
            print("Last Range ERR can not ve insterted to map");
        }

    }

    func setStartEnd (channel : String, start : Int, end : Int){
        if let myList = map[channel] {
            myList.start = start;
            myList.end = end;
        }else {
            map[channel] = MyList(start: start, end: end)
        }
    }

    class MyList{
        var start : Int;
        var end : Int;
        var currentMaxChatListSize : Int;

        init(start : Int, end : Int) {
            self.start = start;
            self.end = end;
            self.currentMaxChatListSize = 1;
        }
    }
}


    
