//
//  RedisGpsProcess.swift
//  SwiftRedisChat
//
//  Created by macos on 8/25/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation

public class RedisAttendancePublisher {

//    private RedisCommand<String, String, Boolean> asynCommand = null;
//    private var channel : String
    private var asyncPublisher : RedisInterface? = nil
//    var redisCallback : RedisCallback? = nil
    
    public init() {
//        self.channel = channel;
//        self.redisCallback = redisCallback
    }
    
    public func initialize(){
        do {
            
            asyncPublisher = RedisInterface(host: RedisConnectionParams.serverAddress, port: RedisConnectionParams.serverPort, auth: RedisConnectionParams.auth)
            asyncPublisher?.connect()
            
        } catch {
            print("gps connection exception can not initialize")
        }
    }
    
    public func publish(channel: String, message : String) {
        
        self.asyncPublisher?.publish(channel, value: message, completionHandler: { success, key, data, cmd in
            if !success {
                print ("attendance publish command for \(channel) is not successful")
            }
        })
    }
    
    public func disconnect() {
       self.asyncPublisher?.disconnect()
    }
}
