//
//  RedisCallBackFunctions.swift
//  SwiftRedisChat
//
//  Created by macos on 8/29/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation

public protocol RedisCallback {
    func commandExecuted(message : String)
    func commandFailed()
}
