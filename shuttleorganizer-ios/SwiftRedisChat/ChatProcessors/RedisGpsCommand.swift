//
//  RedisGpsProcess.swift
//  SwiftRedisChat
//
//  Created by macos on 8/25/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation

public class RedisGpsCommand {

//    private RedisCommand<String, String, Boolean> asynCommand = null;
    private var asyncCommand : RedisInterface? = nil
    
    init() {
    }
    
    public func initialize(){
        do {
            
            asyncCommand = RedisInterface(host: RedisConnectionParams.serverAddress, port: RedisConnectionParams.serverPort, auth: RedisConnectionParams.auth)
            asyncCommand!.connect()
            
        } catch {
            print("gps command connection exception can not initialize")
        }
    }
    
    public func writeValue(key: String, message : String) {
        self.asyncCommand!.setDataForKey(key, data: message.data(using: String.Encoding.utf8)!, completionHandler: { success, cmd in
            if !success {
                print ("gps set command for \(key) is not successful")
            }
        })
    }
    
    public func deleteValue(key: String) {
        self.asyncCommand!.delDataForKey(key, completionHandler: { success, cmd in
            if !success {
                print ("gps del command for \(key) is not successful")
            }
        })
    }

    
    
//    private void getFirstGpsAsync() {
//        RedisFuture<String> get = self.connection.get(self.channel + "_gps");
//        func Consumer<String> () -> get.thenAccept(new {
//
//            @Override
//            public void accept(gpsValue : String) {
//                var latlon : String = " 41.036641, 28.986563";
//                if(gpsValue != null) {
//                    latlon = gpsValue;
//                }
//                redisListener.message(channel, latlon);
//                System.out.println(gpsValue);
//            }
//        });
//    }
    
    public func getValue(key: String, redisCallback : RedisCallback? = nil) {
        
        self.asyncCommand!.existsDataForKey(key, completionHandler: { success, key, data, cmd in
            if data?.intVal != nil && data!.intVal! > 0 {
                print("exists int val \(data!.intVal!)")
                self.asyncCommand!.getDataForKey(key, completionHandler: { success, key, data, cmd in
                    if success && data?.stringVal != nil {
                        redisCallback?.commandExecuted(message: data!.stringVal!)
                    }else {
                        redisCallback?.commandFailed()
                        print ("gps set command for \(key) is not successful")
                    }
//                    self.redisCallback?.commandExecuted(message: latlon)
                })
            }else{
                redisCallback?.commandFailed()
//                let latlon: String = " 41.036641, 28.986563"
//                self.redisCallback?.commandExecuted(message: latlon)
            }
        })
    }
    
//    private void unsubscribe() {
//        self.asyncSub.unsubscribe(self.channel);
//        self.asyncSub.removeListener(self.redisListener);
//    }
}
