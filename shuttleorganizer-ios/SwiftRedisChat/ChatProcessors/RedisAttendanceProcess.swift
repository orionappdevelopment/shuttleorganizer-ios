//
//  RedisGpsProcess.swift
//  SwiftRedisChat
//
//  Created by macos on 8/25/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation

public class RedisAttendanceProcess {

//    private RedisCommand<String, String, Boolean> asynCommand = null;
    private var channel : String
    private var asyncPublisher : RedisInterface? = nil
    private var asyncSubscriber : RedisInterface? = nil
    var redisCallback : RedisCallback? = nil
    
    public init(channel : String, redisCallback : RedisCallback?) {
        self.channel = channel;
        self.redisCallback = redisCallback
    }
    
    public func initialize(){
        do {
            
            asyncPublisher = RedisInterface(host: RedisConnectionParams.serverAddress, port: RedisConnectionParams.serverPort, auth: RedisConnectionParams.auth)
            asyncPublisher!.connect()
            asyncSubscriber = RedisInterface(host: RedisConnectionParams.serverAddress, port: RedisConnectionParams.serverPort, auth: RedisConnectionParams.auth)
            asyncSubscriber!.connect()
            
            self.subscribe()
//            asyncSubscriber.subscribe(self.channel, completionHandler: { success, channel, data, cmd in
//            })
            
        } catch {
            print("gps connection exception can not initialize \(self.channel)")
        }
    }
    
    public func publish(message : String) {
        
        self.asyncPublisher!.publish(self.channel, value: message, completionHandler: { success, key, data, cmd in
            if !success {
                print ("attendance publish command for \(self.channel) is not successful")
            }
        })
    }

    private func subscribe() {
        //tested place
//        getFirstGps();
        
        self.asyncSubscriber!.subscribe(self.channel, completionHandler: { success, channel, data, cmd in
            
            if let arrayVal = data?.arrayVal{
                if arrayVal.count == 3 && arrayVal[0].stringVal == "message" && arrayVal[1].stringVal  == self.channel{
                    print("successfully get message for attendance channel \(channel) and message \(arrayVal[2].stringVal ?? "")")
                    self.redisCallback?.commandExecuted(message: arrayVal[2].stringVal ?? "")
                    
                }else if arrayVal.count == 3 && arrayVal[0].stringVal == "subscribe" && arrayVal[1].stringVal == self.channel{
                    print("successfully subscribed for attendance channel \(channel)")
                }else if arrayVal.count == 3 && arrayVal[0].stringVal == "unsubscribe" && arrayVal[1].stringVal == self.channel{
                    print("successfully unsubscribed for attendance channel \(channel)")
                }else{
                    print("there is a problem attendance subscription for attendance channel \(channel)")
                    self.redisCallback?.commandFailed()
                }
            }
            
        })
        //original place
    }
    
//    private void getFirstGpsAsync() {
//        RedisFuture<String> get = self.connection.get(self.channel + "_gps");
//        func Consumer<String> () -> get.thenAccept(new {
//
//            @Override
//            public void accept(gpsValue : String) {
//                var latlon : String = " 41.036641, 28.986563";
//                if(gpsValue != null) {
//                    latlon = gpsValue;
//                }
//                redisListener.message(channel, latlon);
//                System.out.println(gpsValue);
//            }
//        });
//    }
    
    
//    private void unsubscribe() {
//        self.asyncSub.unsubscribe(self.channel);
//        self.asyncSub.removeListener(self.redisListener);
//    }
}
