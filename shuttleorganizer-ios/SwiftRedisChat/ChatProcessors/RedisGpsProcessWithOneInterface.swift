//
//  RedisGpsProcess.swift
//  SwiftRedisChat
//
//  Created by macos on 8/25/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation

public class RedisGpsProcessWithOneInterface {

//    private RedisCommand<String, String, Boolean> asynCommand = null;
    private var channel : String
    private var asyncPublisher : RedisInterface? = nil
    private var asyncSubscriber : RedisInterface? = nil
    private var asyncCommand : RedisInterface? = nil
    var redisCallback : RedisCallback? = nil
    
    init(channel : String, redisCallback : RedisCallback?) {
        self.channel = channel;
        self.redisCallback = redisCallback
    }
    
    public func initialize(){
        do {
            
            asyncCommand = RedisInterface(host: RedisConnectionParams.serverAddress, port: RedisConnectionParams.serverPort, auth: RedisConnectionParams.auth)
            asyncPublisher = asyncCommand
            asyncSubscriber = RedisInterface(host: RedisConnectionParams.serverAddress, port: RedisConnectionParams.serverPort, auth: RedisConnectionParams.auth)
            asyncCommand!.connect()
            asyncSubscriber!.connect()
            
            self.subscribe()
//            asyncSubscriber.subscribe(self.channel, completionHandler: { success, channel, data, cmd in
//            })
            
        } catch {
            print("gps connection exception can not initialize \(self.channel)")
        }
    }
    
    public func publish(message : String) {
        
//        self.asyncCommand!.setDataForKey(self.channel + "_gps", data: message.data(using: String.Encoding.utf8)!, completionHandler: { success, cmd in
//            if !success {
//                print ("gps set command for \(self.channel) is not successful")
//            }
//        })
        
        self.asyncPublisher!.publish(self.channel, value: message, completionHandler: { success, key, data, cmd in
            if !success {
                print ("gps publish command for \(self.channel) is not successful")
            }else{
                print ("gps publish command for \(self.channel) is successful for \(message)")
            }
        })
        
        self.asyncPublisher!.publish(self.channel+"1", value: message, completionHandler: { success, key, data, cmd in
            if !success {
                print ("gps publish command for \(self.channel+"1") is not successful")
            }else{
                print ("gps publish command for \(self.channel+"1") is successful for \(message)")
            }
        })
    }

    private func subscribe() {
        print("RedisGPSProcess subscribe started")
        
//        getFirstGps();
        
        self.asyncSubscriber!.subscribe(self.channel, completionHandler: { success, channel, data, cmd in
            
            if let arrayVal = data?.arrayVal{
                if arrayVal.count == 3 && arrayVal[0].stringVal == "message" && arrayVal[1].stringVal  == self.channel{
                    print("successfully get message for gps channel \(channel) and message \(arrayVal[2].stringVal ?? "")")
                    self.redisCallback?.commandExecuted(message: arrayVal[2].stringVal ?? "")
                    
                }else if arrayVal.count == 3 && arrayVal[0].stringVal == "subscribe" && arrayVal[1].stringVal == self.channel{
                    print("successfully subscribed for gps channel \(channel)")
                }else if arrayVal.count == 3 && arrayVal[0].stringVal == "unsubscribe" && arrayVal[1].stringVal == self.channel{
                    print("successfully unsubscribed for gps channel \(channel)")
                }else{
                    print("there is a problem gps subscription for gps channel \(channel)")
                    self.redisCallback?.commandFailed()
                }
            }
            
        })
        
//        let otherChannel = self.channel + "1"
//
//        self.asyncSubscriber!.subscribe(otherChannel, completionHandler: { success, channel, data, cmd in
//
//            if let arrayVal = data?.arrayVal{
//                if arrayVal.count == 3 && arrayVal[0].stringVal == "message" && arrayVal[1].stringVal  == otherChannel{
//                    print("successfully get message for gps channel \(otherChannel) and message \(arrayVal[2].stringVal ?? "")")
//                    self.redisCallback?.commandExecuted(message: arrayVal[2].stringVal ?? "")
//
//                }else if arrayVal.count == 3 && arrayVal[0].stringVal == "subscribe" && arrayVal[1].stringVal == otherChannel{
//                    print("successfully subscribed for gps channel \(otherChannel)")
//                }else if arrayVal.count == 3 && arrayVal[0].stringVal == "unsubscribe" && arrayVal[1].stringVal == otherChannel{
//                    print("successfully unsubscribed for gps channel \(otherChannel)")
//                }else{
//                    print("there is a problem gps subscription for gps channel \(otherChannel)")
//                    self.redisCallback?.commandFailed()
//                }
//            }
//
//        })
        
        print("RedisGPSProcess subscribe started")
        
//        getFirstGps();
    }
    
//    private void getFirstGpsAsync() {
//        RedisFuture<String> get = self.connection.get(self.channel + "_gps");
//        func Consumer<String> () -> get.thenAccept(new {
//
//            @Override
//            public void accept(gpsValue : String) {
//                var latlon : String = " 41.036641, 28.986563";
//                if(gpsValue != null) {
//                    latlon = gpsValue;
//                }
//                redisListener.message(channel, latlon);
//                System.out.println(gpsValue);
//            }
//        });
//    }
    
    private func getFirstGps() {
        
        self.asyncCommand!.existsDataForKey(RedisConstants.getRedisLocationChannelId(transportationId: self.channel), completionHandler: { success, key, data, cmd in
            if data?.intVal != nil && data!.intVal! > 0 {
                print("exists int val \(data!.intVal!)")
                self.asyncCommand!.getDataForKey(RedisConstants.getRedisLocationChannelId(transportationId: self.channel), completionHandler: { success, key, data, cmd in
                    var latlon: String = " 41.036641, 28.986563"
                    if success && data?.stringVal != nil {
                        latlon = data!.stringVal!
                    }else {
                        print ("gps set command for \(self.channel) is not successful")
                    }
                    self.redisCallback?.commandExecuted(message: latlon)
                })
            }else{
                let latlon: String = " 41.036641, 28.986563"
                self.redisCallback?.commandExecuted(message: latlon)
            }
        })
    }
    
//    private void unsubscribe() {
//        self.asyncSub.unsubscribe(self.channel);
//        self.asyncSub.removeListener(self.redisListener);
//    }
}
