
import Foundation

public class RedisPublisher {
    
    private var asyncCommand : RedisInterface
    private static let instance : RedisPublisher = RedisPublisher()
    
    
    static public func getInstance() -> RedisPublisher {
        return instance
    }
    
    private init(){
        do {
            self.asyncCommand = RedisInterface(host: RedisConnectionParams.serverAddress, port: RedisConnectionParams.serverPort, auth: RedisConnectionParams.auth)
            self.asyncCommand.connect()
        } catch {
            print("Error can not init RedisPublisher")
        }
    }
    public func publish(prefix : String, channel : String, message : String) {
        
        self.asyncCommand.setDataForKey(prefix + channel, data: message.data(using: String.Encoding.utf8)!, completionHandler: { success, cmd in
            if !success {
                print ("RedisPublisher set opeartion for channel \(channel) for message \(message) is not successful")
            }
        })
        
        self.asyncCommand.publish(prefix + channel, value: message, completionHandler: { success, key, data, cmd in
            if !success {
                print ("RedisPublisher publish opeartion for channel \(channel) for message \(message) is not successful")
            }
        })
    }
}
