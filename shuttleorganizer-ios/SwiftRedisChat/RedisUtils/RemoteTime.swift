//
//  RemoteTime.swift
//  SwiftRedisChat
//
//  Created by macos on 8/25/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation


public class RemoteTime {

    public static func getRemoteNTPTime() -> Int64 {
        let currentTime : Int64 = Date().toMilliseconds()
//        NTPUDPClient client = new NTPUDPClient();
//        // We want to timeout if a response takes longer than 5 seconds
//        client.setDefaultTimeout(5000);
//        //NTP server list
//        List<String> hosts = Arrays.asList(new String("ntp02.oal.ul.pt"), new String("ntp04.oal.ul.pt"), new String("ntp.xs4all.nl"));
//        for (String host : hosts) {
//
//            try {
//                InetAddress hostAddr = InetAddress.getByName(host);
//                TimeInfo info = client.getTime(hostAddr);
//                currentTime = info.getMessage().getTransmitTimeStamp().getTime();
//
//            }
//            catch (Exception e) {
//                e.printStackTrace();
//                return currentTime;
//            }
//            finally{
//                client.close();
//            }
//        }
        return currentTime;
    }
}
