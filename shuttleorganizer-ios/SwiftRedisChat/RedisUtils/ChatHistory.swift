import Foundation


public class ChatHistory {
    private var channel : String ;
    private var orderedChatMessageList : SkipList<ChatMessage> ;
//    private var gson : Gson  = Gson();
    
    public init(channel : String){
        self.orderedChatMessageList = SkipList<ChatMessage>()
        self.channel = channel;
    }

    public func getChannel() -> String {
        return self.channel;
    }

    public func add(chats : [String]) -> Bool {
        let lastIndex : Int = chats.count - 1;
        var isChanged : Bool = false;
        var i = lastIndex
        while i > -1 {
            
            let jsonData = chats[i].data(using: .utf8)!
            var cm: ChatMessage? = nil
            do{
                cm = try JSONDecoder().decode(ChatMessage.self, from: jsonData)
            }catch {
                print("Unexpected error during decoding ChatMessage: \(error).")
                cm = nil
            }
            if(cm != nil && self.orderedChatMessageList.insert(key: cm!)) {
                isChanged = true;
            }else {
                break;
            }
            i -= 1
        }
        return isChanged;
    }
    
    public func getOrderedChatMessageList() -> SkipList<ChatMessage> {
        return orderedChatMessageList;
    }

    public static func getChats (chatList : [ChatMessage]) -> String {
        var builder : String = String();
        for chatMessage in chatList {
            builder.append(chatMessage.description());
            builder.append("\n");
        }
        return builder;
    }

    public func getPureChatsArrayList() -> [ChatMessage] {
        return self.orderedChatMessageList.getSortedElements()
    }
    
    public func getChatsArrayList() -> [ChatMessage] {
        var index : Int = 0;
        let chatList : [ChatMessage] = self.orderedChatMessageList.getSortedElements()
        var chatList2 : [ChatMessage] = [];
        for chatMessage in chatList {
            if(index == 0){
                let cm : ChatMessage = ChatMessageFactory.getInstance().getChatMessage(
                    content: chatMessage.content,
                    senderId: chatMessage.senderId,
                    senderName: chatMessage.senderName,
                    messageType: MessageTypeEnum.DATE);
                cm.time = chatMessage.time
                chatList2.insert(cm, at: 0)
            } else {
                let isSameDate : Bool = checkDateDifference(time1: chatList[index].time, time2: chatList[index-1].time)

                if(!isSameDate){
                    let cm : ChatMessage = ChatMessageFactory.getInstance().getChatMessage(
                        content: chatMessage.content,
                        senderId: chatMessage.senderId,
                        senderName: chatMessage.senderName,
                        messageType: MessageTypeEnum.DATE);
                    cm.time = chatMessage.time
                    chatList2.append(cm)
                }
            }

            chatList2.append(chatMessage);

            index += 1
        }
        return chatList2;
    }

    private func checkDateDifference(time1 : Int64, time2 : Int64) -> Bool {
        let d1 = Double(time1/1000)
        let d2 = Double(time2/1000)
        let date1 = Date(timeIntervalSince1970: d1)
        let date2 = Date(timeIntervalSince1970: d2)
        let dateFormatter = DateFormatter()
//        dateFormatter.timeStyle = DateFormatter.Style.none //Set time style
//        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
//        dateFormatter.timeZone = .current
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        let localDate1 = dateFormatter.string(from: date1)
        let localDate2 = dateFormatter.string(from: date2)
        return localDate1 == localDate2
    }
}
