//
//  ChatMessage.swift
//  SwiftRedisChat
//
//  Created by macos on 8/25/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation

public class ChatMessage : Hashable, Equatable, Comparable, Codable, Identifiable{
    
    public var id : String
    var time : Int64
    var senderId : String
    var senderName : String
    var content : String
    var messageType : MessageTypeEnum

    init (id : String, time : Int64, senderId : String, senderName : String, content : String, messageType : MessageTypeEnum) {
        self.time = time;
        self.senderId = senderId;
        self.senderName = senderName;
        self.content = content;
        self.id = id;
        self.messageType = messageType;

    }

    public func description() -> String {
        var result : String =  String();
        result += self.senderName
        result += ":"
        result += content
        
        return result
    }
    
     public static func == (lhs: ChatMessage, rhs: ChatMessage) -> Bool {
        return lhs.id == rhs.id;
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
        hasher.combine(self.time)
    }
    
    public static func < (lhs: ChatMessage, rhs: ChatMessage) -> Bool {
        if lhs.time == rhs.time{
            return lhs.id < rhs.id
        }else{
            return lhs.time < rhs.time
        }
    }
    
//    public  func hashCode () -> Int {
//        return Objects.hash(self.id, self.time);
//    }
    
//    @Override
//    public  func compareTo (o : ChatMessage) -> Int {
//
//        if(o == null) {
//            throw  RuntimeException("Can not Compare Chat for null object:" + o);
//        }else if(!(o instanceof ChatMessage)) {
//            throw  RuntimeException("Can not Compare Chat for:" + o.toString());
//        }
//
//        var result : Int =  Long(self.time).compareTo(o.time);
//        if(result == 0) {
//            return self.id.compareTo(o.id);
//        }
//
//        return result;
//
//    }
    
}
