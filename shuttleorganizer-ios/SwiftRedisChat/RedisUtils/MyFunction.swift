//
//  MyFunction.swift
//  SwiftRedisChat
//
//  Created by macos on 9/10/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation

public protocol MyFunction {
    func apply(_ chatList : [String]) -> Bool;
}
