
import Foundation

public class RedisSubscriber {
    
    private var asyncSub : RedisInterface;
    private var asyncCommand : RedisInterface;
    private var redisAddress : String
    private var channel : String
    private var redisCallback : RedisCallback?  = nil;
    
    public init (redisAddress : String, prefix : String, channel : String, redisCallback : RedisCallback ) {
        self.redisAddress = redisAddress;
        self.channel = prefix + channel;
        self.asyncSub = RedisInterface(host: RedisConnectionParams.serverAddress, port: RedisConnectionParams.serverPort, auth: RedisConnectionParams.auth)
        self.asyncCommand = RedisInterface(host: RedisConnectionParams.serverAddress, port: RedisConnectionParams.serverPort, auth: RedisConnectionParams.auth)
        
        self.redisCallback = redisCallback;
        initialize();
    }
    
    private func initialize(){
        do {
            self.asyncSub.connect();
            self.asyncCommand.connect();
            self.subscribe();
        } catch {
            print("RedisSubscriber start method connection exception");
        }
    }
    
    private func subscribe() {
        self.asyncSub.subscribe(self.channel, completionHandler: { success, channel, data, cmd in
            
            if let arrayVal = data?.arrayVal{
                if arrayVal.count == 3 && arrayVal[0].stringVal == "message" && arrayVal[1].stringVal  == self.channel{
                    print("successfully get message for gps channel \(channel) and message \(arrayVal[2].stringVal ?? "")")
                    self.redisCallback?.commandExecuted(message: arrayVal[2].stringVal ?? "")
                    
                }else if arrayVal.count == 3 && arrayVal[0].stringVal == "subscribe" && arrayVal[1].stringVal == self.channel{
                    print("successfully subscribed for gps channel \(channel)")
                }else if arrayVal.count == 3 && arrayVal[0].stringVal == "unsubscribe" && arrayVal[1].stringVal == self.channel{
                    print("successfully unsubscribed for gps channel \(channel)")
                }else{
                    print("there is a problem gps subscription for gps channel \(channel)")
                    self.redisCallback?.commandFailed()
                }
            }
        })
        
        getFirstRecordAsync();
    }
    
    
    private func getFirstRecordAsync() {
        
        self.asyncCommand.existsDataForKey(self.channel, completionHandler: { success, key, data, cmd in
            if data?.intVal != nil && data!.intVal! > 0 {
                print("exists int val \(data!.intVal!)")
                self.asyncCommand.getDataForKey(self.channel , completionHandler: { success, key, data, cmd in
                    if success && data?.stringVal != nil {
                        let result = data!.stringVal!
                         self.redisCallback?.commandExecuted(message: result)
                    }else {
                        self.redisCallback?.commandFailed()
                        print ("get command for \(self.channel) is not successful")
                    }
                })
            }else{
                self.redisCallback?.commandFailed()
            }
        })
    }
    
    public func disconnect() {
        self.asyncCommand.disconnect()
        self.asyncSub.disconnect()
    }
}
