//
//  ChatMessageFactory.swift
//  SwiftRedisChat
//
//  Created by macos on 8/25/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation

public class ChatMessageFactory{

    private static let instance : ChatMessageFactory = ChatMessageFactory()
    private var timeDiff : Int64 = 0
    private let lock = NSLock()
    private var initialized = false
    
    private init() {
    }
    
    public static func getInstance () -> ChatMessageFactory {
        return instance;
    }
    
    public func initialize() {
        lock.lock()
        if(initialized == false){
            let now : Int64 = Date().toMilliseconds();
            let realNow : Int64 = RemoteTime.getRemoteNTPTime();
    //        System.out.println("computer date:" + Date(now));
    //        System.out.println("remote date:" + Date(now));
            self.timeDiff =  realNow - now;
            self.initialized = true
        }
        lock.unlock()
    }
    
    public func getChatMessage (content : String, senderId : String, senderName : String, messageType : MessageTypeEnum) -> ChatMessage {
        let realTime : Int64 = Date().toMilliseconds() + timeDiff;
        let id : String = ChatMessageFactory.getRandomUniqueId();
        let cm : ChatMessage =  ChatMessage(id: id, time: realTime, senderId: senderId, senderName: senderName, content: content, messageType: messageType);
        return cm;
    }
    
    public static func getRandomUniqueId () -> String {
        let randomUniqId : UUID = UUID()
        //f46709a2-f27c-11e8-a2f7-a088699d485b like self one //it is always 36 characters
        
        return randomUniqId.uuidString;
    }

//    public static func main (String[] args) -> void {
//        getInstance();
//        instance.init();
//        instance.getTimeDiff();
//    }

    public  func getTimeDiff () -> Int64 {
//        System.out.println(timeDiff);
        return timeDiff;
    }
}

extension Date {
    func toMilliseconds() -> Int64 {
        Int64(self.timeIntervalSince1970 * 1000)
    }

    init(milliseconds:Int) {
        self = Date().advanced(by: TimeInterval(integerLiteral: Int64(milliseconds / 1000)))
    }
}

