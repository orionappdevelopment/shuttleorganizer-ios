//
//  MessageTypeEnum.swift
//  SwiftRedisChat
//
//  Created by macos on 8/25/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation

public enum MessageTypeEnum : String, Codable {
    case TEXT = "TEXT"  //0
    case IMAGE = "IMAGE"//1
    case DATE = "DATE"  //2
    case GPS="GPS" //3
    case NONE = "NONE"  //102

    public static func toMessageTypeEnum(value : Int) -> MessageTypeEnum {
        // retrieve status from status code
        if (value == 0) {
            return TEXT;
        } else if (value == 1) {
            return IMAGE;
        } else if (value == 2) {
            return DATE;
        }else if (value == 3) {
            return GPS;
        }else {
            return NONE;
        }
    }
}
