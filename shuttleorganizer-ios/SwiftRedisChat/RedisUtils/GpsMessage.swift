//
//  ChatMessage.swift
//  SwiftRedisChat
//
//  Created by macos on 8/25/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation

public class GpsMessage : Hashable, Equatable, Codable, Identifiable{
    
    public var id : String
    var time : Int64
    var transportationId : String
    var senderId : String
    var senderName : String
    var latitude : Float64
    var longitude : Float64
    var course : Float64
    var speed : Float64
    var messageType : MessageTypeEnum

    
    init (time : Int64, transportationId : String, senderId : String, senderName : String, latitude : Float64, longitude : Float64,
          course : Float64, speed : Float64, messageType : MessageTypeEnum = .GPS) {
        self.time = time;
        self.transportationId = transportationId
        self.senderId = senderId;
        self.senderName = senderName;
        self.latitude = latitude;
        self.longitude = longitude;
        self.course = course
        self.speed = speed
        self.id = "\(time) \(transportationId) \(senderId) \(latitude) \(longitude)"
        self.messageType = messageType;

    }

    public func description() -> String {
        var result : String =  String();
        result += "senderId:\(self.senderId) transportationId:\(self.transportationId)"
        result += " latitude \(self.latitude), longitude \(self.longitude) "
//        result +=
        return result
    }
    
     public static func == (lhs: GpsMessage, rhs: GpsMessage) -> Bool {
        return lhs.id == rhs.id;
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
        hasher.combine(self.time)
    }
    
}
