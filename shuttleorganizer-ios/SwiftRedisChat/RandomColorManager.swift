//
//  RandomColorManager.swift
//  SwiftRedisChat
//
//  Created by macos on 9/27/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation
import SwiftUI

class RandomColorManager{
    
    static let instance : RandomColorManager = RandomColorManager()
    private var colorMap : [ String : Color ] = [ : ]
    
    private init(){
    }
    
    public func randomColor(id : String) -> Color {
        if let validColor = colorMap[id] {
            return validColor
        }else{
            let validColor = Color(red: .random(in: 0...1),  green: .random(in: 0...1),  blue: .random(in: 0...1))
            colorMap[ id ] = validColor
            return validColor
        }
    }
    
}
