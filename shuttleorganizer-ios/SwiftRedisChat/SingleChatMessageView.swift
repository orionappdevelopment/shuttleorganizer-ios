//
//  SingleChatMessageView.swift
//  SwiftRedisChat
//
//  Created by macos on 9/12/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct SingleChatMessageView: View {
    @State var message: ChatMessage
    
    var background: some View {
        if (message.senderId == MyUser.getInstance().firebaseId) {
            return Color.colorGreen
        } else {
            return Color.colorLightGray
        }
    }
    
//    var title: some View {
//        if message.senderId == "1" {
//            return Text()
//        } else {
//            return Text(message.senderName)
//                .font(.headline)
//                .fontWeight(.semibold)
//                .foregroundColor(.orange)
//        }
//    }
    
    var body: some View {
        HStack {
            if message.senderId != MyUser.getInstance().getUserId(){
                HStack{
                    VStack(alignment: .leading) {
                        Text(message.senderName)
                            .font(.headline)
                            .fontWeight(.semibold)
                            .foregroundColor(RandomColorManager.instance.randomColor(id: message.senderId) )
                        HStack(alignment: .lastTextBaseline){
                            Text(message.content)
                                .fixedSize(horizontal: false, vertical: true)
                            Text(Utils.getHourMinuteFromMilliseconds(timeIntervalSince1970: message.time))
                                .offset(y: 10)
                                .padding(.bottom, 5)
                                .font(.footnote)
                                .foregroundColor(Color.colorDarkGray)
                        }
                    }
                        .padding(8)
                        .background(background)
                        .cornerRadius(10)
                     Spacer()
                } .padding(.trailing, 20)
            }else{
                HStack{
                    Spacer()
                    HStack(alignment: .lastTextBaseline){
                        Text(message.content)
                            .fixedSize(horizontal: false, vertical: true)
                        Text(Utils.getHourMinuteFromMilliseconds(timeIntervalSince1970: message.time))
                            .offset(y: 10)
                            .padding(.bottom, 5)
                            .font(.footnote)
                            .foregroundColor(Color.colorDarkGray)
                    }
                          .padding(8)
                          .background(background)
                          .cornerRadius(10)
                        
                }.padding(.leading, 20)
            }
            
        }
//        .frame(maxWidth: .infinity)
       
    }
}


struct SingleChatMessageView_Previews: PreviewProvider {
    static var previews: some View {
        List{
            SingleChatMessageView(message: ChatMessage(id: "id", time: Date().toMilliseconds(), senderId: "senderId", senderName: "senderName", content: "content", messageType: MessageTypeEnum.TEXT))
            SingleChatMessageView(message: ChatMessage(id: "id", time: Date().toMilliseconds(), senderId: "senderId", senderName: "senderName", content: "content", messageType: MessageTypeEnum.TEXT))
            SingleChatMessageView(message: ChatMessage(id: "id", time: Date().toMilliseconds(), senderId: "1", senderName: "senderName", content: "content", messageType: MessageTypeEnum.TEXT))
             SingleChatMessageView(message: ChatMessage(id: "id", time: Date().toMilliseconds(), senderId: "senderId", senderName: "senderName", content: "content content content content content content content content content content content content content content content content content content content content content content ", messageType: MessageTypeEnum.TEXT))
            SingleChatMessageView(message: ChatMessage(id: "id", time: Date().toMilliseconds(), senderId: "1", senderName: "senderName", content: "content content content content content content content content content content content content content content content content content content content content content content ", messageType: MessageTypeEnum.TEXT))
        }
    }
}
