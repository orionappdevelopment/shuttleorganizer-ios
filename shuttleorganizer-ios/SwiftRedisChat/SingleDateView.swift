//
//  SingleDateView.swift
//  SwiftRedisChat
//
//  Created by macos on 12/6/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct SingleDateView: View {
    @State var message: ChatMessage
    
    var body: some View {
        HStack(alignment: .center){
            Spacer()
            VStack(alignment: .center) {
                Text(getDateFromInt64(timeInMilisecondsParam: message.time)!)
              }
                  .padding(8)
                  .background(Color.gray.opacity(0.45))
                  .cornerRadius(10)
            Spacer()
        }
    }
    
    func getDateFromInt64(timeInMilisecondsParam : Int64?) -> String?{
        if let timeInMiliseconds = timeInMilisecondsParam{
            let dateTimeStamp = Date(timeIntervalSince1970: TimeInterval(Int(timeInMiliseconds/1000)))
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let strDate = dateFormatter.string(from: dateTimeStamp)
            return strDate
        }
        return nil
    }
}

struct SingleDateView_Previews: PreviewProvider {
    static var previews: some View {
        List{
            SingleDateView(message: ChatMessage(id: "id", time: Date().toMilliseconds(), senderId: "senderId", senderName: "senderName", content: "content content content content content content content content content content content content content content content content content content content content content content ", messageType: MessageTypeEnum.TEXT))
            SingleDateView(message: ChatMessage(id: "id", time: Date().toMilliseconds(), senderId: "1", senderName: "senderName", content: "content content content content content content content content content content content content content content content content content content content content content content ", messageType: MessageTypeEnum.TEXT))
        }
    }
}
