//
//  JoinedTransportationsListObservable.swift
//  shuttleorganizer-ios
//
//  Created by macos on 6/2/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Combine
import SwiftUI

final class JoinedTransportationsListObservable: ObservableObject {
    @Published var transportations : [Transportation] = createdTransportations
    @Published var error : Error? = nil
    
    
    private static var instance : JoinedTransportationsListObservable? = nil
    
    public static func getInstance() -> JoinedTransportationsListObservable{
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        if JoinedTransportationsListObservable.instance == nil{
            JoinedTransportationsListObservable.instance = JoinedTransportationsListObservable()
        }
            
        return JoinedTransportationsListObservable.instance!
    }
    
    private init() {
    }

    func fetchData(callBack : @escaping () -> Void = {}){
        NetworkManager.fetch(url: UrlConstants.UrlGetJoinedTransportationList, parameters: Person(id: MyUser.getInstance().getUserId())
            ,handle: {
                (newData : [Transportation.OptionalTransportation] ) -> () in
                self.transportations = newData.map{ Transportation(optionalTransportation: $0) }
                print ("fetched joined transportations:\(self.transportations)")
                callBack()
            }
            , errorHandle: {
                newError in
                self.error = newError})
        
    }
    
    func getTransportation(id : String) -> Transportation{
        let filtered = transportations.filter( { t in
            if let validId = t.id {
                return validId == id }
            return false })
        
        if filtered.count > 0 {
            return filtered[0]
        }
        
        return Transportation()
    }
    
    func getJoinedTransportation(id : String) -> Transportation?{
        let filtered = transportations.filter( { t in
            if let validId = t.id {
                return validId == id
            }
            return false })
        
        if filtered.count > 0 {
            return filtered[0]
        }
        
        return nil
    }
}
