//
//  CreatedShuttles.swift
//  Landmarks
//
//  Created by macos on 5/9/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Combine
import SwiftUI

final class CreatedTransportationsListObservable: ObservableObject {
    @Published var transportations : [Transportation] = createdTransportations
    @Published var error : Error? = nil

    private static var instance : CreatedTransportationsListObservable? = nil
    
    public static func getInstance() -> CreatedTransportationsListObservable{
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        if CreatedTransportationsListObservable.instance == nil{
            CreatedTransportationsListObservable.instance = CreatedTransportationsListObservable()
        }
            
        return CreatedTransportationsListObservable.instance!
    }
    
    private init() {
    }
    
    func fetchData(){
        NetworkManager.fetch(url: UrlConstants.UrlGetCreatedTransportationList, parameters: Person(id: MyUser.getInstance().getUserId())
            ,handle: {
                (newData : [Transportation.OptionalTransportation] ) -> () in
                self.transportations = newData.map{ Transportation(optionalTransportation: $0) } }
            , errorHandle: {
                newError in
                DispatchQueue.main.async {
                    self.error = newError
                }
                })
        
    }
    
    func getTransportation(id : String) -> Transportation{
        let filtered = transportations.filter( { t in
            if let validId = t.id {
                return validId == id }
            return false })
        
        if filtered.count > 0 {
            return filtered[0]
        }
        
        return Transportation()
    }
    
//     init() {
//        print(UrlConstants.UrlGetCreatedTransportationList)
//
//        guard let url = URL(string: UrlConstants.UrlGetCreatedTransportationList) else { return }
//        let parameters: [String: Any] = [
//            "id" : "bQm1w3Uqithl8EJ3DEFP7CvBvI73"
//        ]
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//
//        do {
//            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
//        } catch let error {
//            print(error.localizedDescription)
//        }
//        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
//
//        request.timeoutInterval = 20
//
////        guard let url = URL(string: UrlConstants.UrlGetCreatedTransportationList)
////            else{ return}
//
//        URLSession.shared.dataTask(with: request as URLRequest){(data, response, error) in
//            do {
//                guard let errorResponseData = data else {
//                    print("There in no response from Server")
//                    return
//                }
//                if let errorResponseJson = try JSONSerialization.jsonObject(with: errorResponseData, options: .mutableContainers) as? [String: Any] {
//                    print("Server return response with error: \(errorResponseJson)")
//                    // handle json...
//                }
//
//                if let validJson = data
//                {
//                    let decodedData = try JSONDecoder().decode([Transportation].self, from: validJson)
//                    DispatchQueue.main.async {
//                        self.transportations = decodedData}
//                } else {
//                    if let jsonData = data {
//                        print("Invalid Json Data: \(jsonData)")
//                    }
//                    if let validError = error {
//                        print("Error Data: \(validError)")
//                    }
//                }
//
//            } catch {
//                print("Catched Error: \(error)")
//            }
//        }.resume()
//    }
    
    
}
