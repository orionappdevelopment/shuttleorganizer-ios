//
//  GenericObservableObject.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/27/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

class GenericObservableModel <T> : ObservableObject {
    @Published var value : T { didSet{
//        print("pp ppp parent:\(String(describing: parent))")
        isValueChanged = true
        callBack(value)
        callBackWithParent(value, parent)
        callBackWithParentAndPreviousValue(value, parent, previousValue)
        previousValue = value
    }}
    @Published var isValueChanged : Bool = false { didSet{
//        callBack(value)
//        callBackP(value, parent)
    }}
    
    public var parent : Any = false
    
    var callBack : (T) -> Void? = {_ in return}
    
    var callBackWithParent : (T, Any) -> Void? = {_,_ in return}
    
    var callBackWithParentAndPreviousValue : (T, Any, T) -> Void? = {_,_,_ in return}
    
    var previousValue : T
    
    
    init(value : T, callBack : @escaping (T) -> Void =
    { T -> Void in } ) {
        self.value = value
        self.callBack = callBack
        self.previousValue = value
    }
    
    init(value : T, callBackWithParent : @escaping (T, Any) -> Void) {
        
//        print("init GenericObservable parent:\(parent)")
//        let b = parent is TransportationParticipantsListView
//        print( "b=\(b)")
        self.value = value
        self.callBackWithParent = callBackWithParent
        self.previousValue = value
    }
    
    init(value : T, callBackWithParentAndPreviousValue : @escaping (T, Any, T) -> Void) {
        self.value = value
        self.callBackWithParentAndPreviousValue = callBackWithParentAndPreviousValue
        self.previousValue = value
    }
        
}

