//
//  AppDelegate.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/23/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import GoogleMaps
import GooglePlaces
import GoogleSignIn
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    let gcmMessageIDKey = "gcm.message_id"
    
    private var myUser : MyUser? = nil
    private var singleLocationManager : SingleLocationManagerBasic = SingleLocationManagerBasic.getInstance()
    private var singleNotificationManager : SingleNotificationManager? = nil
    private var sharingLocationTransportationManager : SharingLocationTransportationManager = SharingLocationTransportationManager.getInstance()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        _ = FBRemoteConfig.getInstance()
        //add for Google login start
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        //add for Google login end
        
        //added for Facebool login start
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
        //added for Facebool login end
        
        //with ios key
//        GMSServices.provideAPIKey("AIzaSyDHoUpcqzJIE2UBNw7BuM9ViSms09MS-1k")
//        GMSPlacesClient.provideAPIKey("AIzaSyDHoUpcqzJIE2UBNw7BuM9ViSms09MS-1k")
        //android key
//        GMSPlacesClient.provideAPIKey("AIzaSyCZ0k20No0Psf9hRBNdamlq2CXm48rKB2I")
//        GMSServices.provideAPIKey("AIzaSyCZ0k20No0Psf9hRBNdamlq2CXm48rKB2I")
        //key 8
//        GMSServices.provideAPIKey("AIzaSyDHayfnXjP1DqlsgEZNfAazlnspcBLwfwk")
//        GMSPlacesClient.provideAPIKey("AIzaSyDHayfnXjP1DqlsgEZNfAazlnspcBLwfwk")
        //kisisel
        GMSServices.provideAPIKey("AIzaSyArCaCS4FLMmUZPwsnPNjWbwEaKFqZO_3w")
        GMSPlacesClient.provideAPIKey("AIzaSyArCaCS4FLMmUZPwsnPNjWbwEaKFqZO_3w")
        
//        singleNotificationManager = SingleNotificationManager.getInstance()
//        singleNotificationManager.registerNotifications()
        myUser = MyUser.getInstance()
        singleNotificationManager = SingleNotificationManager.getInstance()
        singleLocationManager = SingleLocationManagerBasic.getInstance(didFinishLaunchingWithOptions: launchOptions)
        sharingLocationTransportationManager = SharingLocationTransportationManager.getInstance()
        
//        //Start setting up Cloud Messaging
//        Messaging.messaging().delegate = self
//        //End setting up Cloud Messaging
//        
//        //start authorize for remote notifications, it can be anywhere
//        if #available(iOS 10.0, *) {
//          // For iOS 10 display notification (sent via APNS)
//          UNUserNotificationCenter.current().delegate = self
//
//          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//          UNUserNotificationCenter.current().requestAuthorization(
//            options: authOptions,
//            completionHandler: {_, _ in })
//        } else {
//          let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//          application.registerUserNotificationSettings(settings)
//        }
//
//        application.registerForRemoteNotifications()
//        //end authorize for remote notifications, it can be anywhere
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    //added for Facebook login start
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return ApplicationDelegate.shared.application(
            app,
            open: url,
            options: options
        )
    }
    //added for Facebook login end
    
    //added for Google login start
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
      if let error = error {
        print("AppDelagate.sign Method:" + error.localizedDescription)
        MyUser.getInstance().singInError(error: error)
        return
      }

      guard let authentication = user.authentication else { return }
      let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                        accessToken: authentication.accessToken)
        Auth.auth().signIn(with: credential){ (res, error) in
            if let error = error {
                print("AppDelagate.signIn Method:" + (error.localizedDescription))
                MyUser.getInstance().singInError(error: error)
                return
            }
        MyUser.getInstance().signIn(authDataResult: res)
//            print(res?.user.email ?? "res?.user.email is null")
//            UserDefaults.standard.set(true, forKey: "status")
//            NotificationCenter.default.post(name: NSNotification.Name("statusChange"), object: nil)
        }
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
        print("sign out google method called")
    }
    //added for Google login end
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("applicationDidEnterBackground")
        print("applicationDidEnterBackground")
//        self.singleLocationManager.createRegion()
        //does not call
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        print("applicationWillTerminate")
        print("applicationWillTerminate")
//        self.singleLocationManager.createRegion()
//        self.sharingLocationTransportationManager.storeData()
    }
    
    func applicationWillResignActive(_ application: UIApplication){
        print("applicationWillResignActive")
        print("applicationWillResignActive")
        //does not call
    }
    
    //start Push Notification, did receive remote notification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification

        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)

        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }

        // Print full message.
        print("Notification came to method 3, when notification open, app is dead, will not be called")
        print(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    //start Push Notification, did receive remote notification
    
    //In order to receive notifications you need to implement these method KAV soft
//    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//        print("Fail to Register For Remote Notification With Error: \(error)")
//    }
//    
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        print("Device Register For Remote Notification for deviceToken : \(deviceToken)")
//    }

}

////Cloud Messaging
//extension AppDelegate : MessagingDelegate{
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
//      print("Firebase registration token: \(String(describing: fcmToken))")
//
//      let dataDict:[String: String] = ["token": fcmToken ?? ""]
//      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
//      // TODO: If necessary send token to application server.
//      // Note: This callback is fired at each app startup and whenever a new token is generated.
//    }
//}
//
////User Notifications ...
//@available(iOS 10, *)
//extension AppDelegate : UNUserNotificationCenterDelegate {
//
//  // Receive displayed notifications for iOS 10 devices.
//  func userNotificationCenter(_ center: UNUserNotificationCenter,
//                              willPresent notification: UNNotification,
//    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//    let userInfo = notification.request.content.userInfo
//
//    // With swizzling disabled you must let Messaging know about the message, for Analytics
//    // Messaging.messaging().appDidReceiveMessage(userInfo)
//
//    //Do something with Msg Data
//    if let messageID = userInfo[gcmMessageIDKey]{
//        print("Message ID: \(messageID)")
//    }
//
////    UNUserNotificationCenter.current().removeAllDeliveredNotifications()
//
//    // Print full message.
//    print("Notification came to method 1, when application is open")
//    print(userInfo)
//
//    // Change this to your preferred presentation option
//    completionHandler([[.banner, .badge, .sound]])
//  }
//
//  func userNotificationCenter(_ center: UNUserNotificationCenter,
//                              didReceive response: UNNotificationResponse,
//                              withCompletionHandler completionHandler: @escaping () -> Void) {
//    let userInfo = response.notification.request.content.userInfo
//
//    // ...
//
//    // With swizzling disabled you must let Messaging know about the message, for Analytics
//    // Messaging.messaging().appDidReceiveMessage(userInfo)
//
//    //Do something with Msg Data
//    if let messageID = userInfo[gcmMessageIDKey]{
//        print("Message ID: \(messageID)")
//    }
//
//    // Print full message.
//    print("Notification came to method 2, when application is in background")
//    print(userInfo)
//
//    completionHandler()
//  }
//}
