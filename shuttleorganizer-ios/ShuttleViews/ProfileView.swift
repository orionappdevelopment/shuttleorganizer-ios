//
//  ProfileView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 6/14/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI
import UIKit
import class Kingfisher.KingfisherManager
import class Kingfisher.ImageCache
import struct Kingfisher.KFImage
import struct Kingfisher.RoundCornerImageProcessor
import struct Kingfisher.DownsamplingImageProcessor
import protocol Kingfisher.ImageProcessor
import class Kingfisher.ImageDownloader
import struct Kingfisher.KingfisherWrapper

struct ProfileView: View {
    @ObservedObject private var personName : GenericObservableModel<String> = GenericObservableModel(value: "")
    @ObservedObject private var personPhone : GenericObservableModel<String> = GenericObservableModel(value: "")
    @ObservedObject private var person : GenericObservableModel<Person> = GenericObservableModel(value: Person())
    
    @State private var email : String = ""
    @State private var imageUrl : String = ""
    
//    @ObservedObject private var notifications : GenericObservableModel<[Bool]> = GenericObservableModel(value : [Bool](repeating: true, count:10))
//    @State private var termsAccepted : Bool = false
    
    @ObservedObject private var no1 : GenericObservableModel<Bool> = GenericObservableModel(value: true, callBackWithParentAndPreviousValue: notificationFunction)
    @ObservedObject private var no2 : GenericObservableModel<Bool> = GenericObservableModel(value: true, callBackWithParentAndPreviousValue: notificationFunction)
    @ObservedObject private var no3 : GenericObservableModel<Bool> = GenericObservableModel(value: true, callBackWithParentAndPreviousValue: notificationFunction)
    @ObservedObject private var no4 : GenericObservableModel<Bool> = GenericObservableModel(value: true, callBackWithParentAndPreviousValue: notificationFunction)
    @ObservedObject private var no5 : GenericObservableModel<Bool> = GenericObservableModel(value: true, callBackWithParentAndPreviousValue: notificationFunction)
    
    @State private var notifications : [Bool] = [Bool](repeating: true, count:10)
    
    @State private var isNotificationFirstLoadComplete : Bool = false
    var error : GenericObservableModel<Error>? = nil
    
    @State private var image: Image? = nil
    @State private var shouldPresentImagePicker = false
    @State private var shouldPresentActionScheet = false
    @State private var shouldPresentCamera = false
    
    @State private var showAlert = false
    
    public static var notificationFunction =
        {(notification: Bool, parent : Any, previousValue : Bool) in
            if let validParent = parent as? ProfileView{
                if notification != previousValue && validParent.isNotificationFirstLoadComplete {
                    validParent.savePersonNotification()
                    print(notification)
                }
            }
        }
    
    private func fetchPersonNotification(){
        let personNotification : PersonNotification = PersonNotification(personId: MyUser.getInstance().getUserId(), token: SingleNotificationManager.getInstance().personNotification.token)
        
        NetworkManager.fetch(url: UrlConstants.UrlGetNotification, parameters: personNotification
            , handle: {
                (newData : PersonNotification) -> ()  in
                    self.notifications = newData.getNotifications()
                    self.no1.value = newData.notification1
                    self.no2.value = newData.notification2
                    self.no3.value = newData.notification3
                    self.no4.value = newData.notification4
                    self.no5.value = newData.notification5
                    self.isNotificationFirstLoadComplete = true
                }
            , errorHandle: {
                newError in
                    self.error?.value = newError
                    let errorString : String = "\(newError)"
                    if errorString .contains("ProfileView, keyNotFound(CodingKeys(stringValue: \"personId\", intValue: nil)"){
                        print("Can not found PersonNotification so directly create PersonNotification")
                        self.savePersonNotification()
                    }else{
                        print(newError)
                    }
                    
                })
    }
    
    private func savePersonNotification(){
        let personNotification : PersonNotification = PersonNotification(personId: MyUser.getInstance().getUserId(), token: SingleNotificationManager.getInstance().personNotification.token, loggedIn: true,
            notification1: self.no1.value, notification2: self.no2.value, notification3: self.no3.value,
            notification4: self.no4.value, notification5: self.no5.value,
            notification6: self.notifications[5], notification7: self.notifications[6], notification8: self.notifications[7],
            notification9: self.notifications[8], notification10: self.notifications[9])

        NetworkManager.fetch(url: UrlConstants.UrlSaveNotification, parameters: personNotification
            , handle: {
                (newData : PersonNotification) -> ()  in
                    print("PersonNotification Saved: \(newData)")
                    SingleNotificationManager.getInstance().personNotification = newData
                }
            , errorHandle: {
                newError in
                self.error?.value = newError})
    }
    
    private func fetchPerson(){
        //TODO: need to fetch image url from db then fetch it
        
        let person : Person = Person(id: MyUser.getInstance().getUserId())
        
        NetworkManager.fetch(url: UrlConstants.UrlGetPerson, parameters: person
            , handle: {
                (newData : Person) -> ()  in
//                    self.savedPersonName = newData.name
                    
                    self.personName.value = newData.name
                    self.personPhone.value = newData.phone
                   
                    self.email = newData.email
                    self.person.value = newData
                    self.imageUrl = newData.profilePictureURL
                    self.fetchProfileImage(imageUrl: self.imageUrl)
                }
            , errorHandle: {
                newError in
                self.error?.value = newError})
    }
//    TODO: to save efficently because each change leads to another save in sb
//    https://stackoverflow.com/questions/37805885/how-to-create-dispatch-queue-in-swift-3
        //here 2. Parameterized debounce function this method you can look
//    https://stackoverflow.com/questions/37805885/how-to-create-dispatch-queue-in-swift-3
//    https://stackoverflow.com/questions/37805885/how-to-create-dispatch-queue-in-swift-3
    
    private func savePerson(){
        let person : Person = Person(id: MyUser.getInstance().getUserId(), name: self.personName.value, email: self.email, profilePictureURL: self.imageUrl, phone: self.personPhone.value)
        
        NetworkManager.fetch(url: UrlConstants.UrlSavePerson, parameters: person
            , handle: {
                (newData : Person) -> ()  in
                    print("Person Saved: \(newData)")
                
                    if newData.name != MyUser.getInstance().firebaseName{
                        MyUser.getInstance().changeDisplayNameInFirebase(displayName: newData.name)
                    }
                    if newData.profilePictureURL != MyUser.getInstance().firebasePhotoUrl {
                        MyUser.getInstance().changeProfilePhotoUrlInFirebase(photoUrl: newData.profilePictureURL)
                    }
                    
                    self.person.value = newData
                    self.personName.value = newData.name
                    self.personPhone.value = newData.phone

                    self.hideKeyboard()
                }
            , errorHandle: {
                newError in
                self.error?.value = newError})
    }
    
    func profilePhotoPressed(){
        print("profile pressed")
        self.shouldPresentActionScheet = true
    }
    
    private func saveProfileImage(image : UIImage?){
        if self.person.value.id == ""{
            print("can not save Profile Image because personId not valid")
            return
        }
        
        if image == nil{
            print("can not save Profile Image because image is nil")
            return
        }

        let newImage = image!

        let directoryPath = Utils.getImageDirectoryFromFileName(self.person.value.id)

//        let urlString = "http://46.101.106.121/test/img/profile_images/main\(directoryPath)\(self.person.value.id)\("_1612639666471").jpg"
        guard let data = newImage.jpegData(compressionQuality: 0) else {
                return
            }
        let imageData : String = data.base64EncodedString()
        let currentTimeInMiliseconds = Int64(Date().timeIntervalSince1970 * 1000)
        let imageName = "\(person.value.id)_\(currentTimeInMiliseconds).jpg"
        
        var mapParams : [String : String] = [:]
        mapParams["name"] = imageName
        mapParams["image_data"] = imageData
//        mapParams["thumb_image_data"] = imageData
        mapParams["type"] = "profile_images"
        mapParams["path"] = directoryPath
        mapParams["user_id"] = person.value.id

        NetworkManager.fetch(url: UrlConstants.UrlSaveImage, parameters: mapParams
            , handle: {
                (newData : UploadImageResponse) -> ()  in
                    print("Person image Saved: \(newData)")
                    if newData.result == .ok{
                        self.imageUrl = newData.url
                        savePerson()
                    }
                }
            , errorHandle: {
                newError in
                self.error?.value = newError})
    }
    
    private func fetchProfileImage(imageUrl : String){
        if self.person.value.id == ""{
            return
        }
        
//        let directoryPath = Utils.getImageDirectoryFromFileName(self.person.value.id)
//
////        let urlString = "http://46.101.106.121/test/img/main/\(self.person.value.id).jpg"
//        let urlString = "http://46.101.106.121/test/img/profile_images/main\(directoryPath)\(self.person.value.id)\("_1612639666471").jpg"
        
//        let urlString = self.person.value.profilePictureURL
        let urlString = imageUrl
        print("urlStringImage:\(urlString)")
//        let urlString = "http://46.101.106.121/test/img/profile_images/main/194/137/bQm1w3Uqithl8EJ3DEFP7CvBvI73_1612639666471.jpg"
        let url = URL(string: urlString)
        //        let url = URL(string: "http://46.101.106.121/test/img/main/bQm1w3Uqithl8EJ3DEFP7CvBvI73.jpg")
        //    let url = URL(string: "https://example.com/high_resolution_image.png")
        //        url = URL(string: "https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg")
        //        url = URL(string: "https://upload.wikimedia.org/wikipedia/commons/f/ff/Pizigani_1367_Chart_10MB.jpg")
        
        //        let processor = DownsamplingImageProcessor(size: imageView.bounds.size) |> RoundCornerImageProcessor(cornerRadius: 0)
        //        let processor = DownsamplingImageProcessor(size: imageView.bounds.size).append(another: RoundCornerImageProcessor(cornerRadius: 0))
        let processor = RoundCornerImageProcessor(cornerRadius: 0)
        let placeholder = UIImage(named: "user_placeholder")
        
        let uiImage = UIImage(named: "user_placeholder")
        let uiImageView = UIImageView(image: uiImage)
        //        imageDownloaders[personId]
        uiImageView.kf.indicatorType = .activity
        uiImageView.kf.setImage(
            with: url,
            placeholder: placeholder,
            options: [
                .processor(processor),
                //                .scaleFactor(10),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ], completionHandler:
                {
                    result in
                    switch result {
                    case .success(let value):
                        print("Task done for: \(value.source.url?.absoluteString ?? "")")
                        if let validUIImage = uiImageView.image{
                            self.image = Image(uiImage: validUIImage)
                        }
                    case .failure(let error):
                        print("Job failed: \(error.localizedDescription)")
                    }
                })
    }
    
    let radius: CGFloat = 70
    var offset: CGFloat {
        sqrt(radius * radius / 2)
    }
    
    //ProfileView
    var body: some View {
        GeometryReader { geometry in
            NavigationView{
//            ZStack{
//                Color.colorBackground.edgesIgnoringSafeArea(.all) // remove this part othervise it affect the tab opacity
                ScrollView(.vertical, showsIndicators: true ) {
                    VStack(){
                        VStack{
                            if self.image == nil{
                                Image("user_placeholder")
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(
                                            width: radius * 2,
                                            height: radius * 2)
                                        .clipShape(Circle())
                                        .overlay(
                                            Image("camera")
                                                .renderingMode(.template)
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .foregroundColor(Color.white)
                                                .frame(
                                                    width: 30,
                                                    height: 30)
                                                .padding(8)
    //                                            .background(Color.gray)
                                                .clipShape(Circle())
                                                .background(
                                                    Circle()
    //                                                        .stroke(Color.white, lineWidth: 2)
                                                        .fill(Color.colorPrimary)
                                                )
                                                .offset(x: offset, y: offset)
                                        )
                                
                                    .onTapGesture {
                                        self.profilePhotoPressed()
                                    }
                            }else{
                                self.image?
                                    .renderingMode(.original)
                                    .resizable()
                                    .aspectRatio(contentMode: .fill)
                                    .frame(
                                        width: radius * 2,
                                        height: radius * 2)
                                    .clipShape(Circle())
                                    .overlay(
                                        Image("camera")
                                            .renderingMode(.template)
                                            .resizable()
                                            .aspectRatio(contentMode: .fit)
                                            .foregroundColor(Color.white)
                                            .frame(
                                                width: 30,
                                                height: 30)
                                            .padding(8)
//                                            .background(Color.gray)
                                            .clipShape(Circle())
                                            .background(
                                                Circle()
//                                                        .stroke(Color.white, lineWidth: 2)
                                                    .fill(Color.colorPrimary)
                                            )
                                            .offset(x: offset, y: offset)
                                    )
                                    .onTapGesture {
                                        self.profilePhotoPressed()
                                    }
                            }
                        }
                        
                        
                        VStack{
                        
                            TextFieldWithTopLabel(width: geometry.size.width * 0.92, titleBackgroundColor: Color.colorBackground,
                                                  titleValue : GenericObservableModel(value: "ProfileView.Name.text"),
                                                  textFieldValue: self.personName)
                                .padding(EdgeInsets(top: 10, leading: 0, bottom: 0, trailing: 0))
                           
                            
                            TextFieldWithTopLabel(width: geometry.size.width * 0.92, titleBackgroundColor: Color.colorBackground,
                                                  titleValue : GenericObservableModel(value: "ProfileView.Phone.text"),
                                                  textFieldValue: self.personPhone)
                                .padding(EdgeInsets(top: 10, leading: 0, bottom: 0, trailing: 0))
                            
                            TextFieldWithTopLabel(width: geometry.size.width * 0.92,
                                                  titleForegroundColor: Color.colorDarkGray,
                                                  titleBackgroundColor: Color.colorBackground,
                                                  titleValue : GenericObservableModel(value: "ProfileView.Email.text"),
                                                  textFieldValue: GenericObservableModel(value: self.email)
                                                  )
                                .disabled(true)
                                .foregroundColor(Color.colorDarkGray)
                                .padding(EdgeInsets(top: 10, leading: 0, bottom: 0, trailing: 0))
                        }
                        .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 2))
                        
                        Spacer()
                            .frame(height: 30, alignment: .bottom)
                        
                        VStack(alignment: .leading){
                            Text("ProfileView.NotificationTitle.text").font(.title)
                            Toggle(isOn: $no1.value, label: {Text("ProfileView.ManagementNotification.text")})
                                .toggleStyle(SwitchToggleStyle(tint: Color.colorSecondary))
                            Toggle(isOn: $no2.value, label: { Text("ProfileView.ShuttleNotification.text") })
                                .toggleStyle(SwitchToggleStyle(tint: Color.colorSecondary))
                            Toggle(isOn: $no3.value, label: { Text("ProfileView.ChatNotification.text") })
                                .toggleStyle(SwitchToggleStyle(tint: Color.colorSecondary))
                        }
                            .padding(.bottom, 20)
                        
                        /*
                        VStack(alignment: .leading){
                            Text("ProfileView.PrivacyTitle.text").font(.title)
                            Toggle(isOn: $no4.value, label: { Text("ProfileView.HidePhoneNumber.text") })
                                .toggleStyle(SwitchToggleStyle(tint: Color.colorSecondary))
                            Toggle(isOn: $no5.value, label: { Text("ProfileView.HideEmail.text") })
                                .toggleStyle(SwitchToggleStyle(tint: Color.colorSecondary))
                        }
                        */
                         
                        Spacer()
                            .frame(height: 20, alignment: .bottom)
                        
                        Button(action: {self.showAlert = true /*MyUser.getInstance().signOut()*/}){
                            HStack(){
                                Text("ProfileView.SignOut.text")
                                    .foregroundColor(Color.red)
                                    .fontWeight(.bold)
                                    .font(.system(size: 20))
                            }
                            .frame(maxWidth: .infinity, minHeight: 50, idealHeight: 50)
                        }
                        .onTapGesture {self.showAlert = true /*MyUser.getInstance().signOut()*/}
                        .padding(.bottom, 10)
                        
//                        Spacer()
//                            .frame(height: 50, alignment: .bottom)
                    }
                    .padding(EdgeInsets(top: 20, leading: 20, bottom: 0, trailing: 20))
                    .background(Color.colorBackground)
                    .onAppear(){
                        print("on appear called")
                        self.no1.parent = self
                        self.no2.parent = self
                        self.no3.parent = self
                        self.no4.parent = self
                        self.no5.parent = self
                        self.fetchPerson()
                        self.fetchPersonNotification()
                        
                    }
                    .onTapGesture {
                        self.hideKeyboard()
                    }
                    .alert(isPresented: self.$showAlert) {
                        Alert(title: Text(""),
                              message: Text(toStringFromLocalizedStringKey(localizedStringKey: "ProfileView.Alert.SignOut.text")),
                              primaryButton: .destructive(Text("ProfileView.Alert.SignOutButton.text"), action: {MyUser.getInstance().signOut()}),
                              secondaryButton: .cancel())
                    }
                    .sheet(isPresented: $shouldPresentImagePicker) {
                        SUImagePickerView(sourceType: self.shouldPresentCamera ? .camera : .photoLibrary, image: self.$image, isPresented: self.$shouldPresentImagePicker, doSubmit: self.saveProfileImage)}
                    .actionSheet(isPresented: $shouldPresentActionScheet) { () -> ActionSheet in
                        ActionSheet(title: Text("ProfileView.ActionSheet.Title.text"),
                            buttons: [ActionSheet.Button.default(Text("ProfileView.ActionSheet.CameraButton.text"), action: {
                            self.shouldPresentImagePicker = true
                            self.shouldPresentCamera = true
                        }), ActionSheet.Button.default(Text("ProfileView.ActionSheet.PhotoLibraryButton.text"), action: {
                            self.shouldPresentImagePicker = true
                            self.shouldPresentCamera = false
                        }), ActionSheet.Button.cancel()])
                    }
                    .navigationBarItems(
                        leading:
                            VStack{
//                                if self.personName.value != self.personNameState || self.personPhone.value != self.personPhoneState {
                                if self.person.value.name != self.personName.value || self.person.value.phone != self.personPhone.value {
                                    Button("ProfileView.Navigation.CancelButton.text"){
                                        self.fetchPerson()
                                        self.hideKeyboard()
                                    }
                                }
                            },
                        trailing:
                            VStack{
//                                if self.personName.value != self.personNameState || self.personPhone.value != self.personPhoneState {
                                if self.person.value.name != self.personName.value || self.person.value.phone != self.personPhone.value {
                                    Button("ProfileView.Navigation.SaveButton.text"){
                                        self.savePerson()
                                    }
                                }
                            }
                    )
                    .navigationBarTitle("ProfileView.Navigation.Title.text", displayMode: .inline)
                    //.navigationBarHidden(true)
                }
//            }
        }}
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
