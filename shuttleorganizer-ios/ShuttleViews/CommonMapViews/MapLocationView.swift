//
//  MapLocationSelectView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 6/28/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation
import UIKit
import GooglePlaces
import GoogleMaps
import SwiftUI

struct MapLocationView: UIViewControllerRepresentable {
    @ObservedObject var observedCoordinate: GenericObservableModel<CLLocationCoordinate2D>
    @State var cornerRadius : CGFloat = 0
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    //mapViewCoordinator added to prove that coordinator can be controlled outside,tested with next location
//    @ObservedObject var mapLocationViewCoordinator : GenericObservableModel<MapLocationViewCoordinator?>
    
    var doSubmit : () -> () = {}
    
    var doClose : () -> () = {}
    
    
    public class MapLocationViewCoordinator: NSObject, UINavigationControllerDelegate, GMSAutocompleteViewControllerDelegate {
        var mapLocationView: MapLocationView
        var family : MapLocationViewGoogleMapController? = nil

        init(_ mapLocationView: MapLocationView) {
            self.mapLocationView = mapLocationView
        }
        
        func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//            print("Place name: \(place.name)")
//            print("Place ID: \(place.placeID)")
//            print("Place attributions: \(place.attributions)")
            print("Place coordinate: \(place.coordinate)")
            viewController.dismiss(animated: true, completion: nil)
            family?.nextLocation(coordinate: place.coordinate)
        }

        func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
            // TODO: handle the error.
            print("Error: ", error.localizedDescription)
        }

        // User canceled the operation.
        func wasCancelled(_ viewController: GMSAutocompleteViewController) {
            viewController.dismiss(animated: true, completion: nil)
        }

        // Turn the network activity indicator on and off again.
        func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }

        func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }

    }

    func makeCoordinator() -> MapLocationViewCoordinator {
        MapLocationViewCoordinator(self)
    }
    
    
    func makeUIViewController(context: Context) -> MapLocationViewGoogleMapController {
        let mapLocationViewGoogleMapController = MapLocationViewGoogleMapController()
        mapLocationViewGoogleMapController.mapLocationViewCoordinator = context.coordinator
        context.coordinator.family = mapLocationViewGoogleMapController
        return mapLocationViewGoogleMapController
    }

    func updateUIViewController(_ uiViewController: MapLocationViewGoogleMapController, context: Context) {
        print("MapLocationView updateUIViewController() called")
//        if self.mapLocationViewCoordinator.value != context.coordinator{
//            self.mapLocationViewCoordinator.value = context.coordinator
//        }
//        let currentCoordinate = context.coordinator.mapLocationView.observedCoordinate.value
//
//        if let mapLocationViewGoogleMapController = context.coordinator.family{
//            mapLocationViewGoogleMapController.nextLocation(coordinate: currentCoordinate)
//        }
        let currentCoordinate = self.observedCoordinate.value

        if let mapLocationViewGoogleMapController = context.coordinator.family{
            mapLocationViewGoogleMapController.nextLocation(coordinate: currentCoordinate)
        }
    }
}


class MapLocationViewGoogleMapController: UIViewController {
    var mapLocationViewCoordinator : MapLocationView.MapLocationViewCoordinator? = nil
    var mapView : GMSMapView?
    var marker : GMSMarker?
    var centerMarkerImageView : UIImageView?
    final var defaultZoomLevel : Float = 10
    final var defaultCoordinate : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 35.86, longitude: 40.20)
    
    let locationManager = CLLocationManager()
    
    public func nextLocation(coordinate : CLLocationCoordinate2D){
        let currentZoom = self.mapView?.camera.zoom;
        let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: currentZoom ?? defaultZoomLevel)
        mapView?.camera = camera
    }
    
    override func loadView() {
        super.loadView()
        viewWillLayoutSubviews()
        print(self.view.bounds)
        
        let camera : GMSCameraPosition
        if let validBindingCoordinate = mapLocationViewCoordinator?.mapLocationView.observedCoordinate{
            camera = GMSCameraPosition.camera(withLatitude: validBindingCoordinate.value.latitude, longitude: validBindingCoordinate.value.longitude, zoom: defaultZoomLevel)
        }else{
            camera = GMSCameraPosition.camera(withLatitude: defaultCoordinate.latitude, longitude: defaultCoordinate.longitude, zoom: defaultZoomLevel)
        }
        
        mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        mapView?.isUserInteractionEnabled = false
//        mapView?.settings.compassButton = true
        mapView?.isMyLocationEnabled = true
//        mapView?.settings.myLocationButton = true
//        mapView?.accessibilityElementsHidden = true
        
        if let validCornerRadius = mapLocationViewCoordinator?.mapLocationView.cornerRadius{
            mapView?.layer.cornerRadius = validCornerRadius
        }
//        self.view.layer.cornerRadius = 10
        
        self.view.addSubview(mapView!)
        makeFullScreenForUIView(of: mapView!, to: self.view)
  }
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.centerMarkerImageView!.center = CGPoint(x: self.mapView!.bounds.width / 2, y: (self.mapView!.bounds.height - self.centerMarkerImageView!.frame.height) / 2  )
//        checkLocationServices()
        print("did load")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let camera : GMSCameraPosition
        if let validBindingCoordinate = mapLocationViewCoordinator?.mapLocationView.observedCoordinate{
            camera = GMSCameraPosition.camera(withLatitude: validBindingCoordinate.value.latitude, longitude: validBindingCoordinate.value.longitude, zoom: defaultZoomLevel)
        }else{
            camera = GMSCameraPosition.camera(withLatitude: defaultCoordinate.latitude, longitude: defaultCoordinate.longitude, zoom: defaultZoomLevel)
        }
        //ViewDidAppear better place for center map and checklocationservices otherwise map center to invalid place
        self.mapView?.camera = camera
        checkLocationServices()
        
//        let closeImage = UIImage(systemName: "xmark.circle.fill")!
//            .withRenderingMode(.alwaysTemplate)
//            .withTintColor(UIColor.colorIosCloseButtonInsideXColor!)
//            .circularImage(cgSize: CGSize(width: 30, height: 30))!
////            .withTintColor()
        
//        let closeButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
////        closeButton.backgroundColor = UIColor.black
//        closeButton.setImage(closeImage, for: .normal)
//        closeButton.center = CGPoint(x: self.view.bounds.width - 8 - closeButton.frame.size.width/2, y: 8 + closeButton.frame.size.height/2 )
//        closeButton.addTarget(self, action: #selector(showMyCoordinate), for: .touchUpInside)
//        //make button circle start
////        closeButton.layer.masksToBounds = true
////        closeButton.layer.cornerRadius = closeButton.frame.width/2
//        //make button circle end
//        self.mapView?.addSubview(closeButton)
        
//        let showMyCoordinateButton = UIButton(frame: CGRect(x: 0, y: 20, width: self.view.bounds.width, height: 50))
//        showMyCoordinateButton.setTitle(toStringFromLocalizedStringKey(localizedStringKey: "MapLocationSelectUIView.SelectThisLocationButton.text"), for: .normal)
//        showMyCoordinateButton.backgroundColor = .white
//        showMyCoordinateButton.setTitleColor(UIColor.primary, for: .normal)
//        showMyCoordinateButton.center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height - showMyCoordinateButton.frame.size.height/2 )
//        showMyCoordinateButton.addTarget(self, action: #selector(showMyCoordinate), for: .touchUpInside)
//        self.mapView?.addSubview(showMyCoordinateButton)
        
        let centerMarkerImage : UIImage = getResizedUIImage(image: UIImage(named: "map_pin_big_head_hole")!, newWidth: 50)
        self.centerMarkerImageView = UIImageView(image: centerMarkerImage)
//        self.centerMarkerImageView?.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        self.centerMarkerImageView!.center = CGPoint(x: self.mapView!.bounds.width / 2, y: (self.mapView!.bounds.height - self.centerMarkerImageView!.frame.height) / 2  )
        self.mapView?.addSubview(self.centerMarkerImageView!)
        
//        let searchButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
//        let searchImage : UIImage? = UIImage(named: "search")
//        searchButton.setImage(searchImage, for: .normal)
//        searchButton.center = CGPoint(x: self.view.bounds.width * 0.9, y: 0 + (searchButton.frame.height * 2) )
//        searchButton.addTarget(self, action: #selector(autocompleteClicked), for: .touchUpInside)
//        self.mapView?.addSubview(searchButton)
        
//        let currentLocationButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
//        let currentLocationImage : UIImage? = UIImage(named: "current_location")
//        currentLocationButton.setImage(currentLocationImage, for: .normal)
//        currentLocationButton.center = CGPoint(x: self.view.bounds.width * 0.9, y: 0 + (searchButton.frame.height * 3) )
//        currentLocationButton.addTarget(self, action: #selector(didTapOnMyLocation), for: .touchUpInside)
//        self.mapView?.addSubview(currentLocationButton)
        
        
//        let showMyCoordinateButton = UIButton(frame: CGRect(x: 0, y: 20, width: self.view.bounds.width, height: 50))
//        showMyCoordinateButton.setTitle(toStringFromLocalizedStringKey(localizedStringKey: "MapLocationSelectUIView.SelectThisLocation"), for: .normal)
//        showMyCoordinateButton.backgroundColor = .white
//        showMyCoordinateButton.setTitleColor(toUIColor(color: Color.primary), for: .normal)
//        showMyCoordinateButton.center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height - showMyCoordinateButton.frame.size.height/2 )
//        showMyCoordinateButton.addTarget(self, action: #selector(showMyCoordinate), for: .touchUpInside)
//        self.mapView?.addSubview(showMyCoordinateButton)
        
        print("view did appear called")
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        print("bounds = \(self.view.bounds)")
    }
    
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled(){
            setupLocationManager()
            checkLocationAuthorization()
        } else {
            // Show alert letting the user know they have to turn this on.
            print("Please open the device location")
        }
    }
    
    
    func checkLocationAuthorization() {
        switch CLLocationManager().authorizationStatus{
            case .authorizedWhenInUse:
//                didTapOnMyLocation(nil)
                locationManager.startUpdatingLocation()
                break
            case .denied:
                // Show alert instructing them how to turn on permissions
                print("Device location service restricted")
                break
            case .notDetermined:
    //            locationManager.requestWhenInUseAuthorization()
                locationManager.requestAlwaysAuthorization()
            case .restricted:
                // Show an alert letting them know what's up
                print("Device location service restricted")
                break
            case .authorizedAlways:
                print("location authorization is authorizedAlways")
                break
            default :
                print("location authorization is default which is unexpected")
                break
        }
    }
}

extension MapLocationViewGoogleMapController: CLLocationManagerDelegate {
    
    //called when device current location changed
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        guard let location = locations.last else { return }
//        let region = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
//        mapView.setRegion(region, animated: true)
//        didTapOnMyLocation(nil)
        locationManager.stopUpdatingLocation()
    }
    
    //called when device location permission changed
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
}
