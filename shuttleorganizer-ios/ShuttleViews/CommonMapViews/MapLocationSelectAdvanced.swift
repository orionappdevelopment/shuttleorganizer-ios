//
//  MapLocationSelectView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 6/28/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation
import UIKit
import GooglePlaces
import GoogleMaps
import SwiftUI

struct MapLocationSelectAdvanced: UIViewControllerRepresentable {
    @Binding var bindingCoordinate: CLLocationCoordinate2D
    @Binding var realMap: GMSMapView?
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    var isFullScreen : Bool
    
    public class MapLocationSelectAdvancedSelectCoordinator: NSObject, UINavigationControllerDelegate, GMSAutocompleteViewControllerDelegate {
        var mapLocationSelectUIView: MapLocationSelectAdvanced
        var family : MapLocationSelectAdvancedGoogleMapController? = nil
        var isFullScreen : Bool
        
        init(_ mapLocationSelectUIView: MapLocationSelectAdvanced, isFullScreen: Bool) {
            self.mapLocationSelectUIView = mapLocationSelectUIView
            self.isFullScreen = isFullScreen
        }
        
        func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//            print("Place name: \(place.name)")
//            print("Place ID: \(place.placeID)")
//            print("Place attributions: \(place.attributions)")
            print("Place coordinate: \(place.coordinate)")
            viewController.dismiss(animated: true, completion: nil)
            family?.nextLocation(coordinate: place.coordinate)
        }

        func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
            // TODO: handle the error.
            print("Error: ", error.localizedDescription)
        }

        // User canceled the operation.
        func wasCancelled(_ viewController: GMSAutocompleteViewController) {
            viewController.dismiss(animated: true, completion: nil)
        }

        // Turn the network activity indicator on and off again.
        func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }

        func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }

    }

    func makeCoordinator() -> MapLocationSelectAdvancedSelectCoordinator {
        MapLocationSelectAdvancedSelectCoordinator(self, isFullScreen: self.isFullScreen)
    }
    
    
    func makeUIViewController(context: Context) -> MapLocationSelectAdvancedGoogleMapController {
        let mapLocationSelectGoogleMapController = MapLocationSelectAdvancedGoogleMapController()
        mapLocationSelectGoogleMapController.mapLocationSelectCoordinator = context.coordinator
        context.coordinator.family = mapLocationSelectGoogleMapController
        return mapLocationSelectGoogleMapController
    }

    func updateUIViewController(_ uiViewController: MapLocationSelectAdvancedGoogleMapController, context: Context) {
        print("MapLocationSelectAdvanced updateUIViewController() called")
    }
}


class MapLocationSelectAdvancedGoogleMapController: UIViewController {
    var mapLocationSelectCoordinator : MapLocationSelectAdvanced.MapLocationSelectAdvancedSelectCoordinator? = nil
    var mapView : GMSMapView?
    var marker : GMSMarker?
    var centerMarkerImageView : UIImageView?
    final var defaultZoomLevel : Float = 10
    final var defaultCoordinate : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 41.0370, longitude: 28.9851)

    let locationManager = CLLocationManager()
    
    public func nextLocation(coordinate : CLLocationCoordinate2D){
        print("next Location: \(coordinate)")
        let currentZoom = self.mapView?.camera.zoom;
        let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: currentZoom ?? defaultZoomLevel)
        mapView?.camera = camera
    }
    
    override func loadView() {
        super.loadView()
        viewWillLayoutSubviews()
        print(self.view.bounds)
        
        let camera : GMSCameraPosition
        if let validBindingCoordinate = mapLocationSelectCoordinator?.mapLocationSelectUIView.bindingCoordinate{
            camera = GMSCameraPosition.camera(withLatitude: validBindingCoordinate.latitude, longitude: validBindingCoordinate.longitude, zoom: defaultZoomLevel)
        }else{
            camera = GMSCameraPosition.camera(withLatitude: defaultCoordinate.latitude, longitude: defaultCoordinate.longitude, zoom: defaultZoomLevel)
        }
        
        mapView = GMSMapView.map(withFrame: self.view.frame  , camera: camera)
//        mapView?.settings.compassButton = true
        mapView?.isMyLocationEnabled = true
//        mapView?.settings.myLocationButton = true
//        mapView?.accessibilityElementsHidden = true
        
        self.view.addSubview(mapView!)
        if mapLocationSelectCoordinator != nil && mapLocationSelectCoordinator!.isFullScreen {
            makeFullScreenForUIView(of: mapView!, to: self.view)
            mapView?.camera = camera
        }
  }
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.centerMarkerImageView!.center = CGPoint(x: self.mapView!.bounds.width / 2, y: (self.mapView!.bounds.height - self.centerMarkerImageView!.frame.height) / 2  )
//        checkLocationServices()
        print("did load")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.mapLocationSelectCoordinator?.mapLocationSelectUIView.realMap = mapView
        
//        let showMyCoordinateButton = UIButton(frame: CGRect(x: 0, y: 20, width: self.view.bounds.width, height: 50))
//        showMyCoordinateButton.setTitle("Select", for: .normal)
//        showMyCoordinateButton.backgroundColor = .white
//        showMyCoordinateButton.setTitleColor(UIColor.primary, for: .normal)
//        showMyCoordinateButton.center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height - showMyCoordinateButton.frame.size.height/2 )
//        showMyCoordinateButton.addTarget(self, action: #selector(showMyCoordinate), for: .touchUpInside)
//        self.mapView?.addSubview(showMyCoordinateButton)
        let camera : GMSCameraPosition
        if let validBindingCoordinate = mapLocationSelectCoordinator?.mapLocationSelectUIView.bindingCoordinate{
            camera = GMSCameraPosition.camera(withLatitude: validBindingCoordinate.latitude, longitude: validBindingCoordinate.longitude, zoom: defaultZoomLevel)
        }else{
            camera = GMSCameraPosition.camera(withLatitude: defaultCoordinate.latitude, longitude: defaultCoordinate.longitude, zoom: defaultZoomLevel)
        }
        //ViewDidAppear better place for center map and checklocationservices otherwise map center to invalid place
        self.mapView?.camera = camera
        checkLocationServices()
        
        let centerMarkerImage : UIImage = getResizedUIImage(image: UIImage(named: "map_pin_big_head_hole")!, newWidth: 50)
        self.centerMarkerImageView = UIImageView(image: centerMarkerImage)
//        self.centerMarkerImageView?.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        self.centerMarkerImageView!.center = CGPoint(x: self.mapView!.bounds.width / 2, y: (self.mapView!.bounds.height - self.centerMarkerImageView!.frame.height) / 2  )
        self.mapView?.addSubview(self.centerMarkerImageView!)
        
        let searchButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        let searchImage : UIImage? = UIImage(named: "search")
        searchButton.setImage(searchImage, for: .normal)
        searchButton.center = CGPoint(x: self.view.bounds.width * 0.9, y: 0 + (searchButton.frame.height * 2) )
        searchButton.addTarget(self, action: #selector(autocompleteClicked), for: .touchUpInside)
        self.mapView?.addSubview(searchButton)
        
        let currentLocationButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        let currentLocationImage : UIImage? = UIImage(named: "current_location")
        currentLocationButton.setImage(currentLocationImage, for: .normal)
        currentLocationButton.center = CGPoint(x: self.view.bounds.width * 0.9, y: 0 + (searchButton.frame.height * 3) )
        currentLocationButton.addTarget(self, action: #selector(didTapOnMyLocation), for: .touchUpInside)
        self.mapView?.addSubview(currentLocationButton)
        
        
//        let showMyCoordinateButton = UIButton(frame: CGRect(x: 0, y: 20, width: self.view.bounds.width, height: 50))
//        showMyCoordinateButton.setTitle(toStringFromLocalizedStringKey(localizedStringKey: "MapLocationSelectAdvanced.SelectThisLocation"), for: .normal)
//        showMyCoordinateButton.backgroundColor = .white
//        showMyCoordinateButton.setTitleColor(toUIColor(color: Color.primary), for: .normal)
//        showMyCoordinateButton.center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height - showMyCoordinateButton.frame.size.height/2 )
//        showMyCoordinateButton.addTarget(self, action: #selector(showMyCoordinate), for: .touchUpInside)
//        self.mapView?.addSubview(showMyCoordinateButton)
        
        print("view did appear called")
    }
    
    // Present the Autocomplete view controller when the button is pressed.
    // this is called when GMSPlace selected
    @objc func autocompleteClicked(_ sender: UIButton) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = mapLocationSelectCoordinator!
        print("autocompleted clicked")

        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue) )
        autocompleteController.placeFields = fields

        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        if let validCountryCode = Utils.getCountryCode(){
            filter.countries = [validCountryCode]
        }
        autocompleteController.autocompleteFilter = filter

        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }

    @objc func didTapOnMyLocation(_ sender: UIButton?) {
    //This code is used for move the marker to the user based location
        if self.mapView != nil && self.mapView?.myLocation != nil {
            let myCLLocation : CLLocation = (self.mapView?.myLocation!)!
            let myCLLocationCoordinate2D : CLLocationCoordinate2D = myCLLocation.coordinate
            if CLLocationCoordinate2DIsValid( myCLLocationCoordinate2D ) {
                let currentZoom = self.mapView?.camera.zoom;
                let camera  = GMSCameraPosition.camera(withLatitude: myCLLocationCoordinate2D.latitude, longitude: myCLLocationCoordinate2D.longitude, zoom: currentZoom ?? defaultZoomLevel)
                self.mapView?.camera = camera
            }
        }
    }
    
    @objc func showMyCoordinate(_ sender: UIButton){
//        let point : CGPoint = self.mapView!.center
//        let centerCoordinate : CLLocationCoordinate2D = self.mapView!.projection.coordinate(for: point)
//        print("centerCoordinate: \(centerCoordinate)")
        let centerCoordinate : CLLocationCoordinate2D = self.mapView!.camera.target
        print("centerCoordinate: \(centerCoordinate)")
        //this one return back to caller navigation page
        self.mapLocationSelectCoordinator?.mapLocationSelectUIView.bindingCoordinate = centerCoordinate
        self.mapLocationSelectCoordinator?.mapLocationSelectUIView.presentationMode.wrappedValue.dismiss()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        print("bounds = \(self.view.bounds)")
    }
    
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled(){
            setupLocationManager()
            checkLocationAuthorization()
        } else {
            // Show alert letting the user know they have to turn this on.
            print("Please open the device location")
        }
    }
    
    
    func checkLocationAuthorization() {
        switch CLLocationManager().authorizationStatus {
            case .authorizedWhenInUse:
                didTapOnMyLocation(nil)
                locationManager.startUpdatingLocation()
                break
            case .denied:
                // Show alert instructing them how to turn on permissions
                print("Device location service restricted")
                break
            case .notDetermined:
    //            locationManager.requestWhenInUseAuthorization()
                locationManager.requestAlwaysAuthorization()
            case .restricted:
                // Show an alert letting them know what's up
                print("Device location service restricted")
                break
            case .authorizedAlways:
                print("location authorization is authorizedAlways")
                break
            default :
                print("location authorization is default which is unexpected")
                break
        }
    }
}

extension MapLocationSelectAdvancedGoogleMapController: CLLocationManagerDelegate {
    
    //called when device current location changed
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        guard let location = locations.last else { return }
//        let region = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
//        mapView.setRegion(region, animated: true)
        didTapOnMyLocation(nil)
        locationManager.stopUpdatingLocation()
    }
    
    //called when device location permission changed
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
}
