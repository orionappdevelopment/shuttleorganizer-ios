//
//  MainTransportationsTabview.swift
//  shuttleorganizer-ios
//
//  Created by macos on 6/13/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct MainTransportationsTabview: View {
    @ObservedObject private var selectedTabIndex: GenericObservableModel<Int> = GenericObservableModel(value: 0)
//    @State private var selectedTabIndex = 0
    
    
    //these variables added because after selectedTabIndex ObservedObject does not update.
//    @State private var joinedTransportationsListObservable : JoinedTransportationsListObservable = JoinedTransportationsListObservable()
//    @State private var createdTransportationsListObservable : CreatedTransportationsListObservable = CreatedTransportationsListObservable()
    
    @State private var joinedTransportationsListView : JoinedTransportationsListView = JoinedTransportationsListView()
    @State private var createdTransportationsListView : CreatedTransportationsListView = CreatedTransportationsListView()
    
    init(){
        let coloredNavAppearance = UINavigationBarAppearance()
//        coloredNavAppearance.configureWithOpaqueBackground()
        coloredNavAppearance.shadowColor = .clear
//        coloredNavAppearance.backgroundColor = UIColor.colorBarBackground
        coloredNavAppearance.titleTextAttributes = [.foregroundColor: UIColor.colorPrimary!]
        coloredNavAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.colorPrimary!]

        UINavigationBar.appearance().standardAppearance = coloredNavAppearance
        UINavigationBar.appearance().backgroundColor = UIColor.colorBarBackground

        let transportationNotificationName = ShuttleNotificationEventFactory.getInstance().getEventNotificationName(shuttleNotificationTypeEnum: .TRANSPORTATION_NOTIFICATION)
        NotificationCenter.default.addObserver(forName: transportationNotificationName, object: nil, queue: nil, using: self.notificationCame)
    }
    
    func notificationCame(_ notification: Notification) {
        if let shuttleNotificationWrapper : ShuttleNotificationWrapper = notification.object as? ShuttleNotificationWrapper{
            print("shuttleNotificationWrapper: \(shuttleNotificationWrapper)")
            let shuttleNotificationTypeEnum : ShuttleNotificationTypeEnum = shuttleNotificationWrapper.shuttleNotificationModel.getShuttleNotificationTypeEnum()
            if shuttleNotificationTypeEnum == .TRANSPORTATION_NOTIFICATION && self.selectedTabIndex.value != 0 {
                self.selectedTabIndex.value = 0
            }else{
                print("MainTransportationsTabview notification event will disregard for notification type: \(shuttleNotificationTypeEnum)")
            }
        }else{
            print("Error : MainTransportationsTabview can not parse notification for notification.object:\(notification.object ?? "")")
        }
    }
    
    var body: some View {
        NavigationView{
            VStack(alignment: .leading, spacing : -8) {
                
                SlidingTabView(selection: self.$selectedTabIndex.value, tabs: ["MainTransportationsTabView.Joined.text", "MainTransportationsTabView.Created.text"])
//                SlidingTabView(selection: self.$selectedTabIndex, tabs: ["MainTransportationsTabView.Joined.text", "MainTransportationsTabView.Created.text"])
                
                VStack(spacing : 0){
//                    if self.selectedTabIndex.value == 0 {
                    if self.selectedTabIndex.value == 0 {
                        joinedTransportationsListView
//                            .environmentObject(JoinedTransportationsListObservable.getInstance())
//                            .environmentObject(createdTransportationsListObservable)
                    } else {
                        createdTransportationsListView
//                            .environmentObject(createdTransportationsListObservable)
//                            .environmentObject(JoinedTransportationsListObservable.getInstance())
                    }
                }
                Spacer()
            }
            .navigationBarTitle("MainTransportationsTabView.MainTitle.text", displayMode: .inline)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct MainTransportationsTabview_Previews: PreviewProvider {
    static var previews: some View {
        MainTransportationsTabview()
    }
}
