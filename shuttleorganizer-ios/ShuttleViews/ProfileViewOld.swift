//
//  ProfileViewOld.swift
//  shuttleorganizer-ios
//
//  Created by macos on 26.09.2021.
//  Copyright © 2021 Orion. All rights reserved.
//

import Foundation
//
//  ProfileView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 6/14/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI
import UIKit
import class Kingfisher.KingfisherManager
import class Kingfisher.ImageCache
import struct Kingfisher.KFImage
import struct Kingfisher.RoundCornerImageProcessor
import struct Kingfisher.DownsamplingImageProcessor
import protocol Kingfisher.ImageProcessor
import class Kingfisher.ImageDownloader
import struct Kingfisher.KingfisherWrapper

struct ProfileViewOld: View {
    @ObservedObject private var personName : GenericObservableModel<String> = GenericObservableModel(value: "")
    @ObservedObject private var person : GenericObservableModel<Person> = GenericObservableModel(value: Person())
    
    @State private var email : String = ""
    @State private var imageUrl : String = ""
//    @ObservedObject private var notifications : GenericObservableModel<[Bool]> = GenericObservableModel(value : [Bool](repeating: true, count:10))
//    @State private var termsAccepted : Bool = false
    
    @ObservedObject private var no1 : GenericObservableModel<Bool> = GenericObservableModel(value: true, callBackWithParentAndPreviousValue: notificationFunction)
    @ObservedObject private var no2 : GenericObservableModel<Bool> = GenericObservableModel(value: true, callBackWithParentAndPreviousValue: notificationFunction)
    @ObservedObject private var no3 : GenericObservableModel<Bool> = GenericObservableModel(value: true, callBackWithParentAndPreviousValue: notificationFunction)
    @ObservedObject private var no4 : GenericObservableModel<Bool> = GenericObservableModel(value: true, callBackWithParentAndPreviousValue: notificationFunction)
    @ObservedObject private var no5 : GenericObservableModel<Bool> = GenericObservableModel(value: true, callBackWithParentAndPreviousValue: notificationFunction)
    
    @State private var notifications : [Bool] = [Bool](repeating: true, count:10)
    
    @State private var isNotificationFirstLoadComplete : Bool = false
    var error : GenericObservableModel<Error>? = nil
    
    @State private var image: Image? = nil
    @State private var shouldPresentImagePicker = false
    @State private var shouldPresentActionScheet = false
    @State private var shouldPresentCamera = false
    
    @State private var showAlert = false
    
    public static var notificationFunction =
        {(notification: Bool, parent : Any, previousValue : Bool) in
            if let validParent = parent as? ProfileViewOld{
                if notification != previousValue && validParent.isNotificationFirstLoadComplete {
                    validParent.savePersonNotification()
                    print(notification)
                }
            }
        }
    
    private func fetchPersonNotification(){
        let personNotification : PersonNotification = PersonNotification(personId: MyUser.getInstance().getUserId(), token: SingleNotificationManager.getInstance().personNotification.token)
        
        NetworkManager.fetch(url: UrlConstants.UrlGetNotification, parameters: personNotification
            , handle: {
                (newData : PersonNotification) -> ()  in
                    self.notifications = newData.getNotifications()
                    self.no1.value = newData.notification1
                    self.no2.value = newData.notification2
                    self.no3.value = newData.notification3
                    self.no4.value = newData.notification4
                    self.no5.value = newData.notification5
                    self.isNotificationFirstLoadComplete = true
                }
            , errorHandle: {
                newError in
                    self.error?.value = newError
                    let errorString : String = "\(newError)"
                    if errorString .contains("ProfileView, keyNotFound(CodingKeys(stringValue: \"personId\", intValue: nil)"){
                        print("Can not found PersonNotification so directly create PersonNotification")
                        self.savePersonNotification()
                    }else{
                        print(newError)
                    }
                    
                })
    }
    
    private func savePersonNotification(){
        let personNotification : PersonNotification = PersonNotification(personId: MyUser.getInstance().getUserId(), token: SingleNotificationManager.getInstance().personNotification.token, loggedIn: true,
            notification1: self.no1.value, notification2: self.no2.value, notification3: self.no3.value,
            notification4: self.no4.value, notification5: self.no5.value,
            notification6: self.notifications[5], notification7: self.notifications[6], notification8: self.notifications[7],
            notification9: self.notifications[8], notification10: self.notifications[9])

        NetworkManager.fetch(url: UrlConstants.UrlSaveNotification, parameters: personNotification
            , handle: {
                (newData : PersonNotification) -> ()  in
                    print("PersonNotification Saved: \(newData)")
                    SingleNotificationManager.getInstance().personNotification = newData
                }
            , errorHandle: {
                newError in
                self.error?.value = newError})
    }
    
    private func fetchPerson(){
        //TODO: need to fetch image url from db then fetch it
        
        let person : Person = Person(id: MyUser.getInstance().getUserId())
        
        NetworkManager.fetch(url: UrlConstants.UrlGetPerson, parameters: person
            , handle: {
                (newData : Person) -> ()  in
//                    self.savedPersonName = newData.name
                    self.personName.value = newData.name
                    self.personName.isValueChanged = false
                    self.email = newData.email
                    self.person.value = newData
                    self.imageUrl = newData.profilePictureURL
                    self.fetchProfileImage(imageUrl: self.imageUrl)
                }
            , errorHandle: {
                newError in
                self.error?.value = newError})
    }
//    TODO: to save efficently because each change leads to another save in sb
//    https://stackoverflow.com/questions/37805885/how-to-create-dispatch-queue-in-swift-3
        //here 2. Parameterized debounce function this method you can look
//    https://stackoverflow.com/questions/37805885/how-to-create-dispatch-queue-in-swift-3
//    https://stackoverflow.com/questions/37805885/how-to-create-dispatch-queue-in-swift-3
    
    private func savePerson(){
        let person : Person = Person(id: MyUser.getInstance().getUserId(), name: self.personName.value, email: self.email, profilePictureURL: self.imageUrl)
        
        NetworkManager.fetch(url: UrlConstants.UrlSavePerson, parameters: person
            , handle: {
                (newData : Person) -> ()  in
                    print("Person Saved: \(newData)")
                
                    if newData.name != MyUser.getInstance().firebaseName{
                        MyUser.getInstance().changeDisplayNameInFirebase(displayName: newData.name)
                    }
                    if newData.profilePictureURL != MyUser.getInstance().firebasePhotoUrl {
                        MyUser.getInstance().changeProfilePhotoUrlInFirebase(photoUrl: newData.profilePictureURL)
                    }
                    self.hideKeyboard()
                }
            , errorHandle: {
                newError in
                self.error?.value = newError})
    }
    
    func profilePhotoPressed(){
        print("profile pressed")
        self.shouldPresentActionScheet = true
    }
    
    private func saveProfileImage(image : UIImage?){
        if self.person.value.id == ""{
            print("can not save Profile Image because personId not valid")
            return
        }
        
        if image == nil{
            print("can not save Profile Image because image is nil")
            return
        }

        let newImage = image!

        let directoryPath = Utils.getImageDirectoryFromFileName(self.person.value.id)

//        let urlString = "http://46.101.106.121/test/img/profile_images/main\(directoryPath)\(self.person.value.id)\("_1612639666471").jpg"
        guard let data = newImage.jpegData(compressionQuality: 0) else {
                return
            }
        let imageData : String = data.base64EncodedString()
        let currentTimeInMiliseconds = Int64(Date().timeIntervalSince1970 * 1000)
        let imageName = "\(person.value.id)_\(currentTimeInMiliseconds).jpg"
        
        var mapParams : [String : String] = [:]
        mapParams["name"] = imageName
        mapParams["image_data"] = imageData
//        mapParams["thumb_image_data"] = imageData
        mapParams["type"] = "profile_images"
        mapParams["path"] = directoryPath
        mapParams["user_id"] = person.value.id

        NetworkManager.fetch(url: UrlConstants.UrlSaveImage, parameters: mapParams
            , handle: {
                (newData : UploadImageResponse) -> ()  in
                    print("Person image Saved: \(newData)")
                    if newData.result == .ok{
                        self.imageUrl = newData.url
                        savePerson()
                    }
                }
            , errorHandle: {
                newError in
                self.error?.value = newError})
    }
    
    private func fetchProfileImage(imageUrl : String){
        if self.person.value.id == ""{
            return
        }
        
//        let directoryPath = Utils.getImageDirectoryFromFileName(self.person.value.id)
//
////        let urlString = "http://46.101.106.121/test/img/main/\(self.person.value.id).jpg"
//        let urlString = "http://46.101.106.121/test/img/profile_images/main\(directoryPath)\(self.person.value.id)\("_1612639666471").jpg"
        
//        let urlString = self.person.value.profilePictureURL
        let urlString = imageUrl
        print("urlStringImage:\(urlString)")
//        let urlString = "http://46.101.106.121/test/img/profile_images/main/194/137/bQm1w3Uqithl8EJ3DEFP7CvBvI73_1612639666471.jpg"
        let url = URL(string: urlString)
        //        let url = URL(string: "http://46.101.106.121/test/img/main/bQm1w3Uqithl8EJ3DEFP7CvBvI73.jpg")
        //    let url = URL(string: "https://example.com/high_resolution_image.png")
        //        url = URL(string: "https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg")
        //        url = URL(string: "https://upload.wikimedia.org/wikipedia/commons/f/ff/Pizigani_1367_Chart_10MB.jpg")
        
        //        let processor = DownsamplingImageProcessor(size: imageView.bounds.size) |> RoundCornerImageProcessor(cornerRadius: 0)
        //        let processor = DownsamplingImageProcessor(size: imageView.bounds.size).append(another: RoundCornerImageProcessor(cornerRadius: 0))
        let processor = RoundCornerImageProcessor(cornerRadius: 0)
        let placeholder = UIImage(named: "user_placeholder")
        
        let uiImage = UIImage(named: "user_placeholder")
        let uiImageView = UIImageView(image: uiImage)
        //        imageDownloaders[personId]
        uiImageView.kf.indicatorType = .activity
        uiImageView.kf.setImage(
            with: url,
            placeholder: placeholder,
            options: [
                .processor(processor),
                //                .scaleFactor(10),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ], completionHandler:
                {
                    result in
                    switch result {
                    case .success(let value):
                        print("Task done for: \(value.source.url?.absoluteString ?? "")")
                        if let validUIImage = uiImageView.image{
                            self.image = Image(uiImage: validUIImage)
                        }
                    case .failure(let error):
                        print("Job failed: \(error.localizedDescription)")
                    }
                })
    }
    
    //ProfileView
    var body: some View {
        NavigationView{
            ZStack{
                Color.colorBackground.edgesIgnoringSafeArea(.all)
                ScrollView{
                    VStack(alignment: .leading){
                        HStack(alignment: .top){
                            if self.image == nil{
                                Image("user_placeholder")
                                    .renderingMode(.template)
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 100, height: 100)
                                    .clipShape(Circle())
                                    .overlay(Circle().stroke(Color.colorLightGray, lineWidth: 2))
            //                        .shadow(radius: 7)
                                    .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 10))
                                    .foregroundColor(Color.colorPrimary)
                                    .onTapGesture {
                                        self.profilePhotoPressed()
                                    }
                            }else{
                                self.image?
                                    .renderingMode(.original)
                                    .resizable()
                                    .aspectRatio(contentMode: .fill)
                                    .frame(width: 100, height: 100)
                                    .clipShape(Circle())
                                    .overlay(Circle().stroke(Color.colorLightGray, lineWidth: 2))
            //                        .shadow(radius: 7)
                                    .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 10))
                                    .foregroundColor(Color.colorPrimary)
                                    .onTapGesture {
                                        self.profilePhotoPressed()
                                    }
                            }
                            VStack(alignment: .leading){
                                Text("ProfileView.Name.text")
                                TextField("", text: self.$personName.value)
                                    .autocapitalization(.none)
                                Divider()
                                    .padding(.top, -5)
                                Text("ProfileView.Email.text")
                                TextField("", text: $email)
                                    .disabled(true)
                                    .foregroundColor(Color.colorDarkGray)
                                Divider()
                                    .padding(.top, -5)
                            }
                        }
                            .padding(.bottom, 20)
                        
                        VStack(alignment: .leading){
                            Text("ProfileView.NotificationTitle.text").font(.title)
                            Toggle(isOn: $no1.value, label: {Text("ProfileView.ManagementNotification.text")})
                                .toggleStyle(SwitchToggleStyle(tint: Color.colorSecondary))
                            Toggle(isOn: $no2.value, label: { Text("ProfileView.ShuttleNotification.text") })
                                .toggleStyle(SwitchToggleStyle(tint: Color.colorSecondary))
                            Toggle(isOn: $no3.value, label: { Text("ProfileView.ChatNotification.text") })
                                .toggleStyle(SwitchToggleStyle(tint: Color.colorSecondary))
                        }
                            .padding(.bottom, 20)
                            
                        VStack(alignment: .leading){
                            Text("ProfileView.PrivacyTitle.text").font(.title)
                            Toggle(isOn: $no4.value, label: { Text("ProfileView.HidePhoneNumber.text") })
                                .toggleStyle(SwitchToggleStyle(tint: Color.colorSecondary))
                            Toggle(isOn: $no5.value, label: { Text("ProfileView.HideEmail.text") })
                                .toggleStyle(SwitchToggleStyle(tint: Color.colorSecondary))
                        }
                        Spacer()
                        Button(action: {self.showAlert = true /*MyUser.getInstance().signOut()*/}){
                            HStack(){
                                Text("ProfileView.SignOut.text")
                                    .foregroundColor(Color.red)
                                    .fontWeight(.bold)
                            }
                            .frame(maxWidth: .infinity, minHeight: 50, idealHeight: 50)
                        }
                        .onTapGesture {self.showAlert = true /*MyUser.getInstance().signOut()*/}
                        .padding(.bottom, 10)
                    }
                    .padding(EdgeInsets(top: 20, leading: 20, bottom: 0, trailing: 20))
                    .background(Color.colorBackground)
                    .onAppear(){
                        print("on appear called")
                        self.no1.parent = self
                        self.no2.parent = self
                        self.no3.parent = self
                        self.no4.parent = self
                        self.no5.parent = self
                        self.fetchPerson()
                        self.fetchPersonNotification()
                        
                    }
                    .onTapGesture {
                        self.hideKeyboard()
                    }
                    .alert(isPresented: self.$showAlert) {
                        Alert(title: Text(""),
                              message: Text(toStringFromLocalizedStringKey(localizedStringKey: "ProfileView.Alert.SignOut.text")),
                              primaryButton: .destructive(Text("ProfileView.Alert.SignOutButton.text"), action: {MyUser.getInstance().signOut()}),
                              secondaryButton: .cancel())
                    }
                    .sheet(isPresented: $shouldPresentImagePicker) {
                        SUImagePickerView(sourceType: self.shouldPresentCamera ? .camera : .photoLibrary, image: self.$image, isPresented: self.$shouldPresentImagePicker, doSubmit: self.saveProfileImage)}
                    .actionSheet(isPresented: $shouldPresentActionScheet) { () -> ActionSheet in
                        ActionSheet(title: Text("ProfileView.ActionSheet.Title.text"),
                            buttons: [ActionSheet.Button.default(Text("ProfileView.ActionSheet.CameraButton.text"), action: {
                            self.shouldPresentImagePicker = true
                            self.shouldPresentCamera = true
                        }), ActionSheet.Button.default(Text("ProfileView.ActionSheet.PhotoLibraryButton.text"), action: {
                            self.shouldPresentImagePicker = true
                            self.shouldPresentCamera = false
                        }), ActionSheet.Button.cancel()])
                    }
                    .navigationBarItems(
                        leading:
                            VStack{
                                if self.personName.isValueChanged == true {
                                    Button("ProfileView.Navigation.CancelButton.text"){
                                        self.fetchPerson()
                                        self.hideKeyboard()
                                    }
                                }
                            },
                        trailing:
                            VStack{
                                if self.personName.isValueChanged == true {
                                    Button("ProfileView.Navigation.SaveButton.text"){
                                        self.savePerson()
                                        self.personName.isValueChanged = false
                                    }
                                }
                            }
                    )
                    .navigationBarTitle("ProfileView.Navigation.Title.text", displayMode: .inline)
                }
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct ProfileViewOld_Previews: PreviewProvider {
    static var previews: some View {
        ProfileViewOld()
    }
}
