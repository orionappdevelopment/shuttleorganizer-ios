//
//  AlreadyCreatedTransportationView.swift
//  Landmarks
//
//  Created by macos on 5/17/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import SwiftUI

struct AlreadyCreatedTransportationView: View {
    var transportation: Transportation
    @State private var showShareSheet = false
//    @State var toastCopied = false
    @ObservedObject var isShowToastCopied: ToastObservableModel = ToastObservableModel()
    
    var body: some View {
        VStack{
            Text(toStringFromLocalizedStringKey(localizedStringKey: "AlreadyCreatedTransportationView.ShareTransportation.text", transportation.name, (TransportationTypeEnum(rawValue: transportation.transportationType)!.description)))
                .padding(.all, 5)
                .padding(.bottom, 20)
//                .padding(.top, 20)
            HStack{
                RectangleDots(insideView: Text(transportation.id == nil ? "" : transportation.id!))
                    .padding(.leading, 7)
                    .padding(.bottom, 7)
                
                Button(action: {
                    
                    // write to clipboard
                    UIPasteboard.general.string = self.transportation.id == nil ? "" : self.transportation.id!
                    // read from clipboard
                    // let content = UIPasteboard.general.stringastCopied
                    
                    self.isShowToastCopied.isToastShowing = true
                    print("copied pressed")
                }) {
                    Image("copy")
                        .renderingMode(.template)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 30 ,height:30)
                        .padding(5)
                        .foregroundColor(Color.colorPrimary)
                }
                
                Spacer()

//                Image("share")
//                   .renderingMode(.template)
//                   .resizable()
//                   .aspectRatio(contentMode: .fit)
//                   .frame(width: 30, height: 30)
//                   .padding(5)
//                   .foregroundColor(Color.colorPrimary)
//                   .padding(.trailing, 7)
                
               Button(action: {
                        self.showShareSheet = true
                    }) {
                        Image("share")
                            .renderingMode(.template)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 30, height: 30)
                            .padding(5)
                            .foregroundColor(Color.colorPrimary)
                            .padding(.trailing, 7)
                    }
//                Button(action: {
//                        self.showShareSheet = true
//                    }) {
//                        Text("Share")
//                    }
            }
            Spacer()
        }
        .sheet(isPresented: $showShareSheet) {
            ShareSheet(activityItems: [self.transportation.id == nil ? "" : self.transportation.id!])
        }
//        .toast(isShowing: $toastCopied, text: Text("clipboard.text"))
        .toastObservableView(toastObservableModel: isShowToastCopied, text: Text("AlreadyCreatedTransportationView.Clipboard.text"))
        
    }
}

struct AlreadyCreatedTransportationView_Previews: PreviewProvider {
    static var t = Transportation(id: "123456789m", name: "kadikoy", latitude: 1, longitude: 1, transportationType: TransportationTypeEnum.DEPARTURE.rawValue , days: 2, status: TransportationStatusEnum.IN_THE_GARAGE.rawValue, creatorPersonId: "creator_1", startTime: LTime(hour: 1, minute: 1, second: 1), timeZone: 3);
    
    static var previews: some View {
        Group {
//            AlreadyCreatedTransportationView(transportation: t)
            AlreadyCreatedTransportationView(transportation: t).environment(\.locale, .init(identifier: "tr"))
        }
    }
}
