//
//  CreateTransportationViewOld.swift
//  Landmarks
//
//  Created by macos on 5/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import SwiftUI
import UIKit
import Foundation
import CoreLocation
import GoogleMaps

struct CreateTransportationViewOld: View {
    @State private var transportationType: TransportationTypeEnum = TransportationTypeEnum.DEPARTURE
    @State private var transportationName: String = ""
    @State private var transportationPlate: String = ""
    @State private var transportationStartTime: Date = Date()
    
    
    @State private var showingConfirmation = false
    @State private var confirmationMessage = ""
    @State var mapView : GMSMapView? = nil
    @ObservedObject var selectedCoordinate : GenericObservableModel<CLLocationCoordinate2D> = GenericObservableModel(value: CLLocationCoordinate2D(latitude: 41.0370, longitude: 28.9851))
    
    @State private var error : Error? = nil
    
    @State private var bottomPanelHeight : CGFloat = 190
    @State private var isTimeViewPresented = false
    @State private var isMapViewVisible = false
    
    @State private var createdTransportation : Transportation? = nil //createdTransportations[0]
    @State private var isSaveStarted = false
    @ObservedObject var showAlert = GenericObservableModel(value: false)
//    @State private var createdTransportation : Transportation? = createdTransportations[0]
    
    
    private func save(){
        if transportationName.isEmpty{
            self.showAlert.value = true
            return
        }
        
        let transportation: Transportation = Transportation(name: transportationName, latitude: self.selectedCoordinate.value.latitude
            , longitude: self.selectedCoordinate.value.longitude, transportationType: transportationType.rawValue
            , days: DayEnum.weekDays, status: TransportationStatusEnum.IN_THE_GARAGE.rawValue
            , creatorPersonId: MyUser.getInstance().getUserId(), startTime: Utils.toLTime(time: transportationStartTime)
            , timeZone: Utils.getTimeZoneOfComputer(), plate: transportationPlate)
        
        isSaveStarted = true
        NetworkManager.fetch(url: UrlConstants.UrlCreateTransportation, parameters: transportation
            , handle: {
                (newData : Transportation.OptionalTransportation ) -> () in
                
                self.createdTransportation = Transportation(optionalTransportation: newData)
                print("Created Transporatation: \(self.createdTransportation!)")
                self.showingConfirmation = true
                self.confirmationMessage = NSLocalizedString("CreateTransportationView.TransportationCreated.Toggle.text", comment: "")
             }
            , errorHandle: {
                newError -> () in
                print("Create Transportation Error: \(newError)")
                self.error = newError})
    }
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                if nil != createdTransportation {
                    VStack{
                        AlreadyCreatedTransportationView(transportation: createdTransportation!)
                        //.alert(isPresented: $showingConfirmation){
                        //Alert(title: Text("Thank you!"), message: Text(confirmationMessage), dismissButton: .default(Text("OK"))) }
                            .toast(isShowing: $showingConfirmation, text: Text(confirmationMessage))
                        Spacer()
                    }
    //                .navigationBarTitle("")
    //                .navigationBarHidden(true)
                    
                }else{
                    VStack{
                        Picker(.init("CreateTransportationView.TransportationType.text"), selection: $transportationType) {
                            Text("CreateTransportationView.DepartureTransportation.text").tag(TransportationTypeEnum.DEPARTURE)
                            Text("CreateTransportationView.ReturnTransportation.text").tag(TransportationTypeEnum.RETURN)
                        }
                            .pickerStyle(SegmentedPickerStyle())
                        .padding()
                        
                        HStack{
                            Text("CreateTransportationView.TransportationName.text")
                                .foregroundColor(Color.colorText)
                            TextField("", text: $transportationName)
                                .multilineTextAlignment(.trailing)
                                .foregroundColor(Color.colorText)
                        }
                        
                        
                        HStack{
                            Text("CreateTransportationView.PlateNumber.text")
                                .foregroundColor(Color.colorText)
                            ZStack{
                                HStack{
                                    Spacer()
                                    Image("licence_plate")
    //                                    .renderingMode(.template)
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width: 180, height: 40, alignment: .trailing)
//                                        .padding(5)
                                }
                                
                                TextField("", text: $transportationPlate)
                                    .multilineTextAlignment(.trailing)
                                    .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 10))
                                    .foregroundColor(Color.colorText)
                            }
                        }
                        
                        HStack{
                            if transportationType == TransportationTypeEnum.DEPARTURE{
                                Text("CreateTransportationView.DeparturePoint.text")
                                    .foregroundColor(Color.colorText)
                            } else {
                                Text("CreateTransportationView.ReturnPoint.text")
                                    .foregroundColor(Color.colorText)
                            }
                            
                            Spacer()
                            Image("map")
                                .renderingMode(.template)
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 35, height: 35)
            //                                .padding(5)
                                .foregroundColor(Color.colorPrimary)
                                .onTapGesture(perform: {self.isMapViewVisible = true})
                        }
                        
                        HStack{
                            if transportationType == TransportationTypeEnum.DEPARTURE{
                                Text("CreateTransportationView.DepartureTime.text")
                                    .foregroundColor(Color.colorText)
                            } else {
                                Text("CreateTransportationView.ReturnTime.text")
                                    .foregroundColor(Color.colorText)
                            }
                            Spacer()
                            Text(getHourMinuteFormat().string(from: self.transportationStartTime))
                                .foregroundColor(Color.colorText)
                                .onTapGesture {
                                    self.isTimeViewPresented.toggle()
                                }
                        }
                        
                        Spacer()
                        
//                        Button(action: {
//                            self.save()
//                            }, label: {
//                                Text("CreateTransportationView.Save.text")
//                            })
//                            .disabled(!isValid())
//                            .padding(.bottom, 15)
                    }
                    .padding(EdgeInsets(top: 10, leading: geometry.size.width*0.04, bottom: 0, trailing: geometry.size.width*0.04))
                    .background(Color.colorBackground)
                }
                   
    //                .navigationBarHidden(false)
            }
            .navigationBarItems(
                trailing:
                    VStack{
                        if nil == self.createdTransportation && self.isSaveStarted == false{
                            Button("CreateTransportationView.Save.text"){
                                self.save()
                            }
                        }else{
                            EmptyView()
                        }
                    }
            )
            .disabled(self.isTimeViewPresented)
            .blur(radius: self.isTimeViewPresented ? 15 : 0)
            
            
            if self.isTimeViewPresented{
                HStack{
                    Spacer()
                    VStack(){
                        Spacer()
                        TimeView(date: self.$transportationStartTime, isPresented: self.$isTimeViewPresented, doSubmit: {})
                        Spacer()
                    }
                    Spacer()
                }
            }
        }
        .sheet(isPresented: self.$isMapViewVisible) {
            MapLocationSelectUIView(observedCoordinate: self.selectedCoordinate)
        }
        .alert(isPresented: self.$showAlert.value) {
            Alert(title: Text(""), message: Text("CreateTransportationView.ShuttleNameEmptyAlert.text"), dismissButton: .default(Text("CreateTransportationView.ShuttleNameEmptyAlert.Ok.text")))
        }
        .navigationBarTitle(nil != createdTransportation ? "CreateTransportationView.AlreadyCreateShuttle.text" : "CreateTransportationView.CreateShuttle.text")
        
    }
    
}

struct CreateTransportationViewOld_Previews: PreviewProvider {
    static var previews: some View {
        CreateTransportationViewOld().environment(\.locale, .init(identifier: "tr"))
    }
}
