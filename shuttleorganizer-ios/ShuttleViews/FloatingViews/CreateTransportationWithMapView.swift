//
//  CreateTransportationView.swift
//  Landmarks
//
//  Created by macos on 5/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import SwiftUI
import UIKit
import Foundation
import CoreLocation
import GoogleMaps

struct CreateTransportationWithMapView: View {
    @State private var transportationType: TransportationTypeEnum = TransportationTypeEnum.DEPARTURE
    @State private var transportationName: String = ""
    @State private var transportationPlate: String = ""
    @State private var transportationStartTime: Date = Date()
    
    
    @State private var showingConfirmation = false
    @State private var confirmationMessage = ""
    @State var mapView : GMSMapView? = nil
    @State var selectedCoordinate : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 41.0370, longitude: 28.9851)
    
    @State private var error : Error? = nil
    
    @State private var bottomPanelHeight : CGFloat = 190
    @State private var isTimeViewPresented = false
    
    @State private var createdTransportation : Transportation? = nil //createdTransportations[0]
//    @State private var createdTransportation : Transportation? = createdTransportations[0]
    
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                if nil != createdTransportation {
                    VStack{
                        AlreadyCreatedTransportationView(transportation: createdTransportation!)
                        //.alert(isPresented: $showingConfirmation){
                        //Alert(title: Text("Thank you!"), message: Text(confirmationMessage), dismissButton: .default(Text("OK"))) }
                            .toast(isShowing: $showingConfirmation, text: Text(confirmationMessage))
                        Spacer()
                    }
    //                .navigationBarTitle("")
    //                .navigationBarHidden(true)
                    
                }else{
                    VStack {
                        VStack{
                            ZStack{
                                MapLocationSelectAdvanced(bindingCoordinate: self.$selectedCoordinate, realMap: self.$mapView, isFullScreen: true)
                                VStack{
                                    Text("CreateTransportationView.MapTitle.text")
                                        .padding(EdgeInsets(top: 10, leading: 0, bottom: 0, trailing: 0))
                                    Spacer()
                                }
                            }
                        }.frame(width: geometry.size.width, height: geometry.size.height - bottomPanelHeight, alignment: .leading)
                        
                        VStack{
                            Picker(.init("CreateTransportationView.TransportationType.text"), selection: $transportationType) {
                                Text("CreateTransportationView.DepartureTransportation.text").tag(TransportationTypeEnum.DEPARTURE)
                                Text("CreateTransportationView.ReturnTransportation.text").tag(TransportationTypeEnum.RETURN)
                            }
                                .pickerStyle(SegmentedPickerStyle())
                            
                            HStack{
                                TextField("CreateTransportationView.TransportationName.title.text", text: $transportationName)
                                    .multilineTextAlignment(.leading)
                                Spacer()
                            }
                            
                            ZStack{
                                HStack{
                                    Image("licence_plate")
                                    Spacer()
                                }
                                HStack{
                                    TextField("CreateTransportationView.PlateNumber.title.text", text: $transportationPlate)
                                        .multilineTextAlignment(.leading)
                                        .padding(EdgeInsets(top: 0, leading: geometry.size.width * 0.07, bottom: 0, trailing: geometry.size.width * 0.001))
                                    Spacer()
                                }
                            }
                            
                            HStack{
                                Text("CreateTransportationView.DepartureTime.text")
                                Spacer()
                                Text(getHourMinuteFormat().string(from: self.transportationStartTime))
                                    .onTapGesture {
                                        self.isTimeViewPresented.toggle()
                                    }
                            }
                            
                            Spacer()
                            
                            Button(action: {
                                self.save()
                                }, label: {
                                    Text("CreateTransportationView.Save.text")
                                })
                                .disabled(!isValid())
                                .padding(.bottom, 15)
                        }
                        .frame(width: geometry.size.width * 0.92, height: bottomPanelHeight, alignment: .leading)
                        .padding(EdgeInsets(top: 0, leading: geometry.size.width*0.04, bottom: 0, trailing: geometry.size.width*0.04))
                    }
                   
    //                .navigationBarHidden(false)
                }
            }
            .disabled(self.isTimeViewPresented)
            .blur(radius: self.isTimeViewPresented ? 15 : 0)
            
            if self.isTimeViewPresented{
                HStack{
                    Spacer()
                    VStack(){
                        Spacer()
                        TimeView(date: self.$transportationStartTime, isPresented: self.$isTimeViewPresented, doSubmit: {})
                        Spacer()
                    }
                    Spacer()
                }
            }
        }
        .navigationBarTitle(nil != createdTransportation ? "CreateTransportationView.AlreadyCreateShuttle.text" : "CreateTransportationView.CreateShuttle.text")
        
    }
    
    private func isValid() -> Bool {
//        if transportationName.isEmpty {
//            return false
//        }
        return true
    }
    
    private func save(){
        let transportation: Transportation = Transportation(name: transportationName, latitude: self.selectedCoordinate.latitude
            , longitude: self.selectedCoordinate.longitude, transportationType: transportationType.rawValue
            , days: DayEnum.weekDays, status: TransportationStatusEnum.IN_THE_GARAGE.rawValue
            , creatorPersonId: MyUser.getInstance().getUserId(), startTime: Utils.toLTime(time: transportationStartTime)
            , timeZone: Utils.getTimeZoneOfComputer(), plate: transportationPlate)
        
        NetworkManager.fetch(url: UrlConstants.UrlCreateTransportation, parameters: transportation
            , handle: {
                (newData : Transportation ) -> () in
                
                self.createdTransportation = newData
                print("Created Transporatation: \(self.createdTransportation!)")
                self.showingConfirmation = true
                self.confirmationMessage = "Your transportation is created"
             }
            , errorHandle: {
                newError -> () in
                print("Create Transportation Error: \(newError)")
                self.error = newError})
    }
}

struct CreateTransportationWithMapView_Previews: PreviewProvider {
    static var previews: some View {
        CreateTransportationWithMapView().environment(\.locale, .init(identifier: "tr"))
    }
}
