//
//  JoinTransportationViewOld.swift
//  shuttleorganizer-ios
//
//  Created by macos on 6/14/20.
//  Copyright © 2020 Orion. All rights reserved.
// wbfkGu8h28

import SwiftUI
import UIKit
import Foundation
import CoreLocation
import GoogleMaps

struct JoinTransportationViewOld: View {
    @State var transportationId = ""
    @State var mapView : GMSMapView? = nil
    @State var selectedCoordinate : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 41.0370, longitude: 28.9851)
    @State var transportation : Transportation? //=  Transportation()
    @ObservedObject var joinedTransportationsList: JoinedTransportationsListObservable = JoinedTransportationsListObservable.getInstance()
    @State var joinedTransportation :Transportation? = nil
    @State var showAlert = false
    @State var joinTransportationAlert : JoinTransporationAlertEnum = .none
    @State var createdPersonTransportation : PersonTransportation? = nil
//    @State var isFirstPageVisible = true
    @State var isSheet = false
    @ObservedObject var isSheetPresented : GenericObservableModel<Bool> = GenericObservableModel(value: false)
    
//    @State var activeAlert : Any? = nil
    
    @State private var error : Error? = nil
    
    enum JoinTransporationAlertEnum{
        case none
        case transportationNotFound
        case transportationAlreadyJoined
    }
    
    private func fetchTransportation(){
        let fetchTransportation : Transportation = Transportation(id: self.transportationId)
        
        NetworkManager.fetch(url: UrlConstants.UrlGetTransportation, parameters: fetchTransportation
            , handle: {
                (newData : Transportation.OptionalTransportation ) -> () in
//                    self.savedPersonName = newData.name
                    self.transportation = Transportation(optionalTransportation: newData)
                }
            , errorHandle: {
                newError in
                self.error = newError
                self.transportation =  nil
                self.transportationId = ""
                self.showAlert = true
                self.joinTransportationAlert = .transportationNotFound
            })
    }
    
    private func doContinue(){
        self.joinedTransportation = self.joinedTransportationsList.getJoinedTransportation(id: self.transportationId)
        if self.joinedTransportation == nil{
            self.fetchTransportation()
        }else{
            self.showAlert = true
            self.joinTransportationAlert = .transportationAlreadyJoined
        }
    }
    
    private func save(){
        var latitude = self.selectedCoordinate.latitude
        var longitude = self.selectedCoordinate.longitude
        
        if let centerCoordinate = self.mapView?.camera.target{
            print("centerCoordinate: \(centerCoordinate)")
            latitude = centerCoordinate.latitude
            longitude = centerCoordinate.longitude
        }
        let personTransportation: PersonTransportation = PersonTransportation(personId: MyUser.getInstance().getUserId(), transportationId: self.transportationId, personRoleType: RoleTypeEnum.PASSENGER.rawValue, latitude: latitude, longitude: longitude, dailyTimeDiffInSecs: 0)
            
        NetworkManager.fetch(url: UrlConstants.UrlSavePersonTransportation, parameters: personTransportation
            , handle: {
                (newData : PersonTransportation ) -> () in
                
                self.createdPersonTransportation = newData
                print("Created PersonTransportation: \(String(describing: self.createdPersonTransportation))")
             }
            , errorHandle: {
                newError -> () in
                print("Create PersonTransportation Error: \(newError)")
                self.error = newError})
    }
    
    
    var body: some View {
        GeometryReader { geometry in
            ZStack{
                if self.transportation == nil && self.isSheet == false {
                    VStack{
                        Text("JoinTransportationView.JoinTransportationText.text")
                            .padding(EdgeInsets(top: 10, leading: 20, bottom: 0, trailing: 20))
                            .foregroundColor(Color.colorText)
            //            Form {
                            GenericRectangleDots(
                                insideView: VStack(alignment: .leading){
                                    HStack{
                                        TextField("JoinTransportationView.JoinTransportationHint.text", text: self.$transportationId)
                                        .autocapitalization(.none)
                                    }
                                }.padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
                                , width: geometry.size.width - 20, height: 50)
                            //.frame(width: geometry.size.width, height: 50, alignment: .leading)
            //            }
                        Spacer()
                        Button(action: {
    //                        print("Save clicked")
                            self.doContinue()
                        }, label: {
                            HStack(){
                                Spacer()
                                Text("JoinTransportationView.Continue.text")
                                Spacer()
                            }
                        })
                        .disabled(self.transportationId == "")
                        .padding(.bottom, 15)
                    }
                }
                else if(self.createdPersonTransportation == nil){
                    ZStack{
                        VStack{
                            MapLocationSelectAdvanced(bindingCoordinate: self.$selectedCoordinate, realMap: self.$mapView, isFullScreen: false)
    //                            .edgesIgnoringSafeArea(.top)
    //                            .padding(.bottom, -8)
                        }
                        VStack{
                            if self.transportation?.transportationType == TransportationTypeEnum.RETURN.rawValue{
                                Text(toStringFromLocalizedStringKey(localizedStringKey: "JoinTransportationView.SelectGetOffPoint.text", self.transportation?.name ?? ""))
                                    .bold()
                                    .padding(EdgeInsets(top: 10, leading: 50, bottom: 0, trailing: 50))
                                    .foregroundColor(Color.colorText)
                            }else{
                                Text(toStringFromLocalizedStringKey(localizedStringKey: "JoinTransportationView.SelectGetOnPoint.text", self.transportation?.name ?? ""))
                                    .bold()
                                    .padding(EdgeInsets(top: 10, leading: 50, bottom: 0, trailing: 50))
                                    .foregroundColor(Color.colorText)
                            }

                            Spacer()
                            Spacer()
                            Button(action: {
                                print(self.selectedCoordinate)
                                self.save()
                            }, label: {
                                HStack(){
                                    Spacer()
                                    Text("JoinTransportationView.Join.text")
                                    Spacer()
                                }
                            })
                                .disabled(self.transportation == nil)
                                .frame(width: geometry.size.width, height: 50, alignment: .leading)
                                .background(Color.white)
                        }
                    }
                }
                else{
                    VStack(alignment: .leading){
                        HStack{
                            Text(toStringFromLocalizedStringKey(localizedStringKey: "JoinTransportationView.JoinTransportationSuccessfully.text", self.transportation?.name ?? ""))
                                .foregroundColor(Color.colorText)
                        }.padding(EdgeInsets(top: 30, leading: 0, bottom: 20, trailing: 0))
                        
                        HStack{
                            if self.transportation?.transportationType == TransportationTypeEnum.RETURN.rawValue{
                                Text("JoinTransportationView.SetGetOffTime.text")
                                    .foregroundColor(Color.colorText)
                            }else{
                                Text("JoinTransportationView.SetGetOnTime.text")
                                    .foregroundColor(Color.colorText)
                            }
                        }
                        Spacer()
                    }
                    .padding(EdgeInsets(top: 10, leading: 20, bottom: 0, trailing: 20))
                }
                if self.isSheet {
                    VStack{
                        HStack{
                            Spacer()
                            Image(systemName:"xmark.circle.fill")
                                .renderingMode(.template)
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 25, height: 25, alignment: .center)
                                .foregroundColor(Color.colorIosCloseButtonOutsideXColor)
                                .background(Color.colorIosCloseButtonInsideXColor)
                                .clipShape(Circle())
                                .onTapGesture {
                //                                    self.isPresented.value = false
                                    self.isSheetPresented.value = false
                                }
                                .padding(EdgeInsets(top: 10, leading: 10, bottom: 0, trailing: 10))
                        }
                        Spacer()
                    }
                }
            }
        }
        .background(Color.colorBackground)
        .alert(isPresented: self.$showAlert) {
            if self.joinTransportationAlert == .transportationAlreadyJoined{
                return Alert(title: Text(""),
                             message: Text(toStringFromLocalizedStringKey(localizedStringKey: "JoinTransportationView.AlreadyJoinedTransportationAlert.text", self.joinedTransportation?.name ?? "")),dismissButton: .default(Text("JoinTransportationView.AlreadyJoinedTransportationAlert.Button.text")){
                                self.transportationId = ""
                                self.joinedTransportation = nil
                            })

            }else if self.joinTransportationAlert == .transportationNotFound{
                return Alert(title: Text(""),
                         message: Text("JoinTransportationView.TransportationCodeNotValidAlert.text"), dismissButton: .default(Text("JoinTransportationView.TransportationCodeNotValidAlert.Button.text")){
                                self.transportationId = ""
                                self.joinedTransportation = nil
                            })
            }else{
                return Alert(title: Text(""), message: Text("There is unexpected error"), dismissButton: .default(Text("OK")))
            }
        }
        .onAppear(perform: {
            self.joinedTransportationsList.fetchData()
            if transportationId != "" {
                self.fetchTransportation()
            }
        })
        .navigationBarTitle("JoinTransportationView.JoinShuttle.text")
    }
    
}

struct JoinTransportationViewOld_Previews: PreviewProvider {
    static var previews: some View {
        JoinTransportationView()
    }
}

/*
VStack{
    VStack{
        Text("Lütfen yerinizi seçin")
    }
        .frame(width: geometry.size.width, height: 70)
    VStack{
        MapLocationSelectAdvanced(bindingCoordinate: self.$selectedCoordinate, realMap: self.$mapView)
        .edgesIgnoringSafeArea(.top)
        .padding(.bottom, -8)
    }.frame(width: geometry.size.width, height: geometry.size.height - 250)
    VStack{
        Spacer()
        Button(action: {
            print(self.selectedCoordinate)
        }, label: {
            HStack(){
                Spacer()
                Text("Select")
                Spacer()
            }
        })
    }.frame(width: geometry.size.width, height: 130)
}
*/
