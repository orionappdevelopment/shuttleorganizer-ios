//
//  WrappedCreateView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 2/21/21.
//  Copyright © 2021 Orion. All rights reserved.
//

import SwiftUI
import UIKit
import Foundation
import CoreLocation
import GoogleMaps

struct WrappedCreateTransportationView: View {
    @State var mapView : GMSMapView? = nil
    @State var selectedCoordinate : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 41.0370, longitude: 28.9851)
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                VStack{
                    MapLocationSelectAdvanced(bindingCoordinate: self.$selectedCoordinate, realMap: self.$mapView, isFullScreen: true)
                }.frame(width: geometry.size.width, height: geometry.size.height - 200, alignment: .leading)
                VStack{
                    Text("asd")
                }.frame(width: geometry.size.width, height: 200, alignment: .leading)
            }
        }
        
    }
}

struct WrappedCreateTransportationView_Previews: PreviewProvider {
    static var previews: some View {
        WrappedCreateTransportationView()
    }
}
