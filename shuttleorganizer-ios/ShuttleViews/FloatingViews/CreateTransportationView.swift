//
//  CreateTransportationView.swift
//  Landmarks
//
//  Created by macos on 5/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import SwiftUI
import UIKit
import Foundation
import CoreLocation
import GoogleMaps

struct CreateTransportationView: View {
    @State private var transportationType: TransportationTypeEnum = TransportationTypeEnum.DEPARTURE
    @ObservedObject private var transportationName: GenericObservableModel<String> = GenericObservableModel(value: "")
    @ObservedObject private var transportationPlate: GenericObservableModel<String> = GenericObservableModel(value: "")
    @ObservedObject private var transportationStartTime: GenericObservableModel<Date> = GenericObservableModel(value: Date())
    
    
    @State private var showingConfirmation = false
    @State private var confirmationMessage = ""
    @State var mapView : GMSMapView? = nil
    @ObservedObject var selectedCoordinate : GenericObservableModel<CLLocationCoordinate2D> = GenericObservableModel(value: CLLocationCoordinate2D(latitude: 41.0370, longitude: 28.9851))
    
    @State private var error : Error? = nil
    
    @State private var bottomPanelHeight : CGFloat = 190
    @State private var isTimeViewPresented = false
    @State private var isMapViewVisible = false
    
    @State private var createdTransportation : Transportation? = nil //createdTransportations[0]
    @State private var isSaveStarted = false
    @ObservedObject var showAlert = GenericObservableModel(value: false)
//    @State private var createdTransportation : Transportation? = createdTransportations[0]
    
    
    private func save(){
        if transportationName.value.isEmpty{
            self.showAlert.value = true
            return
        }
        
        let transportation: Transportation = Transportation(name: self.transportationName.value, latitude: self.selectedCoordinate.value.latitude
            , longitude: self.selectedCoordinate.value.longitude, transportationType: transportationType.rawValue
            , days: DayEnum.weekDays, status: TransportationStatusEnum.IN_THE_GARAGE.rawValue
            , creatorPersonId: MyUser.getInstance().getUserId(), startTime: Utils.toLTime(time: self.transportationStartTime.value)
            , timeZone: Utils.getTimeZoneOfComputer(), plate: transportationPlate.value)
        
        isSaveStarted = true
        NetworkManager.fetch(url: UrlConstants.UrlCreateTransportation, parameters: transportation
            , handle: {
                (newData : Transportation.OptionalTransportation ) -> () in
                
                self.createdTransportation = Transportation(optionalTransportation: newData)
                print("Created Transporatation: \(self.createdTransportation!)")
                self.showingConfirmation = true
                self.confirmationMessage = NSLocalizedString("CreateTransportationView.TransportationCreated.Toggle.text", comment: "")
             }
            , errorHandle: {
                newError -> () in
                print("Create Transportation Error: \(newError)")
                self.error = newError})
    }
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                if nil != createdTransportation {
                    VStack{
                        AlreadyCreatedTransportationView(transportation: createdTransportation!)
                        //.alert(isPresented: $showingConfirmation){
                        //Alert(title: Text("Thank you!"), message: Text(confirmationMessage), dismissButton: .default(Text("OK"))) }
                            .toast(isShowing: $showingConfirmation, text: Text(confirmationMessage))
                        Spacer()
                    }
    //                .navigationBarTitle("")
    //                .navigationBarHidden(true)
                    
                }else{
                    VStack{
                        Picker(.init("CreateTransportationView.TransportationType.text"), selection: $transportationType) {
                            Text("CreateTransportationView.GoingTransportation.text").tag(TransportationTypeEnum.DEPARTURE)
                            Text("CreateTransportationView.ReturnTransportation.text").tag(TransportationTypeEnum.RETURN)
                        }.onChange(of: transportationType) { _ in
                            print(transportationType)
                        }
                        .pickerStyle(SegmentedPickerStyle())
                        .padding()
                        
                        TextFieldWithTopLabel(width: geometry.size.width * 0.92, titleBackgroundColor: Color.colorBackground,
                                              titleValue : GenericObservableModel(value: "CreateTransportationView.TransportationName.text"),
                                              textFieldValue: self.transportationName)
                            .padding(EdgeInsets(top: 5, leading: 0, bottom: 0, trailing: 0))
                        
                        
                        
                        HStack{

                            TextFieldWithTopLabel(width: geometry.size.width * 0.45 - 5, titleBackgroundColor: Color.colorBackground,
                                                  titleValue : GenericObservableModel(value: "CreateTransportationView.PlateNumber.text"),
                                                  textFieldValue: self.transportationPlate)
                                .padding(EdgeInsets(top: 10, leading: 0, bottom: 0, trailing: 0))
                            
                            
                            Spacer()
                            
                            DateWithTopLabel(width: geometry.size.width * 0.45 - 5,
                                             titleBackgroundColor: Color.colorBackground,
                                             titleValue : transportationType == TransportationTypeEnum.DEPARTURE ?
                                                GenericObservableModel(value: "CreateTransportationView.StartingTime.text") :
                                                GenericObservableModel(value: "CreateTransportationView.DepartureTime.text"),
                                             observedDate: self.transportationStartTime)
                                .onTapGesture {
                                    self.isTimeViewPresented.toggle()
                                }
                                .padding(EdgeInsets(top: 10, leading: 0, bottom: 0, trailing: 0))
                        }
                        
                        MapLocationViewWithTopLabel(width: geometry.size.width * 0.92,
                                                    height: geometry.size.height * 0.2,
                                                    titleBackgroundColor: Color.colorBackground,
                                                    titleValue : transportationType == TransportationTypeEnum.DEPARTURE ?
                                                       GenericObservableModel(value: "CreateTransportationView.DestinationPoint.text") :
                                                       GenericObservableModel(value: "CreateTransportationView.DeparturePoint.text"),
                                                    observedCoordinate: self.selectedCoordinate)
                            .onTapGesture {
                                self.isMapViewVisible = true
                                print("map pressed")
                            }
                            .padding(EdgeInsets(top: 10, leading: 0, bottom: 0, trailing: 0))
                        
                    }
                    .padding(EdgeInsets(top: 10, leading: geometry.size.width*0.04, bottom: 0, trailing: geometry.size.width*0.04))
                    .background(Color.colorBackground)
                }
                   
    //                .navigationBarHidden(false)
            }
            .navigationBarItems(
                trailing:
                    VStack{
                        if nil == self.createdTransportation && self.isSaveStarted == false{
                            Button("CreateTransportationView.Save.text"){
                                self.save()
                            }
                        }else{
                            EmptyView()
                        }
                    }
            )
            .disabled(self.isTimeViewPresented)
            .blur(radius: self.isTimeViewPresented ? 15 : 0)
            
            
            if self.isTimeViewPresented{
                HStack{
                    Spacer()
                    VStack(){
                        Spacer()
                        TimeView(date: self.$transportationStartTime.value, isPresented: self.$isTimeViewPresented, doSubmit: {})
                        Spacer()
                    }
                    Spacer()
                }
            }
        }
        .sheet(isPresented: self.$isMapViewVisible) {
            MapLocationSelectUIView(observedCoordinate: self.selectedCoordinate)
        }
        .alert(isPresented: self.$showAlert.value) {
            Alert(title: Text(""), message: Text("CreateTransportationView.ShuttleNameEmptyAlert.text"), dismissButton: .default(Text("CreateTransportationView.ShuttleNameEmptyAlert.Ok.text")))
        }
        .navigationBarTitle(nil != createdTransportation ? "CreateTransportationView.AlreadyCreateShuttle.text" : "CreateTransportationView.CreateShuttle.text")
        
    }
    
}

struct CreateTransportationView_Previews: PreviewProvider {
    static var previews: some View {
        CreateTransportationView().environment(\.locale, .init(identifier: "tr"))
    }
}
