//
//  Deneme1.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/26/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI


struct DenemeCustomRows: View {
    @State var isLinkVisible = false
    @State var transportations : [Transportation] = createdTransportations
    @State var error : Error? = nil

    
    var body: some View {
        
        NavigationView{
            ZStack {
                List {
                    ForEach(transportations) { transportation in
                        CustomCreatedTransportationRow(transportation: transportation)
                    }
                }
                
                FloatingButton(isLinkVisible: $isLinkVisible, doNavigationDisappear: {print("do nothing")})
                
            }
            .navigationBarTitle(Text("Created Shuttles"))
            .onAppear(perform: {
//                self.isLinkVisible = false
//                self.fetchData()
//                print("created andac shuttle views")
            })
        }
    }
    
    func fetchData(){
       NetworkManager.fetch(url: UrlConstants.UrlGetCreatedTransportationList, parameters: Person(id: MyUser.getInstance().getUserId())
           , handle: {
               newData in
               self.transportations = newData}
           , errorHandle: {
               newError in
               self.error = newError})
       
   }

}

struct DenemeCustomRows_Previews: PreviewProvider {
    static var previews: some View {
        DenemeCustomRows()
    }
}
