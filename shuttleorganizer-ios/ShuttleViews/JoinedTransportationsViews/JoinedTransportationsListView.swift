//
//  JoinedTransportationsListView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 6/2/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct JoinedTransportationsListView: View {
    @ObservedObject var joinedTransportationsList: JoinedTransportationsListObservable = JoinedTransportationsListObservable.getInstance()
    @State var isShortcutsVisible = false
    @ObservedObject var isShowToastCopied: ToastObservableModel = ToastObservableModel()
    @ObservedObject var sharedTransportationId : GenericObservableModel = GenericObservableModel(value: "")
    
    @ObservedObject var shuttleNotificationWrapper : GenericObservableModel<ShuttleNotificationWrapper?> = GenericObservableModel(value: nil)
    @ObservedObject var selectedTransportationId : GenericObservableModel<String?> = GenericObservableModel(value: nil)
//    func notificationCame(_ notification: Notification) {
//        if let shuttleNotificationWrapper : ShuttleNotificationWrapper = notification.object as? ShuttleNotificationWrapper{
//            print("shuttleNotificationWrapper: \(shuttleNotificationWrapper)")
//            let shuttleNotificationTypeEnum : ShuttleNotificationTypeEnum = shuttleNotificationWrapper.shuttleNotificationModel.getShuttleNotificationTypeEnum()
//            if shuttleNotificationTypeEnum == .TRANSPORTATION_NOTIFICATION && self.selectedTabIndex.value != 0 {
//                self.selectedTabIndex.value = 0
//            }else{
//                print("MainTransportationsTabview notification event will disregard for notification type: \(shuttleNotificationTypeEnum)")
//            }
//        }else{
//            print("Error : MainTransportationsTabview can not parse notification for notification.object:\(notification.object ?? "")")
//        }
//    }
    
    func notificationCame(_ notification: Notification) {
//        print("notification.object: \(notification.object)")
        if let notificationWrapper : ShuttleNotificationWrapper = notification.object as? ShuttleNotificationWrapper{
            let notificationModel = notificationWrapper.shuttleNotificationModel
            if notificationModel.getShuttleNotificationTypeEnum() == .TRANSPORTATION_NOTIFICATION {
                self.shuttleNotificationWrapper.value = notificationWrapper
                JoinedTransportationsListObservable.getInstance().fetchData(
                    callBack: {
                        if let notificationWrapper = self.shuttleNotificationWrapper.value{
                            self.shuttleNotificationWrapper.value = nil
                            if let notificationTransportation = self.joinedTransportationsList.transportations.first(where: { $0.id == notificationWrapper.shuttleNotificationModel.transportationId }){
                                self.selectedTransportationId.value = notificationTransportation.id
                            }
                        }
                    }
                )
//                if nil == self.joinedTransportationsList.transportations.first(where: { $0.id == notificationModel.transportationId}){
//                    self.shuttleNotificationWrapper.value = notificationWrapper
////                    self.fetchJoinedTransportations() // calls to time need to do something in fetchJoinedTransportations
//                }else{
//                    if let notificationTransportation = self.joinedTransportationsList.transportations.first(where: { $0.id == notificationModel.transportationId }){
//                        self.selectedTransportationId.value = notificationTransportation.id
//                    }
//                }
            }
        }
    }
    
    
    init(){
        let notificationName = ShuttleNotificationEventFactory.getInstance().getEventNotificationName(shuttleNotificationTypeEnum: .TRANSPORTATION_NOTIFICATION)
        NotificationCenter.default.addObserver(forName: notificationName, object: nil, queue: nil, using: self.notificationCame)
    }
    
    func doAppear(){
        self.isShortcutsVisible = false
        self.joinedTransportationsList.fetchData(
            callBack: {
                if let notificationWrapper = self.shuttleNotificationWrapper.value{
                    self.shuttleNotificationWrapper.value = nil
                    if let notificationTransportation = self.joinedTransportationsList.transportations.first(where: { $0.id == notificationWrapper.shuttleNotificationModel.transportationId }){
                        self.selectedTransportationId.value = notificationTransportation.id
                    }
                }
            }
        )
    }
    
    var body: some View {
        ZStack {
            VStack{
                ScrollView(.vertical, showsIndicators: false ) {
                    ForEach(joinedTransportationsList.transportations) { transportation in
//                        NavigationLink(
//                            destination: JoinedTransportationsTabView(transportation: GenericObservableModel(value: transportation))
//                                .onDisappear(){
//                                    self.doAppear()
//                                }
//                        )  {
//                            JoinedTransportationRow(transportation: transportation, toastObservableModel: self.isShowToastCopied, sharedTransportationId: self.sharedTransportationId)
//                        }

                        NavigationLink(destination: JoinedTransportationsTabView(transportation: GenericObservableModel(value: transportation)).onDisappear(){ self.doAppear()},
                                       tag: transportation.id ?? "",
                                       selection: self.$selectedTransportationId.value
                        )  {
                            JoinedTransportationRow(transportation: transportation, toastObservableModel: self.isShowToastCopied, sharedTransportationId: self.sharedTransportationId)
                        }
                    }
                    Spacer()
                        .frame(height: 100, alignment: .bottom)
                }
                .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
            }
            .disabled(self.isShortcutsVisible)
            .blur(radius: self.isShortcutsVisible ? 15 : 0)
            
            FloatingButton(isLinkVisible: $isShortcutsVisible, doNavigationDisappear: { self.doAppear()})
        }
        .onTapGesture {
            if self.isShortcutsVisible{
                self.isShortcutsVisible = false
            }
        }
        .background(Color.colorListViewBackground)
        .onAppear(perform: {
            self.doAppear()
        })
//        .onDisappear(){
//            self.isShortcutsVisible = false
//        }
        .toastObservableView(toastObservableModel: isShowToastCopied, text: Text("JoinedTransportationsListView.Clipboard.text"))
        .sheet(isPresented: $sharedTransportationId.isValueChanged) {
            ShareSheet(activityItems: [self.sharedTransportationId.value])
        }
    }
}

struct JoinedTransportationsListView_Previews: PreviewProvider {
    static var previews: some View {
        JoinedTransportationsListView()
//            .environmentObject(JoinedTransportationsListObservable())
    }
}
