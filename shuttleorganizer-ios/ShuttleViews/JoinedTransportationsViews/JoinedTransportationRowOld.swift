//
//  JoinedTransportationRowOld.swift
//  shuttleorganizer-ios
//
//  Created by macos on 6.06.2021.
//  Copyright © 2021 Orion. All rights reserved.
//

import SwiftUI

struct JoinedTransportationRowOld: View {
    var transportation: Transportation
    //    @State private var showShareSheet = false
    //    @State private var toastCopied = false
        @ObservedObject var toastObservableModel : ToastObservableModel
        @ObservedObject var sharedTransportationId : GenericObservableModel<String>
        
        var body: some View {
           VStack{
                
                HStack() {
                    Text(transportation.name)
                        .foregroundColor(Color.black)
                        .font(.title)
                        .padding(EdgeInsets(top: 0, leading: 5, bottom: -20, trailing: 5))
                    Spacer()
                }

                HStack() {
                    RectangleDots(insideView:
                        Text(transportation.id!)
                            .font(.body)
                            .foregroundColor(Color.black))
                        .padding(EdgeInsets(top: 0, leading: 0, bottom: 10, trailing: 0))
                    
                    Image("copy")
                        .renderingMode(.template)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 30 ,height:30)
                        .padding(5)
                        .foregroundColor(Color.colorPrimary)
                        .onTapGesture {
                            UIPasteboard.general.string = self.transportation.id == nil ? "" : self.transportation.id!
                            self.toastObservableModel.isToastShowing = true
                    }

                    Spacer()

                    Image("share")
                        .renderingMode(.template)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 30, height: 30)
                        .padding(5)
                        .foregroundColor(Color.colorPrimary)
                        .padding(.trailing, 7)
                        .onTapGesture {
                            print("share pressed pressed")
                            if let validTransportationId = self.transportation.id {
                                print("shared transportation id changed at row")
                                self.sharedTransportationId.value = validTransportationId
    //                            self.sharedTransportationId.isValueChanged = true
                            }
                        }
                    
                }
            }
    //        // Set the size of entire row. maxWidth makes it take up whole width of device.
    //        .frame(maxWidth: .infinity, maxHeight: 60, alignment: .leading)
    //        .padding(10) // Spacing around all the contents
    //        // Add a solid colored background that you can put a shadow on
    //        // (corner radius optional)
    //        .background(Color.white.cornerRadius(10).shadow(radius: 20))
            
        }
}

struct JoinedTransportationRowOld_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            JoinedTransportationRowOld(transportation: createdTransportations[2], toastObservableModel: ToastObservableModel(), sharedTransportationId: GenericObservableModel(value: "asdasdas123"))
            JoinedTransportationRowOld(transportation: createdTransportations[0], toastObservableModel: ToastObservableModel(), sharedTransportationId: GenericObservableModel(value: "asdasdas123"))
            JoinedTransportationRowOld(transportation: createdTransportations[1], toastObservableModel: ToastObservableModel(), sharedTransportationId: GenericObservableModel(value: "asdasdas123"))
        }
        .previewLayout(.fixed(width: 400, height: 700))
    }
}
