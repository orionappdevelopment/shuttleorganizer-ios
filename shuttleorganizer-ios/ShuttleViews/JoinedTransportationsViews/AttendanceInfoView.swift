//
//  AttendanceInfoView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 6/8/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI
import UIKit
import Foundation
import CoreLocation
import GoogleMaps

struct AttendanceInfoView: View {
    @ObservedObject var transportation : GenericObservableModel<Transportation>
    @ObservedObject var attendanceList : GenericObservableModel<[Attendance]> = GenericObservableModel.init(value: [])
    
    @ObservedObject var personTransportation : GenericObservableModel<PersonTransportation> = GenericObservableModel(value: PersonTransportation(personId: MyUser.getInstance().getUserId(), transportationId: ""))
    @ObservedObject var transportationStartTime: GenericObservableModel<Date> = GenericObservableModel(value: Date())
    
    @ObservedObject private var personTime: GenericObservableModel<Date> = GenericObservableModel(value: Date())
    @State private var isTimeViewPresented = false
    @State private var personTimeInvalid : Bool = false { didSet {
            if personTimeInvalid {
                self.showAlert.value = true
            }
        }}
    
    @ObservedObject var removingAttendance : GenericObservableModel<Attendance> = GenericObservableModel(value: Attendance() ,                                                                                                   callBackWithParent: { (rAttendance: Attendance, parent : Any) in
                                if let validParent = parent as? AttendanceInfoView{
                                    print("will remove attendance")
                                    validParent.showAlert.value = true
                                    print(validParent.removingAttendance.isValueChanged)
                                }
                            })
    
    @ObservedObject var yCalendarManager = YManager(calendar: Calendar.current, minimumDate: Date(), maximumDate: Date().addingTimeInterval(60*60*24*365),
                                    mode: 5, callBack: {print("callBack called")})
    @ObservedObject var yCalendarButtonIsClicked : GenericObservableModel<Bool> = GenericObservableModel(value: false,                                                                                                         callBackWithParent: { (isButtonClicled: Bool, parent : Any) in
                                print("yCalendarButtonIsClicked updated and now in callBackWithParent")
                                if let validParent = parent as? AttendanceInfoView {
                                    print("will add attendance")
                                    validParent.yCalendarButtonClickedAction()
                                }
                            })
    @ObservedObject var showAlert = GenericObservableModel(value: false,
        callBackWithParent: { (isShowAlert: Bool, parent : Any) in
                                print("show Alert will change")
                                if let validParent = parent as? AttendanceInfoView {
                                    if isShowAlert == false {
                                        validParent.removingAttendance.isValueChanged = false
                                        validParent.personTimeInvalid = false
                                        print("removingAttendance.isValueChanged and personTimeInvalid are set to false")
                                    }
                                }
                            })
    
    @State private var isAttendanceInfoViewFirstLoadComplete : Bool = false
    
    @ObservedObject var selectedCoordinate : GenericObservableModel<CLLocationCoordinate2D> = GenericObservableModel(value : CLLocationCoordinate2D(latitude: 41.0370, longitude: 28.9851))
    @ObservedObject var sheetIsPresented = GenericObservableModel(value: false)
    @State private var attendanceInfoSheetPresenting : AttendanceInfoSheetPresenting = .none
    @State var error : Error? = nil
    
    @ObservedObject var redisAttendancePublisher : GenericObservableModel<RedisAttendancePublisher?> = GenericObservableModel(value: nil)
    
    private enum AttendanceInfoSheetPresenting{case none, yCalendarPresenting, mapViewPresenting}
    
    init(transportation: GenericObservableModel<Transportation>){
        self.transportation = transportation
        self.transportationStartTime.value = transportation.value.startTime.toDate()
//        self.personTransportationView = personTransportationView
        
        self.yCalendarButtonIsClicked.parent = self
        self.removingAttendance.parent = self
        self.showAlert.parent = self
    }
    
    
    public func yCalendarButtonClickedAction(){
        print("yCalendarButtonClickedAction is called")
        if self.yCalendarButtonIsClicked.value == true && self.yCalendarManager.startDate != nil && self.yCalendarManager.endDate != nil {
            let startDate : LDate = Utils.toLDate(date: yCalendarManager.startDate)
            let endDate : LDate = Utils.toLDate(date: yCalendarManager.endDate)
            let attendance : Attendance = Attendance(personId: MyUser.getInstance().getUserId(), transportationId: self.transportation.value.id!,
                                                     attended: false, startDate: startDate, endDate: endDate)
            self.saveAttendance(attendance: attendance)
            self.yCalendarManager.startDate = nil
            self.yCalendarManager.endDate = nil
            self.yCalendarButtonIsClicked.value = false
        }
    }
    
    private func fetchPersonTransportation(){
        print("TransportationParticipantsListView.self = \(TransportationParticipantsListView.self)")
        let personTransportation : PersonTransportation = PersonTransportation(personId: MyUser.getInstance().getUserId(), transportationId: self.transportation.value.id!)
        
        NetworkManager.fetch(url: UrlConstants.UrlGetPersonTransportation, parameters: personTransportation
            , handle: {
                (newData : PersonTransportation) -> ()  in
                    self.personTransportation.value = newData
                self.personTime.value = Utils.addSecondsToLTime(lTime: self.transportation.value.startTime, addSeconds: newData.dailyTimeDiffInSecs).toDate()
                    self.isAttendanceInfoViewFirstLoadComplete = true
                    if let latitude = newData.latitude, let longitude = newData.longitude{
                        self.selectedCoordinate.value = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    }
                }
            , errorHandle: {
                newError in
                self.error = newError})
        
    }
    
    private func saveAttendance(attendance : Attendance){
        
        NetworkManager.fetch(url: UrlConstants.UrlSaveAttendance, parameters: attendance
            , handle: {
                (newData : Attendance ) -> () in
                    print("Created Attendance : \(newData)")
                    print("save Attendance successfully")
                    self.fetchAttendanceList()
                    let transportationId = attendance.transportationId
                    print(self.redisAttendancePublisher.value ?? "")
                    if newData.startDate == Utils.toLDate(date: Date()){
                        self.redisAttendancePublisher.value?.publish(channel: RedisConstants.getRedisAttendanceChannelId(transportationId: transportationId), message: "updated")
                    }
             }
            , errorHandle: {
                newError -> () in
                print("Create Attendace Error: \(newError)")
                self.error = newError})
    }
    
    private func savePersonTransportation(){
        let dailyTimeDiffInSecs = Utils.lTimesDiffInSeconds(lTime1: self.transportation.value.startTime, lTime2: Utils.toLTime(time: self.personTime.value))
        
        let personTransportation : PersonTransportation = PersonTransportation(personId: self.personTransportation.value.personId, transportationId: self.personTransportation.value.transportationId, personRoleType: self.personTransportation.value.personRoleType, latitude: self.selectedCoordinate.value.latitude, longitude: self.selectedCoordinate.value.longitude, dailyTimeDiffInSecs: dailyTimeDiffInSecs )

        NetworkManager.fetch(url: UrlConstants.UrlSavePersonTransportation, parameters: personTransportation
            , handle: {
                (newData : PersonTransportation ) -> () in
                
                print("saved dailyTimeDiffInSecs:\(newData.dailyTimeDiffInSecs)")
                self.personTime.value = Utils.addSecondsToLTime(lTime: self.transportation.value.startTime,
                                                          addSeconds: newData.dailyTimeDiffInSecs).toDate()
                print("saved personTime:\(self.personTime)")
                print("Saved PersonTransportation")
//                TODO: you may want to fetch data to set it back
             }
            , errorHandle: {
                newError -> () in
                print("Saved PersonTransportation Error: \(newError)")
                self.error = newError})
    }

    private func fetchAttendanceList(){
        let attendance: Attendance = Attendance(personId: MyUser.getInstance().getUserId(), transportationId: transportation.value.id!, attended: false, startDate: Utils.toLDate(date: Date()), endDate: Utils.toLDate(date: Date()) )
        
        NetworkManager.fetch(url: UrlConstants.UrlGetAttendanceList, parameters: attendance
            , handle: {
                (newData : [Attendance] ) -> () in
                    print("fetched [Attendance] : \(newData)")
                    print("fetched [Attendance] successfully")
                    self.attendanceList.value = newData
                    //                    getDateListBetweenDates
                    var absentDateList : [Date] = []
                    for attendance in newData {
                        absentDateList = Utils.getDateListBetweenDates(startDate: attendance.startDate, endDate: attendance.endDate)
                        self.yCalendarManager.absentDates.append(contentsOf: absentDateList)
                    }
             }
            , errorHandle: {
                newError -> () in
                print("fetch [Attendace] Error: \(newError)")
                self.error = newError})
    }
    
    private func removeAttendance(attendance : Attendance){
        
        let attendance : Attendance = Attendance(id: attendance.id!, personId: attendance.personId, transportationId: attendance.transportationId,
                                                 attended: attendance.attended, startDate: attendance.startDate, endDate: attendance.endDate)
        print("remove Attendance called for \(attendance)")
        NetworkManager.fetch(url: UrlConstants.UrlDeleteAttendance, parameters: attendance
            , handle: {
                (newData : Attendance.DeletedAttance) in
                    let filteredList = self.attendanceList.value.filter{ att in
                        att.id != attendance.id
                    }
                    let deletedAttendance : Attendance? = self.attendanceList.value.first(where: {
                        att in
                            att.id == attendance.id
                    })
                
                    self.attendanceList.value = filteredList
                    print("remove attendance is successful")
                    //start update ycalendar
                    var absentDateList : [Date] = []
                    for attendance in filteredList {
                        let tmpList = Utils.getDateListBetweenDates(startDate: attendance.startDate, endDate: attendance.endDate)
                        absentDateList.append(contentsOf: tmpList)
                    }
                    self.yCalendarManager.absentDates = absentDateList
                    let transportationId = attendance.transportationId
                
                    
                    if deletedAttendance?.startDate == Utils.toLDate(date: Date()){
                        self.redisAttendancePublisher.value?.publish(channel: RedisConstants.getRedisAttendanceChannelId(transportationId: transportationId), message: "updated")
                    }
                    //end
                }
            , errorHandle: {
                newError in
                self.error = newError
                print("can not remove attendance \(newError)")
                })
    }
    
    private func savePersonTime(){
        print("Date picker disappear")
        print("save clicked")
        if Utils.isValidTimeDiffBeetweenDepartureAndPersonGetOntime(transportationTime: self.transportation.value.startTime,
                                                                    personGetOnTime: Utils.toLTime(time: self.personTime.value)) {
            self.savePersonTransportation()
        } else {
            self.personTimeInvalid = true
            self.personTime.value = Utils.addSecondsToLTime(lTime: self.transportation.value.startTime,
                                                      addSeconds: self.personTransportation.value.dailyTimeDiffInSecs).toDate()
        }
    }
    
    private func savePersonGetOnOffPoint(){
        print("Date picker disappear")
        print("save clicked")
        if self.personTransportation.value.latitude != self.selectedCoordinate.value.latitude ||
            self.personTransportation.value.longitude != self.selectedCoordinate.value.longitude{
            self.savePersonTransportation()
        }
    }
    
    var body: some View {
        GeometryReader { geometry in
            ScrollView(.vertical, showsIndicators: true ) {
            ZStack{
                VStack {

                    DateWithTopLabel(width: geometry.size.width * 0.92,
                                     roundedRectangleStrokeColor: Color.colorDarkGray,
                                     titleForegroundColor: Color.colorDarkGray,
                                     titleBackgroundColor: Color.colorBackground,
                                     titleValue : TransportationTypeEnum.toTransportationTypeEnum(transportation.value.transportationType) == TransportationTypeEnum.DEPARTURE ?
                                            GenericObservableModel(value: "AttendanceInfoView.StartingTime.text") :
                                            GenericObservableModel(value: "AttendanceInfoView.DepartureTime.text"),
                                     dateForegroundColor: Color.colorDarkGray,
                                     observedDate: self.transportationStartTime)
                        .onTapGesture {
                            //just show transportation time
//                            self.isTimeViewPresented.toggle()
                        }
                        .padding(EdgeInsets(top: 10, leading: 0, bottom: 0, trailing: 0))
                    
                    
                    if TransportationTypeEnum.toTransportationTypeEnum(self.transportation.value.transportationType) == TransportationTypeEnum.DEPARTURE{
                        DateWithTopLabel(width: geometry.size.width * 0.92,
                                         titleBackgroundColor: Color.colorListLight,
                                         titleValue : GenericObservableModel(value: "AttendanceInfoView.GetOnTime.text"),
                                         observedDate: self.personTime)
                            .onTapGesture {
                                self.isTimeViewPresented.toggle()
                            }
                            .padding(EdgeInsets(top: 10, leading: 0, bottom: 0, trailing: 0))
                    }
                    
                    
                    MapLocationViewWithTopLabel(width: geometry.size.width * 0.92,
                                                height: geometry.size.height * 0.20,
                                                titleBackgroundColor: Color.colorBackground,
                                                titleValue : TransportationTypeEnum.toTransportationTypeEnum(transportation.value.transportationType) == TransportationTypeEnum.DEPARTURE ?
                                                        GenericObservableModel(value: "AttendanceInfoView.GetOnPoint.text") :
                                                        GenericObservableModel(value: "AttendanceInfoView.GetOffPoint.text"),
                                                observedCoordinate: self.selectedCoordinate)
                        .onTapGesture {
                            self.attendanceInfoSheetPresenting = AttendanceInfoSheetPresenting.mapViewPresenting
                            self.sheetIsPresented.value = true
                            print("map pressed")
                        }
                        .padding(EdgeInsets(top: 10, leading: 0, bottom: 0, trailing: 0))
                    
                    

                    VStack(alignment: .leading){
                        HStack(){
                            Spacer()
                            Text("AttendanceInfoView.MyOffDays.text")
                                .foregroundColor(Color.colorPrimary)
                                .font(.headline)
                            Spacer()
                        }.padding(.top, 20)
                        
                        HStack(){
                            Text("AttendanceInfoView.StartDate.text")
                                .foregroundColor(Color.colorPrimary)
                                .frame(width: geometry.size.width * 0.400 , alignment: Alignment.leading)
                            Text("AttendanceInfoView.EndDate.text")
                                .foregroundColor(Color.colorPrimary)
                                .frame(width: geometry.size.width * 0.410 , alignment: Alignment.leading)
                            Image("calendar_old")
                                .renderingMode(.original)
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: geometry.size.width * 0.060, height: 30, alignment: Alignment.trailing)
                                .foregroundColor(Color.colorPrimary)
                                .onTapGesture {
                                    print("calendar pressed")
                                    self.attendanceInfoSheetPresenting = AttendanceInfoSheetPresenting.yCalendarPresenting
                                    self.sheetIsPresented.value = true
                                }
                        }.padding(.top, 10)
                        
//                        ScrollView(.vertical, showsIndicators: true ) {
                            ForEach(self.attendanceList.value) { attendance in
                                HStack() {
                                    Text(getYearMonthDateFormat().string(from: attendance.startDate.toDate() ))
                                        .foregroundColor(Color.colorText)
                                        .frame(width: geometry.size.width * 0.400 , alignment: Alignment.leading)
//                                        Spacer()
                                    Text(getYearMonthDateFormat().string(from: attendance.endDate.toDate()))
                                        .foregroundColor(Color.colorText)
                                        .frame(width: geometry.size.width * 0.410 , alignment: Alignment.leading)
//                                        Spacer()
                                    Image("remove")
                                        .renderingMode(.template)
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width: geometry.size.width * 0.060, height: 30, alignment: Alignment.trailing)
//                                            .padding(5)
                                        .foregroundColor(Color.colorPrimary)
//                                            .padding(.trailing, 7)
                                        .onTapGesture {
                                            print("remove pressed")
                                            self.removingAttendance.value = attendance
                                            self.showAlert.value = true
                                        }
                                    Spacer()
                                }
                            }
//                        }
//                        .offset(x: -15)
                        Spacer()
                            .frame(height: 50, alignment: .bottom)
                    }
                    .onAppear(perform: {
                        self.isTimeViewPresented = false
                        self.redisAttendancePublisher.value = RedisAttendancePublisher()
                        self.redisAttendancePublisher.value?.initialize()
                        self.fetchAttendanceList()
                        self.fetchPersonTransportation()
                        print("fetchAttanceList onAppear")
    //                    self.personTime = Utils.addSecondsToLTime(lTime: self.transportation.value.startTime,
    //                                                              addSeconds: self.personTransportation.value.dailyTimeDiffInSecs).toDate()
                    })
                    .sheet(isPresented: self.$sheetIsPresented.value, content: {
                        if self.attendanceInfoSheetPresenting == AttendanceInfoSheetPresenting.yCalendarPresenting{
                            YViewController(isPresented: self.sheetIsPresented, yManager: self.yCalendarManager
                                            , isButtonClicked: self.yCalendarButtonIsClicked)
                        }else /*if self.sheetPresenting == SheetPresenting.mapViewPresenting */{
                            MapLocationSelectUIView(observedCoordinate: self.selectedCoordinate
                                , doSubmit: {
                                    self.sheetIsPresented.value = false
                                    self.savePersonGetOnOffPoint()
                                }
                                , doClose: {
                                    self.sheetIsPresented.value = false
                                }
                            )
                        }
                    })
                    .alert(isPresented: self.$showAlert.value) {
                        if self.removingAttendance.isValueChanged {
                            return
                                Alert(title: Text(""), message: Text("AttendanceInfoView.DeleteAttendanceWarning.text"),
                                  primaryButton: .destructive(Text("AttendanceInfoView.DeleteAttendanceWarning.Delete.text"),
                                    action: {
                                        print("will remove attendance")
                                        self.removeAttendance(attendance: self.removingAttendance.value)
                                        }),
                                  secondaryButton: .cancel({print("cancel Button clicked")}))
                        }else{
                            return
                                Alert(title: Text(""), message: Text("AttendanceInfoView.InvalidPersonStartTime.text"), dismissButton: .default(Text("AttendanceInfoView.InvalidPersonStartTime.Ok.text")))
                        }
                    }
                }
                .padding(EdgeInsets(top: 15, leading: geometry.size.width*0.04, bottom: 0, trailing: geometry.size.width*0.04))
                .disabled(self.isTimeViewPresented)
                .blur(radius: self.isTimeViewPresented ? 15 : 0)
                .background(Color.colorBackground)
            }
            }
            if self.isTimeViewPresented{
                HStack{
                    Spacer()
                    VStack(){
                        Spacer()
                        TimeView(date: self.$personTime.value, isPresented: self.$isTimeViewPresented, doSubmit: {self.savePersonTime()})
                        Spacer()
                    }
                    Spacer()
                }
            }
        }
        .foregroundColor(Color.colorBackground)
    }
}

struct AttendanceInfoView_Previews: PreviewProvider {
    static let t: Transportation = Transportation(id: "vh5vHYd23n", name: "", latitude: 0, longitude: 0, transportationType: 0, days: 210, status: 0, creatorPersonId: "0", startTime: LTime(), timeZone: 3)
    static let dd = [true, false, false, false, true, false, true]
    
    static var previews: some View {
        AttendanceInfoView(transportation: GenericObservableModel(value: t)).environment(\.locale, .init(identifier: "tr"))
    }
}

