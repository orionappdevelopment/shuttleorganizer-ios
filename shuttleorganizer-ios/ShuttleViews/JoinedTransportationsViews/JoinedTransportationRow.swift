//
//  JoinedTransportationRow.swift
//  shuttleorganizer-ios
//
//  Created by macos on 6/2/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct JoinedTransportationRow: View {
    var transportation: Transportation
//    @State private var showShareSheet = false
//    @State private var toastCopied = false
    @ObservedObject var toastObservableModel : ToastObservableModel
    @ObservedObject var sharedTransportationId : GenericObservableModel<String>
    @ObservedObject var joinedTransportationsList: JoinedTransportationsListObservable = JoinedTransportationsListObservable.getInstance()
//    @EnvironmentObject var joinedTransportationsList: JoinedTransportationsListObservable
    
    var body: some View {
//        GeometryReader { geometry in
            ZStack{
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color.colorPrimary, lineWidth: 0.5)
                    .background(Color.colorListLight)
//                    .shadow(radius: 1, y: 1)
                
                VStack{
                    HStack() {
                        Text(transportation.name)
                            .foregroundColor(Color.colorPrimary)
                            .font(.title)
                        
                        Text(TransportationTypeEnum.toTransportationTypeEnum(transportation.transportationType) == TransportationTypeEnum.DEPARTURE ?   "CreatedTransportationRow.DepartureTransportation.text" : "CreatedTransportationRow.ReturnTransportation.text")
                            .foregroundColor(Color.colorTabUnSeledted)
                            .font(.title)
                        
                        Spacer()
                        
                        Image("share")
                            .renderingMode(.template)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 40, height: 40)
                            .padding(5)
                            .foregroundColor(Color.colorPrimary)
                            .onTapGesture {
                                print("share pressed pressed")
                                if let validTransportationId = self.transportation.id {
                                    print("shared transportation id changed at row")
                                    self.sharedTransportationId.value = validTransportationId
                                }
                            }
                    }
                    .padding(EdgeInsets(top: 5, leading: 0, bottom: 0, trailing: 0))
                    
                    Spacer()
                    
                    Divider()
                        .background(Color.colorPrimary)
                        .padding(EdgeInsets(top: 0, leading: 0, bottom: 5, trailing: 0))
                    
                    Spacer()
                    
                    ZStack(){
                        HStack{
                            Image("licence_plate")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 160, height: 40, alignment: .trailing)
//                                        .padding(5)
                            Spacer()
                            
                        }
                        HStack{
                            Text(transportation.plate)
    //                            .multilineTextAlignment(.trailing)
                                .foregroundColor(Color.colorText)
                                .padding(EdgeInsets(top: 0, leading: 40, bottom: 0, trailing: 0))
                            Spacer()
                        }
                    }
                    
                    Spacer()
                    
                    HStack() {
                        RectangleDots(height: 40, insideView:
                            Text(transportation.id!)
                                .font(.body)
                                .foregroundColor(Color.colorText))
                            .padding(EdgeInsets(top: 5, leading: 0, bottom: 5, trailing: 5))
                        
                        Image("copy")
                            .renderingMode(.template)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 40 ,height:40)
                            .padding(5)
                            .foregroundColor(Color.colorPrimary)
                            .onTapGesture {
                                UIPasteboard.general.string = self.transportation.id == nil ? "" : self.transportation.id!
                                self.toastObservableModel.isToastShowing = true
                            }
                        
                        Spacer()
                        
                    }
                    Spacer()
                    Spacer()
                }
                .frame(width: UIScreen.main.bounds.width * 0.88, alignment: .center)
            }
            .frame(width: UIScreen.main.bounds.width * 0.94 , height: 170, alignment: .center)
            .padding(EdgeInsets(top: 13, leading: 0, bottom: 13, trailing: 0))
            .onAppear(){
                self.joinedTransportationsList.fetchData()
            }
//        }
//        // Set the size of entire row. maxWidth makes it take up whole width of device.
//        .frame(maxWidth: .infinity, maxHeight: 60, alignment: .leading)
//        .padding(10) // Spacing around all the contents
//        // Add a solid colored background that you can put a shadow on
//        // (corner radius optional)
//        .background(Color.white.cornerRadius(10).shadow(radius: 20))
        
    }
}

struct JoinedTransportationRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            JoinedTransportationRow(transportation: createdTransportations[2], toastObservableModel: ToastObservableModel(), sharedTransportationId: GenericObservableModel(value: "asdasdas123"))
            JoinedTransportationRow(transportation: createdTransportations[0], toastObservableModel: ToastObservableModel(), sharedTransportationId: GenericObservableModel(value: "asdasdas123"))
            JoinedTransportationRow(transportation: createdTransportations[1], toastObservableModel: ToastObservableModel(), sharedTransportationId: GenericObservableModel(value: "asdasdas123"))
        }
        .previewLayout(.fixed(width: 400, height: 700))
    }
}

