//
//  AttendanceRow.swift
//  shuttleorganizer-ios
//
//  Created by macos on 6/9/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct AttendanceRow: View {
    var startDate : Date
    var endDate : Date
    var attendance : Attendance
    
    @ObservedObject var removigAttendance : GenericObservableModel<Attendance>
    
    var body: some View {
        VStack(alignment: .leading){
            HStack() {
                
                Text(getYearMonthDateFormat().string(from: startDate))
                    .foregroundColor(Color.colorText)
//                    .padding(.leading, 5)

                Spacer()
                
                Text(getYearMonthDateFormat().string(from: endDate))
                    .foregroundColor(Color.colorText)
//                    .padding(.leading, 5)
                
                Spacer()
                
                Image("remove")
                    .renderingMode(.template)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 30, height: 30)
                    .padding(5)
                    .foregroundColor(Color.colorPrimary)
                    .padding(.trailing, 7)
                    .onTapGesture {
                        print("remove pressed")
                        self.removigAttendance.value = self.attendance
                        print(self.removigAttendance.isValueChanged)
                    }
                
            }
        }
    }
}

struct AttendanceRow_Previews: PreviewProvider {
    static var previews: some View {
        List{
            AttendanceRow(startDate: Date(), endDate: Date(), attendance: Attendance(), removigAttendance: GenericObservableModel(value: Attendance()))
            AttendanceRow(startDate: Date(), endDate: Date(), attendance: Attendance(), removigAttendance: GenericObservableModel(value: Attendance())).environment(\.locale, .init(identifier: "tr"))
        }
    }
}
