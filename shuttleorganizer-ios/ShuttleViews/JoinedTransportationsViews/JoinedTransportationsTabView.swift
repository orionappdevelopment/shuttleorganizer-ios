//
//  JoinedTransportationsTabView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 6/13/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct JoinedTransportationsTabView: View {
    @State private var selectedTabIndex = 0
    @ObservedObject var transportation : GenericObservableModel<Transportation>
    
    @State private var attendanceInfoView : AttendanceInfoView
    
    @State private var transportationParticipantsListView : TransportationParticipantsListView
    
    init(transportation : GenericObservableModel<Transportation>){
        self.transportation = transportation
        self.attendanceInfoView = AttendanceInfoView(transportation: transportation)
        self.transportationParticipantsListView =  TransportationParticipantsListView(transportation: transportation.value)
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing : -8) {
            SlidingTabView(selection: self.$selectedTabIndex, tabs: ["JoinedTransportationsTabView.AttendanceInfo.text", "JoinedTransportationsTabView.Participants.text"])
            
            VStack(spacing : 0) {
                if selectedTabIndex == 0 {
//                    AttendanceInfoView(transportation: transportation)
                    self.attendanceInfoView
                } else {
//                    TransportationParticipantsListView(transportation: transportation.value)
//                    TransportationParticipantsAdvancedListView(transportation: transportation.value)
//                    TransportationParticipantsListView(transportation: transportation.value)
                    self.transportationParticipantsListView
                }
            }//
    //            (selectedTabIndex == 0 ? CreateShuttleMapView() : ).padding()
            Spacer()
        }
        .navigationBarTitle(Text(self.transportation.value.name))
    //        .padding(.top, 50)
    //        .animation(.none)
        
    }
}

struct JoinedTransportationsTabView_Previews: PreviewProvider {
    static var previews: some View {
        JoinedTransportationsTabView(transportation: GenericObservableModel(value: Transportation()))
    }
}
