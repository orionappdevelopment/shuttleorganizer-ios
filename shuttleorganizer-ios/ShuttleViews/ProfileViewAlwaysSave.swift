//
//  ProfileViewAlwaysSave.swift
//  shuttleorganizer-ios
//
//  Created by macos on 6/14/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct ProfileViewAlwaysSave: View {
    @ObservedObject private var personName : GenericObservableModel<String> = GenericObservableModel(value: "")
    
    @State private var email : String = ""
//    @ObservedObject private var notifications : GenericObservableModel<[Bool]> = GenericObservableModel(value : [Bool](repeating: true, count:10))
//    @State private var termsAccepted : Bool = false
    
    @ObservedObject private var no1 : GenericObservableModel<Bool> = GenericObservableModel(value: true)
    @ObservedObject private var no2 : GenericObservableModel<Bool> = GenericObservableModel(value: true)
    @ObservedObject private var no3 : GenericObservableModel<Bool> = GenericObservableModel(value: true)
    @ObservedObject private var no4 : GenericObservableModel<Bool> = GenericObservableModel(value: true)
    @ObservedObject private var no5 : GenericObservableModel<Bool> = GenericObservableModel(value: true)
    
    @State private var notifications : [Bool] = [Bool](repeating: true, count:10)
    
    var error : GenericObservableModel<Error>? = nil
    
    private func fetchPersonNotification(){
        let personNotification : PersonNotification = PersonNotification(personId: MyUser.getInstance().getUserId(), token: SingleNotificationManager.getInstance().personNotification.token)
        
        NetworkManager.fetch(url: UrlConstants.UrlGetNotification, parameters: personNotification
            , handle: {
                (newData : PersonNotification) -> ()  in
                    self.notifications = newData.getNotifications()
                    self.no1.value = newData.notification1
                    self.no2.value = newData.notification2
                    self.no3.value = newData.notification3
                    self.no4.value = newData.notification4
                    self.no5.value = newData.notification5
                    self.no1.isValueChanged = false
                    self.no2.isValueChanged = false
                    self.no3.isValueChanged = false
                    self.no4.isValueChanged = false
                    self.no5.isValueChanged = false
                
                }
            , errorHandle: {
                newError in
                    self.error?.value = newError
                    let errorString : String = "\(newError)"
                    if errorString .contains("keyNotFound(CodingKeys(stringValue: \"personId\", intValue: nil)"){
                        print("Can not found PersonNotification so directly create PersonNotification")
                        self.savePersonNotification()
                    }else{
                        print(newError)
                    }
                    
                })
    }
    
    private func saveAndValidateAllData(){
        if self.personName.isValueChanged {
            print("save person")
            self.savePerson()
            self.personName.isValueChanged = false
        }
        
        if self.no1.isValueChanged || self.no2.isValueChanged || self.no3.isValueChanged || self.no4.isValueChanged || self.no5.isValueChanged {
            print("save notification")
            self.savePersonNotification()
            self.no1.isValueChanged = false
            self.no2.isValueChanged = false
            self.no3.isValueChanged = false
            self.no4.isValueChanged = false
            self.no5.isValueChanged = false
        }
    }
    
    private func savePersonNotification(){
        let personNotification : PersonNotification = PersonNotification(personId: MyUser.getInstance().getUserId(), token: SingleNotificationManager.getInstance().personNotification.token, loggedIn: true, notification1: self.no1.value,
                                                                         notification2: self.no2.value, notification3: self.no3.value, notification4: self.no4.value, notification5: self.no5.value,
                                                                         notification6: self.notifications[5], notification7: self.notifications[6], notification8: self.notifications[7],
                                                                         notification9: self.notifications[8], notification10: self.notifications[9])
        
        NetworkManager.fetch(url: UrlConstants.UrlSaveNotification, parameters: personNotification
            , handle: {
                (newData : PersonNotification) -> ()  in
                    print("PersonNotification Saved: \(newData)")
                }
            , errorHandle: {
                newError in
                self.error?.value = newError})
    }
    
    private func fetchPerson(){
        let person : Person = Person(id: MyUser.getInstance().getUserId())
        
        NetworkManager.fetch(url: UrlConstants.UrlGetPerson, parameters: person
            , handle: {
                (newData : Person) -> ()  in
//                    self.savedPersonName = newData.name
                    self.personName.value = newData.name
                    self.personName.isValueChanged = false
                    self.email = newData.email
                }
            , errorHandle: {
                newError in
                self.error?.value = newError})
    }
//    TODO: to save efficently because each change leads to another save in sb
//    https://stackoverflow.com/questions/37805885/how-to-create-dispatch-queue-in-swift-3
        //here 2. Parameterized debounce function this method you can look
//    https://stackoverflow.com/questions/37805885/how-to-create-dispatch-queue-in-swift-3
//    https://stackoverflow.com/questions/37805885/how-to-create-dispatch-queue-in-swift-3
    
    private func savePerson(){
        let person : Person = Person(id: MyUser.getInstance().getUserId(), name: self.personName.value, email: self.email)
        
        NetworkManager.fetch(url: UrlConstants.UrlSavePerson, parameters: person
            , handle: {
                (newData : Person) -> ()  in
                    print("Person Saved: \(newData)")
                }
            , errorHandle: {
                newError in
                self.error?.value = newError})
    }
    
    //ProfileViewAlwaysSave
    var body: some View {
        VStack(alignment: .leading){
            HStack(alignment: .top){
                Image("user_placeholder")
                    .renderingMode(.template)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 80, height: 80)
                    .padding(EdgeInsets(top: 35, leading: 0, bottom: 10, trailing: 10))
                    .foregroundColor(Color.colorPrimary)
                    .onTapGesture {
                        print("map pressed")
                    }
                VStack(alignment: .leading){
                    Text("ProfileView.Name.text")
                    TextField("", text: self.$personName.value)
//                    TextField("", text: self.$personName.value, onEditingChanged: {_ in
//                            print("editing personName")
//                        }, onCommit: {
//                            print("commit personName")
//                        })
//                        .onTapGesture {
//                            print("text")
//                        }
                    Divider()
                        .padding(.top, -5)
                    Text("ProfileView.Email.text")
                    TextField("", text: $email)
                        .disabled(true)
                    Divider()
                        .padding(.top, -5)
                }
            }
                .padding(.bottom, 20)
            
            VStack(alignment: .leading){
                Text("ProfileView.NotificationTitle.text").font(.title)
                Toggle(isOn: $no1.value, label: {Text("ProfileView.ManagementNotification.text")})
                Toggle(isOn: $no2.value, label: { Text("ProfileView.ShuttleNotification.text") })
                Toggle(isOn: $no3.value, label: { Text("ProfileView.ChatNotification.text") })
            }
                .padding(.bottom, 20)
                
            VStack(alignment: .leading){
                Text("ProfileView.PrivacyTitle.text").font(.title)
                Toggle(isOn: $no4.value, label: { Text("ProfileView.HidePhoneNumber.text") })
                Toggle(isOn: $no5.value, label: { Text("ProfileView.HideEmail.text") })
            }
            Spacer()
        }
        .padding(EdgeInsets(top: 20, leading: 20, bottom: 0, trailing: 20))
        .onAppear(){
            print("on appear called")
            self.fetchPerson()
            self.fetchPersonNotification()
        }
        .onDisappear() {
            print("On Dissappear called for Profile page")
            self.saveAndValidateAllData()
        }
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willResignActiveNotification), perform: { _ in
                print("profile page at background")
                self.saveAndValidateAllData()
            })
    }
}

struct ProfileViewAlwaysSave_Previews: PreviewProvider {
    static var previews: some View {
        ProfileViewAlwaysSave()
    }
}
