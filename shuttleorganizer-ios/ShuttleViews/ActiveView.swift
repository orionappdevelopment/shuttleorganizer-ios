//
//  ActiveView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 7/3/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI
import UIKit
import Foundation
import CoreLocation
import GoogleMaps
import class Kingfisher.KingfisherManager
import class Kingfisher.ImageCache
import struct Kingfisher.KFImage
import struct Kingfisher.RoundCornerImageProcessor
import struct Kingfisher.DownsamplingImageProcessor
import protocol Kingfisher.ImageProcessor
import class Kingfisher.ImageDownloader
import struct Kingfisher.KingfisherWrapper

struct ActiveView: View {
//    static let TRANPORTATION_ID_INVALID = "-3001"
    
    @ObservedObject var mapView : GenericObservableModel<GMSMapView?> = GenericObservableModel(value: nil, callBackWithParentAndPreviousValue: {(mapViewValue: GMSMapView?, parent : Any, previousValue: GMSMapView?) in
        //first mapView successfully init call fetch
        if previousValue == nil && mapViewValue != nil{
            if let validParent = parent as? ActiveView {
                validParent.fetchJoinedTransportations()
            }
        }
        })
    @State var mapOfActiveViewGoogleMapController : MapOfActiveViewGoogleMapController? = nil
//    @State var transportations : [Transportation] = []
    @ObservedObject var transportations : GenericObservableModel<[Transportation]> = GenericObservableModel(value: [])
    
    @ObservedObject var personTransportationAttendances : GenericObservableModel<[PersonTransportationAttendance]> = GenericObservableModel(value: [])
    @ObservedObject var comboBoxExpanded : GenericObservableModel<Bool> = GenericObservableModel(value: false)
    
    @ObservedObject var selectedTransportation : GenericObservableModel<Transportation?> = GenericObservableModel(value: nil)
    @ObservedObject var transportationNextValidDate : GenericObservableModel<Date?> = GenericObservableModel(value: nil)
    
//    @State var transportationIdWhenAbsentTabbed : String = ActiveView.TRANPORTATION_ID_INVALID
    
    @ObservedObject var chatVisible : GenericObservableModel<Bool> = GenericObservableModel(value: false)
    
    @ObservedObject var streamChatView :  GenericObservableModel<StreamChatView?> = GenericObservableModel(value: nil)
    
    @ObservedObject var redisAttendanceProcess : GenericObservableModel<RedisAttendanceProcess?> = GenericObservableModel(value: nil)
    
    @ObservedObject var redisGpsSubscriber : GenericObservableModel<RedisGpsSubscriber?> = GenericObservableModel(value: nil)
    @ObservedObject var redisGpsCommand : GenericObservableModel<RedisGpsCommand?> = GenericObservableModel(value: nil)
    
    @ObservedObject var showAlert : GenericObservableModel<Bool> = GenericObservableModel(value: false)
    
    @ObservedObject var activeViewAlert : GenericObservableModel<ActiveViewAlert> = GenericObservableModel(value:.none)
    
    @ObservedObject var isSharingTransportationLocation : GenericObservableModel<Bool> = GenericObservableModel(value: false)
    
    @ObservedObject var transportationMarker : GenericObservableModel<GMSMarker?> = GenericObservableModel(value: nil)
    
    enum ActiveViewAlert{
        case transportationNotSelected, transportationNotActive, locationAuthorizationNotAlways, locationAuthorizationNone,none
    }
    
//    var redisAttendanceCallback : RedisCallback = RedisCallback()
    
    
    
    //be careful this method can create infinite number of Attendance
    @ObservedObject private var isPersonAbsent : GenericObservableModel<Bool> = GenericObservableModel(value: true,
            callBackWithParentAndPreviousValue: {(isPersonAbsent: Bool, parent : Any, previousValue: Bool) in
                if isPersonAbsent != previousValue{
                    if let validParent = parent as? ActiveView {
                        validParent.doAttendanceChange()
                    }
                }
                })
    
    @State var isFirstPersonAttendaceLoadComplete : Bool = false
    
    //    var numberOfMarkerTask : AtomicInteger = AtomicInteger()
    let lock = NSLock()
    
    //personId : Downloader
    @State var imageDownloaders :  [String : UIImageView] = [:]
    
    @ObservedObject var shuttleNotificationWrapper : GenericObservableModel<ShuttleNotificationWrapper?> = GenericObservableModel(value: nil)
    
    var error : GenericObservableModel<Error>? = nil
    
    
    class AttendanceRedisCallback : RedisCallback{
        let parent : ActiveView
        var transportation : Transportation
        
        init(parent : ActiveView, transportation : Transportation){
            self.parent = parent
            self.transportation = transportation
        }
        
        func commandExecuted(message: String) {
            parent.fetchPersonTransportationAttendance(transportation: self.transportation, withRedisSubscribe: false)
        }
        
        func commandFailed() {
            print("Attendance redis callback failed for transportation \(self.transportation)")
        }
    }
    
    class GpsRedisCallback : RedisCallback{
        let parent : ActiveView
        var transportation : Transportation
        
        init(parent : ActiveView, transportation : Transportation){
            self.parent = parent
            self.transportation = transportation
        }
        
        func commandExecuted(message: String) {
            let jsonData = message.data(using: .utf8)!
            var cm: GpsMessage? = nil
            do{
                cm = try JSONDecoder().decode(GpsMessage.self, from: jsonData)
                if let gpsMessage = cm {
                    parent.updateTransportationMarker(gpsMessage: gpsMessage)
                }
                
            }catch {
               print("Unexpected error during decoding ChatMessage: \(error).")
               cm = nil
            }
        }
        
        func commandFailed() {
            print("Attendance redis callback failed for transportation \(self.transportation)")
        }
    }
    
    init(){
        let notificationName = ShuttleNotificationEventFactory.getInstance().getEventNotificationName(shuttleNotificationTypeEnum: .CHAT_MESSAGE_NOTIFICATION)
        NotificationCenter.default.addObserver(forName: notificationName, object: nil, queue: nil, using: self.notificationCame)
    }
    
    func notificationCame(_ notification: Notification) {
//        print("notification.object: \(notification.object)")
        if let notificationWrapper : ShuttleNotificationWrapper = notification.object as? ShuttleNotificationWrapper{
            let notificationModel = notificationWrapper.shuttleNotificationModel
            if notificationModel.getShuttleNotificationTypeEnum() == .CHAT_MESSAGE_NOTIFICATION {
                if nil == self.transportations.value.first(where: { $0.id == notificationModel.transportationId}){
                    self.shuttleNotificationWrapper.value = notificationWrapper
//                    self.fetchJoinedTransportations() // calls to time need to do something in fetchJoinedTransportations
                }else{
                    if let chatTransportation = self.transportations.value.first(where: { $0.id == notificationModel.transportationId }){
                        if chatTransportation == self.selectedTransportation.value{
                            if self.chatVisible.value == false{
                                self.chatVisible.value = true
                            }
                        }else{
                            self.shuttleNotificationWrapper.value = notificationWrapper
                            self.doExpandedComboBoxSelected(transportation: chatTransportation)
    //                        self.chatVisible.value = true
                        }
                    }
                }
            }
        }
    }
    
    private func doAttendanceChange(){
        if let validTransportationId = self.selectedTransportation.value?.id {
            if self.isFirstPersonAttendaceLoadComplete {
                print("Will Save Attendance for transportation:\(validTransportationId)")
                self.savePersonAttendance()
            }
        }
    }
    
    private func fetchJoinedTransportations(){
        
        NetworkManager.fetch(url: UrlConstants.UrlGetJoinedTransportationList, parameters: Person(id: MyUser.getInstance().getUserId())
            , handle: {
                (newData : [Transportation.OptionalTransportation]) -> () in
                self.transportations.value = newData.map({Transportation(optionalTransportation: $0)})
                if self.transportations.value.capacity > 0{
//                    self.selectedTransportation.value = self.transportations.value[0]
                    self.transportationMarker.value = nil
                    
                    if let chatTransportation = self.transportations.value.first(where: { $0.id == self.shuttleNotificationWrapper.value?.shuttleNotificationModel.transportationId }){
                        self.selectedTransportation.value = chatTransportation
                        self.fetchPersonTransportationAttendance(transportation: chatTransportation, withRedisSubscribe: true)
                    }else{
                        self.selectedTransportation.value = self.transportations.value[0]
                        self.fetchPersonTransportationAttendance(transportation: self.transportations.value[0], withRedisSubscribe: true)
                    }
                }
            }
            , errorHandle: {
                newError in
                self.error?.value = newError})
        
    }
    
    private func doShareMyLocation(){
        
        if selectedTransportation.value == nil{
            self.activeViewAlert.value = .transportationNotSelected
            self.showAlert.value.toggle()
            return
        }
        
        //transportation already sharing, no need to check extra details
        if (SharingLocationTransportationManager.getInstance().isTransportationLocationSharing(transportation: self.selectedTransportation.value!)){
            self.isSharingTransportationLocation.value = SharingLocationTransportationManager.getInstance().toggleTransportationLocationSharing(transportation: self.selectedTransportation.value!)
            return
        }
        
        if !Utils.isTransportationActiveNow(transportation: selectedTransportation.value!){
            self.activeViewAlert.value = .transportationNotActive
            self.showAlert.value.toggle()
            return
        }
        
        let authorization = CLLocationManager().authorizationStatus
        print("authorization denied: \(authorization == .denied)")
        print("authorization authorizedWhenInUse: \(authorization == .authorizedWhenInUse)")
        print("authorization notDetermined: \(authorization == .notDetermined)")
        print("authorization authorizedAlways: \(authorization == .authorizedAlways)")
        print("authorization restricted: \(authorization == .restricted)")
        
        if authorization != .authorizedAlways && authorization != .authorizedWhenInUse{
            self.activeViewAlert.value = .locationAuthorizationNone
            self.showAlert.value.toggle()
            return
        }
        
        if authorization != .authorizedAlways{
            SingleLocationManagerBasic.getInstance().requestAlwaysLocationPermission()
            self.activeViewAlert.value = .locationAuthorizationNotAlways
            self.showAlert.value.toggle()
        }
        
        self.isSharingTransportationLocation.value = SharingLocationTransportationManager.getInstance().toggleTransportationLocationSharing(transportation: self.selectedTransportation.value!)
        
    }
    
    /*
     //you may want to divide this method later
     func fetchPersonTransportationAttendance(transportation : Transportation){
     
     NetworkManager.fetch(url: UrlConstants.UrlGetPersonTransportationAttendanceList, parameters: transportation
     , handle: {
     (newData : [PersonTransportationAttendance]) -> () in
     
     //this lock is temp solution need to find better solutions like link below
     // https://medium.com/swiftcairo/avoiding-race-conditions-in-swift-9ccef0ec0b26
     self.lock.lock()
     //cancel tasks before begin
     for (_ , uiImage) in self.imageDownloaders {
     uiImage.kf.cancelDownloadTask()
     }
     //remove image downladers
     self.imageDownloaders.removeAll();
     //clear markers
     self.mapView?.clear()
     
     self.personTransportationAttendances = newData
     //                return
     print("personTransportationAttendances:\(self.personTransportationAttendances)")
     let path = GMSMutablePath()
     
     if self.personTransportationAttendances.count > 0 {
     if let shuttleNextValidDate = self.personTransportationAttendances[0].shuttleNextValidDate {
     self.transportationNextValidDate = shuttleNextValidDate.toDate()
     }else{
     self.transportationNextValidDate = nil
     }
     }
     
     let personId = MyUser.getInstance().getUserId()
     
     for val in self.personTransportationAttendances {
     
     if val.personId == personId {
     self.isPersonAbsent.value = !val.personAttended
     }
     
     let marker : GMSMarker = GMSMarker()
     marker.position = CLLocationCoordinate2D(latitude: val.personLatitude!, longitude: val.personLongitude!)
     path.add(marker.position)
     
     marker.title = val.personName
     //                    marker.snippet = "Australia"
     let uiImage = UIImage(named: "user_placeholder")
     let uiImageView = UIImageView(image: uiImage)
     
     self.imageDownloaders[val.personId] = uiImageView
     
     self.fetchImage(imageView: uiImageView, marker: marker, personTransportationAttendance: val)
     
     }
     let newMapBounds = GMSCoordinateBounds(path: path)
     
     self.mapView?.animate(with: GMSCameraUpdate.fit(newMapBounds, withPadding: 50))
     
     self.isFirstPersonAttendaceLoadComplete = true
     self.lock.unlock()
     }
     , errorHandle: {
     newError in
     self.error?.value = newError})
     }
     */
    
    private func fetchPersonTransportationAttendance(transportation : Transportation, withRedisSubscribe : Bool){
        NetworkManager.fetch(url: UrlConstants.UrlGetPersonTransportationAttendanceList, parameters: transportation
            , handle: {
                (newData : [PersonTransportationAttendance]) -> () in
                self.loadMarkersWithPersonTransportationAttendances(newData: newData)
                if withRedisSubscribe {
                    self.streamChatView.value = StreamChatView(transportation: transportation, senderId: MyUser.getInstance().getUserId(), senderName: MyUser.getInstance().name, isPresented: self.chatVisible)
                    
                    if let transportationId = transportation.id{
                        if let transportationSelected = self.selectedTransportation.value{
                            self.isSharingTransportationLocation.value = SharingLocationTransportationManager.getInstance().isTransportationLocationSharing(transportation: transportationSelected)
                        }
                        
                        self.redisAttendanceProcess.value = RedisAttendanceProcess(channel: RedisConstants.getRedisAttendanceChannelId(transportationId: transportationId), redisCallback: AttendanceRedisCallback(parent: self, transportation: transportation))
                        self.redisAttendanceProcess.value?.initialize()
                        //this part for optimization, right now no need optimization
                        if Utils.isTransportationActiveNow(transportation: transportation) || true {
                            self.redisGpsCommand.value?.getValue(key: RedisConstants.getRedisLocationChannelId(transportationId: transportationId), redisCallback: GpsRedisCallback(parent: self, transportation: transportation))
                            self.redisGpsSubscriber.value = RedisGpsSubscriber(channel: RedisConstants.getRedisLocationChannelId(transportationId: transportationId), redisCallback: GpsRedisCallback(parent: self, transportation: transportation))
                            self.redisGpsSubscriber.value?.initialize()
                        }else{
                            self.redisGpsSubscriber.value = nil
                        }
                    }
                    
                    let shuttleNotificationWrapper = self.shuttleNotificationWrapper.value
                    print("shuttleNotificationWrapper: \(String(describing: shuttleNotificationWrapper))")
                    print(Utils.getCurrentMillis() - (self.shuttleNotificationWrapper.value?.notificationPressedTime ?? 0))
                    if Utils.getCurrentMillis() - (self.shuttleNotificationWrapper.value?.notificationPressedTime ?? 0) < 100000{
                        self.chatVisible.value = true
                        self.shuttleNotificationWrapper.value = nil
                    }
                }
            }
            , errorHandle: {
                newError in
                self.error?.value = newError})
    }
    
    private func loadMarkersWithPersonTransportationAttendances(newData : [PersonTransportationAttendance]){
        print("start loadMarkersWithPersonTransportationAttendances :\(newData)")
        //this lock is temp solution need to find better solutions like link below
        // https://medium.com/swiftcairo/avoiding-race-conditions-in-swift-9ccef0ec0b26
        self.lock.lock()
        defer{self.lock.unlock()}
        self.isFirstPersonAttendaceLoadComplete = false
        //cancel tasks before begin
        for (_ , uiImage) in self.imageDownloaders {
            uiImage.kf.cancelDownloadTask()
        }
        //remove image downladers
        self.imageDownloaders.removeAll();
        //clear marlers
        self.mapView.value?.clear()
        
        self.personTransportationAttendances.value = newData
        print("personTransportationAttendances:\(self.personTransportationAttendances)")
        let path = GMSMutablePath()
        
        if self.personTransportationAttendances.value.count > 0 {
            if let shuttleNextValidDate = self.personTransportationAttendances.value[0].shuttleNextValidDate {
                self.transportationNextValidDate.value = shuttleNextValidDate.toDate()
            }else{
                self.transportationNextValidDate.value = nil
            }
        }
        
        let personId = MyUser.getInstance().getUserId()
        
        var personSet : Set<String> = []
        
        for val in self.personTransportationAttendances.value {
            if !personSet.contains(val.personId){
                personSet.insert(val.personId)
                
                if val.personId == personId {
                    self.isPersonAbsent.value = !val.personAttended
                }
                
                let marker : GMSMarker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: val.personLatitude!, longitude: val.personLongitude!)
                path.add(marker.position)
                
                marker.title = val.personName
                //                    marker.snippet = "Australia"
                let uiImage = UIImage(named: "user_placeholder_filled")
                let uiImageView = UIImageView(image: uiImage)
                
                self.imageDownloaders[val.personId] = uiImageView
                self.fetchImage(imageView: uiImageView, marker: marker, personTransportationAttendance: val)
                
            }else{
                print("There are multiple personTransportationAttendances for personId:\(val.personId)")
            }
            
        }
        //need to show transportation to
        if(transportationMarker.value != nil){
            path.add(transportationMarker.value!.position)
        }else{
            print("transportationMarker is nil")
        }
        
        let newMapBounds = GMSCoordinateBounds(path: path)
        
        self.mapView.value?.animate(with: GMSCameraUpdate.fit(newMapBounds, withPadding: 50))
        
        self.isFirstPersonAttendaceLoadComplete = true
        print("end loadMarkersWithPersonTransportationAttendances :\(newData)")
    }
    
    private func fetchImage(imageView : UIImageView, marker : GMSMarker, personTransportationAttendance: PersonTransportationAttendance){
//        let urlString = "http://46.101.106.121/test/img/main/\(personTransportationAttendance.personId).jpg"
        let urlString = personTransportationAttendance.profilePictureURL ?? ""
        let url = URL(string: urlString)
        //        let url = URL(string: "http://46.101.106.121/test/img/main/bQm1w3Uqithl8EJ3DEFP7CvBvI73.jpg")
        //    let url = URL(string: "https://example.com/high_resolution_image.png")
        //        url = URL(string: "https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg")
        //        url = URL(string: "https://upload.wikimedia.org/wikipedia/commons/f/ff/Pizigani_1367_Chart_10MB.jpg")
        
        //        let processor = DownsamplingImageProcessor(size: imageView.bounds.size) |> RoundCornerImageProcessor(cornerRadius: 0)
        //        let processor = DownsamplingImageProcessor(size: imageView.bounds.size).append(another: RoundCornerImageProcessor(cornerRadius: 0))
        let processor = RoundCornerImageProcessor(cornerRadius: 0)
        let placeholder = imageView.image
        
        //        imageDownloaders[personId]
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(
            with: url,
            placeholder: placeholder,
            options: [
                .processor(processor),
                //                .scaleFactor(10),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ], completionHandler:
                {
                    result in
                    switch result {
                    case .success(let value):
                        print("Task done for: \(value.source.url?.absoluteString ?? "")")
                        let attendanceUIColor : UIColor = (personTransportationAttendance.personAttended ? UIColor.colorGreen : UIColor.gray)!
                        marker.icon = createMarkerImage(uiImage: imageView.image!, uiColor: attendanceUIColor)
                        marker.map = self.mapView.value
                    case .failure(let error):
                        print("Job failed: \(error.localizedDescription)")
                        let attendanceUIColor : UIColor = (personTransportationAttendance.personAttended ? UIColor.colorGreen : UIColor.gray)!
                        if let validImage = placeholder{
                            let updatedImage = validImage
                            //                    let updatedImage = imageWithBackground(uiImage: validImage, color: UIColor.colorWhite!)
                            //                    let updatedImage = imageWithColor(uiImage: validImage, tintColor: UIColor.colorWhite!)
                            //                    let updatedImage = getResizedUIImage(image: validImage, newWidth: CGFloat(validImage.size.width * 0.5))
                            //                    let padding :CGFloat = 100
                            //                    updatedImage = updatedImage.withAlignmentRectInsets(UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding))
                            //                    updatedImage = updatedImage.resizableImage(withCapInsets: UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding), resizingMode: .stretch)
                            marker.icon = createMarkerImage(uiImage: updatedImage, uiColor: attendanceUIColor)
                            marker.map = self.mapView.value
                        }
                    }
                })
    }
    
    //this method only show location update
    func updateTransportationMarker(gpsMessage : GpsMessage){
//        return
        if let selectedTransportationId = self.selectedTransportation.value?.id{
            if selectedTransportationId == gpsMessage.transportationId {
//                let path = GMSMutablePath()
                
                var marker : GMSMarker = GMSMarker()
                if self.transportationMarker.value != nil{
                    self.transportationMarker.value!.map = nil
                    marker = self.transportationMarker.value!
                }
//                let marker : GMSMarker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: gpsMessage.latitude, longitude: gpsMessage.longitude)
                marker.rotation = gpsMessage.course < 0 ? 90 : gpsMessage.course
                marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                marker.map = self.mapView.value
                marker.zIndex = 1
//                marker.tracksInfoWindowChanges = true
//                let doubleDate = Double(gpsMessage.time/1000)
//                let gpsDate = Date(timeIntervalSince1970: doubleDate)
                let gpsDate = Utils.getHourMinuteSecondsFromMilliseconds(timeIntervalSince1970: gpsMessage.time)
                let uiImage = getResizedUIImage(image: UIImage(named: "shuttle_1")!, newWidth: 40)
//                let uiImage = UIImage(named: "shuttle_updated")
//                let uiImage = createMarkerImage(uiImage: UIImage(named: "shuttle_updated")!, uiColor: .black)
//                UIImage(named: "shuttle_updated")
                marker.icon = uiImage
//                marker.
                marker.title = "\(gpsMessage.senderName) \(gpsDate)"
//                path.add(marker.position)
                self.transportationMarker.value = marker
//                let newMapBounds = GMSCoordinateBounds(path: path)
//                self.mapView?.animate(with: GMSCameraUpdate.fit(newMapBounds, withPadding: 205000))
            }else{
                print("Selected TransportationId:\(selectedTransportationId) is different than gpsMessage:\(gpsMessage)")
            }
        }
        
    }
    
    private func savePersonAttendance(){
        
        if nil == self.transportationNextValidDate.value || self.selectedTransportation.value?.id == nil {
            print ("self.transportationNextValidDate is nil so will not save attendance")
            return
        }
        
        let nextValidDate = Utils.toLDate(date: self.transportationNextValidDate.value!)
        
        let attendance : Attendance = Attendance(personId: MyUser.getInstance().getUserId(), transportationId: self.selectedTransportation.value!.id!,
                                                 attended: !self.isPersonAbsent.value, startDate: nextValidDate, endDate: nextValidDate)
        
        NetworkManager.fetch(url: UrlConstants.UrlSaveAttendance, parameters: attendance
            , handle: {
                (newData : Attendance ) -> () in
                print("Created Attendance : \(newData)")
                print("save Attendance successfully")
                self.redisAttendanceProcess.value?.publish(message: "updated")
                //                self.createdTransportation = newData
                //                print("Created Transporatation: \(self.createdTransportation!)")
        }
            , errorHandle: {
                newError -> () in
                print("Create Attendace Error: \(newError)")
                self.error!.value = newError})
    }
    
    private func doComboBoxTapped(){
        self.comboBoxExpanded.value.toggle()
    }
    
    private func doExpandedComboBoxSelected(transportation : Transportation){
        self.selectedTransportation.value = transportation
        self.transportationMarker.value = nil
//        self.transportationIdWhenAbsentTabbed = transportation.id ?? ActiveView.TRANPORTATION_ID_INVALID
        if self.comboBoxExpanded.value == true{
            self.comboBoxExpanded.value = false
        }
//        self.comboBoxExpanded.toggle()
        self.fetchPersonTransportationAttendance(transportation: transportation, withRedisSubscribe: true)
    }
    
    var body: some View {
        VStack{
        GeometryReader { geometry in
            ZStack{
                VStack{
                    ZStack{
                        MapOfActiveView(realMap: self.mapView, realMapOfActiveViewGoogleMapController: self.$mapOfActiveViewGoogleMapController)
                            .edgesIgnoringSafeArea(.top)
                            .padding(.bottom, -8)
                        
                        VStack(alignment: .trailing){
                            Spacer()
                            HStack(alignment: .bottom){
                                Button(
                                    action: {
                                        print("location bıtton clicked")
                                        self.mapOfActiveViewGoogleMapController?.didTapOnMyLocation(nil)
                                    }){
                                    ZStack{
                                        Circle().frame(width: 60, height: 60, alignment: .center)
                                            .foregroundColor(.white)
                                        Image("current_location")
                                            .renderingMode(.template)
                                            .resizable()
                                            .aspectRatio(contentMode: .fit)
                                            .frame(width: 35.0, height: 35.0)
                                            .foregroundColor(Color.colorSecondary)
                                    }
                                }
                                
                                Spacer()
                                Button(
                                    action: {
                                        print("bıtton clicked")
                                        self.doShareMyLocation()
                                        
                                    }){
                                    ZStack{
                                        Capsule().frame(width: 165, height: 50, alignment: .center)
                                            .foregroundColor(self.isSharingTransportationLocation.value == true ? .green : .white)
                                        Text("ActiveView.ShareMyLocation.Button.text")
                                            .foregroundColor(.black)
                                            .bold()
                                    }
                                    .padding(.bottom, 5)
                                }
                                
                                Spacer()
                                Button(
                                    action: {
                                        if self.streamChatView.value != nil {
                                            self.chatVisible.value = true
                                        }
                                    }) {
                                    ZStack{
                                        Circle().frame(width: 60, height: 60, alignment: .center)
                                            .foregroundColor(.white)
                                        Image("chat")
                                            .renderingMode(.template)
                                            .resizable()
                                            .aspectRatio(contentMode: .fit)
                                            .frame(width: 35.0, height: 35.0)
                                            .foregroundColor(Color.colorSecondary)
                                    }
                                }
                            }
                        }
                        .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
                        
                        
                    }
                    .frame(width: geometry.size.width, height: (geometry.size.height - 170) < 0 ? 0 : geometry.size.height - 170)
                    Divider().background(Color.black)
                    VStack{
                        Spacer()
                        Button(action: {self.doComboBoxTapped()}){
                            HStack{
                                Text(self.selectedTransportation.value?.name ?? "")
                                    .fontWeight(.bold)
                                    .foregroundColor(Color.black)
                                    .padding(.leading, 10)
                                Spacer()
                                Image(systemName: "chevron.down")
                                    .renderingMode(.template)
                                    .resizable()
                                    .frame(width: 12, height: 6)
                                    .foregroundColor(Color.black)
                                    .padding(.trailing, 10)
                                
                            }
                            .frame(maxWidth: geometry.size.width , minHeight: 50, idealHeight: 50, maxHeight: 50)
                        }
                        .background(Color.colorVeryLightGray)
                        .buttonStyle(PlainButtonStyle())
                        .border(Color.gray, width: 1)
                        .onTapGesture {
                            self.doComboBoxTapped()
                        }
                            //                .padding(.bottom, 15)
                            //                .padding(.top, 10)
                            .frame(maxWidth: geometry.size.width , minHeight: 70, idealHeight: 70, maxHeight: 70)
                        Spacer()
//                        Toggle(isOn: .constant(false), label: {Text("ActiveView.ShareMyLocationAsShuttleLocation.text")})
//                        //                .padding(.bottom, 15)
//                        Spacer()
                        Toggle(isOn: self.$isPersonAbsent.value)
                        {
                            HStack{
                                Text("ActiveView.WillNotUseShuttle.text")
                                if self.transportationNextValidDate.value != nil {
                                    Text("(\(getMonthDateDayFormat().string(from: self.transportationNextValidDate.value!)))")
                                }
                            }
                        }
                            .toggleStyle(SwitchToggleStyle(tint: Color.colorSecondary))
//                        .onTapGesture {
//                            self.transportationIdWhenAbsentTabbed = self.selectedTransportation.id ?? ActiveView.TRANPORTATION_ID_INVALID
//                        }
                        //                .padding(.bottom, 15)
                        Spacer()
                        Toggle(isOn: .constant(false), label: { Text("ActiveView.WillBeALittleLate.text") })
                            .toggleStyle(SwitchToggleStyle(tint: Color.colorSecondary))
                        //                    .padding(.bottom, 15)
                        Spacer()
                        Spacer()
                    }
                    .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
                    .frame(width: geometry.size.width, height: 170, alignment: .leading)
                    
                    
                }}
            if self.comboBoxExpanded.value {
                VStack{
                    Spacer()
                    ScrollView{
                        ForEach(self.transportations.value) { transportation in
                            Button(action: {self.doExpandedComboBoxSelected(transportation: transportation)}){
                                HStack{
                                    Text(transportation.name)
                                        //                                       .fontWeight(.bold)
                                        .foregroundColor(Color.black)
                                        .padding(.leading, 10)
                                    Spacer()
                                }
                                .frame(maxWidth: .infinity, minHeight: 50, idealHeight: 50)
                            }
                            .background(Color.colorVeryLightGray)
                            .buttonStyle(PlainButtonStyle())
                            .border(Color.gray, width: 1)
                            .onTapGesture {self.doExpandedComboBoxSelected(transportation: transportation)}
                        }
                    }
                        .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
                        .frame(width: geometry.size.width, height: geometry.size.height * 3 / 4, alignment: .center)
                    Spacer()
                }
            }
        }
        .keyboardViewNoMove()
        .keyboardIgnoreSafeArea()
        }//Extra VStack added to prevent white screen when using keyboardViewNoMove and keyboardIgnoreSafeArea
        .onTapGesture {
            if self.comboBoxExpanded.value {
                self.comboBoxExpanded.value.toggle()
            }
        }
        .sheet(isPresented: self.$chatVisible.value){//,
//               onDismiss: {
//                self.hideKeyboard()
//                self.chatVisible = false
//               }){
            //                StreamChatView()
            self.streamChatView.value
        }
        .onAppear(){
            self.mapView.parent = self
            self.isPersonAbsent.parent = self
            self.redisGpsCommand.value = RedisGpsCommand()
            self.redisGpsCommand.value?.initialize()
            //after mapView successfully init call fetch
            //06.07.2021 mapview does not nil after change ContentView selection so directly will call everytime fetchJoinedTransportations
            if self.mapView.value != nil{
                self.fetchJoinedTransportations()
            }
            //            self.fetchPersonTransportationAttendance(transportation: self.transportations[0])
        }
        .onDisappear(){
            if self.comboBoxExpanded.value {
                self.comboBoxExpanded.value.toggle()
            }
        }
        .alert(isPresented: self.$showAlert.value) {
            if self.activeViewAlert.value == .transportationNotSelected{
                return Alert(title: Text(""),
                             message: Text(toStringFromLocalizedStringKey(localizedStringKey: "ActiveView.TransportationNotSelectedAlert.text")),dismissButton: .default(Text("ActiveView.TransportationNotSelectedAlert.Button.text")){
                            })

            }else if self.activeViewAlert.value == .transportationNotActive{
                return Alert(title: Text(""),
                             message: Text(toStringFromLocalizedStringKey(localizedStringKey: "ActiveView.TransportationNotActiveAlert.text")),dismissButton: .default(Text("ActiveView.TransportationNotActiveAlert.Button.text")){
                            })
            }else if self.activeViewAlert.value == .locationAuthorizationNone{
                return Alert(title: Text(""),
                             message: Text(toStringFromLocalizedStringKey(localizedStringKey: "ActiveView.LocationAuthorizationNone.text")),
                             primaryButton: .default(Text("ActiveView.LocationAuthorizationNone.CancelButton.text")),
                             secondaryButton: .default(Text("ActiveView.LocationAuthorizationNone.SettingsButton.text"),
                                                       action: {UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)}))
            }else if self.activeViewAlert.value == .locationAuthorizationNotAlways{
                return Alert(title: Text(""),
                             message: Text(toStringFromLocalizedStringKey(localizedStringKey: "ActiveView.LocationAuthorizationNotAlways.text")),
                             primaryButton: .default(Text("ActiveView.LocationAuthorizationNotAlways.CancelButton.text")),
                             secondaryButton: .default(Text("ActiveView.LocationAuthorizationNotAlways.SettingsButton.text"),
                                                       action: {UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)}))
            }
            else{
                return Alert(title: Text(""), message: Text("There is unexpected error"), dismissButton: .default(Text("OK")))
            }
        }
        .disabled(self.transportations.value.count == 0)
    }
}

struct ActiveView_Previews: PreviewProvider {
    static var previews: some View {
        ActiveView()
    }
}
