//
//  ParticipantDetailedView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/31/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct ParticipantDetailedView: View {
    
    @ObservedObject var personTransportationView : GenericObservableModel<PersonTransportationView>
    
    var transportation: Transportation
    
    @State private var personTime: Date = Date()
    
    @State private var personTimeInvalid : Bool = false
    
    @State private var error : Error? = nil
    
//    init(personTransportationView : GenericObservableModel<PersonTransportationView>, transportation : Transportation){
//        self.personTransportationView = personTransportationView
//        self.transportation = transportation
//    }
    
    var body: some View {
        
        VStack {
            VStack {
                Form {
                    HStack{
                        Text("ParticipantDetailedView.Participant.text")
                        TextField("", text: self.$personTransportationView.value.personName)
                            .multilineTextAlignment(.trailing)
                            .disabled(true)
                    }
                    
                    HStack{
                        if TransportationTypeEnum.toTransportationTypeEnum(transportation.transportationType) == TransportationTypeEnum.DEPARTURE {
                            Text("ParticipantDetailedView.GetOnPoint.text")
                        } else {
                            Text("ParticipantDetailedView.GetOffPoint.text")
                        }
                        
                        Spacer()
                        
                        Image("map")
                            .renderingMode(.template)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 30, height: 30)
                            .padding(5)
                            .foregroundColor(Color.colorPrimary)
                            .onTapGesture {
                                print("map pressed")
                            }
                    }
                    
                    DatePicker(selection: .constant(self.personTransportationView.value.transportationStartTime.toDate()), displayedComponents: .hourAndMinute) { Text("ParticipantDetailedView.DepartureTime.text") }
                        .disabled(true)
                    
                    DatePicker(selection: $personTime, displayedComponents: .hourAndMinute) {
                        TransportationTypeEnum.toTransportationTypeEnum(transportation.transportationType) == TransportationTypeEnum.DEPARTURE ?
                                Text("ParticipantDetailedView.GetOnTime.text") : Text("ParticipantDetailedView.GetOffTime.text") }
                        .foregroundColor(Color.black)
                }
                
                Button(action: {
                        print("save clicked")
                        if Utils.isValidTimeDiffBeetweenDepartureAndPersonGetOntime(transportationTime: self.personTransportationView.value.transportationStartTime,
                                                                                    personGetOnTime: Utils.toLTime(time: self.personTime)) {
                            self.savePersonTransportation()
                        } else {
                            self.personTimeInvalid = true
                        }
                    }, label: {
                        Text("CreateTransportationView.Save.text")
                    })
    //                .disabled(!isValid())
                    .padding(.bottom, 15)
                    
            }
                .navigationBarTitle("ParticipantDetailedView.Navigation.Title.text", displayMode: .inline)
        }
        .onAppear(){
            print("added LTime:\(Utils.addSecondsToLTime(lTime: self.personTransportationView.value.transportationStartTime, addSeconds: self.personTransportationView.value.dailyTimeDiffInSecs))")
            self.personTime = Utils.addSecondsToLTime(lTime: self.personTransportationView.value.transportationStartTime,
                                                      addSeconds: self.personTransportationView.value.dailyTimeDiffInSecs).toDate()
            print("self.personTransportationView.value.transportationStartTime:\(self.personTransportationView.value.transportationStartTime)")
            print("self.personTransportationView.value.dailyTimeDiffInSecs:\(self.personTransportationView.value.dailyTimeDiffInSecs)")
            print("init personTime : \(self.personTime)")
        }
        .alert(isPresented: $personTimeInvalid) {
            Alert(title: Text(""), message: Text("ParticipantDetailedView.InvalidPersonStartTime.text"), dismissButton: .default(Text("ParticipantDetailedView.InvalidPersonStartTime.Ok.text"))) }
    
        
    }
    
    private func savePersonTransportation(){
//        let dailyTimeDiffInSecs = getTimeDiff()
        let dailyTimeDiffInSecs = Utils.lTimesDiffInSeconds(lTime1: self.personTransportationView.value.transportationStartTime, lTime2: Utils.toLTime(time: self.personTime))
        
        let personTransportation : PersonTransportation = PersonTransportation(personId: self.personTransportationView.value.personId, transportationId: self.personTransportationView.value.transportationId, personRoleType: self.personTransportationView.value.personRoleType, latitude: self.personTransportationView.value.latitude, longitude: self.personTransportationView.value.longitude, dailyTimeDiffInSecs: dailyTimeDiffInSecs )

        NetworkManager.fetch(url: UrlConstants.UrlSavePersonTransportation, parameters: personTransportation
            , handle: {
                (newData : PersonTransportation ) -> () in
                
                print("saved dailyTimeDiffInSecs:\(newData.dailyTimeDiffInSecs)")
                self.personTime = Utils.addSecondsToLTime(lTime: self.personTransportationView.value.transportationStartTime,
                                                          addSeconds: newData.dailyTimeDiffInSecs).toDate()
                print("saved personTime:\(self.personTime)")
                print("Saved PersonTransportation")
//                TODO: you may want to fetch data to set it back
             }
            , errorHandle: {
                newError -> () in
                print("Saved PersonTransportation Error: \(newError)")
                self.error = newError})
    }
    
//    private func getTimeDiff() -> Int {
//        let tTime : LTime = self.personTransportationView.value.transportationStartTime
//        print(self.personTime)
//        let pTime : LTime = Utils.toLTime(time: self.personTime)
//
//        let timeDiff = Utils.lTimesDiffInSeconds(lTime1: tTime, lTime2: pTime)
//        print("timeDiff: \(timeDiff)")
//        return timeDiff
//    }
//
//    private func isValid() -> Bool {
////        if transportationName.isEmpty {
////            return false
////        }
//
//        let maxTimeDiffInSec = 60 * 60 * 2
//        let timeDiff = getTimeDiff()
//        if timeDiff > maxTimeDiffInSec {
//            return false
//        }
//
//        return true
//    }
}

struct ParticipantDetailedView_Previews: PreviewProvider {
    static var previews: some View {
        ParticipantDetailedView(personTransportationView: GenericObservableModel(value: PersonTransportationView()), transportation: Transportation())
    }
}
