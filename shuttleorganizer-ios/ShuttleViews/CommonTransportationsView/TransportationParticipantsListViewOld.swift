//
//  TransportationParticipantsListViewOld.swift
//  shuttleorganizer-ios
//
//  Created by macos on 29.05.2021.
//  Copyright © 2021 Orion. All rights reserved.
//
import SwiftUI

struct TransportationParticipantsListViewOld: View {
    @ObservedObject private var personTransportationViewList : GenericObservableModel<[PersonTransportationView]> = GenericObservableModel(value: [])
    @ObservedObject var removingPersonTransportationView : GenericObservableModel<PersonTransportationView>
    @ObservedObject var updatePersonTransportationViewDate : GenericObservableModel<Date> = GenericObservableModel(value: Date())
    
    init(transportation: Transportation){
        self.transportation = transportation
        self.removingPersonTransportationView = GenericObservableModel(value: PersonTransportationView(), callBackWithParent: { (ptv: PersonTransportationView, parent : Any) in
                print("removingPersonTransportationView is changed")
//                print("ptv:\(ptv)")
//                if let validParent = parent as? TransportationParticipantsListView{
////                    validParent.removePersonTransportation(ptv: ptv)
//                    print("will remove person")
//                }
            })
        self.removingPersonTransportationView.parent = self
    }
    
//    init(transportation: Transportation, personTransportationViewList : [PersonTransportationView]){
//        self.transportation = transportation
//        self.removingPersonTransportationView = GenericObservableModel(value: PersonTransportationView(), callBackWithParent: { (ptv: PersonTransportationView, parent : Any) in
//                print("removingPersonTransportationView is changed")
////                print("ptv:\(ptv)")
////                if let validParent = parent as? TransportationParticipantsListView{
//////                    validParent.removePersonTransportation(ptv: ptv)
////                    print("will remove person")
////                }
//            })
//        self.personTransportationViewList.value = personTransportationViewList
//        self.removingPersonTransportationView.parent = self
//    }
    
    var error : GenericObservableModel<Error>? = nil
    var transportation : Transportation
//    @EnvironmentObject var createdTransportationsList: CreatedTransportationsListObservable
    
    
//    ForEach(createdTransportationsList.transportations) { transportation in
//        CreatedTransportationRow(transportation: transportation, toastObservableModel: self.isShowToastCopied, sharedTransportationId: self.sharedTransportationId)
//    }
    
//    personTransportationView.transportationStartTime.add
    
//    func isDeparture() -> Bool {
//        return createdTransportationsList.getTransportation(id: personTransportationViewList[0].transportationId).transportationType == TransportationTypeEnum.DEPARTURE.rawValue
//    }
//    func echo(){
//        print("echo")
//    }
    
    func isDeparture() -> Bool {
        return transportation.transportationType == TransportationTypeEnum.DEPARTURE.rawValue
    }
    func fetchData(){
        print("TransportationParticipantsListView.self = \(TransportationParticipantsListView.self)")
        
        NetworkManager.fetch(url: UrlConstants.UrlGetPersonTransportationList, parameters: transportation
            , handle: {
                newData in
                self.personTransportationViewList.value = newData}
            , errorHandle: {
                newError in
                self.error?.value = newError})
        
    }
    public func removePersonTransportation(ptv : PersonTransportationView){
        let personTransportation : PersonTransportation = PersonTransportation(personId : ptv.personId, transportationId : ptv.transportationId, personRoleType : ptv.personRoleType)
        print("remove Transportation called for \(ptv)")
        NetworkManager.fetch(url: UrlConstants.UrlDeletePersonTransportation, parameters: personTransportation
            , handle: {
                (newData : PersonTransportation) in
                    let filteredList = self.personTransportationViewList.value.filter{ pt in
                        pt.personId != personTransportation.personId || pt.transportationId != personTransportation.transportationId
                    }
                    self.personTransportationViewList.value = filteredList
                    print("remove personTransportation is successful")
                }
            , errorHandle: {
                newError in
                self.error?.value = newError
                print("can not remove Person \(newError)")
                })
    }
    
    
    var body: some View {
        GeometryReader { geometry in
            List {
                HStack{
                    Text("TransportationParticipantsListView.Participant.text")
                        .frame(width: geometry.size.width * 0.31, alignment: .leading)
                        .padding(.leading, 0)
                    Text(self.isDeparture() ? "TransportationParticipantsListView.GetOnTime.text" : "TransportationParticipantsListView.GetOffTime.text")
                        .frame(width: geometry.size.width * 0.22, alignment: .leading)
    //                    .padding(.leading, 5)
                    Text(self.isDeparture() ? "TransportationParticipantsListView.GetOnPoint.text" : "TransportationParticipantsListView.GetOffPoint.text")
                        .frame(width: geometry.size.width * 0.22, alignment: .leading)
    //                    .padding(.leading, 5)
                    Spacer()
                        .frame(width: geometry.size.width * 0.15, alignment: .leading)
    //                    .padding(.leading, 5)
                }
                
                ForEach(self.personTransportationViewList.value , id: \.self) { personTransportationView in
                    NavigationLink(
                        destination: ParticipantDetailedView(personTransportationView: GenericObservableModel(value: personTransportationView),
                                                             transportation: self.transportation)
                    ) {
                        ParticipantRow(personTransportationView: personTransportationView,
                                       personTime: GenericObservableModel(value: Utils.addSecondsToLTime(lTime: personTransportationView.transportationStartTime, addSeconds: personTransportationView.dailyTimeDiffInSecs).toDate()),
                                       removingPersonTransportationView: self.removingPersonTransportationView,
                                       updatePersonTransportationViewDate: self.updatePersonTransportationViewDate)
                    }
                    
                }
            }
        }
        .onAppear(perform: {
            self.fetchData()
            })
        
        .alert(isPresented: $removingPersonTransportationView.isValueChanged) {
            Alert(title: Text(""), message: Text("TransportationParticipantsListView.DeleteShuttlePersonWarning.text"),
                  primaryButton: .destructive(Text("TransportationParticipantsListView.DeleteShuttlePersonWarning.Delete.text"),
                    action: {
//                        print("Ok Button Clicked")
                        print("will remove person")
                        self.removePersonTransportation(ptv: self.removingPersonTransportationView.value)
                        }),
                secondaryButton: .cancel({print("cancel Button clicked")}))
            }
        
//        Alert(title: Text("Important message"), message: Text("Wear sunscreen"),
//        dismissButton: .cancel({_ in print("cancel Button clicled")}),
//        secondaryButton: .default(Text("Ok"), action: {_ in print("Ok Button Clicked")}))
    }
}

struct TransportationParticipantsListViewOld_Previews: PreviewProvider {
    static var list = [PersonTransportationView(personId: "r1234", transportationId: "t123", personRoleType: 0, latitude: 1.2, longitude: 1.3, transportationStartTime: LTime(hour: 5, minute: 15, second: 20), dailyTimeDiffInSecs: 2700, personName: "susususu ththt dfdf", profilePictureURL: ""),
    PersonTransportationView(personId: "r1234", transportationId: "t123", personRoleType: 0, latitude: 1.2, longitude: 1.3, transportationStartTime: LTime(hour: 5, minute: 15, second: 20), dailyTimeDiffInSecs: 2700, personName: "susususu", profilePictureURL: "")]
    static let tt = Transportation(id: "1NAiXUBX2K", name: "", latitude: 1, longitude: 1, transportationType: 0, days: 2, status: 0, creatorPersonId: "", startTime: LTime(), timeZone: 3)
    static let tt1 = Transportation(id: "0lizu3q2S6", name: "", latitude: 1, longitude: 1, transportationType: 0, days: 2, status: 0, creatorPersonId: "", startTime: LTime(), timeZone: 3)
    static let tt2 = Transportation(id: "vh5vHYd23n", name: "", latitude: 1, longitude: 1, transportationType: 0, days: 2, status: 0, creatorPersonId: "", startTime: LTime(), timeZone: 3)
    static var previews: some View {
//        TransportationParticipantsListView(transportation: tt, personTransportationViewList : list)
//        TransportationParticipantsListView(personTransportationViewList: list).environmentObject(CreatedTransportationsListObservable())
        TransportationParticipantsListView(transportation: tt2)
    }
}
