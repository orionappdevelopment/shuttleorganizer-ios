//
//  TransportationParticipantsView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/30/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI
import UIKit
import Foundation
import CoreLocation
import GoogleMaps

struct TransportationParticipantsAdvancedListView: View {
    @ObservedObject private var personTransportationViewList : GenericObservableModel<[PersonTransportationView]> = GenericObservableModel(value: [])
    @ObservedObject var personTransportationView : GenericObservableModel<PersonTransportationView> = GenericObservableModel(value: PersonTransportationView())
    @State var childRow : Any? =  nil
    @ObservedObject var  personTime = GenericObservableModel(value: Date())
    @ObservedObject var selectedCoordinate = GenericObservableModel(value: CLLocationCoordinate2D(latitude: 41.0370, longitude: 28.9851))
    @ObservedObject var isTimeViewPresented = GenericObservableModel(value: false)
    @ObservedObject var sheetIsPresented = GenericObservableModel(value: false)
    @ObservedObject var showAlert = GenericObservableModel(value: false)
    
    @State var error : Error? = nil
    @State var transportation : Transportation
    
//    init(transportation: Transportation){
//        transportation = transportation
//        self.removingPersonTransportationView = GenericObservableModel(value: PersonTransportationView(), callBackWithParent: { (ptv: PersonTransportationView, parent : Any) in
//                print("removingPersonTransportationView is changed")
//                print("ptv:\(ptv)")
//                if let validParent = parent as? TransportationParticipantsListView{
////                    validParent.removePersonTransportation(ptv: ptv)
//                    print("will remove person")
//                }
//            })
//        self.removingPersonTransportationView.parent = self
//    }
    
    func isDeparture() -> Bool {
        return transportation.transportationType == TransportationTypeEnum.DEPARTURE.rawValue
    }
    func fetchData(){
        print("TransportationParticipantsListView.self = \(TransportationParticipantsListView.self)")
        
        NetworkManager.fetch(url: UrlConstants.UrlGetPersonTransportationList, parameters: transportation
            , handle: {
                newData in
                self.personTransportationViewList.value = newData}
            , errorHandle: {
                newError in
                self.error = newError})
        
    }
    private func removePersonTransportation(ptv : PersonTransportationView){
        let personTransportation : PersonTransportation = PersonTransportation(personId : ptv.personId, transportationId : ptv.transportationId, personRoleType : ptv.personRoleType)
        print("remove Transportation called for \(ptv)")
        NetworkManager.fetch(url: UrlConstants.UrlDeletePersonTransportation, parameters: personTransportation
            , handle: {
                (newData : PersonTransportation) in
                    let filteredList = self.personTransportationViewList.value.filter{ pt in
                        pt.personId != personTransportation.personId || pt.transportationId != personTransportation.transportationId
                    }
                    self.personTransportationViewList.value = filteredList
                    print("remove personTransportation is successful")
                }
            , errorHandle: {
                newError in
                self.error = newError
                print("can not remove Person \(newError)")
                })
    }
    
    private func savePersonTransportation(){
        let dailyTimeDiffInSecs = Utils.lTimesDiffInSeconds(lTime1: self.transportation.startTime, lTime2: Utils.toLTime(time: self.personTime.value))
        
        let personTransportationView = self.personTransportationView.value
        
        let personTransportation : PersonTransportation = PersonTransportation(personId: personTransportationView.personId, transportationId: personTransportationView.transportationId, personRoleType: personTransportationView.personRoleType, latitude: self.selectedCoordinate.value.latitude, longitude: self.selectedCoordinate.value.longitude, dailyTimeDiffInSecs: dailyTimeDiffInSecs )

        NetworkManager.fetch(url: UrlConstants.UrlSavePersonTransportation, parameters: personTransportation
            , handle: {
                (newData : PersonTransportation ) -> () in
                
                print("saved dailyTimeDiffInSecs:\(newData.dailyTimeDiffInSecs)")
                self.personTime.value = Utils.addSecondsToLTime(lTime: self.transportation.startTime,
                                                          addSeconds: newData.dailyTimeDiffInSecs).toDate()
                var ptv = PersonTransportationView(personTransportationView: self.personTransportationView.value)
                ptv.dailyTimeDiffInSecs = newData.dailyTimeDiffInSecs
                if let latitude = newData.latitude{
                    ptv.latitude = latitude
                }
                if let longitude = newData.longitude{
                    ptv.longitude = longitude
                }
                
//                print("changed ptv :\(ptv)")
                if childRow != nil && childRow is ParticipantAdvancedRow{
                    let row = childRow as! ParticipantAdvancedRow
                    row.personTransportationView.value = ptv
                    row.personTime.value = Utils.addSecondsToLTime(lTime: ptv.transportationStartTime, addSeconds: ptv.dailyTimeDiffInSecs).toDate()
                    print("row.personTime.value:\(row.personTime.value)")
                    let index = personTransportationViewList.value.firstIndex { (personTransportationView) -> Bool in
                        if personTransportationView.transportationId == ptv.transportationId &&
                            personTransportationView.personId == ptv.personId{
                            return true
                        }
                        return false
                    }
                    if let validIndex = index{
                        personTransportationViewList.value.replaceSubrange(validIndex...validIndex, with: [ptv])
                    }
                    
                }
                
                print("saved personTime:\(String(describing: self.personTransportationView))")
                print("Saved PersonTransportation")
//                TODO: you may want to fetch data to set it back
             }
            , errorHandle: {
                newError -> () in
                print("Saved PersonTransportation Error: \(newError)")
                self.error = newError})
    }
    
    
    var body: some View {
        GeometryReader { geometry in
            ZStack{
                List {
                    HStack{
                        Text("TransportationParticipantsListView.Participant.text")
                            .frame(width: geometry.size.width * 0.31, alignment: .leading)
                            .padding(.leading, 0)
                        Text(self.isDeparture() ? "TransportationParticipantsListView.GetOnTime.text" : "TransportationParticipantsListView.GetOffTime.text")
                            .frame(width: geometry.size.width * 0.25, alignment: .leading)
        //                    .padding(.leading, 5)
                        Text(self.isDeparture() ? "TransportationParticipantsListView.GetOnPoint.text" : "TransportationParticipantsListView.GetOffPoint.text")
                            .frame(width: geometry.size.width * 0.25, alignment: .leading)
        //                    .padding(.leading, 5)
                        Spacer()
                            .frame(width: geometry.size.width * 0.10, alignment: .leading)
        //                    .padding(.leading, 5)
                    }
                    
                    ForEach(self.personTransportationViewList.value , id: \.self) { personTransportationView in
                        ParticipantAdvancedRow(personTransportationView: personTransportationView, parent: self)
                    }
                }
                .disabled(self.isTimeViewPresented.value)
                .blur(radius: self.isTimeViewPresented.value ? 15 : 0)
            }
            if self.isTimeViewPresented.value{
                HStack{
                    Spacer()
                    VStack(){
                        Spacer()
                        TimeView(date: self.$personTime.value, isPresented: self.$isTimeViewPresented.value, doSubmit: {self.savePersonTransportation()})
                        Spacer()
                    }
                    Spacer()
                }
            }
        }
        .onAppear(perform: {
            self.fetchData()
            })
        .alert(isPresented: $showAlert.value) {
            Alert(title: Text(""), message: Text("TransportationParticipantsListView.DeleteShuttlePersonWarning.text"),
                primaryButton: .destructive(Text("TransportationParticipantsListView.DeleteShuttlePersonWarning.Delete.text"),
                action: {
                    print("will remove person")
                    self.removePersonTransportation(ptv: self.personTransportationView.value)
                    }),
                secondaryButton: .cancel({print("cancel Button clicked")}))
        }
        .sheet(isPresented: self.$sheetIsPresented.value, content: {
            MapLocationSelectUIView(observedCoordinate: self.selectedCoordinate
                , doSubmit: {
                    self.sheetIsPresented.value = false
                    self.savePersonTransportation()
                }
                , doClose: {
                    self.sheetIsPresented.value = false
                }
            )
        })
    }
}

struct TransportationParticipantsAdvancedListView_Previews: PreviewProvider {
    static var list = [PersonTransportationView(personId: "r1234", transportationId: "t123", personRoleType: 0, latitude: 1.2, longitude: 1.3, transportationStartTime: LTime(hour: 5, minute: 15, second: 20), dailyTimeDiffInSecs: 2700, personName: "susususu ththt dfdf", profilePictureURL: ""),
    PersonTransportationView(personId: "r1234", transportationId: "t123", personRoleType: 0, latitude: 1.2, longitude: 1.3, transportationStartTime: LTime(hour: 5, minute: 15, second: 20), dailyTimeDiffInSecs: 2700, personName: "susususu", profilePictureURL: "")]
    static let tt = Transportation(id: "1NAiXUBX2K", name: "", latitude: 1, longitude: 1, transportationType: 0, days: 2, status: 0, creatorPersonId: "", startTime: LTime(), timeZone: 3)
    static let tt1 = Transportation(id: "0lizu3q2S6", name: "", latitude: 1, longitude: 1, transportationType: 0, days: 2, status: 0, creatorPersonId: "", startTime: LTime(), timeZone: 3)
    static let tt2 = Transportation(id: "vh5vHYd23n", name: "", latitude: 1, longitude: 1, transportationType: 0, days: 2, status: 0, creatorPersonId: "", startTime: LTime(), timeZone: 3)
    static var previews: some View {
//        TransportationParticipantsListView(transportation: tt, personTransportationViewList : list)
//        TransportationParticipantsListView(personTransportationViewList: list).environmentObject(CreatedTransportationsListObservable())
        TransportationParticipantsListView(transportation: tt2)
    }
}
