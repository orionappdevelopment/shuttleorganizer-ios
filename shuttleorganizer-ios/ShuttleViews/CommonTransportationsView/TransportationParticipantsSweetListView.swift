//
//  TransportationParticipantsView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/30/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI
import UIKit
import Foundation
import CoreLocation
import GoogleMaps

struct TransportationParticipantsSweetListView: View {
    @ObservedObject private var personTransportationViewList : GenericObservableModel<[PersonTransportationView]> = GenericObservableModel(value: [])
    @ObservedObject var personTransportationView : GenericObservableModel<PersonTransportationView> = GenericObservableModel(value: PersonTransportationView())
    @ObservedObject var personTime = GenericObservableModel(value: Date())
    @ObservedObject var selectedCoordinate = GenericObservableModel(value: CLLocationCoordinate2D(latitude: 41.0370, longitude: 28.9851))
    @ObservedObject var isTimeViewPresented = GenericObservableModel(value: false)
    @ObservedObject var sheetIsPresented = GenericObservableModel(value: false)
    @State var showAlert = false
    @State var alertType : TransportationParticipantListAlert = .none
    
    @State var error : Error? = nil
    @State var transportation : Transportation
    
//    init(transportation: Transportation){
//        self.transportation = transportation
//        self.removingPersonTransportationView = GenericObservableModel(value: PersonTransportationView(), callBackWithParent: { (ptv: PersonTransportationView, parent : Any) in
//                print("removingPersonTransportationView is changed")
//                print("ptv:\(ptv)")
//                if let validParent = parent as? TransportationParticipantsListView{
////                    validParent.removePersonTransportation(ptv: ptv)
//                    print("will remove person")
//                }
//            })
//        self.removingPersonTransportationView.parent = self
//    }
    
    enum TransportationParticipantListAlert{case deletePersonAlert, invalidPersonStartTimeAlert, none}
    
    func isDeparture() -> Bool {
        return transportation.transportationType == TransportationTypeEnum.DEPARTURE.rawValue
    }
    func fetchData(){
        print("TransportationParticipantsListView.self = \(TransportationParticipantsListView.self)")
        NetworkManager.fetch(url: UrlConstants.UrlGetPersonTransportationList, parameters: transportation
            , handle: { (newData : [PersonTransportationView]) in
                let list = sortPersonTransportationViewList(personTransportationViewList: newData)
                self.personTransportationViewList.value = list
            }
            , errorHandle: {
                newError in
                self.error = newError})
        
    }
    func sortPersonTransportationViewList(personTransportationViewList : [PersonTransportationView]) -> [PersonTransportationView]{
        var list = personTransportationViewList
        list.sort { (ptv1, ptv2) -> Bool in
            ptv1.dailyTimeDiffInSecs < ptv2.dailyTimeDiffInSecs
        }
        return list
    }
    
    private func removePersonTransportation(ptv : PersonTransportationView){
        let personTransportation : PersonTransportation = PersonTransportation(personId : ptv.personId, transportationId : ptv.transportationId, personRoleType : ptv.personRoleType)
        print("remove Transportation called for \(ptv)")
        NetworkManager.fetch(url: UrlConstants.UrlDeletePersonTransportation, parameters: personTransportation
            , handle: {
                (newData : PersonTransportation) in
                    let filteredList = self.personTransportationViewList.value.filter{ pt in
                        pt.personId != personTransportation.personId || pt.transportationId != personTransportation.transportationId
                    }
                    self.personTransportationViewList.value = filteredList
                    print("remove personTransportation is successful")
                
                    let notificationBody = toStringFromLocalizedStringKey(localizedStringKey: "Notification.TransportationParticipantsUpdated.text", transportation.name, MyUser.getInstance().firebaseName)
                    SendNotificationManager.getInstance().sendNotificationForTransportation(transportation: transportation, body: notificationBody, notificationType: .TRANSPORTATION_NOTIFICATION)
                }
            , errorHandle: {
                newError in
                self.error = newError
                print("can not remove Person \(newError)")
                })
    }
    
    private func savePersonTransportationForPersonTime(){
        let dailyTimeDiffInSecs = Utils.lTimesDiffInSeconds(lTime1: self.transportation.startTime, lTime2: Utils.toLTime(time: self.personTime.value))
        
        let personTransportationView = self.personTransportationView.value
        
        let personTransportation : PersonTransportation = PersonTransportation(personId: personTransportationView.personId, transportationId: personTransportationView.transportationId, personRoleType: personTransportationView.personRoleType, latitude: personTransportationView.latitude, longitude: personTransportationView.longitude, dailyTimeDiffInSecs: dailyTimeDiffInSecs )

        NetworkManager.fetch(url: UrlConstants.UrlSavePersonTransportation, parameters: personTransportation
            , handle: {
                (newData : PersonTransportation ) -> () in
                
                print("saved dailyTimeDiffInSecs:\(newData.dailyTimeDiffInSecs)")
                self.personTime.value = Utils.addSecondsToLTime(lTime: self.transportation.startTime,
                                                          addSeconds: newData.dailyTimeDiffInSecs).toDate()
                var ptv = PersonTransportationView(personTransportationView: self.personTransportationView.value)
                ptv.dailyTimeDiffInSecs = newData.dailyTimeDiffInSecs
                               
                updatePersonTransportationViewList(optionalPersonTransportationView: ptv)
                
             }
            , errorHandle: {
                newError -> () in
                print("Saved PersonTransportation Error: \(newError)")
                self.error = newError})
    }
    
    private func savePersonTransportationForSelectedCoordinate(){
        let personTransportationView = self.personTransportationView.value
        
        let personTransportation : PersonTransportation = PersonTransportation(personId: personTransportationView.personId, transportationId: personTransportationView.transportationId, personRoleType: personTransportationView.personRoleType, latitude: self.selectedCoordinate.value.latitude, longitude: self.selectedCoordinate.value.longitude, dailyTimeDiffInSecs: personTransportationView.dailyTimeDiffInSecs )

        NetworkManager.fetch(url: UrlConstants.UrlSavePersonTransportation, parameters: personTransportation
            , handle: {
                (newData : PersonTransportation ) -> () in
                
                var ptv = PersonTransportationView(personTransportationView: self.personTransportationView.value)
                if let latitude = newData.latitude{
                    ptv.latitude = latitude
                }
                if let longitude = newData.longitude{
                    ptv.longitude = longitude
                }
                
                updatePersonTransportationViewList(optionalPersonTransportationView: ptv)
             }
            , errorHandle: {
                newError -> () in
                print("Saved PersonTransportation Error: \(newError)")
                self.error = newError})
    }
    
    func updatePersonTransportationViewList(optionalPersonTransportationView : PersonTransportationView? = nil){
        var list = self.personTransportationViewList.value
        if let personTransportationView = optionalPersonTransportationView {
            list = list.filter{ pt in
                pt.personId != personTransportationView.personId || pt.transportationId != personTransportationView.transportationId
            }
            list.append(personTransportationView)
        }
        
        list.sort { (ptv1, ptv2) -> Bool in
            if ptv1.dailyTimeDiffInSecs == ptv2.dailyTimeDiffInSecs{
                return ptv1.personName < ptv2.personName
            }else{
                return ptv1.dailyTimeDiffInSecs < ptv2.dailyTimeDiffInSecs
            }
        }
        self.personTransportationViewList.value = list
        
    }
    
    var body: some View {
        GeometryReader { geometry in
            ZStack{
                ScrollView(.vertical, showsIndicators: false) {
                    HStack{
                        Text("TransportationParticipantsListView.Participant.text")
                            .foregroundColor(Color.colorText)
                            .frame(width: geometry.size.width * 0.31, alignment: .leading)
//                            .offset(x : -5)
                        Text(self.isDeparture() ? "TransportationParticipantsListView.GetOnTime.text" : "TransportationParticipantsListView.GetOffTime.text")
                            .foregroundColor(Color.colorText)
                            .frame(width: geometry.size.width * 0.24, alignment: .leading)
                        Text(self.isDeparture() ? "TransportationParticipantsListView.GetOnPoint.text" : "TransportationParticipantsListView.GetOffPoint.text")
                            .foregroundColor(Color.colorText)
                            .frame(width: geometry.size.width * 0.24, alignment: .center)
                        Spacer()
                            .frame(width: geometry.size.width * 0.12, alignment: .center)
                    }
                    
                    ForEach(self.personTransportationViewList.value , id: \.self) { personTransportationView in
                        VStack(alignment: .leading){
                            HStack() {
                                
                                Text(personTransportationView.personName)
                                    .foregroundColor(Color.colorText)
                                    .frame(width: geometry.size.width * 0.31, alignment: .leading)
                                    
                                Text(getHourMinuteFormat().string(from: Utils.addSecondsToLTime(lTime:                      personTransportationView.transportationStartTime, addSeconds: personTransportationView.dailyTimeDiffInSecs).toDate()))
                                    .foregroundColor(Color.colorText)
                                    .frame(width: geometry.size.width * 0.24, height: 30, alignment: .leading)
                                    .onTapGesture {
                                        print("click on Time")
                                        self.personTransportationView.value = personTransportationView
                                        self.personTime.value = Utils.addSecondsToLTime(lTime: personTransportationView.transportationStartTime, addSeconds: personTransportationView.dailyTimeDiffInSecs).toDate()
                                        self.isTimeViewPresented.value = true
                                    }
                                
                                Image("map")
                                    .renderingMode(.template)
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: geometry.size.width * 0.24, height: 30, alignment: .center)
                                    .foregroundColor(Color.colorPrimary)
                                    .onTapGesture {
                                        print("map pressed")
                                        self.personTransportationView.value = personTransportationView
                                        self.selectedCoordinate.value.latitude = self.personTransportationView.value.latitude
                                        self.selectedCoordinate.value.longitude = self.personTransportationView.value.longitude
                                        self.sheetIsPresented.value = true
                                    }

                                Image("remove")
                                    .renderingMode(.template)
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: geometry.size.width * 0.09, height: 30, alignment: .center)
                                    .foregroundColor(Color.colorPrimary)
                                    .onTapGesture {
                                        print("remove pressed")
                                        self.personTransportationView.value = personTransportationView
                                        self.alertType = .deletePersonAlert
                                        self.showAlert = true
                                    }
                            }
                        }
                        
                    }
                }
                .disabled(self.isTimeViewPresented.value)
                .blur(radius: self.isTimeViewPresented.value ? 15 : 0)
            }
            .padding(EdgeInsets(top: 15, leading: geometry.size.width*0.04, bottom: 0, trailing: geometry.size.width*0.04))
            if self.isTimeViewPresented.value{
                HStack{
                    Spacer()
                    VStack(){
                        Spacer()
                        TimeView(date: self.$personTime.value, isPresented: self.$isTimeViewPresented.value, doSubmit: {
                            //check for time diff
                            if false == Utils.isValidTimeDiffBeetweenDepartureAndPersonGetOntime(transportationTime: self.transportation.startTime, personGetOnTime: Utils.toLTime(time: self.personTime.value)) {
                                self.personTime.value = Utils.addSecondsToLTime(lTime: self.personTransportationView.value.transportationStartTime, addSeconds: self.personTransportationView.value.dailyTimeDiffInSecs).toDate()
                                self.alertType = .invalidPersonStartTimeAlert
                                self.showAlert = true
                            }else{
                                self.savePersonTransportationForPersonTime()
                            }
                        })
                        Spacer()
                    }
                    Spacer()
                }
            }
        }
        .background(Color.colorBackground)
        .onAppear(perform: {
            self.fetchData()
            })
        .alert(isPresented: $showAlert) {
            if alertType == TransportationParticipantListAlert.deletePersonAlert {
                return Alert(title: Text(""), message: Text("TransportationParticipantsListView.DeleteShuttlePersonWarning.text"),
                    primaryButton: .destructive(Text("TransportationParticipantsListView.DeleteShuttlePersonWarning.Delete.text"),
                    action: {
                        print("will remove person")
                        self.removePersonTransportation(ptv: self.personTransportationView.value)
                        }),
                    secondaryButton: .cancel({print("cancel Button clicked")}))
            }else{
                return Alert(title: Text(""), message: Text("TransportationParticipantsListView.InvalidPersonStartTime.text"), dismissButton: .default(Text("TransportationParticipantsListView.InvalidPersonStartTime.Ok.text")))
            }
        }
        .sheet(isPresented: self.$sheetIsPresented.value, content: {
            MapLocationSelectUIView(observedCoordinate: self.selectedCoordinate
                , doSubmit: {
                    self.sheetIsPresented.value = false
                    self.savePersonTransportationForSelectedCoordinate()
                }
                , doClose: {
                    self.sheetIsPresented.value = false
                }
            )
        })
        .navigationBarItems(leading: EmptyView(), trailing: EmptyView())
    }
}

struct TransportationParticipantsSweetListView_Previews: PreviewProvider {
    static var list = [PersonTransportationView(personId: "r1234", transportationId: "t123", personRoleType: 0, latitude: 1.2, longitude: 1.3, transportationStartTime: LTime(hour: 5, minute: 15, second: 20), dailyTimeDiffInSecs: 2700, personName: "susususu ththt dfdf", profilePictureURL: ""),
    PersonTransportationView(personId: "r1234", transportationId: "t123", personRoleType: 0, latitude: 1.2, longitude: 1.3, transportationStartTime: LTime(hour: 5, minute: 15, second: 20), dailyTimeDiffInSecs: 2700, personName: "susususu", profilePictureURL: "")]
    static let tt = Transportation(id: "1NAiXUBX2K", name: "", latitude: 1, longitude: 1, transportationType: 0, days: 2, status: 0, creatorPersonId: "", startTime: LTime(), timeZone: 3)
    static let tt1 = Transportation(id: "0lizu3q2S6", name: "", latitude: 1, longitude: 1, transportationType: 0, days: 2, status: 0, creatorPersonId: "", startTime: LTime(), timeZone: 3)
    static let tt2 = Transportation(id: "vh5vHYd23n", name: "", latitude: 1, longitude: 1, transportationType: 0, days: 2, status: 0, creatorPersonId: "", startTime: LTime(), timeZone: 3)
    static var previews: some View {
//        TransportationParticipantsListView(transportation: tt, personTransportationViewList : list)
//        TransportationParticipantsListView(personTransportationViewList: list).environmentObject(CreatedTransportationsListObservable())
        TransportationParticipantsListView(transportation: tt2)
    }
}
