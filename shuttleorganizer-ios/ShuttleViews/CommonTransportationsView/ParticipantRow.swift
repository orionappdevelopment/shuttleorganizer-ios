//
//  ParticipantRow.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/30/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct ParticipantRow: View {
    var personTransportationView : PersonTransportationView //= PersonTransportationView()
    @ObservedObject var personTime : GenericObservableModel<Date> //= Date()
    
    @ObservedObject var removingPersonTransportationView : GenericObservableModel<PersonTransportationView>
    
    @ObservedObject var updatePersonTransportationViewDate : GenericObservableModel<Date>
    
//    init(personTransportationView: PersonTransportationView){
//        self.personTransportationView = personTransportationView
//        self.personTime = Utils.addSecondsToLTime(lTime: personTransportationView.transportationStartTime, addSeconds: personTransportationView.dailyTimeDiffInSecs).toDate()
//    }
    
    var body: some View {
        GeometryReader { geometry in
            VStack(alignment: .leading){
                HStack() {
                    
                    Text(self.personTransportationView.personName)
                        .foregroundColor(Color.black)
    //                    .padding(.leading, 5)
                        .frame(width: geometry.size.width * 0.32, alignment: .leading)
                        
    //                Spacer()
    //                Spacer()
                    Text(getHourMinuteFormat().string(from: self.personTime.value))
    //                    .padding(.leading, 5)
                        .frame(width: geometry.size.width * 0.25, height: 30)
                        .onTapGesture {
                            print("click on Time")
    //                        $removingPersonTransportationView.value = personTime.value
                            }
    //                VStack{
    //                    Form{
    //                        DatePicker(selection: self.$personTime , in: ...Date(), displayedComponents: .hourAndMinute) {
    //                            Text("")
    //                        }
    //                            .labelsHidden()
    //                            .multilineTextAlignment(.leading)
    //                            .padding(.top, -49)
    //                            .foregroundColor(Color.black)
    //                    }
    //                        .frame(width: 100, height: 37, alignment: .leading)
    //                        .cornerRadius(10)
    //                }

    //                Spacer()
                    
                    Image("map")
                        .renderingMode(.template)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: geometry.size.width * 0.20, height: 30, alignment: .center)
    //                    .padding(5)
                        .foregroundColor(Color.colorPrimary)
                        .onTapGesture {
                            print("map pressed")
                        }

    //                Spacer()


                    Image("remove")
                        .renderingMode(.template)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: geometry.size.width * 0.15, height: 30)
    //                    .padding(5)
                        .foregroundColor(Color.colorPrimary)
                        .padding(.trailing, 7)
                        .onTapGesture {
                            print("remove pressed")
                            self.removingPersonTransportationView.value = self.personTransportationView
                        }
                    
                }
            }
        }
    }
    
}

struct ParticipantRow_Previews: PreviewProvider {
    static var previews: some View {
        
        List{
            ParticipantRow(personTransportationView : PersonTransportationView(personId: "r1234", transportationId: "t123", personRoleType: 0, latitude: 1.2, longitude: 1.3, transportationStartTime: LTime(hour: 5, minute: 15, second: 20), dailyTimeDiffInSecs: 2700, personName: "susususu sd asd asd asd asd asd ", profilePictureURL: ""), personTime: GenericObservableModel(value: LTime(hour: 5, minute: 15, second: 20).toDate()), removingPersonTransportationView: GenericObservableModel(value: PersonTransportationView()), updatePersonTransportationViewDate: GenericObservableModel(value: Date()))
            ParticipantRow(personTransportationView : PersonTransportationView(personId: "r1234", transportationId: "t123", personRoleType: 0, latitude: 1.2, longitude: 1.3, transportationStartTime: LTime(hour: 5, minute: 15, second: 20), dailyTimeDiffInSecs: 2700, personName: "susususu", profilePictureURL: ""), personTime: GenericObservableModel(value: LTime(hour: 5, minute: 15, second: 20).toDate()), removingPersonTransportationView: GenericObservableModel(value: PersonTransportationView()), updatePersonTransportationViewDate: GenericObservableModel(value: Date()))
        }
        
    }
}
