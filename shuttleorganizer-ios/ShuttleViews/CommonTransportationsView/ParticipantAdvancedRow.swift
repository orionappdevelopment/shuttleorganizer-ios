//
//  ParticipantRow.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/30/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI

struct ParticipantAdvancedRow: View {
    @ObservedObject var personTransportationView : GenericObservableModel<PersonTransportationView>
    @ObservedObject var parent : GenericObservableModel<TransportationParticipantsAdvancedListView>
    @ObservedObject var personTime : GenericObservableModel<Date>
    
    init(personTransportationView : PersonTransportationView, parent : TransportationParticipantsAdvancedListView){
        self.personTransportationView = GenericObservableModel(value: personTransportationView)
        self.parent = GenericObservableModel(value: parent)
        self.personTime = GenericObservableModel(value: Utils.addSecondsToLTime(lTime: personTransportationView.transportationStartTime, addSeconds: personTransportationView.dailyTimeDiffInSecs).toDate())
    }
    
    var body: some View {
        GeometryReader { geometry in
            VStack(alignment: .leading){
                HStack() {
                    
                    Text(self.personTransportationView.value.personName)
                        .foregroundColor(Color.black)
    //                    .padding(.leading, 5)
                        .frame(width: geometry.size.width * 0.31, alignment: .leading)
                        
                    Text(getHourMinuteFormat().string(from: self.personTime.value))
    //                    .padding(.leading, 5)
                        .frame(width: geometry.size.width * 0.25, height: 30)
                        .onTapGesture {
                            print("click on Time")
                            self.parent.value.childRow = self
                            self.parent.value.personTransportationView.value = self.personTransportationView.value
                            self.parent.value.personTime.value = Utils.addSecondsToLTime(lTime: personTransportationView.value.transportationStartTime, addSeconds: personTransportationView.value.dailyTimeDiffInSecs).toDate()
                            self.parent.value.isTimeViewPresented.value = true
                            
                        }
                    
                    Image("map")
                        .renderingMode(.template)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: geometry.size.width * 0.25, height: 30, alignment: .center)
    //                    .padding(5)
                        .foregroundColor(Color.colorPrimary)
                        .onTapGesture {
                            print("map pressed")
                        }


                    Image("remove")
                        .renderingMode(.template)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: geometry.size.width * 0.10, height: 30)
                        .foregroundColor(Color.colorPrimary)
//                        .padding(.trailing, 7)
                        .onTapGesture {
                            print("remove pressed")
                        }
                    
                }
            }
        }
    }
    
}

struct ParticipantAdvancedRow_Previews: PreviewProvider {
    static var previews: some View {
        
        List{
            ParticipantRow(personTransportationView : PersonTransportationView(personId: "r1234", transportationId: "t123", personRoleType: 0, latitude: 1.2, longitude: 1.3, transportationStartTime: LTime(hour: 5, minute: 15, second: 20), dailyTimeDiffInSecs: 2700, personName: "susususu sd asd asd asd asd asd ", profilePictureURL: ""), personTime: GenericObservableModel(value: LTime(hour: 5, minute: 15, second: 20).toDate()), removingPersonTransportationView: GenericObservableModel(value: PersonTransportationView()), updatePersonTransportationViewDate: GenericObservableModel(value: Date()))
            ParticipantRow(personTransportationView : PersonTransportationView(personId: "r1234", transportationId: "t123", personRoleType: 0, latitude: 1.2, longitude: 1.3, transportationStartTime: LTime(hour: 5, minute: 15, second: 20), dailyTimeDiffInSecs: 2700, personName: "susususu", profilePictureURL: ""), personTime: GenericObservableModel(value: LTime(hour: 5, minute: 15, second: 20).toDate()), removingPersonTransportationView: GenericObservableModel(value: PersonTransportationView()), updatePersonTransportationViewDate: GenericObservableModel(value: Date()))
        }
        
    }
}
