//
//  TransportationParticipantsView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/30/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI
import UIKit
import Foundation
import CoreLocation
import GoogleMaps

struct TransportationParticipantsListView: View {
    @ObservedObject private var personTransportationViewList : GenericObservableModel<[PersonTransportationView]> = GenericObservableModel(value: [])
    @ObservedObject var personTransportationView : GenericObservableModel<PersonTransportationView> = GenericObservableModel(value: PersonTransportationView())
    @ObservedObject var personTime = GenericObservableModel(value: Date())
    @ObservedObject var selectedCoordinate = GenericObservableModel(value: CLLocationCoordinate2D(latitude: 41.0370, longitude: 28.9851))
    @ObservedObject var isTimeViewPresented = GenericObservableModel(value: false)
    @ObservedObject var sheetIsPresented = GenericObservableModel(value: false)
    @State var showAlert = false
    @State var alertType : TransportationParticipantListAlert = .none
    
    @State var error : Error? = nil
    @State var transportation : Transportation
    
//    init(transportation: Transportation){
//        self.transportation = transportation
//        self.removingPersonTransportationView = GenericObservableModel(value: PersonTransportationView(), callBackWithParent: { (ptv: PersonTransportationView, parent : Any) in
//                print("removingPersonTransportationView is changed")
//                print("ptv:\(ptv)")
//                if let validParent = parent as? TransportationParticipantsListView{
////                    validParent.removePersonTransportation(ptv: ptv)
//                    print("will remove person")
//                }
//            })
//        self.removingPersonTransportationView.parent = self
//    }
    
    enum TransportationParticipantListAlert{case deletePersonAlert, invalidPersonStartTimeAlert, none}
    
    init(transportation : Transportation) {
        self.transportation = transportation
        
        let transportationNotificationName = ShuttleNotificationEventFactory.getInstance().getEventNotificationName(shuttleNotificationTypeEnum: .TRANSPORTATION_NOTIFICATION)
        NotificationCenter.default.addObserver(forName: transportationNotificationName, object: nil, queue: nil, using: self.notificationCame)
    }

    func notificationCame(_ notification: Notification) {
        if let notificationWrapper : ShuttleNotificationWrapper = notification.object as? ShuttleNotificationWrapper{
            print("shuttleNotificationWrapper: \(notificationWrapper)")
            let notificationModel = notificationWrapper.shuttleNotificationModel
            if notificationModel.getShuttleNotificationTypeEnum() == .TRANSPORTATION_NOTIFICATION && notificationModel.transportationId == self.transportation.id{
                self.fetchData()
            }
        }
    }
    
    func isDeparture() -> Bool {
        return transportation.transportationType == TransportationTypeEnum.DEPARTURE.rawValue
    }
    func fetchData(){
        print("TransportationParticipantsListView.self = \(TransportationParticipantsListView.self)")
        NetworkManager.fetch(url: UrlConstants.UrlGetPersonTransportationList, parameters: transportation
            , handle: { (newData : [PersonTransportationView]) in
                let list = sortPersonTransportationViewList(personTransportationViewList: newData)
                self.personTransportationViewList.value = list
            }
            , errorHandle: {
                newError in
                self.error = newError})
        
    }
    func sortPersonTransportationViewList(personTransportationViewList : [PersonTransportationView]) -> [PersonTransportationView]{
        var list = personTransportationViewList
        list.sort { (ptv1, ptv2) -> Bool in
            ptv1.dailyTimeDiffInSecs < ptv2.dailyTimeDiffInSecs
        }
        return list
    }
    
    private func removePersonTransportation(ptv : PersonTransportationView){
        let personTransportation : PersonTransportation = PersonTransportation(personId : ptv.personId, transportationId : ptv.transportationId, personRoleType : ptv.personRoleType)
        print("remove Transportation called for \(ptv)")
        NetworkManager.fetch(url: UrlConstants.UrlDeletePersonTransportation, parameters: personTransportation
            , handle: {
                (newData : PersonTransportation) in
                    let filteredList = self.personTransportationViewList.value.filter{ pt in
                        pt.personId != personTransportation.personId || pt.transportationId != personTransportation.transportationId
                    }
                    self.personTransportationViewList.value = filteredList
                    print("remove personTransportation is successful")
                
                    let notificationBody = toStringFromLocalizedStringKey(localizedStringKey: "Notification.TransportationParticipantsUpdated.text", transportation.name, MyUser.getInstance().firebaseName)
                    SendNotificationManager.getInstance().sendNotificationForTransportation(transportation: transportation, body: notificationBody, notificationType: .TRANSPORTATION_NOTIFICATION)
                }
            , errorHandle: {
                newError in
                self.error = newError
                print("can not remove Person \(newError)")
                })
    }
    
    private func savePersonTransportationForPersonTime(){
        let dailyTimeDiffInSecs = Utils.lTimesDiffInSeconds(lTime1: self.transportation.startTime, lTime2: Utils.toLTime(time: self.personTime.value))
        
        let personTransportationView = self.personTransportationView.value
        
        let personTransportation : PersonTransportation = PersonTransportation(personId: personTransportationView.personId, transportationId: personTransportationView.transportationId, personRoleType: personTransportationView.personRoleType, latitude: personTransportationView.latitude, longitude: personTransportationView.longitude, dailyTimeDiffInSecs: dailyTimeDiffInSecs )

        NetworkManager.fetch(url: UrlConstants.UrlSavePersonTransportation, parameters: personTransportation
            , handle: {
                (newData : PersonTransportation ) -> () in
                
                print("saved dailyTimeDiffInSecs:\(newData.dailyTimeDiffInSecs)")
                self.personTime.value = Utils.addSecondsToLTime(lTime: self.transportation.startTime,
                                                          addSeconds: newData.dailyTimeDiffInSecs).toDate()
                var ptv = PersonTransportationView(personTransportationView: self.personTransportationView.value)
                ptv.dailyTimeDiffInSecs = newData.dailyTimeDiffInSecs
                               
                updatePersonTransportationViewList(optionalPersonTransportationView: ptv)
                
             }
            , errorHandle: {
                newError -> () in
                print("Saved PersonTransportation Error: \(newError)")
                self.error = newError})
    }
    
    private func savePersonTransportationForSelectedCoordinate(){
        let personTransportationView = self.personTransportationView.value
        
        let personTransportation : PersonTransportation = PersonTransportation(personId: personTransportationView.personId, transportationId: personTransportationView.transportationId, personRoleType: personTransportationView.personRoleType, latitude: self.selectedCoordinate.value.latitude, longitude: self.selectedCoordinate.value.longitude, dailyTimeDiffInSecs: personTransportationView.dailyTimeDiffInSecs )

        NetworkManager.fetch(url: UrlConstants.UrlSavePersonTransportation, parameters: personTransportation
            , handle: {
                (newData : PersonTransportation ) -> () in
                
                var ptv = PersonTransportationView(personTransportationView: self.personTransportationView.value)
                if let latitude = newData.latitude{
                    ptv.latitude = latitude
                }
                if let longitude = newData.longitude{
                    ptv.longitude = longitude
                }
                
                updatePersonTransportationViewList(optionalPersonTransportationView: ptv)
             }
            , errorHandle: {
                newError -> () in
                print("Saved PersonTransportation Error: \(newError)")
                self.error = newError})
    }
    
    func updatePersonTransportationViewList(optionalPersonTransportationView : PersonTransportationView? = nil){
        var list = self.personTransportationViewList.value
        if let personTransportationView = optionalPersonTransportationView {
            list = list.filter{ pt in
                pt.personId != personTransportationView.personId || pt.transportationId != personTransportationView.transportationId
            }
            list.append(personTransportationView)
        }
        
        list.sort { (ptv1, ptv2) -> Bool in
            if ptv1.dailyTimeDiffInSecs == ptv2.dailyTimeDiffInSecs{
                return ptv1.personName < ptv2.personName
            }else{
                return ptv1.dailyTimeDiffInSecs < ptv2.dailyTimeDiffInSecs
            }
        }
        self.personTransportationViewList.value = list
        
    }
    
    var body: some View {
        GeometryReader { geometry in
            ZStack{
                ScrollView(.vertical, showsIndicators: false) {
                    ForEach(self.personTransportationViewList.value , id: \.self) { personTransportationView in
                        ZStack(){
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(Color.colorPrimary, lineWidth: 0.5)
                                .background(Color.colorListLight)
//                                .foregroundColor(Color.colorListLight)
                            
//                            RoundedRectangle(cornerRadius: 5)
//                                .foregroundColor(Color.colorListLight)
////                                .shadow(radius: 1, y: 1)
//                                .border(Color.colorPrimary, width: 0.5)
                            
                            VStack(){
                                HStack{
                                    Text(personTransportationView.personName)
                                        .foregroundColor(Color.colorText)
                                        .frame(width: geometry.size.width * 0.71, alignment: .leading)
                                        .font(.title)
                                    
                                    Spacer()
                                    
                                    Image("delete")
                                        .renderingMode(.template)
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width: geometry.size.width * 0.09, height: 30, alignment: .trailing)
                                        .foregroundColor(Color.colorText)
                                        .onTapGesture {
                                            print("remove pressed")
                                            self.personTransportationView.value = personTransportationView
                                            self.alertType = .deletePersonAlert
                                            self.showAlert = true
                                        }
                                }
                                .padding(EdgeInsets(top: 10, leading: geometry.size.width * 0.03, bottom: 10, trailing: geometry.size.width * 0.03))
                                
                                
                                HStack{
                                    if self.isDeparture(){
                                        DateWithTopLabel(width: geometry.size.width * 0.40 - 5,
                                                         titleBackgroundColor: Color.colorListLight,
                                                         titleValue : GenericObservableModel(value: "TransportationParticipantsListView.GetOnTime.text"),
                                                         observedDate: GenericObservableModel(value: Utils.addSecondsToLTime(lTime:personTransportationView.transportationStartTime, addSeconds: personTransportationView.dailyTimeDiffInSecs).toDate()))
                                            .onTapGesture {
                                                print("click on Time")
                                                self.personTransportationView.value = personTransportationView
                                                self.personTime.value = Utils.addSecondsToLTime(lTime: personTransportationView.transportationStartTime, addSeconds: personTransportationView.dailyTimeDiffInSecs).toDate()
                                                self.isTimeViewPresented.value = true
                                            }
                                    }
                                    
                                    Spacer()
                                    
                                    ImageWithTopLabel(width: geometry.size.width * 0.40 - 5,
                                                      titleBackgroundColor : Color.colorListLight,
                                                      titleValue: self.isDeparture() ?
                                                        GenericObservableModel(value: "TransportationParticipantsListView.GetOnPoint.text") :
                                                        GenericObservableModel(value: "TransportationParticipantsListView.GetOffPoint.text"),
                                                      image: Image("map"))
                                        .onTapGesture {
                                            print("map pressed")
                                            self.personTransportationView.value = personTransportationView
                                            self.selectedCoordinate.value.latitude = self.personTransportationView.value.latitude
                                            self.selectedCoordinate.value.longitude = self.personTransportationView.value.longitude
                                            self.sheetIsPresented.value = true
                                        }
                                }
                                .padding(EdgeInsets(top: 10, leading: geometry.size.width * 0.03, bottom: 10, trailing: geometry.size.width * 0.03))
                            }
                            .frame(width: geometry.size.width * 0.92, alignment: .center)
                            
                        }
                        .frame(width: geometry.size.width * 0.94 , height: 150, alignment: .center)
                        .padding(EdgeInsets(top: 0, leading: 0, bottom: 7, trailing: 0))
                        
                    }
                }
                .disabled(self.isTimeViewPresented.value)
                .blur(radius: self.isTimeViewPresented.value ? 15 : 0)
            }
            .padding(EdgeInsets(top: 15, leading: geometry.size.width*0.03, bottom: 15, trailing: geometry.size.width*0.03))
            
            if self.isTimeViewPresented.value{
                HStack{
                    Spacer()
                    VStack(){
                        Spacer()
                        TimeView(date: self.$personTime.value, isPresented: self.$isTimeViewPresented.value, doSubmit: {
                            //check for time diff
                            if false == Utils.isValidTimeDiffBeetweenDepartureAndPersonGetOntime(transportationTime: self.transportation.startTime, personGetOnTime: Utils.toLTime(time: self.personTime.value)) {
                                self.personTime.value = Utils.addSecondsToLTime(lTime: self.personTransportationView.value.transportationStartTime, addSeconds: self.personTransportationView.value.dailyTimeDiffInSecs).toDate()
                                self.alertType = .invalidPersonStartTimeAlert
                                self.showAlert = true
                            }else{
                                self.savePersonTransportationForPersonTime()
                            }
                        })
                        Spacer()
                    }
                    Spacer()
                }
            }
        }
        .background(Color.colorBackground)
        .onAppear(perform: {
            self.fetchData()
            })
        .alert(isPresented: $showAlert) {
            if alertType == TransportationParticipantListAlert.deletePersonAlert {
                return Alert(title: Text(""), message: Text("TransportationParticipantsListView.DeleteShuttlePersonWarning.text"),
                    primaryButton: .destructive(Text("TransportationParticipantsListView.DeleteShuttlePersonWarning.Delete.text"),
                    action: {
                        print("will remove person")
                        self.removePersonTransportation(ptv: self.personTransportationView.value)
                        }),
                    secondaryButton: .cancel({print("cancel Button clicked")}))
            }else{
                return Alert(title: Text(""), message: Text("TransportationParticipantsListView.InvalidPersonStartTime.text"), dismissButton: .default(Text("TransportationParticipantsListView.InvalidPersonStartTime.Ok.text")))
            }
        }
        .sheet(isPresented: self.$sheetIsPresented.value, content: {
            MapLocationSelectUIView(observedCoordinate: self.selectedCoordinate
                , doSubmit: {
                    self.sheetIsPresented.value = false
                    self.savePersonTransportationForSelectedCoordinate()
                }
                , doClose: {
                    self.sheetIsPresented.value = false
                }
            )
        })
        .navigationBarItems(leading: EmptyView(), trailing: EmptyView())
    }
}

struct TransportationParticipantsListView_Previews: PreviewProvider {
    static var list = [PersonTransportationView(personId: "r1234", transportationId: "t123", personRoleType: 0, latitude: 1.2, longitude: 1.3, transportationStartTime: LTime(hour: 5, minute: 15, second: 20), dailyTimeDiffInSecs: 2700, personName: "susususu ththt dfdf", profilePictureURL: ""),
    PersonTransportationView(personId: "r1234", transportationId: "t123", personRoleType: 0, latitude: 1.2, longitude: 1.3, transportationStartTime: LTime(hour: 5, minute: 15, second: 20), dailyTimeDiffInSecs: 2700, personName: "susususu", profilePictureURL: "")]
    static let tt = Transportation(id: "1NAiXUBX2K", name: "", latitude: 1, longitude: 1, transportationType: 0, days: 2, status: 0, creatorPersonId: "", startTime: LTime(), timeZone: 3)
    static let tt1 = Transportation(id: "0lizu3q2S6", name: "", latitude: 1, longitude: 1, transportationType: 0, days: 2, status: 0, creatorPersonId: "", startTime: LTime(), timeZone: 3)
    static let tt2 = Transportation(id: "vh5vHYd23n", name: "", latitude: 1, longitude: 1, transportationType: 0, days: 2, status: 0, creatorPersonId: "", startTime: LTime(), timeZone: 3)
    static var previews: some View {
//        TransportationParticipantsListView(transportation: tt, personTransportationViewList : list)
//        TransportationParticipantsListView(personTransportationViewList: list).environmentObject(CreatedTransportationsListObservable())
        TransportationParticipantsListView(transportation: tt2)
    }
}
