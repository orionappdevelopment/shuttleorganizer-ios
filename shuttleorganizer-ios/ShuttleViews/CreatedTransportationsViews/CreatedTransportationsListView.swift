//
//  CreatedShuttlesView.swift
//  Landmarks
//
//  Created by macos on 5/9/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import SwiftUI


struct CreatedTransportationsListView: View {
//    @EnvironmentObject var createdTransportationsList : CreatedTransportationsListObservable
//    @EnvironmentObject var joinedTransportationsList : JoinedTransportationsListObservable
    @ObservedObject var createdTransportationsList : CreatedTransportationsListObservable = CreatedTransportationsListObservable.getInstance()
    @ObservedObject var joinedTransportationsList : JoinedTransportationsListObservable = JoinedTransportationsListObservable.getInstance()
    @State var isShortcutsVisible = false
    @ObservedObject var isShowToastCopied : ToastObservableModel = ToastObservableModel()
    @ObservedObject var sharedTransportationId : GenericObservableModel = GenericObservableModel(value: "")
    
    @ObservedObject var isSheetPresented : GenericObservableModel = GenericObservableModel(value: false)
    @ObservedObject var presentedSheet : GenericObservableModel = GenericObservableModel(value: PresentedSheet.none)
    
    enum PresentedSheet{
        case sharePresented, joinPresented, none
    }
    
    func doAppear(){
        //remove all form space between top such as TransportationInfoView
//        UITableView.appearance().tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: Double.leastNonzeroMagnitude))
        self.isShortcutsVisible = false
        self.createdTransportationsList.fetchData()
        self.joinedTransportationsList.fetchData()
    }
    
    var body: some View {
        ZStack {
            VStack{
                ScrollView(.vertical, showsIndicators: false ) {
                    ForEach(createdTransportationsList.transportations) { transportation in
                        NavigationLink(
                            destination: CreatedTransportationsTabView(transportation: GenericObservableModel(value: transportation))
                                .onDisappear(){
                                    self.doAppear()
                                }
                        ) {
                            CreatedTransportationRow(transportation: transportation, toastObservableModel: self.isShowToastCopied, sharedTransportationId: self.sharedTransportationId, isSheetPresented: self.isSheetPresented, presentedSheet: self.presentedSheet)
                        }
                    }
                    Spacer()
                        .frame(height: 100, alignment: .bottom)
                    
                }
                .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
            }
            .disabled(self.isShortcutsVisible)
            .blur(radius: self.isShortcutsVisible ? 15 : 0)
            
            FloatingButton(isLinkVisible: $isShortcutsVisible, doNavigationDisappear: {self.doAppear()})
            
        }
        .onTapGesture {
            if self.isShortcutsVisible{
                self.isShortcutsVisible = false
            }
        }
        .background(Color.colorListViewBackground)
        .onAppear(perform: {
            self.doAppear()
        })
//        .onDisappear(){
//            self.isShortcutsVisible = false
//        }
        .toastObservableView(toastObservableModel: isShowToastCopied, text: Text("CreatedTransportationsListView.Clipboard.text"))
        .sheet(isPresented: $isSheetPresented.value) {
            if presentedSheet.value == PresentedSheet.sharePresented {
                ShareSheet(activityItems: [self.sharedTransportationId.value])
            }else{
                JoinTransportationView(transportationId: self.sharedTransportationId.value, isSheet: true, isSheetPresented: self.isSheetPresented)
                    .onDisappear(){
                        self.doAppear()
                    }
            }
        }
    }
}

struct CreatedTransportationsListViewPreviews: PreviewProvider {
    static var previews: some View {
        CreatedTransportationsListView()
//            .environmentObject(CreatedTransportationsListObservable())
    }
}
