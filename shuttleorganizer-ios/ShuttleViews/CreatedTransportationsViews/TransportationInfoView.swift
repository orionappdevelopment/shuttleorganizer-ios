//
//  TransportationInfoView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/28/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import SwiftUI

struct TransportationInfoView: View {
    @ObservedObject var transportation : GenericObservableModel<Transportation>
    @ObservedObject var transportationName: GenericObservableModel<String>
    @ObservedObject var transportationPlate: GenericObservableModel<String>
    @ObservedObject var transportationStartTime: GenericObservableModel<Date>
    @ObservedObject var days : GenericObservableModel<[Bool]>
    
    @ObservedObject var coordinate : GenericObservableModel<CLLocationCoordinate2D>
    @State private var isTimeViewPresented = false
    @State private var sheetIsPresented = false
    @ObservedObject var showAlert = GenericObservableModel(value: false)
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State var error : Error? = nil
    
    
    private func isValid() -> Bool {
        if self.transportationName.value.isEmpty {
            return false
        }
        return true
    }
    
    private func save(){
//        let transportation: Transportation = Transportation(id: self.transportation.value.id!, name: transportationName.value, latitude: self.transportation.value.latitude, longitude: self.transportation.value.longitude
//            , transportationType: self.transportation.value.transportationType, days: DayEnum.multiplicationFromDays(days: self.days.value), status: self.transportation.value.status
//            , creatorPersonId: self.transportation.value.creatorPersonId, startTime: Utils.toLTime(time: transportationStartTime.value), timeZone: self.transportation.value.timeZone)
        
        let transportation: Transportation = Transportation(id: self.transportation.value.id!, name: transportationName.value,
                                                            latitude: self.coordinate.value.latitude, longitude: self.coordinate.value.longitude,
                                                            transportationType: self.transportation.value.transportationType, days: DayEnum.multiplicationFromDays(days: self.days.value),
                                                            status: self.transportation.value.status, creatorPersonId: self.transportation.value.creatorPersonId,
                                                            startTime: Utils.toLTime(time: transportationStartTime.value), timeZone: self.transportation.value.timeZone, plate: self.transportationPlate.value)
        
        NetworkManager.fetch(url: UrlConstants.UrlCreateTransportation, parameters: transportation
            , handle: {
                (newData : Transportation ) -> () in
                    self.transportation.value = newData
                    self.transportationName.value = self.transportation.value.name
                    self.transportationStartTime.value = self.transportation.value.startTime.toDate()
                    self.days.value = DayEnum.daysFromMultiplication(multiplication: self.transportation.value.days)
                    self.transportationPlate.value = self.transportation.value.plate
                    
                
                    print("save TransporrationInfoView save successfully")
                    print("self.transportation.name:\(self.transportation.value.name)")
                    print("newData transportationName:\(newData.name)")
                    print("transportationName:\(self.transportationName)")
                
                let notificationBody = toStringFromLocalizedStringKey(localizedStringKey: "Notification.TransportationUpdated.text", transportation.name, MyUser.getInstance().firebaseName)
                SendNotificationManager.getInstance().sendNotificationForTransportation(transportation: transportation, body: notificationBody, notificationType: .TRANSPORTATION_NOTIFICATION)
//                self.createdTransportation = newData
//                print("Created Transporatation: \(self.createdTransportation!)")
             }
            , errorHandle: {
                newError -> () in
                print("Create Transportation Error: \(newError)")
                self.error = newError})
    }
    
    private func deleteTransportation(){
        let transportation: Transportation = Transportation(transportation: self.transportation.value)
        
        NetworkManager.fetch(url: UrlConstants.UrlDeleteTransportation, parameters: transportation
            , handle: {
                (newData : Transportation.OptionalTransportation ) -> () in
                    print("transportation deleted successfully")
                    let notificationBody = toStringFromLocalizedStringKey(localizedStringKey: "Notification.TransportationDeleted.text", transportation.name, MyUser.getInstance().firebaseName)
                    SendNotificationManager.getInstance().sendNotificationForTransportation(transportation: transportation, body: notificationBody, notificationType: .TRANSPORTATION_NOTIFICATION)
                    presentationMode.wrappedValue.dismiss()
             }
            , errorHandle: {
                newError -> () in
                print("Can not Delete Transportation Error: \(newError)")
                self.error = newError})
    }
    
    var body: some View {
        GeometryReader { geometry in
            ZStack{
                VStack {
                    VStack(alignment: .leading) {
                        
                        TextFieldWithTopLabel(width: geometry.size.width * 0.92, titleBackgroundColor: Color.colorBackground,
                                              titleValue : GenericObservableModel(value: "TransportationInfoView.TransportationName.text"),
                                              textFieldValue: self.transportationName)
                            .padding(EdgeInsets(top: 5, leading: 0, bottom: 0, trailing: 0))
                        
                        HStack{
                            TextFieldWithTopLabel(width: geometry.size.width * 0.45 - 5, titleBackgroundColor: Color.colorBackground,
                                                  titleValue : GenericObservableModel(value: "TransportationInfoView.Plate.text"),
                                                  textFieldValue: self.transportationPlate)
                                .padding(EdgeInsets(top: 10, leading: 0, bottom: 0, trailing: 0))
                            
                            Spacer()
                            
                            DateWithTopLabel(width: geometry.size.width * 0.45 - 5,
                                             titleBackgroundColor: Color.colorBackground,
                                             titleValue : TransportationTypeEnum.toTransportationTypeEnum(transportation.value.transportationType) == TransportationTypeEnum.DEPARTURE ?
                                                    GenericObservableModel(value: "TransportationInfoView.StartingTime.text") :
                                                    GenericObservableModel(value: "TransportationInfoView.DepartureTime.text"),
                                             observedDate: self.transportationStartTime)
                                .onTapGesture {
                                    self.isTimeViewPresented.toggle()
                                }
                                .padding(EdgeInsets(top: 10, leading: 0, bottom: 0, trailing: 0))
                        }
                        
                        MapLocationViewWithTopLabel(width: geometry.size.width * 0.92,
                                                    height: geometry.size.height * 0.2,
                                                    titleBackgroundColor: Color.colorBackground,
                                                    titleValue : TransportationTypeEnum.toTransportationTypeEnum(transportation.value.transportationType) == TransportationTypeEnum.DEPARTURE ?
                                                            GenericObservableModel(value: "TransportationInfoView.DestinationPoint.text") :
                                                            GenericObservableModel(value: "TransportationInfoView.DeparturePoint.text"),
                                                    observedCoordinate: self.coordinate)
                            .onTapGesture {
                                 self.sheetIsPresented = true
                                 print("map pressed")
                            }
                            .padding(EdgeInsets(top: 10, leading: 0, bottom: 0, trailing: 0))
                        
                        
//                        HStack{
//                            if TransportationTypeEnum.toTransportationTypeEnum(transportation.value.transportationType) == TransportationTypeEnum.DEPARTURE {
//                                Text("TransportationInfoView.DeparturePoint.text")
//                                    .foregroundColor(Color.colorText)
//                            } else {
//                                Text("TransportationInfoView.ReturnPoint.text")
//                                    .foregroundColor(Color.colorText)
//                            }
//
//                            Spacer()
//
//                            Image("map")
//                                .renderingMode(.template)
//                                .resizable()
//                                .aspectRatio(contentMode: .fit)
//                                .frame(width: 30, height: 30)
//                                .padding(5)
//                                .foregroundColor(Color.colorPrimary)
//                                .onTapGesture {
//                                     self.sheetIsPresented = true
//                                     print("map pressed")
//                                }
//                        }
                        
                        HStack{
                            Spacer()
                            Text("TransportationInfoView.ActiveDays.text")
                                .foregroundColor(Color.colorPrimary)
                            Spacer()
                        }
                            .padding(EdgeInsets(top: 20, leading: 0, bottom: 5, trailing: 0))
                        HStack(){
                            Spacer()
                            ForEach((0...6), id: \.self) { i in
        //                        Text("i=\(i)")
                                Text(Utils.getWeekDayShortName(dayIndex: i))
                                    .foregroundColor(self.days.value[i] ? Color.white : Color.gray)
                                    .frame(width: 40, height: 40)
                                    .background(self.days.value[i] ? Color.colorPrimary : Color.white)
                                    .clipShape(Circle())
                                    .overlay(Circle().stroke(Color.white, lineWidth: 4))
                                    .onTapGesture {
                                        self.days.value[i].toggle()
        //                                    print("self.coordinate.value \(self.coordinate)")
                                        self.save()
                                    }
                                Spacer()
                            }
                            
                        }
                    }
                    .alert(isPresented: self.$showAlert.value) {
                        Alert(title: Text(""), message: Text("TransportationInfoView.DeleteTransportationAlert.text"),
                          primaryButton: .destructive(Text("TransportationInfoView.DeleteTransportationAlert.Delete.text"),
                            action: {
                                print("will remove attendance")
                                self.deleteTransportation()
                                }),
                          secondaryButton: .cancel({print("cancel Button clicked")}))
                    }
                    .sheet(isPresented: self.$sheetIsPresented, content: {
                            MapLocationSelectUIView(observedCoordinate: self.coordinate
                                , doSubmit: {
                                    self.sheetIsPresented = false
                                    self.save()
                                }
                                , doClose: {
                                    self.sheetIsPresented = false
                                }
                            )
                    })
                    .navigationBarItems(
                        leading:
                            VStack{
                                if self.transportationName.value != self.transportation.value.name || self.transportationPlate.value != self.transportation.value.plate {
                                    Button("TransportationInfoView.Navigation.CancelButton.text"){
                                        self.transportationName.value = self.transportation.value.name
                                        self.transportationPlate.value = self.transportation.value.plate
                                        
                                        self.hideKeyboard()
                                    }
                                }
                            },
                        trailing:
                            VStack{
                                if self.transportationName.value != self.transportation.value.name || self.transportationPlate.value != self.transportation.value.plate {
                                    Button("TransportationInfoView.Navigation.SaveButton.text"){
                                        self.save()
                                        self.hideKeyboard()
                                    }
                                }else{
                                    Image("delete")
                                        .renderingMode(.template)
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width: 30, height: 30)
//                                        .padding(5)
                                        .foregroundColor(Color.colorPrimary)
                                        .onTapGesture {
                                            self.showAlert.value = true
                                            print("show Alert")
                                        }
                                }
                            }
                    )
                    
                    Spacer()
                    
        //            Button(action: {
        //                self.save()
        //                }, label: {
        //                    Text("TransportationInfoView.Save.text")
        //                })
        //                .disabled(!isValid())
        //                .padding(.bottom, 15)
                        
                }
                .padding(EdgeInsets(top: 15, leading: geometry.size.width*0.04, bottom: 0, trailing: geometry.size.width*0.04))
                .disabled(self.isTimeViewPresented)
                .blur(radius: self.isTimeViewPresented ? 15 : 0)
                .background(Color.colorBackground)
            }
            if self.isTimeViewPresented{
                HStack{
                    Spacer()
                    VStack(){
                        Spacer()
                        TimeView(date: self.$transportationStartTime.value, isPresented: self.$isTimeViewPresented,
                                 doSubmit: {
                                    print("save Transporatation time")
                                    self.save()
                                 })
                        Spacer()
                    }
                    Spacer()
                }
            }
        }
               
    }
}

struct TransportationInfoView_Previews: PreviewProvider {
    static let t: Transportation = createdTransportations[0]
    static let dd = [true, false, false, false, true, false, true]
    
    static var previews: some View {
        TransportationInfoView(transportation: GenericObservableModel(value: t), transportationName: GenericObservableModel(value: t.name), transportationPlate: GenericObservableModel(value: t.plate), transportationStartTime: GenericObservableModel(value: Utils.toDate(lTime: t.startTime)), days: GenericObservableModel(value:dd),
            coordinate: GenericObservableModel(value: CLLocationCoordinate2D(latitude: t.latitude, longitude: t.longitude) )).environment(\.locale, .init(identifier: "tr"))
//        TransportationInfoView().environment(\.locale, .init(identifier: "tr"))
    }
}
