//
//  CreatedTransportationRow.swift
//  Landmarks
//
//  Created by macos on 5/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import SwiftUI

struct CreatedTransportationRow: View {
    var transportation: Transportation
//    @State private var showShareSheet = false
//    @State private var toastCopied = false
    @ObservedObject var toastObservableModel : ToastObservableModel
    @ObservedObject var sharedTransportationId : GenericObservableModel<String>
//    @EnvironmentObject var joinedTransportationsList: JoinedTransportationsListObservable
    @ObservedObject var joinedTransportationsList: JoinedTransportationsListObservable = JoinedTransportationsListObservable.getInstance()
    
    @ObservedObject var isSheetPresented : GenericObservableModel<Bool>
    @ObservedObject var presentedSheet : GenericObservableModel<CreatedTransportationsListView.PresentedSheet>
    
    var body: some View {
//        GeometryReader { geometry in
            ZStack{
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color.colorPrimary, lineWidth: 0.5)
                    .background(Color.colorListLight)
//                    .shadow(radius: 1, y: 1)
                
                VStack{
                    HStack() {
                        Text(transportation.name)
                            .foregroundColor(Color.colorPrimary)
                            .font(.title)
                        
                        Text(TransportationTypeEnum.toTransportationTypeEnum(transportation.transportationType) == TransportationTypeEnum.DEPARTURE ?   "CreatedTransportationRow.DepartureTransportation.text" : "CreatedTransportationRow.ReturnTransportation.text")
                            .foregroundColor(Color.colorTabUnSeledted)
                            .font(.title)
                        
                        Spacer()
                        
                        Image("share")
                            .renderingMode(.template)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 40, height: 40)
                            .padding(5)
                            .foregroundColor(Color.colorPrimary)
                            .onTapGesture {
                                print("share pressed pressed")
                                if let validTransportationId = self.transportation.id {
                                    print("shared transportation id changed at row")
                                    self.sharedTransportationId.value = validTransportationId
                                    self.presentedSheet.value = CreatedTransportationsListView.PresentedSheet.sharePresented
                                    self.isSheetPresented.value = true
                //                            self.sharedTransportationId.isValueChanged = true
                                }
                            }
                    }
                    .padding(EdgeInsets(top: 5, leading: 0, bottom: 0, trailing: 0))
                    
                    Spacer()
                    
                    Divider()
                        .background(Color.colorPrimary)
                        .padding(EdgeInsets(top: 0, leading: 0, bottom: 5, trailing: 0))
                    
                    Spacer()
                    
                    ZStack(){
                        HStack{
                            Image("licence_plate")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 160, height: 40, alignment: .trailing)
//                                        .padding(5)
                            Spacer()
                            
                        }
                        HStack{
                            Text(transportation.plate)
    //                            .multilineTextAlignment(.trailing)
                                .foregroundColor(Color.colorText)
                                .padding(EdgeInsets(top: 0, leading: 40, bottom: 0, trailing: 0))
                            Spacer()
                        }
                    }
                    
                    Spacer()
                    
                    HStack() {
                        RectangleDots(height: 40, insideView:
                            Text(transportation.id!)
                                .font(.body)
                                .foregroundColor(Color.colorText))
                            .padding(EdgeInsets(top: 5, leading: 0, bottom: 5, trailing: 5))
                        
                        Image("copy")
                            .renderingMode(.template)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 40 ,height:40)
                            .padding(5)
                            .foregroundColor(Color.colorPrimary)
                            .onTapGesture {
                                UIPasteboard.general.string = self.transportation.id == nil ? "" : self.transportation.id!
                                self.toastObservableModel.isToastShowing = true
                        }
                        
                        Spacer()
                        
                        if joinedTransportationsList.transportations.firstIndex(of: transportation) == nil{
                            Image("join_shuttle")
                                .renderingMode(.template)
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 40, height: 40)
                                .padding(5)
                                .foregroundColor(Color.colorPrimary)
                                .onTapGesture {
                                    print("join shuttle pressed")
//                                        print(joinedTransportationsList.transportations)
//                                        print(transportation)
                                    if let validTransportationId = self.transportation.id {
                                        self.sharedTransportationId.value = validTransportationId
                                        self.presentedSheet.value = CreatedTransportationsListView.PresentedSheet.joinPresented
                                        self.isSheetPresented.value = true
                                    }
                                }
                        }
                        
                    }
                    Spacer()
                    Spacer()
                }
                .frame(width: UIScreen.main.bounds.width * 0.88, alignment: .center)
            }
            .frame(width: UIScreen.main.bounds.width * 0.94 , height: 170, alignment: .center)
            .padding(EdgeInsets(top: 13, leading: 0, bottom: 13, trailing: 0))
            .onAppear(){
                self.joinedTransportationsList.fetchData()
            }
//        }
//        // Set the size of entire row. maxWidth makes it take up whole width of device.
//        .frame(maxWidth: .infinity, maxHeight: 60, alignment: .leading)
//        .padding(10) // Spacing around all the contents
//        // Add a solid colored background that you can put a shadow on
//        // (corner radius optional)
//        .background(Color.white.cornerRadius(10).shadow(radius: 20))
        
    }
}

struct CreatedTransportationRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CreatedTransportationRow(transportation: createdTransportations[2], toastObservableModel: ToastObservableModel(), sharedTransportationId: GenericObservableModel(value: "asdasdas123"), isSheetPresented: GenericObservableModel(value: false), presentedSheet : GenericObservableModel(value: CreatedTransportationsListView.PresentedSheet.none))
            CreatedTransportationRow(transportation: createdTransportations[0], toastObservableModel: ToastObservableModel(), sharedTransportationId: GenericObservableModel(value: "asdasdas123"), isSheetPresented: GenericObservableModel(value: false), presentedSheet : GenericObservableModel(value: CreatedTransportationsListView.PresentedSheet.none))
            CreatedTransportationRow(transportation: createdTransportations[1], toastObservableModel: ToastObservableModel(), sharedTransportationId: GenericObservableModel(value: "asdasdas123"), isSheetPresented: GenericObservableModel(value: false), presentedSheet : GenericObservableModel(value: CreatedTransportationsListView.PresentedSheet.none))
        }
        .previewLayout(.fixed(width: 400, height: 700))
    }
}

