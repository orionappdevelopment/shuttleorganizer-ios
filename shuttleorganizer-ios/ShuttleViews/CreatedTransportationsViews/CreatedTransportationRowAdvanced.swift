//
//  CreatedTransportationRow.swift
//  Landmarks
//
//  Created by macos on 5/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import SwiftUI

struct CreatedTransportationRowAdvanced: View {
    var transportation: Transportation
//    @State private var showShareSheet = false
//    @State private var toastCopied = false
    @ObservedObject var toastObservableModel : ToastObservableModel
    @ObservedObject var sharedTransportationId : GenericObservableModel<String>
//    @EnvironmentObject var joinedTransportationsList: JoinedTransportationsListObservable
    @ObservedObject var joinedTransportationsList: JoinedTransportationsListObservable = JoinedTransportationsListObservable.getInstance()
    
    @ObservedObject var isSheetPresented : GenericObservableModel<Bool>
    @ObservedObject var presentedSheet : GenericObservableModel<CreatedTransportationsListView.PresentedSheet>
    
    var body: some View {
//        GeometryReader { geometry in
            ZStack{
                RoundedRectangle(cornerRadius: 0)
                    .foregroundColor(Color.colorListLight)
                    .shadow(radius: 1, y: 1)
//                    .frame(width: UIScreen.main.bounds.width * 0.98 , height: 100)
                
                
                VStack{
                    ZStack{
                        VStack{
                            RoundedRectangle(cornerRadius: 0)
                                .foregroundColor(Color.colorSecondary)
    //                            .shadow(radius: 1, y: 1)
                                .frame(width: UIScreen.main.bounds.width * 0.96, height: 50)
                            
                           Spacer()
                        }
                        VStack{
                            HStack() {
                                Text(transportation.name)
                                    .foregroundColor(Color.colorListRowTitle)
                                    .font(.title)
                                    .padding(EdgeInsets(top: 10, leading: 10, bottom: 0, trailing: 0))
                                Spacer()
                            }
                            Spacer()
                        }
                    }
                    
                    ZStack(){
                        HStack{
                            Image("licence_plate")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 180, height: 40, alignment: .trailing)
//                                        .padding(5)
                            Spacer()
                            
                            if joinedTransportationsList.transportations.firstIndex(of: transportation) == nil{
                                Image("join_shuttle")
                                    .renderingMode(.template)
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 30, height: 30)
                                    .padding(5)
                                    .foregroundColor(Color.colorPrimary)
                                    .onTapGesture {
                                        print("join shuttle pressed")
//                                        print(joinedTransportationsList.transportations)
//                                        print(transportation)
                                        if let validTransportationId = self.transportation.id {
                                            self.sharedTransportationId.value = validTransportationId
                                            self.presentedSheet.value = CreatedTransportationsListView.PresentedSheet.joinPresented
                                            self.isSheetPresented.value = true
                                        }
                                    }
                            }
                        }
                        HStack{
                            Text(transportation.plate)
    //                            .multilineTextAlignment(.trailing)
                                .foregroundColor(Color.colorText)
                                .padding(EdgeInsets(top: 0, leading: 40, bottom: 0, trailing: 0))
                            Spacer()
                        }
                    }
                    
                    Spacer()
                    Spacer()
                    
                    HStack() {
                        RectangleDots(insideView:
                            Text(transportation.id!)
                                .font(.body)
                                .foregroundColor(Color.colorText))
                            .padding(EdgeInsets(top: 5, leading: 0, bottom: 5, trailing: 5))
                        
                        Image("copy")
                            .renderingMode(.template)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 30 ,height:30)
                            .padding(5)
                            .foregroundColor(Color.colorPrimary)
                            .onTapGesture {
                                UIPasteboard.general.string = self.transportation.id == nil ? "" : self.transportation.id!
                                self.toastObservableModel.isToastShowing = true
                        }
                        
                        Spacer()
                        
                        Image("share")
                            .renderingMode(.template)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 30, height: 30)
                            .padding(5)
                            .foregroundColor(Color.colorPrimary)
                            .padding(.trailing, 7)
                            .onTapGesture {
                                print("share pressed pressed")
                                if let validTransportationId = self.transportation.id {
                                    print("shared transportation id changed at row")
                                    self.sharedTransportationId.value = validTransportationId
                                    self.presentedSheet.value = CreatedTransportationsListView.PresentedSheet.sharePresented
                                    self.isSheetPresented.value = true
                //                            self.sharedTransportationId.isValueChanged = true
                                }
                            }
                        
                    }
                    Spacer()
                    Spacer()
                }
                .frame(width: UIScreen.main.bounds.width * 0.92, alignment: .center)
            }
            .frame(width: UIScreen.main.bounds.width * 0.96 , height: 170, alignment: .center)
            .padding(EdgeInsets(top: 15, leading: 0, bottom: 0, trailing: 0))
            .onAppear(){
                self.joinedTransportationsList.fetchData()
            }
//        }
//        // Set the size of entire row. maxWidth makes it take up whole width of device.
//        .frame(maxWidth: .infinity, maxHeight: 60, alignment: .leading)
//        .padding(10) // Spacing around all the contents
//        // Add a solid colored background that you can put a shadow on
//        // (corner radius optional)
//        .background(Color.white.cornerRadius(10).shadow(radius: 20))
        
    }
}

struct CreatedTransportationRowAdvanced_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CreatedTransportationRowAdvanced(transportation: createdTransportations[2], toastObservableModel: ToastObservableModel(), sharedTransportationId: GenericObservableModel(value: "asdasdas123"), isSheetPresented: GenericObservableModel(value: false), presentedSheet : GenericObservableModel(value: CreatedTransportationsListView.PresentedSheet.none))
            CreatedTransportationRowAdvanced(transportation: createdTransportations[0], toastObservableModel: ToastObservableModel(), sharedTransportationId: GenericObservableModel(value: "asdasdas123"), isSheetPresented: GenericObservableModel(value: false), presentedSheet : GenericObservableModel(value: CreatedTransportationsListView.PresentedSheet.none))
            CreatedTransportationRowAdvanced(transportation: createdTransportations[1], toastObservableModel: ToastObservableModel(), sharedTransportationId: GenericObservableModel(value: "asdasdas123"), isSheetPresented: GenericObservableModel(value: false), presentedSheet : GenericObservableModel(value: CreatedTransportationsListView.PresentedSheet.none))
        }
        .previewLayout(.fixed(width: 400, height: 700))
    }
}

