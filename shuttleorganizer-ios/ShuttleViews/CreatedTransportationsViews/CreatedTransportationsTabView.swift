//
//  CreatedTransportationsTabView.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/30/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import SwiftUI
import CoreLocation

struct CreatedTransportationsTabView: View {
    @State private var selectedTabIndex = 0
    @ObservedObject var transportation : GenericObservableModel<Transportation>
     
    var body: some View {
        VStack(alignment: .leading, spacing : -8) {
            SlidingTabView(selection: self.$selectedTabIndex, tabs: ["CreatedTransportationsTabView.ShuttleInfo.text", "CreatedTransportationsTabView.Participants.text"])
            
            VStack(spacing : 0){
                if selectedTabIndex == 0 {
                    TransportationInfoView(transportation: transportation,
                                           transportationName: GenericObservableModel(value: transportation.value.name), transportationPlate: GenericObservableModel(value: transportation.value.plate),
                                           transportationStartTime: GenericObservableModel(value: Utils.toDate(lTime: transportation.value.startTime)),
                                           days: GenericObservableModel(value: DayEnum.daysFromMultiplication(multiplication: transportation.value.days)),
                                           coordinate: GenericObservableModel(value: CLLocationCoordinate2D(latitude: transportation.value.latitude, longitude: transportation.value.longitude)))
                } else {
                    TransportationParticipantsListView(transportation: transportation.value)
//                    TransportationParticipantsSweetListView(transportation: transportation.value)
                }
            }//
//            (selectedTabIndex == 0 ? CreateShuttleMapView() : ).padding()
            Spacer()
        }
        .navigationBarTitle(Text(self.transportation.value.name))
//        .padding(.top, 50)
//        .animation(.none)
        
    }
}

struct CreatedTransportationsTabView_Previews: PreviewProvider {
    static var previews: some View {
        CreatedTransportationsTabView(transportation: GenericObservableModel(value: Transportation()))
    }
}
