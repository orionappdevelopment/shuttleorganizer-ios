//
//  CreatedTransportationRow.swift
//  Landmarks
//
//  Created by macos on 5/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import SwiftUI

struct CustomCreatedTransportationRow: View {
    var transportation: Transportation
    
    var body: some View {
        VStack(alignment: .leading, spacing: 4) {
//            Text(transportation.name)
//                .font(.system(size: 14, weight: .black, design: .rounded))
//                .foregroundColor(Color.black)
//                .background(Rectangle().foregroundColor(Color.blue), alignment: Alignment.leading)
            
//            BackgroundedText(transportation.name, .black, .green, .green, CGSize(width: 300, height: 30), 50, 50)
            VStack{
                ZStack(alignment: .leading){
                    Rectangle()
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height:30)
            //                .frame(width: self.size.width,
            //                       height: self.size.height)
                        .foregroundColor(Color.colorSecondaryLight)

            //            Rectangle()
            //            .frame(width: self.size.width - xOffset,
            //                   height: self.size.height - yOffset)
            //            .foregroundColor(self.second_color)

                    Text(transportation.name)
                        .foregroundColor(Color.black)
                        .font(.title)
                    .padding(EdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 5))

                }
            }
            VStack(){
                HStack() {
                    Spacer()
                    Image("join_shuttle")
                        .renderingMode(.template)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 30, height: 30)
                        .padding(5)
                        .foregroundColor(Color.colorPrimary)
                }
                HStack() {
                    ZStack{
                        RectangleDots(insideView:
                            Text(transportation.id!)
                                .font(.body)
                                .foregroundColor(Color.black))
                        
                    }
                    
                    Image("copy")
                        .renderingMode(.template)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 30 ,height:30)
                        .padding(5)
                        .foregroundColor(Color.colorPrimary)
                    Spacer()
                    
                    Image("share")
                        .renderingMode(.template)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 30, height: 30)
                        .padding(5)
                        .foregroundColor(Color.colorPrimary)
                }
            }
                .background(Color.colorListLight)
                .offset(x: 0, y: -5)
        }
        .cornerRadius(10)
//        .overlay(
//            RoundedRectangle(cornerRadius: 10)
//                .stroke(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.1), lineWidth: 1)
//        )
//        .padding([.top, .horizontal])
        
        
//        // Set the size of entire row. maxWidth makes it take up whole width of device.
//        .frame(maxWidth: .infinity, maxHeight: 60, alignment: .leading)
//        .padding(10) // Spacing around all the contents
//        // Add a solid colored background that you can put a shadow on
//        // (corner radius optional)
//        .background(Color.white.cornerRadius(10).shadow(radius: 20))
        
    }
}

struct CustomCreatedTransportationRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CustomCreatedTransportationRow(transportation: createdTransportations[0])
            CustomCreatedTransportationRow(transportation: createdTransportations[1])
        }
        .previewLayout(.fixed(width: 350, height: 120))
    }
}

