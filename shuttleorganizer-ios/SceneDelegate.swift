//
//  SceneDelegate.swift
//  shuttleorganizer-ios
//
//  Created by macos on 5/23/20.
//  Copyright © 2020 Orion. All rights reserved.
//

import UIKit
import SwiftUI
import class Kingfisher.KingfisherManager

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
       
        
        InternetConnectivity.getInstance().start()
//        sleep(5)
        //remove all form space between top such as TransportationInfoView
//        UITableView.appearance().tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: Double.leastNonzeroMagnitude))
        
        //kingfisher clear cache
//        KingfisherManager.shared.cache.clearMemoryCache()
//        KingfisherManager.shared.cache.clearDiskCache()
//        KingfisherManager.shared.cache.cleanExpiredDiskCache()
        
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).

        // Create the SwiftUI view that provides the window contents.
//        let contentView = CreateTransportationView()
//        let contentView = WrappedCreateTransportationView()
//        let contentView = ProfileView()
//        let contentView = ContentView()
//        let contentView = ActiveView()
        let contentView = LoginContentView()
//        let contentView = JoinTransportationView()
//        var transportation = Transportation()
//        transportation.id = "vh5vHYd23n"
//        let contentView = StreamChatView(transportation: transportation, senderId: "bQm1w3Uqithl8EJ3DEFP7CvBvI73", senderName: "efe")
        // Use a UIHostingController as window root view controller.
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = UIHostingController(rootView: contentView)
            
//            let a = ParticipantRow(personTransportationView: PersonTransportationView(personId: "r1234", transportationId: "t123", personRoleType: 0, latitude: 1.2, longitude: 1.3, transportationStartTime: LTime(hour: 5, minute: 15, second: 20), dailyTimeDiffInSecs: 2700, personName: "susuds"), personTime: LTime(hour: 5, minute: 15, second: 20).toDate())
            
//            let a = TransportationParticipantsListView(transportation: Transportation(id: "1fMO92nLjj", name: "", latitude: 1, longitude: 1, transportationType: 0, days: 2, status: 0, creatorPersonId: "", startTime: LTime(), timeZone: 3))
            
//            let a = TransportationInfoView(transportation: GenericObservableModel(value: tt), transportationName: GenericObservableModel(value: tt.name), transportationStartTime: GenericObservableModel(value:tt.startTime.toDate()), days: GenericObservableModel(value:DayEnum.daysFromMultiplication(multiplication: tt.days)))
        
//        let list = [PersonTransportationView(personId: "r1234", transportationId: "t123", personRoleType: 0, latitude: 1.2, longitude: 1.3, transportationStartTime: LTime(hour: 5, minute: 15, second: 20), dailyTimeDiffInSecs: 2700, personName: "susususu ththt dfdf"),
//        PersonTransportationView(personId: "r1234", transportationId: "t123", personRoleType: 0, latitude: 1.2, longitude: 1.3, transportationStartTime: LTime(hour: 5, minute: 15, second: 20), dailyTimeDiffInSecs: 2700, personName: "susususu")]
//        let tt = Transportation(id: "1NAiXUBX2K", name: "", latitude: 1, longitude: 1, transportationType: 0, days: 2, status: 0, creatorPersonId: "", startTime: LTime(), timeZone: 3)
//        let b = TransportationParticipantsListView(transportation: tt, personTransportationViewList : list)
//
//            let a =  ParticipantDetailedView(personTransportationView: GenericObservableModel(value: list[0]), transportation: Transportation())
//
//            window.rootViewController = UIHostingController(rootView: a)
//            window.rootViewController = UIHostingController(rootView: CreatedTransportationsListView().environmentObject(CreatedTransportationsListObservable()))
//            window.rootViewController = UIHostingController(rootView: JoinedTransportationsListView().environmentObject(JoinedTransportationsListObservable()))
//            window.rootViewController = UIHostingController(rootView: MainTransportationsTabview())
           
//            let t: Transportation = createdTransportations[0]
//            let dd = [true, false, false, false, true, false, true]
//            window.rootViewController = UIHostingController(rootView:TransportationInfoView(transportation: t, transportationName: t.name, transportationStartTime: Utils.toDate(lTime: t.startTime), days: dd ))
//            window.rootViewController = UIHostingController(rootView: Deneme1())
            
//            window.rootViewController = UIHostingController(rootView:AlreadyCreatedTransportationView(transportation: createdTransportations[0]))
//            let tt = Transportation(id: "vh5vHYd23n", name: "", latitude: 1, longitude: 1, transportationType: 0, days: 2, status: 0, creatorPersonId: "", startTime: LTime(), timeZone: 3)
//            window.rootViewController = UIHostingController(rootView: AttendanceInfoView(transportation: GenericObservableModel(value: tt)))
            
//            window.rootViewController = UIHostingController(rootView: ProfileView())
//            window.rootViewController = UIHostingController(rootView: ActiveView())
//            window.rootViewController = UIHostingController(rootView: CreatedTransportationsListView().environmentObject(CreatedTransportationsListObservable()))
            
            self.window = window
            window.makeKeyAndVisible()
        }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

